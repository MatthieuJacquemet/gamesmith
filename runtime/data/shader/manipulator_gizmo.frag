#version 450

layout(location = 0) out vec4 final_color;

in Data { vec4 color; vec4 position; };

void main() {
    final_color = color;
    // final_color = vec4(1.0, 0.0, 0.0, 1.0);
}