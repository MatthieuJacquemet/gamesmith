#version 450

#extension GL_GOOGLE_include_directive : require

uniform mat4 p3d_ModelMatrix;
uniform mat4 p3d_ViewMatrix;
uniform vec4 p3d_Color;
uniform mat4 p3d_ProjectionMatrixTranspose;
uniform mat4 p3d_ProjectionMatrix;

uniform vec2 u_view_scale;
uniform vec2 u_near_far;

layout(location=0) out vec4 color;
layout(constant_id = 0) const int c_grid_resolution = 128;

in vec3 v_coord;
in float v_alpha;

#include "common.glsl"


void main() {
    color = p3d_Color;
    float z;
    // vec4 r = p3d_ProjectionMatrix * vec4(0,0, lz, 1.0);
    // float z = r.z;
    // z = v_coord.z;

    // float x = sqrt(lz * u_near_far.y);
    // float n = sqrt(u_near_far.x * u_near_far.y);

    // float z = ( x - n ) / (u_near_far.y - n);

    // z = invLerp(u_near_far.x, u_near_far.y, abs(z));
    bool persp = p3d_ProjectionMatrixTranspose[3] == vec4(0.0, 0.0, -1.0, 0.0);
    if (persp) {
        float scale = p3d_ModelMatrix[0][0];
        float len = 1.0 - length(v_coord) / (scale * c_grid_resolution);
        len = clamp(len, 0.0, 1.0);

        vec3 viewNormal = p3d_ViewMatrix[2].xyz;
        float cosTheta = abs(dot(normalize(v_coord), viewNormal));
        cosTheta = smoothstep(0.0, 1.0, cosTheta);

        color.w *= clamp(len * cosTheta, 0.0, 1.0);
    }
    color.w *= frustum_fading(-v_coord.z, u_near_far.x, u_near_far.y, persp);
    color.w *= v_alpha;
}