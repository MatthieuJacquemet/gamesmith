#version 450

uniform mat4 p3d_ProjectionMatrix;
uniform mat4 p3d_ModelViewMatrix;
uniform vec4 p3d_ColorScale;

in vec4 p3d_Vertex;
in vec4 p3d_Color;

out vec4 v_color;
out vec3 v_coord;

void main() {
    vec4 coord = p3d_ModelViewMatrix * p3d_Vertex;
    gl_Position = p3d_ProjectionMatrix * coord;

    v_color = p3d_Color * p3d_ColorScale;
    v_coord = coord.xyz;
}