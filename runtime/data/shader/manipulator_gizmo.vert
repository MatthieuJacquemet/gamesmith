#version 450

uniform vec4 p3d_ClipPlane[];
uniform mat4 p3d_ProjectionMatrix;
in mat4x3 instance_matrix;
in vec4 p3d_Vertex;
in vec4 axis_color;

out Data { vec4 color; vec4 position; };

out gl_PerVertex {
    vec4  gl_Position;
    float gl_PointSize;
    float gl_ClipDistance[];
}; 

void main() {

    position = vec4(instance_matrix * p3d_Vertex, 1.0);
    color = axis_color;

    gl_Position = p3d_ProjectionMatrix * position;
    gl_ClipDistance[0] = dot(position, p3d_ClipPlane[0]);
}


// layout(constant_id = 0) const float c_arc_handle_overshoot = 0.1;


// vec3 centerVector() {
//     if (p3d_ProjectionMatrixTranspose[3] == vec4(0.0, 0.0, -1.0, 0.0)) {
//         return normalize(p3d_ModelViewMatrix[3].xyz);
//     } else {
//         return vec3(0.0, 0.0, 1.0);
//     }
// }

// void main() {

    // vec4 coord = p3d_Vertex;

    // float tmp = coord[gl_InstanceID];
    // coord[gl_InstanceID] = coord.z;
    // coord.z = tmp;

    // gl_Position = p3d_ModelViewProjectionMatrix * coord;
    // v_color = u_colors[gl_InstanceID];

    // if (u_info[0] == MODE_ROTATE) {
    //     vec3 mu = normalize(p3d_NormalMatrixTranspose[2]);
    //     v_color.w *= smoothstep(-c_arc_handle_overshoot, 0.0,
    //                             dot(mu, coord.xyz));
    // } else {
    //     vec3 mu = normalize(p3d_ModelViewMatrix[gl_InstanceID].xyz);
    //     v_color.w *= smoothstep(1.0, 0.9, abs(dot(mu, centerVector())));
    // }
    // if (u_info[0] == u_info[1] && u_info[2] == gl_InstanceID) {
    //     v_color.rgb = hsl2rgb(rgb2hsl(v_color.rgb) * vec3(1.0, 1.0, 1.3));
    // }
// }