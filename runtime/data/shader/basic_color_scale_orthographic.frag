#version 450

layout(location = 0) out vec4 color;
uniform vec4 p3d_ColorScale;

void main() {
    color = p3d_ColorScale;
}