#version 450

layout(location = 0) out vec4 final_color;
uniform vec4 p3d_ColorScale;
uniform ivec2 u_view_scale;
uniform usampler2D u_stencil;

in vec2 texCoord;

#define SELECTION_MASK 0x80

void main() {
    final_color = vec4(1.0,0.5,0.0,1.0);
    // final_color = vec4(0.3,0.7,1.0,1.0);

    // vec2 texSize = vec2(1.0) / vec2(u_view_scale);
    // vec2 coord = gl_FragCoord.xy / vec2(u_view_scale) - texSize * 0.5;
    // vec2 coord = texCoord - texSize * 0.5;

    // uvec4 tapes[4];
    // tapes[0] = textureGather(u_stencil, coord + texSize * vec2(-1.0, 1.0), 0);
    // tapes[1] = textureGather(u_stencil, coord + texSize * vec2( 1.0, 1.0), 0);
    // tapes[2] = textureGather(u_stencil, coord + texSize * vec2(-1.0,-1.0), 0);
    // tapes[3] = textureGather(u_stencil, coord + texSize * vec2( 1.0,-1.0), 0);

    // if ((tapes[0][2] & SELECTION_MASK) != 0) {
    //     discard;
    // }
    // const ivec2 indices[] = {   ivec2(0,0), ivec2(0,1), ivec2(1,1), ivec2(0,3),
    //                             ivec2(1,2), ivec2(2,3), ivec2(2,3), ivec2(3,2)};
    // float sum = 0.0;
    // for (int i = 0; i < 8; i++) {
    //     ivec2 index = indices[i];
    //     if ((tapes[index.x][index.y] & SELECTION_MASK) != 0) {
    //         sum += 5.0;
    //     }
    // }
    // final_color.a *= sum / 8;

    ivec2 coord = ivec2(gl_FragCoord.xy);
    const int KERNEL = 3;

    float sum = 0.0;
    for (int i = -KERNEL; i <= KERNEL; i++) {
        for (int j = -KERNEL; j <= KERNEL; j++) {
            uint value = texelFetch(u_stencil, coord + ivec2(i, j), 0).r;
            if ((value & SELECTION_MASK) != 0) {
                if (i == 0 && j == 0) {
                    discard;
                } else {
                    sum += smoothstep(3.0, 2.0, length(vec2(i,j)));
                }
            }
        }
    }
    final_color.a *= sum;
}