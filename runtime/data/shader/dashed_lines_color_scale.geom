#version 450

layout (lines) in;
layout (line_strip, max_vertices = 2) out;

uniform ivec2 u_view_scale;
noperspective out float lineCoord;

void main() {
    vec4 p0 = gl_in[0].gl_Position;
    vec4 p1 = gl_in[1].gl_Position;

    vec2 p0_scaled = vec2(u_view_scale) * p0.xy / p0.w;
    vec2 p1_scaled = vec2(u_view_scale) * p1.xy / p1.w;

    gl_Position = p0;
    lineCoord = 0.0;
    EmitVertex();

    gl_Position = p1;
    lineCoord = length(p1_scaled - p0_scaled);
    EmitVertex();
}