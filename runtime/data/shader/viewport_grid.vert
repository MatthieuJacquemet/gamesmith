#version 450

uniform mat4 p3d_ModelViewProjectionMatrix;
uniform mat4 p3d_ModelViewMatrix;
uniform mat4 p3d_ModelMatrix;
uniform mat4 p3d_ViewMatrixInverse;
uniform mat4 p3d_ViewMatrix;

uniform float u_height;

in vec4 p3d_Vertex;
out vec3 v_coord;
out float v_alpha;

layout(constant_id = 0) const int c_grid_resolution = 128;
layout(constant_id = 1) const float c_falloff_threshold = 0.1;


void main() {

    gl_Position = p3d_ModelViewProjectionMatrix * p3d_Vertex;
    float scale = p3d_ModelMatrix[0][0];
    float height = p3d_ViewMatrixInverse[3][2];

    const float GRID_RES = 128;

    v_coord = (p3d_ModelViewMatrix * p3d_Vertex).xyz;

    // v_alpha = invLerp(0.1, 1.0, u_height / (GRID_RES * scale * 0.01));
    float t = clamp(abs(u_height / (scale * c_grid_resolution)), 0.0, 1.0);

    // if (t < c_falloff_threshold) {
    //     v_alpha = max(t / c_falloff_threshold, 0.0);
    // } else {
    //     v_alpha = max((1.0 - t) / (1.0 - c_falloff_threshold), 0.0);
    // }
    v_alpha = clamp((1.0 - abs(1.0 - pow(t, 0.5) * 2.0)) * 1.0, 0.0, 1.0);
}