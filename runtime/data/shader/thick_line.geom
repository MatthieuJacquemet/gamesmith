#version 450

layout (lines) in;
layout (triangle_strip, max_vertices = 4) out;

uniform vec4 p3d_ClipPlane[];
uniform ivec2 u_view_scale;
uniform float u_thickness;

in  Data { vec4 color; vec4 position; } input_data[];
out Data { vec4 color; vec4 position; } output_data;


void do_vertex(vec4 pos, vec2 offset, int index) {

    output_data.color = input_data[index].color;

    gl_Position = pos + vec4(offset * pos.w, 0.0, 0.0);
    gl_ClipDistance[0] = gl_in[index].gl_ClipDistance[0];
    EmitVertex();

    gl_Position = pos - vec4(offset * pos.w, 0.0, 0.0);
    gl_ClipDistance[0] = gl_in[index].gl_ClipDistance[0];
    EmitVertex();
}

void main() {
    vec4 p1 = gl_in[0].gl_Position;
    vec4 p2 = gl_in[1].gl_Position;

    vec2 dir    = normalize((p2.xy / p2.w - p1.xy / p1.w) * u_view_scale);
    vec2 offset = vec2(-dir.y, dir.x) * u_thickness / vec2(u_view_scale);

    do_vertex(p1, offset, 0);
    do_vertex(p2, offset, 1);
}