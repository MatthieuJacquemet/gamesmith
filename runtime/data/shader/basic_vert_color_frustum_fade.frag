#version 450

#extension GL_GOOGLE_include_directive : require

uniform mat4 p3d_ProjectionMatrixTranspose;
uniform vec2 u_near_far;

layout(location = 0) out vec4 color;

in vec4 v_color;
in vec3 v_coord;

#include "common.glsl"

void main() {
    bool persp = p3d_ProjectionMatrixTranspose[3] == vec4(0.0, 0.0, -1.0, 0.0);

    color = v_color;
    color.w *= frustum_fading(-v_coord.z, u_near_far.x, u_near_far.y, persp);
}