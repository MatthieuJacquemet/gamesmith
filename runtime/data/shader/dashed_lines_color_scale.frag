#version 450

layout(location = 0) out vec4 color;
uniform vec4 p3d_ColorScale;

noperspective in float lineCoord;

const float DASH_SCALE = 15.0;

void main() {
    color = p3d_ColorScale;
    float t = step(fract(lineCoord / DASH_SCALE), 0.5);
    // color.w *= smoothstep(0.5, 0.5, t);
    color.w *= t;
}