// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef COMMON_GLSL_INCLUDED
#define COMMON_GLSL_INCLUDED


float log_base(float x, float base) {
    return log(x) / log(base);
}

float inv_lerp(float a, float b, float t) {
    return clamp((t - a) / (b - a), 0.0, 1.0);
}

float frustum_fading(float z, float near, float far, bool persp) {
    float t = persp ? log_base(z / far, near / far) : inv_lerp(near, far, z);
    return smoothstep(0.0, 0.1, t) * smoothstep(0.0, 0.1, 1.0 - t);
}

vec3 hsl2rgb(in vec3 c) {
    vec3 rgb = mod(c.x * 6.0 + vec3(0.0, 4.0, 2.0), 6.0);
    rgb = clamp(abs(rgb - 3.0) - 1.0, 0.0, 1.0);

    return c.z + c.y * (rgb - 0.5) * (1.0 - abs(2.0 * c.z - 1.0));
}

vec3 rgb2hsl(in vec3 c){

	float c_min = min(c.r, min(c.g, c.b));
	float c_max = max(c.r, max(c.g, c.b));

	float l = (c_max + c_min) * 0.5;
    float h = 0.0, s = 0.0;
	
    if ( c_max > c_min ) {
		float c_delta = c_max - c_min;
		s = l < 0.0 ?   c_delta / (      (c_max + c_min)) :
                        c_delta / (2.0 - (c_max + c_min));
        
		if (c.r == c_max)       h = 0.0 + (c.g - c.b) / c_delta;
		else if (c.g == c_max)  h = 2.0 + (c.b - c.r) / c_delta;
		else                    h = 4.0 + (c.r - c.g) / c_delta;

		if ( h < 0.0) h += 6.0;
		h = h / 6.0;
	}
	return vec3(h, s, l);
}

#endif // COMMON_GLSL_INCLUDED