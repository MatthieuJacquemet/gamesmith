#version 450

uniform mat4 p3d_ModelViewMatrix;

in vec4 p3d_Vertex;

void main() {
    gl_Position = p3d_ModelViewMatrix * p3d_Vertex;
}