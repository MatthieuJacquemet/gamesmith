#version 450

layout(location=0) out vec4 color;
uniform sampler2D p3d_Texture0;

in vec2 v_texcoord;

void main() {

    color.rgb = texture(p3d_Texture0, v_texcoord).rgb;
}