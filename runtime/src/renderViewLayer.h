// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __RENDERVIEWLAYER_H__
#define __RENDERVIEWLAYER_H__

#include <panda3d/callbackObject.h>
#include <panda3d/pStatCollector.h>
#include <panda3d/weakPointerTo.h>

#include <utils.h>

class ViewportService;
class SceneData;
class RenderPipeline;
class DisplayRegion;

class RenderViewLayer : public CallbackObject {

    REGISTER_TYPE("RenderViewLayer", CallbackObject);

public:
    RenderViewLayer(ViewportService* viewport,
                    const std::string& name);
    virtual ~RenderViewLayer() = default;

    void do_callback(CallbackData* cbdata) override;
    virtual void draw_callback() = 0;
    virtual void setup_pipeline(RenderPipeline* pipeline);
    virtual void setup_display_region(DisplayRegion* dr);

    SceneData* get_scene_data() const;

protected:
    PStatCollector _collector;
    ViewportService* const _viewport;
};

#endif // __RENDERVIEWLAYER_H__