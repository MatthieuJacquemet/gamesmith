// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __PANDANODESERVICE_H__
#define __PANDANODESERVICE_H__

#include <panda3d/boundingVolume.h>
#include <panda3d/drawMask.h>
#include <property.h>

#include "serialize_ops.h"
#include "serviceBase.h"


extern void register_legacy_render_effects();


struct [[meta::serialize]] PropertyInfo {
    std::string name; Variant value;
    PropertyBase::Attributes attributes;
};
struct [[meta::serialize]] RenderAttribInfo {
    uint8_t id, priority;
    std::vector<PropertyInfo> properties;
};
struct [[meta::serialize]] TagInfo {
    std::string key, value;
};
struct [[meta::serialize]] EffectInitializer {
    std::string label; std::vector<std::string> parameters;
};
struct [[meta::serialize]] RenderEffectInfo {
    enum Action { A_set, A_add, A_remove, A_init };
    int32_t typeId;
    std::vector<EffectInitializer> initializers;
    std::vector<PropertyInfo> properties;
};
struct [[meta::serialize]] NodeInfo {
    uint64_t id = 0; int32_t type_id; std::string name;
    DrawMask draw_mask, control_mask;
    LVector3f position, rotation, scale, shear;
    std::vector<TagInfo> tags;
    BoundingVolume::BoundsType bounds_type;
    std::vector<RenderEffectInfo> effects; 
    std::vector<RenderAttribInfo> attribs;
    std::vector<PropertyInfo> properties;
};


class [[meta::service]] PandaNodeService : public ServiceBase {
    
    REGISTER_SERVICE

public:
    PandaNodeService(uint16_t id);
    virtual ~PandaNodeService();


    [[meta::rpc]] NodeInfo get_node_info();

    [[meta::rpc]] void set_name(const std::string& name);

    [[meta::rpc]] void set_draw_masks(DrawMask show, DrawMask control);

    [[meta::rpc]] void add_attrib(int32_t attribId);
    [[meta::rpc]] void remove_attrib(int32_t attribId);
    [[meta::rpc]] void update_attrib(int32_t id, const std::string& p, 
                                    const std::vector<Variant>& args);

    [[meta::rpc]] RenderEffectInfo add_effect(int32_t effectId);
    [[meta::rpc]] void remove_effect(int32_t effectId);
    [[meta::rpc]] void update_effect(int32_t id, const std::string& p,
                                    const std::vector<Variant>& args,
                                    RenderEffectInfo::Action action);
    
    [[meta::rpc]] void set_transform(const LVector3& position,
                                    const LVector3& rotation,
                                    const LVector3& scale,
                                    const LVector3& shear);

    [[meta::rpc]] void set_tags(const std::vector<TagInfo>& tags);

    [[meta::rpc]] void set_bound_type(BoundingVolume::BoundsType type);

    [[meta::rpc]] void set_property(const std::string& name,
                                    const Variant& data);

    [[meta::rpc]] Variant get_property(const std::string& name);

    [[meta::signal]] void transforms_changed(const LVector3& position,
                                            const LVector3& rotation,
                                            const LVector3& scale,
                                            const LVector3& shear);

private:
    struct PandaNodeServicePrivate;
    dimpl<PandaNodeServicePrivate> _d;
};

#endif // __PANDANODESERVICE_H__