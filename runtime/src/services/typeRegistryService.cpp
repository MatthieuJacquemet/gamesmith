// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/typeRegistry.h>
#include "typeRegistryService.h"


TypeRegistryService::TypeRegistryService(uint16_t id) : ServiceBase(id) {
    
}

::TypeRegistryService::Types TypeRegistryService::get_types() {

    int num_types = TypeRegistry::ptr()->get_num_typehandles();
    std::vector<TypeEntry> types;
    types.reserve(num_types);
    
    for (int i = 0; i < num_types; ++i) {
        TypeHandle type = TypeRegistry::ptr()->get_typehandle(i);

        auto& entry = types.emplace_back(TypeEntry{ type.get_index(),
                                                    type.get_name()});

        int num_parents = type ? type.get_num_parent_classes() : 0;
        entry.parents.reserve(num_parents);
        for (int j = 0; j < num_parents; ++j) {
            entry.parents.push_back(type.get_parent_class(j).get_index());
        }
        int num_children = type ? type.get_num_child_classes() : 0;
        entry.children.reserve(num_children);
        for (int j = 0; j < num_children; ++j) {
            entry.children.push_back(type.get_child_class(j).get_index());
        }
    }
    return types;
}