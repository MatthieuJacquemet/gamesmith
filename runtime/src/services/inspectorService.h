// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __INSPECTORSERVICE_H__
#define __INSPECTORSERVICE_H__

#include <panda3d/weakPointerCallback.h>
#include <panda3d/drawMask.h>
#include <panda3d/boundingVolume.h>
#include <bitset>
#include "property.h"
#include "serviceBase.h"
#include "utils.h"


class [[meta::service]] InspectorService :  WeakPointerCallback,
                                            public ServiceBase {

    REGISTER_SERVICE
public:
    InspectorService(uint16_t id);
    virtual ~InspectorService();

    [[meta::signal]] void node_changed(int32_t type_id);
    static PointerTo<PandaNode> get_current_node();

protected:
    void wp_callback(void* ptr) override;

    class InspectorServicePrivate;
    dimpl<InspectorServicePrivate> _d;
};


#endif // __INSPECTORSERVICE_H__