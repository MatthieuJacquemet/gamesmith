// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SELECTTOOL_H__
#define __SELECTTOOL_H__

#include "inputEventListener.h"
#include "toolService.h"
#include "eventDispatcher.h"

class SceneSetup;

class [[meta::service]] SelectTool: public InputEventListener,
                                    public ToolService {

    REGISTER_DERIVED_SERVICE(ToolService)

public:
    typedef std::vector<PT(PandaNode)> SelectedNodes;
    REGISTER_EVENT(selection_changed, SelectedNodes);
    REGISTER_EVENT(selection_updated, SelectedNodes);

    SelectTool(uint16_t id);
    virtual ~SelectTool();

    void enabled(bool active, uint32_t mode) override;
    void draw_overlay(RenderOverlayLayer* drawer) override;
    void setup_pipeline(RenderPipeline* pipeline) override;

    enum Mode {
        M_none = -1, M_rectangle, M_circle, M_lasso
    };
    enum Operation {
        O_replace, O_add, O_remove, O_intersect, O_difference
    };
    SelectedNodes get_selected_nodes() const;
    bool is_node_selected(PandaNode* node) const;

protected:
    int get_priority() const override {
        return InputEventListener::get_priority();
    }
    void finish_drag_operation();
    void viewport_changed(ViewportService* old_viewport) override;
    void handle_button_event(const ButtonEvent& event) override;

    SceneSetup setup_scene(const SceneSetup* initial_setup);
    void occlusion_test(RenderOverlayLayer* drawer);

    void selection_updated(const SelectedNodes& selected);

private:
    struct SelectToolPrivate;
    dimpl<SelectToolPrivate> _d;
};

extern TypeHandle _get_type_handle(const SelectTool::SelectedNodes*);


#endif // __SELECTTOOL_H__