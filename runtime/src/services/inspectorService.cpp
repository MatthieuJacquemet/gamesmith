// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/pandaNode.h>
#include <panda3d/renderAttrib.h>
#include <panda3d/renderState.h>
#include <panda3d/transformState.h>
#include <panda3d/weakPointerCallback.h>

#include <metaTypeRegistry.h>
#include <metaClass.h>
#include <eventDispatcher.h>
#include "sceneGraphService.h"
#include "selectTool.h"
#include "inspectorService.h"





struct InspectorService::InspectorServicePrivate {
    uint64_t token;
    static WeakPointerTo<PandaNode> node;
};

WeakPointerTo<PandaNode> InspectorService::InspectorServicePrivate::node;




InspectorService::InspectorService(uint16_t id) : ServiceBase(id) {
    using sel_t = std::vector<PT(PandaNode)>;

    const auto callback = [this](const sel_t& selection) {
        TypeHandle type = TypeHandle::none();
        if (selection.size() == 1) {
            const PT(PandaNode)& node = selection.front();
            _d->node.remove_callback(this);
            _d->node = node;
            _d->node.add_callback(this);
            type = node->get_type();
        }
        // nout << "node_changed(" << type.get_index() << ")" << std::endl;
        node_changed(type.get_index());
    };
    _d->token = EventDispatcher::get_global_dispatcher()->add_listener(
        SelectTool::get_selection_changed(), callback
    );
}

void InspectorService::wp_callback(void* ptr) {
    node_changed(0);
}

InspectorService::~InspectorService() {
    EventDispatcher::get_global_dispatcher()->remove_listener(
        SelectTool::get_selection_changed(), _d->token
    );
    InspectorServicePrivate::node.remove_callback(this);
}

PointerTo<PandaNode> InspectorService::get_current_node() {
    return InspectorServicePrivate::node.lock();
}