// Copyright 2023 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <asyncTask.h>
#include <asyncTaskManager.h>
#include <bitArray.h>
#include <displayRegion.h>
#include <eventDispatcher.h>
#include <panda3d/modelPool.h>
#include <panda3d/loader.h>
#include <panda3d/modelLoadRequest.h>
#include <panda3d/graphicsWindow.h>
#include <panda3d/dataGraphTraverser.h>
#include <panda3d/mouseAndKeyboard.h>
#include <panda3d/virtualMouse.h>
#include <panda3d/graphicsEngine.h>
#include <panda3d/orthographicLens.h>
#include <panda3d/displayRegionDrawCallbackData.h>
#include <panda3d/depthTestAttrib.h>
#include <panda3d/pStatTimer.h>
#include <panda3d/pointLight.h>
#include <panda3d/spotlight.h>

#include <math_utils.h>

#include <renderState.h>
#include <renderTarget.h>
#include <render_utils.h>
#include <renderPipeline.h>
#include <sceneData.h>
#include <decalVolume.h>

#include "inputEventListener.h"
#include "renderViewLayer.h"
#include "toolService.h"
#include "editTool.h"
#include "applicationFramework.h"
#include "navigationCamera.h"
#include "viewportService.h"
#include "keyboardAndMouseHandler.h"
#include "renderOverlayLayer.h"


typedef std::unordered_map<GraphicsOutput*, ViewportService*> ViewportMap;



class ViewportService::AsyncModelLoadRequest: public ModelLoadRequest {
     ViewportService* _viewport;
public:
    virtual bool cancel() override {
        bool return_value;
        if ((return_value = ModelLoadRequest::cancel())) {
            _viewport = nullptr;
        }
        return return_value;
    }
    ~AsyncModelLoadRequest() = default;
    AsyncModelLoadRequest(  const std::string& filename,
                            ViewportService* viewport,
                            Loader* loader):
        ModelLoadRequest(std::string("model:") + filename,
                        filename, LoaderOptions(), loader),
        _viewport(viewport) {}

    virtual AsyncTask::DoneStatus do_task() override {
        AsyncTask::DoneStatus state = ModelLoadRequest::do_task();
        if (LIKELY(_viewport != nullptr)) {
            _viewport->set_root_node(get_model());
        }
        return state;
    }
};



class RenderOutputLayer final : public RenderViewLayer {
    ShaderState _render_state;
    WPT(GraphicsWindow) _framebuffer;
    PT(DecalVolume) _volume = new DecalVolume;

public:
    float _depth = 0.0f;
    RenderOutputLayer(ViewportService* service, const std::string& name):
        RenderViewLayer(service, name) {

        _render_state.set_shader(Shader::load(SHADER_LANG,
            GDK_SHADER_PATH VERT_SHADER("quad"),
            GDK_SHADER_PATH FRAG_SHADER("simpleTexture"))
        );
        _render_state.set_material_off();
        _render_state.set_color_off();
        _render_state.set_two_sided(true);
        _render_state.set_attrib(DepthTestAttrib::make(DepthTestAttrib::M_always));
    }
    void setup_pipeline(RenderPipeline* pipeline) override {
        _render_state.set_shader_input("u_color",
            pipeline->get_render_target("org.gdk.rt.output-color")
        );
        _render_state.set_shader_input("u_depth",
            pipeline->get_render_target("org.gdk.rt.depth-stencil")
        );
    }
    void draw_callback() override {
      
        _render_state.set_shader_input("u_near_far", get_scene_data()->_near_far);
        draw_fullscreen_quad(get_scene_data()->get_gsg(), _render_state);
    }
};

struct ViewportService::ViewportServicePrivate {
    NodePath root, scene;
    LVector2 prev_mouse_pos;

    WPT(AsyncModelLoadRequest) current_load_request;
    PT(GraphicsWindow) framebuffer;
    PT(NavigationCamera) camera;
    PT(DataNode) input_node;
    PT(RenderPipeline) render_pipeline;
    
    static ViewportMap viewport_map;
    static ViewportService* active_viewport;

    static void setup_events_handlers();
    static void update_window_event(const Event* event);
};


ViewportMap ViewportService::ViewportServicePrivate::viewport_map;
ViewportService* ViewportService::ViewportServicePrivate::active_viewport;


void ViewportService::ViewportServicePrivate::setup_events_handlers() {
    CHECK_INITIALIZED
    auto* event_handler = EventHandler::get_global_event_handler();
    auto* task_manager = AsyncTaskManager::get_global_ptr();

    AsyncTask* task = task_manager->add("viewport-update", [](AsyncTask* task) {
        GraphicsWindow* window;
        if (active_viewport && (window = active_viewport->get_framebuffer())) {
            InputEventListener::dispatch(window);
        }
        return AsyncTask::DS_cont;
    });
    if (auto* process_inputs = task_manager->find_task("process-inputs")) {
        task->set_sort(process_inputs->get_sort() + 1);
    }
    event_handler->add_hook("window-event", update_window_event);
}



void ViewportService::
ViewportServicePrivate::update_window_event(const Event* event) {
    if (UNLIKELY(event->get_num_parameters() == 0)) {
        return;
    }
    EventParameter param = event->get_parameter(0);
    GraphicsWindow* window;
    DCAST_INTO_V(window, param.get_ptr());

    if (window && !window->is_valid()) {
        GraphicsEngine::get_global_ptr()->remove_window(window);
        return;
    }
    ViewportMap::const_iterator it = viewport_map.find(window);
    LVector2i size = window->get_size();

    if (it != viewport_map.end()) {
        if (LIKELY(size.get_y())) {
            Camera* camera = it->second->get_navigation_camera();
            Lens* lens;
            float aspect_ratio = float(size[0]) / float(size[1]);
            for (int i=0; (lens = camera->get_lens(i)); ++i) {
                lens->set_aspect_ratio(aspect_ratio);
            }
        }
        if (auto* pipeline = it->second->get_render_pipeline()) {
            pipeline->update_size(window->get_size());
        }
        if (it->second != active_viewport) {
            const auto props = window->get_properties();
            if (props.has_foreground() && props.get_foreground()) {

                EventDispatcher::get_global_dispatcher()->dispatch_event(
                    ViewportService::get_active_changed(), 
                    std::exchange(active_viewport, it->second)
                );
            }
        }
    }
}



#include <spotLightNode.h>
#include <pointLightNode.h>
#include <panda3d/cardMaker.h>


ViewportService::ViewportService(uint16_t id) : ServiceBase(id) {
    _d->camera  = new NavigationCamera;
    _d->root    = NodePath("root");

    if (UNLIKELY(_d->active_viewport == nullptr)) {
        EventDispatcher::get_global_dispatcher()->dispatch_event(
            ViewportService::get_active_changed(), 
            std::exchange(_d->active_viewport, this)
        );
    }
    ViewportServicePrivate::setup_events_handlers();

    NodePath cam_np(_d->camera);
    cam_np.set_pos(5.f, 5.0f,  5.0f);
    cam_np.look_at(0.0f, 0.0f, 0.0f);
    cam_np.reparent_to(_d->root);

    _d->camera->set_lens(new PerspectiveLens);
    _d->camera->get_lens()->set_fov(60.0f);
    _d->camera->get_lens()->set_near(0.1f);
    _d->camera->get_lens()->set_far(10000.0f);
    _d->camera->set_tag_state_key("main");
    

    // NodePath model(ModelPool::load_model("/home/matthieu/Projects/amongz/data/map/dead-end.bam"));
    NodePath model(ModelPool::load_model("/home/matthieu/Documents/3d-model/sponza/sponza_scene.glb"));

    // CardMaker maker("card");
    // NodePath model(maker.generate());
    // model.set_scale(100.0);
    // model.set_pos(-50,-50,0);
    model.set_p(90.0);
    NodePath root("root");
    model.reparent_to(root);
    // auto spotlights = model.find_all_matches("**/+Spotlight");
    // for (int i=0; i<spotlights.size(); ++i) {
    //     spotlights[i].set_h(spotlights[i].get_h() + 90.0);
    // }
    // auto nodes = model.find_all_matches("**/+Light");
    // for (int i=0; i<nodes.size(); ++i) {
    //     nodes[i].detach_node();   
    // }
    SpotLightNode* spotlight = new SpotLightNode;
    PointLightNode* pointlight = new PointLightNode;

    spotlight->set_max_distance(20.0f);
    pointlight->set_max_distance(80.0f);

    NodePath light = root.attach_new_node(spotlight);
    NodePath light2 = root.attach_new_node(pointlight);
    light.set_pos(2,-2,5);
    light.look_at(5,-5,0);

    light2.set_pos(0,0,5);

    set_root_node(root.node());
    // PandaNode* rootnode = new PandaNode("root");

    // struct AA : public Spotlight {
    //     AA(): Spotlight("aa") {
    //         set_renderable();
    //     }
    // };

    // rootnode->add_child(new AA);
    // rootnode->add_child(new PointLight("point"));
    // set_root_node(rootnode);
}


ViewportService::~ViewportService() {

    if (auto load_request = _d->current_load_request.lock()) {
        load_request->cancel();
    }
    if (_d->camera != nullptr) {
        NodePath::any_path(_d->camera).remove_node();
    }
    if (_d->render_pipeline != nullptr) {
        GraphicsEngine::get_global_ptr()->remove_window(_d->render_pipeline);
    }
    if (_d->framebuffer != nullptr) {
        _d->viewport_map.erase(_d->framebuffer);
        GraphicsEngine::get_global_ptr()->remove_window(_d->framebuffer);
    }
    if (_d->active_viewport == this) {
        _d->active_viewport = nullptr;
    }
}


bool ViewportService::load_model(const std::string& model_path) {
     bool clear_old_loader_success = true;

    if (auto load_request = _d->current_load_request.lock()) {
        clear_old_loader_success = load_request->cancel();
    }
    if (clear_old_loader_success) {
        Loader* loader = Loader::get_global_ptr();
        
        _d->current_load_request = new AsyncModelLoadRequest(
            model_path, this, loader
        );
        loader->load_async(_d->current_load_request.p());
    }
    return clear_old_loader_success;
}


uint64_t ViewportService::create_window(uint64_t parent, int sx, int sy) {

    if (UNLIKELY(_d->framebuffer != nullptr)) {
        return uint64_t(-1);
    }
    FrameBufferProperties fbprops   = FrameBufferProperties::get_default();
    WindowProperties winprops       = WindowProperties::get_default();

    fbprops.set_multisamples(16);
    winprops.set_parent_window(parent);
    winprops.set_foreground(true);
    winprops.set_size(sx, sy);

    auto* framework = ApplicationFramework::get_global_ptr();
    auto* task_mgr = AsyncTaskManager::get_global_ptr();

    GraphicsOutput* output = framework->open_window("viewport",
                                    std::move(fbprops), std::move(winprops),
                                    GraphicsPipe::BF_require_window |
                                    GraphicsPipe::BF_refuse_parasite);

    if (output->is_of_type(GraphicsWindow::get_class_type())) {
        _d->framebuffer = (GraphicsWindow*)output;
        _d->framebuffer->set_sort(100);
        _d->framebuffer->disable_clears();

        add_view_layer(new RenderOutputLayer(this, "output"));
        add_view_layer(new RenderOverlayLayer(this, "overlay"));

        set_pipeline("/build/pbr_pipeline.bam");

        _d->viewport_map.insert(std::make_pair(output, this));
        return _d->framebuffer->get_window_handle()->get_int_handle();
    }
    return uint64_t(-1);
}

void ViewportService::
enable_overlay_component(RenderOverlayLayer::OverlayComponent comp, bool enabled) {
    if (LIKELY(_d->framebuffer != nullptr)) {
        if (auto dr = _d->framebuffer->get_display_region(VL_overlay)) {
            if (auto ol = DCAST(RenderOverlayLayer, dr->get_draw_callback())) {
                ol->set_enabled(comp, enabled);
            };
        }
    }
}


bool ViewportService::set_pipeline(const std::string& path) {
    if (UNLIKELY(_d->framebuffer == nullptr)) {
        return false;
    }
    GraphicsOutput* host = _d->framebuffer;
    LVector2i size = host->get_fb_size();

    if ((_d->render_pipeline = RenderPipeline::create(host, path, size))) {
        GraphicsEngine::get_global_ptr()->add_window(_d->render_pipeline, 0);
        // bind the camera to the render pipeline
        auto* dr = _d->render_pipeline->make_mono_display_region();
        dr->set_camera(NodePath::any_path(_d->camera));
        dr->set_depth_range(-1.0f, 1.0f);
        _d->render_pipeline->set_tag_name("main");
        // notify any render callback of the new pipeline
        int num_display_region = host->get_num_display_regions();
        for (int i = 0; i < num_display_region; ++i) {
            PT(DisplayRegion) dr = host->get_display_region(i);    
            CallbackObject* cb = dr->get_draw_callback();

            if (cb && cb->is_of_type(RenderViewLayer::get_class_type())) {
                ((RenderViewLayer*)cb)->setup_pipeline(_d->render_pipeline);
            }
        }
    }
    return !_d->render_pipeline.is_null();
}


void ViewportService::enable_view_layer(uint32_t layer, bool enable) {
    if (UNLIKELY(_d->framebuffer == nullptr)) {
        std::cerr << "No view layer at index " << layer << std::endl;
    } 
    else if (auto dr = _d->framebuffer->get_display_region(layer)) {
        dr->set_active(enable);
    }
}


void ViewportService::set_root_node(PandaNode* node) {

    NodePath cam_np = NodePath::any_path(_d->camera);
    _d->scene = _d->root.attach_new_node(node);
    EventDispatcher::get_global_dispatcher()->dispatch_event(
        get_scene_changed(), this
    );
    _d->camera->set_scene(_d->root);

    if (LIKELY(_d->framebuffer != nullptr)) {
        if (node->is_of_type(ModelRoot::get_class_type())) {
            ModelRoot* root = (ModelRoot*)node;
            auto& name = const_cast<std::string&>(_d->framebuffer->get_name());
            name = root->get_fullpath();
        }
        DisplayRegion* dr = _d->framebuffer->get_display_region(0);
        dr->set_camera(std::move(cam_np));
    }
}

void ViewportService::set_camera_mask(DrawMask camera_mask) {
    _d->camera->set_camera_mask(camera_mask);
}

DrawMask ViewportService::get_camera_mask() {
    return _d->camera->get_camera_mask();
}
void ViewportService::add_view_layer(RenderViewLayer* render_layer) {
    nassertv(_d->framebuffer != nullptr);
    DisplayRegion* dr = _d->framebuffer->make_mono_display_region();
    dr->disable_clears();
    dr->set_draw_callback(render_layer);
    render_layer->setup_display_region(dr);
}

GraphicsWindow* ViewportService::get_framebuffer() const {
    return _d->framebuffer.p();
}

NavigationCamera* ViewportService::get_navigation_camera() const {
    return _d->camera;
}

RenderPipeline* ViewportService::get_render_pipeline() const {
    return _d->render_pipeline;
}

NodePath ViewportService::get_scene_nodepath() const {
    return _d->scene;
}


ViewportService* ViewportService::get_active_viewport() {
    return ViewportServicePrivate::active_viewport;
}