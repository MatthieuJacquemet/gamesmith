// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SYSTEMRESSOURCESDVC_H__
#define __SYSTEMRESSOURCESDVC_H__

#include <bitset>
#include <panda3d/displayInformation.h>

#include "serviceBase.h"
#include "systemRessourceEnums.h"
#include "ressource_types.h"


class [[meta::service]] SystemRessources : public SystemRessourceEnums,
                                           public ServiceBase {
    
    REGISTER_SERVICE

public:
    SystemRessources(uint16_t id);
    ~SystemRessources() = default;

    struct RessourceTypes: public std::bitset<RT_COUNT> {
        using parent_type = std::bitset<RT_COUNT>;

        RessourceTypes() = default;
        RessourceTypes(const std::initializer_list<RessourceType>& other) {
            for (RessourceType type: other) parent_type::set(type);
        }
        auto& operator=(const std::initializer_list<RessourceType>& other) {
            for (RessourceType type: other) parent_type::set(type);
            return *this;
        }
    };
    struct [[meta::serialize]] RessourceEntry final {
        RessourceType type;
        RessourceData data;
    };
    using RessourceEntries = std::vector<RessourceEntry>;
    using RessourceTypesBS = RessourceTypes::parent_type;

    [[meta::rpc]] ::SystemRessources::RessourceEntries querry_entries(
                    ::SystemRessources::RessourceTypesBS types, bool update);

private:
    void update_ressources(DisplayInformation* info);
};

#endif // __SYSTEMRESSOURCESDVC_H__