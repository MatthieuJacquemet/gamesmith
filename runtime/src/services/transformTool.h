// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __TRANSFORMTOOL_H__
#define __TRANSFORMTOOL_H__

#include <eventDispatcher.h>
#include "toolService.h"
#include "operation.h"
#include "iostream_ops.h"

class TransformState;

class [[meta::service]] TransformTool:  public ToolService,
                                        public IOperation {

    REGISTER_DERIVED_SERVICE(ToolService)

public:
    typedef std::unordered_map<PandaNode*,
                    const TransformState*> Transforms;
    REGISTER_EVENT(transforms_changed, Transforms)
    REGISTER_EVENT(transforms_modified, Transforms)

    TransformTool(uint16_t id);
    virtual ~TransformTool();

    virtual void enabled(bool active, uint32_t mode) override;
    int get_priority() const override;

    void draw_overlay(RenderOverlayLayer* drawer) override;
    void selection_changed(const std::vector<PT(PandaNode)>& nodes);

    enum Mode {
        M_none = -1, M_translate, M_rotate, M_scale, M_unified
    };
    enum Pivot {
        P_bounding_box_center, P_median, P_individual_origins
    };
    enum Space {
        S_global, S_local, S_view
    };
    [[meta::rpc]] void set_pivot(::TransformTool::Pivot pivot);
    [[meta::rpc]] void set_space(::TransformTool::Space space);

protected:
    void updated_nodes_transforms();
    virtual void update() override;
    virtual void confirm() override;
    virtual void cancel() override;

private:   
    class TransformToolPrivate;
    dimpl<TransformToolPrivate> _d;
};

extern TypeHandle _get_type_handle(const TransformTool::Transforms*);


#endif // __TRANSFORMTOOL_H__