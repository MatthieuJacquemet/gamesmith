// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.


#include <cstdint>
#include <panda3d/asyncTaskManager.h>
#include <panda3d/weakReferenceList.h>
#include <panda3d/lightMutexHolder.h>
#include <panda3d/lightMutex.h>
#include <panda3d/pandaNode.h>
#include <panda3d/nodePath.h>
#include <panda3d/weakPointerCallback.h>

#include <utils.h>
#include <eventDispatcher.h>

#include "iostream_ops.h"
#include "viewportService.h"
#include "selectTool.h"
#include "bidirectional_hashmap.h"
#include "sceneGraphService.h"
#include "viewportService.h"


class NodeLookup :  private BidirectionalHashMap<PandaNode*, uint64_t>, 
                    private WeakPointerCallback {
    
    std::vector<uint64_t> _stale_ids;
    LightMutex _lock;
    uint64_t _next_id = 0UL;

public:
    virtual void wp_callback(void* ptr) override {
        LightMutexHolder holder(_lock);
        uint64_t id = erase(static_cast<PandaNode*>(ptr), -1UL);
        if (LIKELY(id != -1UL)) {
            _stale_ids.push_back(id);
        }
    }
    std::vector<uint64_t> garbage_collect() {
        LightMutexHolder holder(_lock);
        return std::move(this->_stale_ids);
    }
    PT(PandaNode) lookup_node(uint64_t id) {
        LightMutexHolder holder(_lock);
        return BidirectionalHashMap<PandaNode*, uint64_t>::get_key(id);
    }
    uint64_t lookup_node(PandaNode* node) {
        nassertr(node != nullptr, 0)
        LightMutexHolder holder(_lock);
        uint64_t node_id = get_value(node, -1UL);
        if (UNLIKELY(node_id == -1UL)) {
            node->get_weak_list()->add_callback(this, node);
            insert(node, (node_id = ++_next_id));
        }
        return node_id;
    }
};

struct SceneGraphService::SceneGraphServicePrivate {

    uint64_t selection_changed_token, graph_changed_token;
    uint64_t node_name_changed_token, viewport_changed_token;
    PT(AsyncTask) task;

    static NodeLookup lookup;
};


NodeLookup SceneGraphService::SceneGraphServicePrivate::lookup;



SceneGraphService::SceneGraphService(uint16_t id) : ServiceBase(id) {

    EventDispatcher* dispatcher = EventDispatcher::get_global_dispatcher();
    using sel_t = std::vector<PT(PandaNode)>;

    _d->selection_changed_token = dispatcher->add_listener(
        SelectTool::get_selection_changed(), this, [this](const sel_t& sel) {
            std::vector<uint64_t> selected_ids;
            selected_ids.reserve(sel.size());
            for (PandaNode* node : sel) {
                selected_ids.push_back(lookup_node(node));
            }
            selection_changed(selected_ids);
        }
    );
    _d->graph_changed_token = dispatcher->add_listener(
        ViewportService::get_scene_changed(), this, [this](ViewportService*) {
            graph_changed(get_graph(0));
        }
    );
    _d->viewport_changed_token = dispatcher->add_listener(
        ViewportService::get_active_changed(), this, [this](ViewportService*) {
            graph_changed(get_graph(0));
        }
    );
    _d->node_name_changed_token = dispatcher->add_listener(
        SceneGraphService::get_node_name_changed(), this,
        [this](PandaNode* node, const std::string& name) {
            node_renamed(lookup_node(node), name);
        }
    );
    _d->task = AsyncTaskManager::get_global_ptr()->add("graph", [this](auto*) {
        auto stale_nodes_ids = _d->lookup.garbage_collect();
        if (UNLIKELY(stale_nodes_ids.size() > 0)) {
            stale_nodes(std::move(stale_nodes_ids));
        }
        return AsyncTask::DS_cont;
    });
    if (ViewportService* viewport = ViewportService::get_active_viewport()) {
        Graph graph = get_graph(0);
        if (LIKELY(!graph.empty())) {
            AsyncTaskManager::get_global_ptr()->add("graph", [graph, this](auto*) {
                graph_changed(std::move(graph));
                return AsyncTask::DS_done;
            });
        }
    }
}

SceneGraphService::~SceneGraphService() {
    EventDispatcher* dispatcher = EventDispatcher::get_global_dispatcher();

    dispatcher->remove_listener(
        SelectTool::get_selection_changed(), _d->selection_changed_token
    );
    dispatcher->remove_listener(
        ViewportService::get_scene_changed(), _d->graph_changed_token
    );
    dispatcher->remove_listener(
        ViewportService::get_active_changed(), _d->viewport_changed_token
    );
    dispatcher->remove_listener(
        SceneGraphService::get_node_name_changed(), _d->node_name_changed_token
    );
    AsyncTaskManager::get_global_ptr()->remove(_d->task);
}



void SceneGraphService::rename_node(uint64_t id, const std::string& name) {
    if (PT(PandaNode) node = _d->lookup.lookup_node(id)) {
        node->set_name(name);
    }
}

void SceneGraphService::update_selection(const std::vector<uint64_t>& selected) {

    std::vector<PT(PandaNode)> selected_nodes;
    selected_nodes.reserve(selected.size());
    for (uint64_t id : selected) {
        if (PT(PandaNode) node = _d->lookup.lookup_node(id)) {
            selected_nodes.emplace_back(std::move(node));
        }
    }
    EventBlocker blocker(this);
    EventDispatcher::get_global_dispatcher()->dispatch_event(
        SelectTool::get_selection_updated(), selected_nodes
    );
}

PT(PandaNode) SceneGraphService::lookup_node(uint64_t id) {
    return SceneGraphServicePrivate::lookup.lookup_node(id);    
}

uint64_t SceneGraphService::lookup_node(PandaNode* node) {
    return SceneGraphServicePrivate::lookup.lookup_node(node);
}

SceneGraphService::Node SceneGraphService::get_node(uint64_t id) {
    ViewportService* viewport;
    PT(PandaNode) node;
    if (UNLIKELY(id == 0)) { // the root node
        if (auto viewport = ViewportService::get_active_viewport()) {
            node = viewport->get_scene_nodepath().node();
            id = _d->lookup.lookup_node(node);
        }
    } else {
        node = _d->lookup.lookup_node(id);
    }
    if (UNLIKELY(node != nullptr)) {
        std::vector<uint64_t> children;
        int num_children = node->get_num_children();
        children.reserve(num_children);
        for (int index = 0; index < num_children; ++index) {
            children.push_back(lookup_node(node->get_child(index)));
        }
        return NodeEntry{
            .id = id, .classId = node->get_type().get_index(),
            .name = node->get_name(), .children = std::move(children)
        };
    }
    return SceneGraphService::Node{};
}

SceneGraphService::Graph SceneGraphService::get_graph(uint64_t id) {

    std::unordered_set<PandaNode*> visited;
    std::list<NodeEntry> node_entries;
    std::function<uint64_t (PandaNode*)> visit_node;

    visit_node = [&, this](PandaNode* node) mutable -> uint64_t {
        uint64_t node_id = lookup_node(node);
        if (UNLIKELY(!visited.insert(node).second)) {
            return node_id;
        }
        NodeEntry entry{node_id, node->get_type().get_index(),
                        node->get_name(), std::vector<uint64_t>{}};

        int num_children = node->get_num_children();
        entry.children.reserve(num_children);
        for (int index = 0; index < num_children; ++index) {
            entry.children.push_back(visit_node(node->get_child(index)));
        }
        node_entries.emplace_back(std::move(entry));
        return node_id;
    };
    if (UNLIKELY(id == 0)) { // the root node
        if (auto viewport = ViewportService::get_active_viewport()) {
            NodePath root = viewport->get_scene_nodepath();
            if (LIKELY(!root.is_empty())) visit_node(root.node());
        }
    } else if (PT(PandaNode) node = _d->lookup.lookup_node(id)) {
        visit_node(node);
    }
    return node_entries;
}
