// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <aa_luse.h>
#include <algorithm>
#include <event.h>
#include <numeric_types.h>
#include <panda3d/boundingBox.h>
#include "manipulatorGizmo.h"
#include "toolService.h"
#include "transformTool.h"

#include <sceneData.h>
#include <eventDispatcher.h>
#include <transformState.h>
#include <unordered_set>

#include "selectTool.h"
#include "renderOverlayLayer.h"




struct TransformTool::TransformToolPrivate {

    void compute_bounding_box_center(const std::vector<PT(PandaNode)>& nodes);
    void compute_median_point(const std::vector<PT(PandaNode)>& nodes);
    void initialize_gizmo(const LPoint3& pivot_point);

    struct NodeInfo {
        NodePath nodepath;
        CPT(TransformState) node_transform;
        CPT(TransformState) net_transform;
    };
    std::vector<NodeInfo> nodes;

    ManipulatorGizmo gizmo;
    uint64_t token0, token1;
    Space space         = TransformTool::Space::S_local;
    Pivot pivot_point   = TransformTool::Pivot::P_median;

};

void TransformTool::TransformToolPrivate::
compute_bounding_box_center(const std::vector<PT(PandaNode)>& selection) {

    Thread* current_thread = Thread::get_current_thread();
    BoundingBox bounding_box;
    bounding_box.local_object();

    for (const PT(PandaNode)& node : selection) {
        auto& [nodepath, transform, net_transform] = nodes.emplace_back();

        nodepath = NodePath::any_path(node);
        transform = node->get_transform(current_thread);
        net_transform = nodepath.get_net_transform(current_thread);
        
        const auto volume = node->get_internal_bounds(current_thread);
        if (auto* fvolume = volume->as_finite_bounding_volume()) {
            PT(FiniteBoundingVolume) internal_bounding_volume =
                static_cast<FiniteBoundingVolume*>(fvolume->make_copy());
            
            internal_bounding_volume->xform(net_transform->get_mat());
            bounding_box.extend_by(internal_bounding_volume);
        }
    }
    if (!bounding_box.is_empty()) {
        initialize_gizmo(bounding_box.get_approx_center());
    } else if (!nodes.empty()){
        initialize_gizmo(nodes.front().net_transform->get_pos());
    }
}


void TransformTool::TransformToolPrivate::
compute_median_point(const std::vector<PT(PandaNode)>& selection) {
    
    Thread* current_thread = Thread::get_current_thread();
    LPoint3 median_point_sum(0.0f);
    int count = 0;
    for (const PT(PandaNode)& node : selection) {
        auto& [nodepath, transform, net_transform] = nodes.emplace_back();

        nodepath = NodePath::any_path(node);
        transform = node->get_transform(current_thread);
        net_transform = nodepath.get_net_transform(current_thread);

        median_point_sum += net_transform->get_pos(); ++count;
    }
    initialize_gizmo(median_point_sum / PN_stdfloat(count));
}


void TransformTool::TransformToolPrivate::
initialize_gizmo(const LPoint3& pivot_point) {
    LQuaternion quat = LQuaternion::ident_quat();
    if (nodes.size() == 1 && space == TransformTool::S_local) {
        quat = nodes.front().net_transform->get_quat();
    }
    gizmo.set_transform(TransformState::make_pos_quat(pivot_point, quat));
}



TransformTool::TransformTool(uint16_t id) : ToolService(id) {
    const auto event = SelectTool::get_selection_changed();
    
    _d->gizmo.set_operator(this);
    _d->token0 = EventDispatcher::get_global_dispatcher()->add_listener(
        event, &TransformTool::selection_changed, this
    );
    _d->token1 = EventDispatcher::get_global_dispatcher()->add_listener(
        get_transforms_modified(), [this](const Transforms&) {
            confirm(); // update the gizmo transform
        }
    );
    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
    if (auto tool = broker->get_service<SelectTool>()) {
        selection_changed(tool->get_selected_nodes());
    }
}

TransformTool::~TransformTool() {
    EventDispatcher::get_global_dispatcher()->remove_listener(
        SelectTool::get_selection_changed(), _d->token0
    );
    EventDispatcher::get_global_dispatcher()->remove_listener(
        TransformTool::get_transforms_modified(), _d->token1
    );
}

void TransformTool::enabled(bool active, uint32_t mode) {
    ToolService::enabled(active, mode);
    if (active) {
        _d->gizmo.set_mode(ManipulatorGizmo::Mode(mode));
    }
}

int TransformTool::get_priority() const {
    return _d->gizmo.get_priority();
}

void TransformTool::draw_overlay(RenderOverlayLayer* drawer) {
    if (!_d->nodes.empty()) {
        if (_d->space == TransformTool::S_view) {
            SceneData* data = drawer->get_scene_data();
            const TransformState* transform = _d->gizmo.get_transform();
            _d->gizmo.set_transform(transform->set_hpr(
                data->get_camera_transform()->get_hpr()
            ));
        }
        _d->gizmo.draw(drawer->get_scene_data());
    }
}

void TransformTool::selection_changed(const std::vector<PT(PandaNode)>& nodes) {
    _d->nodes.clear();
    _d->gizmo.reset_state();
    if (nodes.empty()) {
        _d->gizmo.set_enabled(false);
    } else {
        _d->gizmo.set_enabled(true);
        _d->nodes.reserve(nodes.size());

        switch (_d->pivot_point) {
            case P_bounding_box_center:
                _d->compute_bounding_box_center(nodes); break;
            case P_median:
            case P_individual_origins:
                _d->compute_median_point(nodes); break;
        }
    }
}

void TransformTool::updated_nodes_transforms() {

    std::unordered_map<PandaNode*, const TransformState*> transforms;
    transforms.reserve(_d->nodes.size());
    for (const auto& [node_path, _0, _1] : _d->nodes) {
        transforms.insert(std::make_pair(node_path.node(),
                                         node_path.get_transform()));
    }
    EventDispatcher::get_global_dispatcher()->dispatch_event(
        get_transforms_changed(), std::move(transforms)
    );
}

void TransformTool::set_pivot(TransformTool::Pivot pivot) {
    _d->pivot_point = pivot;
    confirm();
}

void TransformTool::set_space(::TransformTool::Space space) {
    _d->space = space;
    confirm();
}

void TransformTool::update() {
    Thread* current_thread = Thread::get_current_thread();
    const TransformState* grab_transform = _d->gizmo.get_grab_transform();
    const TransformState* pivot_transform = _d->gizmo.get_transform();

    for (auto& [node_path, node_transform, net_transform] : _d->nodes) {
        LQuaternion rotation; LPoint3 pivot_point;
        switch (_d->pivot_point) {
            case P_bounding_box_center:
            case P_median:
                pivot_point = pivot_transform->get_pos(); break;
            case P_individual_origins:
                pivot_point = net_transform->get_pos();
        };
        switch (_d->space) {
            case S_global: rotation = LQuaternion::ident_quat(); break;
            case S_local: rotation = net_transform->get_norm_quat(); break;
            case S_view: rotation = pivot_transform->get_norm_quat();
        };
        auto space = TransformState::make_pos_quat(pivot_point, rotation);
        auto transform = space->invert_compose(net_transform);

        transform = space->compose(grab_transform->compose(transform));
        NodePath root = node_path.get_top(current_thread);
        node_path.set_transform(root, transform, current_thread);
    }
    updated_nodes_transforms();
}

void TransformTool::confirm() {
    std::vector<PT(PandaNode)> nodes;
    std::transform(_d->nodes.begin(), _d->nodes.end(), std::back_inserter(nodes),
        [](const auto& node) { return node.nodepath.node(); });

    selection_changed(nodes);
}

void TransformTool::cancel() {
    Thread* current_thread = Thread::get_current_thread();
    for (auto& [node_path, transform, net_trans] : _d->nodes) {
        node_path.set_transform(transform, current_thread);
    }
    updated_nodes_transforms();
}


TypeHandle _get_type_handle(const TransformTool::Transforms*) {
    return TypeHandle::none();
}