// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SELECTIONTRAVERSER_H__
#define __SELECTIONTRAVERSER_H__

#include <panda3d/drawCullHandler.h>
#include <panda3d/cullTraverser.h>

class OcclusionQueryContext;


class SelectionTraverser :  public CullTraverser,
                            public DrawCullHandler {

public:
    SelectionTraverser(GraphicsStateGuardian* gsg);
    virtual ~SelectionTraverser() = default;

    void record_object(CullableObject* object,
                    const CullTraverser* trav) override;

    void set_scene( SceneSetup* scene_setup,
                    GraphicsStateGuardianBase* gsg,
                    bool dr_incomplete_render) override;
                        
    void do_traverse(CullTraverserData& data) override;
    void end_traverse() override;

    struct OcclusionObjectData {
        PandaNode* _node = nullptr;
        PT(OcclusionQueryContext) _query;
    };
    inline auto& get_objects() const { return _objects; }

private:
    PandaNode* _current_node = nullptr;
    std::vector<OcclusionObjectData> _objects;
};


#endif // __SELECTIONTRAVERSER_H__