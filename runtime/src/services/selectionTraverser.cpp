// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/graphicsStateGuardian.h>
#include "selectionTraverser.h"



SelectionTraverser::SelectionTraverser(GraphicsStateGuardian* gsg):
    DrawCullHandler(gsg) {
    
}

void SelectionTraverser::set_scene( SceneSetup* scene_setup,
                                    GraphicsStateGuardianBase* gsg,
                                    bool dr_incomplete_render) {
    

    CullTraverser::set_scene(scene_setup, gsg, dr_incomplete_render);
    set_cull_handler(this);
    _objects = std::vector<OcclusionObjectData>(1);
}

void SelectionTraverser::do_traverse(CullTraverserData& data) {
    _current_node = data.node();
    CullTraverser::do_traverse(data);
}

void SelectionTraverser::end_traverse() {
    auto* gsg = DCAST(GraphicsStateGuardian, get_gsg());

    if (_objects.back()._node != nullptr) {
        if (!_objects.back()._query) {
            _objects.back()._query = gsg->end_occlusion_query();
        }
    } else { // this is the sentinel entry, discard it
        _objects.pop_back();
    }
}

void SelectionTraverser::record_object(CullableObject* object,
                                    const CullTraverser* traverser) {

    auto* gsg = DCAST(GraphicsStateGuardian, get_gsg());
    if (_objects.back()._node != _current_node) {
        // we are treversing a new node, finish the previous one
        if (_objects.back()._node != nullptr) {
            _objects.back()._query = gsg->end_occlusion_query();
            _objects.emplace_back()._node = _current_node;
        } else {
            _objects.back()._node = _current_node;
        }
        gsg->begin_occlusion_query(); // start a new occlusion query
    }
    DrawCullHandler::record_object(object, traverser);
}
