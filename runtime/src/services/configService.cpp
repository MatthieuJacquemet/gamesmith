// Copyright 2023 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <panda3d/configPageManager.h>
#include <panda3d/configVariableManager.h>
#include <panda3d/configVariableCore.h>

#include "configService.h"


inline LColor read_color(const ConfigDeclaration* decl) {
    LColor color;
    for (int i=0; i<LColor::num_components; ++i) {
        color[i] = decl->get_double_word(i);
    }
    return color;
}

inline void write_color(ConfigDeclaration* decl, const LColor& color) {
    for (int i=0; i<LColor::num_components; ++i) {
        decl->set_double_word(i, color[i]);
    }
}

bool read_variable(ConfigVariableCore* core, ConfigService::ValueSpec& spec) {

    const ConfigDeclaration* decl = core->get_declaration(0);

    switch (spec.value_type = core->get_value_type()) {
        
        case ConfigFlags::VT_int: spec.value = decl->get_int_word(0);       break;
        case ConfigFlags::VT_int64: spec.value = decl->get_int64_word(0);   break;
        case ConfigFlags::VT_filename:
        case ConfigFlags::VT_search_path: 
        case ConfigFlags::VT_enum:
        case ConfigFlags::VT_string: spec.value = decl->get_string_word(0); break;
        case ConfigFlags::VT_color: spec.value = read_color(decl);          break;
        case ConfigFlags::VT_list: {
            int num_refs = core->get_num_trusted_references();
            auto& values = spec.value.emplace<std::vector<std::string>>();

            values.reserve(num_refs);
            for (int i=0; i<num_refs; ++i) {
                if (const auto* decl = core->get_trusted_reference(i)) {
                    values.emplace_back(decl->get_string_value());
                }
            }                                                               break;          
        }
        case ConfigFlags::VT_bool: spec.value = decl->get_bool_word(0);     break;
        case ConfigFlags::VT_double: spec.value = decl->get_double_word(0); break;

        default: return false;
    }
    return true;
}


void write_variable(ConfigVariableCore* core, const ConfigService::ValueSpec& spec) {

    ConfigDeclaration* decl = core->make_local_value();

    switch (spec.value_type) {
        case ConfigFlags::VT_int:
            decl->set_int_word(0, std::get<int>(spec.value));           break;
        case ConfigFlags::VT_int64:
            decl->set_int_word(0, std::get<int64_t>(spec.value));       break;
        case ConfigFlags::VT_search_path:
        case ConfigFlags::VT_filename:
        case ConfigFlags::VT_enum:
        case ConfigFlags::VT_string:
            decl->set_string_word(0, std::get<std::string>(spec.value)); break;
        case ConfigFlags::VT_color:
            write_color(decl, std::get<LColor>(spec.value));            break;
        case ConfigFlags::VT_list:
            nout << "can't write list config variable at runtime" << std::endl;
            break;
        case ConfigFlags::VT_bool:
            decl->set_bool_word(0, std::get<bool>(spec.value));         break;
        case ConfigFlags::VT_double:
            decl->set_double_word(0, std::get<double>(spec.value));     break;

        default: nout << "unknown config variable type: "
                        << int(spec.value_type) << std::endl;
    }
}


std::vector<ConfigService::ConfigSpec> ConfigService::get_config_specs() {
    std::vector<ConfigService::ConfigSpec> variable_specs;
    
    ConfigVariableManager* manager = ConfigVariableManager::get_global_ptr();

    int num_variables = manager->get_num_variables();
    variable_specs.reserve(num_variables);

    for (int i=0; i<num_variables; ++i) {
        auto& variable_spec = variable_specs.emplace_back();

        ConfigVariableCore* core = manager->get_variable(i);
        variable_spec.name = core->get_name();
        variable_spec.description = core->get_description();

        if (!read_variable(core, variable_spec.default_value)) {
            nout << "unknown config variable type: " << int(core->get_value_type())
                << " for " << core->get_name() << std::endl;
            variable_specs.pop_back();
        } else {
            // nout << "adding " << core->get_name() << " " << int(variable_spec.default_value.value_type) << std::endl;
        }
        // std::cout << "reading variable " << variable_specs.back().default_value.value.index() << std::endl;
        // if (variable_spec.default_value.value_type == ConfigFlags::VT_enum) {
            
        // }
    }
    return variable_specs;
}


void ConfigService::set_config_variable(const std::string& name,
                                    const ConfigService::ValueSpec& value) {
    
    ConfigVariableManager* manager = ConfigVariableManager::get_global_ptr();

    int num_variables = manager->get_num_variables();
    for (int i=0; i<num_variables; ++i) {
        ConfigVariableCore* core = manager->get_variable(i);
        
        if (core->get_name() == name) {
            try {
                write_variable(core, value); break;
            } catch (std::bad_variant_access& e) {
                nout << "bad variant access: " << value.value.index() << std::endl;
            }
        }
    }
}
