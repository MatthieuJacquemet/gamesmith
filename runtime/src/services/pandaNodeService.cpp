// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.


#include <eventDispatcher.h>
#include <panda3d/characterJointEffect.h>
#include <panda3d/billboardEffect.h>
#include <panda3d/compassEffect.h>
#include <panda3d/decalEffect.h>
#include <panda3d/occluderEffect.h>
#include <panda3d/polylightEffect.h>
#include <panda3d/scissorEffect.h>
#include <panda3d/showBoundsEffect.h>
#include <panda3d/texProjectorEffect.h>
#include <panda3d/nearly_zero.h>

#include <custom_types.h>
#include <metaClass.h>
#include <metaTypeRegistry.h>

#include "transformTool.h"
#include "sceneGraphService.h"
#include "inspectorService.h"
#include "pandaNodeService.h"



struct RenderEffectAdaptor {

    using ArgList = std::vector<Variant>;
    using Mutator = std::function<void (PandaNode*, const ArgList&)>;
    using Accessor = std::function<Variant (PandaNode*)>;
    
    struct InitializerEntry {
        Mutator constructor; std::vector<std::string> accessors;
    };
    struct PropertyWrapperEntry {
        Accessor getter;
        std::array<Mutator, RenderEffectInfo::A_init> actions;
        PropertyBase::Attributes attributes;
    };
    std::unordered_map<std::string, InitializerEntry> initializers;
    std::unordered_map<std::string, PropertyWrapperEntry> properties;
};


template<class T, class D> constexpr T apply(D value) noexcept { 
    if constexpr (std::is_convertible_v<D, T>) {    
        return std::move(value);
    } else {
        std::cerr << "Warning: missmatch argument type" << std::endl;
        return T{};
    }
}

template<>
inline NodePath apply<NodePath>(uint64_t id) noexcept {
    return NodePath::any_path(SceneGraphService::lookup_node(id));
}

template<>
inline PandaNode* apply<PandaNode*>(uint64_t id) noexcept {
    return SceneGraphService::lookup_node(id);
}



template<class Type>
constexpr Type convert(const Type& value) noexcept { return value; }


inline LVecBase2f convert(const LVector2f& value) noexcept { return value; }
inline LVecBase2d convert(const LVector2d& value) noexcept { return value; }
inline LVecBase2i convert(const LVector2i& value) noexcept { return value; }
inline LVecBase2f convert(const LPoint2f& value) noexcept { return value; }
inline LVecBase2d convert(const LPoint2d& value) noexcept { return value; }
inline LVecBase2i convert(const LPoint2i& value) noexcept { return value; }

inline LVecBase3f convert(const LVector3f& value) noexcept { return value; }
inline LVecBase3d convert(const LVector3d& value) noexcept { return value; }
inline LVecBase3i convert(const LVector3i& value) noexcept { return value; }
inline LVecBase3f convert(const LPoint3f& value) noexcept { return value; }
inline LVecBase3d convert(const LPoint3d& value) noexcept { return value; }
inline LVecBase3i convert(const LPoint3i& value) noexcept { return value; }

inline LVecBase4f convert(const LVector4f& value) noexcept { return value; }
inline LVecBase4d convert(const LVector4d& value) noexcept { return value; }
inline LVecBase4i convert(const LVector4i& value) noexcept { return value; }
inline LVecBase4f convert(const LPoint4f& value) noexcept { return value; }
inline LVecBase4d convert(const LPoint4d& value) noexcept { return value; }
inline LVecBase4i convert(const LPoint4i& value) noexcept { return value; }

inline uint64_t convert(const NodePath& path) noexcept {
    if (UNLIKELY(path.is_empty())) {
        return 0;
    }
    return SceneGraphService::lookup_node(path.node());
}

inline uint64_t convert(PandaNode* node) noexcept {
    return SceneGraphService::lookup_node(node);
}

inline uint64_t convert(Character* node) noexcept {
    return SceneGraphService::lookup_node(node);
}

template<class T>
inline uint64_t convert(const PointerTo<T>& node) noexcept {
    if constexpr (std::is_base_of_v<PandaNode, T>) {
        return node ? SceneGraphService::lookup_node(node) : 0;
    }
    return 0;
}


template<class Type>
std::decay_t<Type> apply_arg(const Variant& value) noexcept {
    using type_t = decltype(convert(std::declval<std::decay_t<Type>>()));
    return apply<std::decay_t<Type>>(value.get_value<type_t>());
}

template<class... Args, size_t... Is>
inline void call(const std::function<CPT(RenderEffect)(Args...)>& function,
                const RenderEffectAdaptor::ArgList& args,
                PandaNode* node, std::index_sequence<Is...>) {
    
    static_assert((std::is_default_constructible_v<std::decay_t<Args>> && ...),
                "wrappers arguments must be default constructible");

    node->set_effect(
        function((Is < args.size() ? apply_arg<Args>(args[Is]) : Args{})...)
    );
}



template<class T>
RenderEffectAdaptor::Mutator effect_setter(T&& value) { return value; }

template<class T>
RenderEffectAdaptor::Accessor effect_getter(T&& value) { return value; }



template<class Type, class... Args>
auto effect_setter(CPT(RenderEffect) (Type::*method)(Args...) const) {
    using ArgList = RenderEffectAdaptor::ArgList;

    return [method](PandaNode* node, const ArgList& args) -> void {
        if (auto ptr = node->get_effect(Type::get_class_type())) {
            std::function<CPT(RenderEffect)(Args...)> fn = [&](auto&&... data) {
                auto* effect = static_cast<const Type*>(ptr.p());
                return (effect->*method)(std::forward<Args>(data)...);
            };
            call(fn, args, node, std::make_index_sequence<sizeof...(Args)>{});
        }
    };
}

template<class Type, class R, class... Args>
auto effect_getter(R (Type::*method)(Args...) const, Args... args) {
    return [method, args...](PandaNode* node) {
        if (auto ptr = node->get_effect(Type::get_class_type())) {
            auto* effect = static_cast<const Type*>(ptr.p());
            return Variant(convert((effect->*method)(args...)));
        }
        return Variant{};
    };
}


template<class Type, class R>
auto effect_getter( int (Type::*count_getter)() const,
                    R (Type::*item_getter)(int) const) {
    
    return [count_getter, item_getter](PandaNode* node) {
        using item_t = decltype(convert(std::declval<std::decay_t<R>>()));
        std::vector<item_t> result;

        if (auto ptr = node->get_effect(Type::get_class_type())) {
            auto* effect = static_cast<const Type*>(ptr.p());
            int count = (effect->*count_getter)();
            result.reserve(count);
            for (int i = 0; i < count; ++i) {
                result.emplace_back(convert((effect->*item_getter)(i)));
            }
        }
        return Variant(result);
    };
}

template<class... Args>
auto effect_factory(CPT(RenderEffect) (*factory)(Args...)) {
    using ArgList = RenderEffectAdaptor::ArgList;

    return [factory](PandaNode* node, const ArgList& args) -> void {
        std::function<CPT(RenderEffect)(Args...)> func = factory;
        call(func, args, node, std::make_index_sequence<sizeof...(Args)>{});
    };
}



const PropertyBase* find_property(PandaNode* node, const std::string& name) {

    if (auto* cls = MetaTypeRegistry::get_meta_class(node->get_type())) {
        auto& props = cls->get_properties();
        auto it = std::find_if(props.begin(), props.end(), [&](auto* prop) {
            return prop->get_name() == name;
        });
        if (LIKELY(it != props.end())) {
            return *it;
        }
    }
    return static_cast<const PropertyBase*>(nullptr);
}


using EffectsAdaptiors = TypeHandler<RenderEffectAdaptor>;

static EffectsAdaptiors effect_adaptors;


template<class T>
inline void register_effect(RenderEffectAdaptor&& adaptor) {
    static_assert(std::is_base_of_v<RenderEffect, T>, "Not a RenderEffect");
    effect_adaptors.set_handler(T::get_class_type(), adaptor);
}


#define REGISTER_INITIALIZER(NAME, FUNC, ...) {NAME,                        \
    {effect_factory(FUNC), {__VA_ARGS__}}}

#define REGISTER_PROPERTY(NAME, GETTER, SETTER, ...) {NAME,                 \
    RenderEffectAdaptor::PropertyWrapperEntry {                             \
        .getter     = effect_getter(GETTER),                                \
        .actions    = {effect_setter(SETTER), 0, 0},                        \
        .attributes = PropertyBase::Attributes{__VA_ARGS__}                 \
    }}

#define REGISTER_PROPERTY_SEQ(  NAME, COUNT_GETTER, ITEM_GETTER,            \
                                ADDER, REMOVER, ...) {NAME,                 \
    RenderEffectAdaptor::PropertyWrapperEntry {                             \
        .getter     = effect_getter(COUNT_GETTER, ITEM_GETTER),             \
        .actions    = {0, effect_setter(ADDER), effect_setter(REMOVER)},    \
        .attributes = PropertyBase::Attributes{__VA_ARGS__}                 \
    }}


void register_legacy_render_effects() {

    register_effect<OccluderEffect>(RenderEffectAdaptor {
        .initializers = {
            REGISTER_INITIALIZER("", OccluderEffect::make, "occluders"),
        },
        .properties = {
            REGISTER_PROPERTY_SEQ("occluders",
                &OccluderEffect::get_num_on_occluders, &OccluderEffect::get_on_occluder,
                &OccluderEffect::add_on_occluder, &OccluderEffect::remove_on_occluder
            ),
        }
    });
    register_effect<BillboardEffect>(RenderEffectAdaptor {
        .initializers = {
            REGISTER_INITIALIZER("", BillboardEffect::make, "up vector",
                                "eye relative", "axial rotate", "offset",
                                "look at", "look at point", "fixed depth"),
        },
        .properties = {
            REGISTER_PROPERTY("up vector", &BillboardEffect::get_up_vector, nullptr),
            REGISTER_PROPERTY("eye relative", &BillboardEffect::get_eye_relative, nullptr),
            REGISTER_PROPERTY("axial rotate", &BillboardEffect::get_axial_rotate, nullptr),
            REGISTER_PROPERTY("offset", &BillboardEffect::get_offset, nullptr),
            REGISTER_PROPERTY("look at", &BillboardEffect::get_look_at, nullptr),
            REGISTER_PROPERTY("look at point", &BillboardEffect::get_look_at_point, nullptr),
            REGISTER_PROPERTY("fixed depth", &BillboardEffect::get_fixed_depth, nullptr),
        }
    });
    auto make_node = Overload<const LPoint3&, const LPoint3&,
                    const NodePath&>::of(ScissorEffect::make_node);
    register_effect<ScissorEffect>(RenderEffectAdaptor {
        .initializers = {
            REGISTER_INITIALIZER("screen", ScissorEffect::make_screen, "frame", "clip"),
            REGISTER_INITIALIZER("node", make_node, "a", "b", "node"),
        },
        .properties = {
            REGISTER_PROPERTY("frame", &ScissorEffect::get_frame, nullptr),
            REGISTER_PROPERTY("clip", &ScissorEffect::get_clip, nullptr),
            REGISTER_PROPERTY("a", effect_getter(&ScissorEffect::get_point, 0), nullptr),
            REGISTER_PROPERTY("b", effect_getter(&ScissorEffect::get_point, 1), nullptr),
            REGISTER_PROPERTY("node", effect_getter(&ScissorEffect::get_node, 0), nullptr)
        }
    });
    register_effect<CharacterJointEffect>(RenderEffectAdaptor {
        .initializers = {
            REGISTER_INITIALIZER("", CharacterJointEffect::make, "character"),
        },
        .properties = {
            REGISTER_PROPERTY("character", &CharacterJointEffect::get_character, nullptr),
        }
    });

    register_effect<CompassEffect>(RenderEffectAdaptor {
        .initializers = {
            REGISTER_INITIALIZER("", CompassEffect::make, "reference", "properties"),
        },
        .properties = {
            REGISTER_PROPERTY("reference", &CompassEffect::get_reference, nullptr),
            REGISTER_PROPERTY("properties", &CompassEffect::get_properties, nullptr,
                                {PropertyBase::A_enum_flags,
                                std::string("CompassEffect.Properties") }),
        },
    });
    register_effect<DecalEffect>(RenderEffectAdaptor {
        .initializers = { REGISTER_INITIALIZER("", DecalEffect::make) }
    });
    register_effect<ShowBoundsEffect>(RenderEffectAdaptor {
        .initializers = { REGISTER_INITIALIZER("", ShowBoundsEffect::make, "tight") },
        .properties = { REGISTER_PROPERTY("tight", &ShowBoundsEffect::get_tight, nullptr) }
    });
}


struct PandaNodeService::PandaNodeServicePrivate {
    uint64_t transform_token;
};



PandaNodeService::PandaNodeService(uint16_t id) : ServiceBase(id) {
    
    auto* dispatcher = EventDispatcher::get_global_dispatcher();
    auto transforms_event = TransformTool::get_transforms_changed();

    using Transforms = TransformTool::Transforms;
    _d->transform_token = dispatcher->add_listener(transforms_event,
        [this](const TransformTool::Transforms& transforms) {
            if (PT(PandaNode) node = InspectorService::get_current_node()) {
                TransformTool::Transforms::const_iterator it;
                if ((it = transforms.find(node)) != transforms.end()) {
                    transforms_changed( it->second->get_pos(),
                                        it->second->get_hpr(),
                                        it->second->get_scale(),
                                        it->second->get_shear());
                }
            }
        }
    );
}

PandaNodeService::~PandaNodeService() {
    EventDispatcher::get_global_dispatcher()->remove_listener(
        TransformTool::get_transforms_changed(), _d->transform_token
    );
}

RenderEffectInfo
get_render_effet_info(PandaNode* node, const RenderEffect* effect) {
    RenderEffectInfo effect_info;
    if (UNLIKELY(effect == nullptr)) {
        return effect_info;
    }
    auto& adaptor = effect_adaptors.get_handler(effect->get_type());

    effect_info.typeId = effect->get_type().get_index();
    
    effect_info.initializers.reserve(adaptor.initializers.size());
    for (const auto& [name, initializer] : adaptor.initializers) {
        auto& data = effect_info.initializers;
        data.push_back({name, initializer.accessors});
    }
    for (const auto& [name, property] : adaptor.properties) {
        auto& data = effect_info.properties;
        auto& info = data.emplace_back(PropertyInfo{
            name, property.getter(node), property.attributes
        });

        if (!property.actions[RenderEffectInfo::A_set]) {
            info.attributes[PropertyBase::A_readonly] = true;
        }
        if (property.actions[RenderEffectInfo::A_add]) {
            info.attributes[PropertyBase::A_add] = true;
        }
        if (property.actions[RenderEffectInfo::A_remove]) {
            info.attributes[PropertyBase::A_remove] = true;
        }
    }
    return effect_info;
}

NodeInfo create_node_info(PandaNode* node) {
    NodeInfo info;
    if (node == nullptr) return info;

    CPT(TransformState) transform = node->get_transform();
    CPT(RenderState) render_state = node->get_state();
    CPT(RenderEffects) render_effects = node->get_effects();

    info.id             = SceneGraphService::lookup_node(node);
    info.name           = node->get_name();
    info.type_id        = node->get_type().get_index();
    info.draw_mask      = node->get_draw_show_mask();
    info.control_mask   = node->get_draw_control_mask();
    info.bounds_type    = node->get_bounds_type();
    info.position       = transform->get_pos();
    info.rotation       = transform->get_hpr();
    info.scale          = transform->get_scale();
    info.shear          = transform->get_shear();

    int num_tags = node->get_num_tags();
    info.tags.reserve(num_tags);
    for (int i = 0; i < num_tags; ++i) {
        TagInfo& tag = info.tags.emplace_back();
        tag.key = node->get_tag_key(i);
        tag.value = node->get_tag(tag.key);
    }
    if (render_state != nullptr) {
        for (int i = 0; i < RenderAttribRegistry::_max_slots; ++i) {
            if (CPT(RenderAttrib) attrib = render_state->get_attrib(i)) {
                auto& render_attrib = info.attribs.emplace_back();
                render_attrib.id = i;
                render_attrib.priority = render_state->get_override(i);
            }
        }
    }
    if (render_effects != nullptr) {
        for (int i = 0; i < render_effects->get_num_effects(); ++i) {
            info.effects.emplace_back(
                get_render_effet_info(node, render_effects->get_effect(i))
            );
        }
    }

    if (auto* meta = MetaTypeRegistry::get_meta_class(node->get_type())) {
        for (auto* property : meta->get_properties()) {
            auto& prop_info = info.properties.emplace_back();
            prop_info.name = property->get_name();
            prop_info.attributes = property->get_attributes();
        }
    }
    return info;
}



NodeInfo PandaNodeService::get_node_info() {
    if (PT(PandaNode) node = InspectorService::get_current_node()) {
        // node->set_effect(BillboardEffect::make_axis());
        return create_node_info(node);
    }
    return NodeInfo{};
}

void PandaNodeService::set_name(const std::string& name) {
    if (PT(PandaNode) node = InspectorService::get_current_node()) {
        node->set_name(name);
        EventDispatcher::get_global_dispatcher()->dispatch_event(
            SceneGraphService::get_node_name_changed(), node.p(), name
        );
    }    
}

void PandaNodeService::set_draw_masks(DrawMask show, DrawMask control) {
    
    if (PT(PandaNode) node = InspectorService::get_current_node()) {
        node->adjust_draw_mask( DrawMask::all_off(),
                                DrawMask::all_off(),
                                DrawMask::all_on());
        
        node->adjust_draw_mask( show & control,
                               ~show & control,
                                show & ~control);
    }
}

void PandaNodeService::add_attrib(int32_t attribId) {
    
}

void PandaNodeService::remove_attrib(int32_t attribId) {
    
}

void PandaNodeService::update_attrib(int32_t id, const std::string& p, 
                                    const std::vector<Variant>& args) {
    
}

RenderEffectInfo PandaNodeService::add_effect(int32_t effectId) {
    if (PT(PandaNode) node = InspectorService::get_current_node()) {

        TypeHandle type = TypeHandle::from_index(effectId);
        auto& adaptor = effect_adaptors.get_handler(type);
        if (LIKELY(adaptor.initializers.size() > 0)) {
            auto& entry = adaptor.initializers.begin()->second;
            entry.constructor(node, {} /* empty parametters */);

            return get_render_effet_info(node, node->get_effect(type));
        }
    }
    return RenderEffectInfo{};
}

void PandaNodeService::remove_effect(int32_t effectId) {
    if (PT(PandaNode) node = InspectorService::get_current_node()) {
        node->clear_effect(TypeHandle::from_index(effectId));
    }
}

void PandaNodeService::update_effect(int32_t id, const std::string& prop, 
                                    const std::vector<Variant>& data,
                                    RenderEffectInfo::Action action) {
    
    if (PT(PandaNode) node = InspectorService::get_current_node()) {
        auto& adaptor = effect_adaptors.get_handler(TypeHandle::from_index(id));
        
        if (action == RenderEffectInfo::A_init) {
            auto it = adaptor.initializers.find(prop);
            if (LIKELY(it != adaptor.initializers.end())) {
                it->second.constructor(node, data);
            }
        } else {
            auto it = adaptor.properties.find(prop);
            if (LIKELY(it != adaptor.properties.end())) {
                if (auto& wrapper = it->second.actions[action]) {
                    wrapper(node, data);
                }
            }
        }
    }
}

void PandaNodeService::set_transform(const LVector3& position,
                                    const LVector3& rotation,
                                    const LVector3& scale,
                                    const LVector3& shear) {
    
    if (PT(PandaNode) node = InspectorService::get_current_node()) {
        LVector3 scale_adjusted;
        for (int i = 0; i < 3; ++i) {
            float value = std::max(1.0e-5f, std::abs(scale[i]));
            scale_adjusted[i] = std::copysign(value, scale[i]);
        }
        node->set_transform(TransformState::make_pos_hpr_scale_shear(
            position, rotation, scale_adjusted, shear
        ));
        EventDispatcher::get_global_dispatcher()->dispatch_event(
            TransformTool::get_transforms_modified(), {}
        );
    }
}

void PandaNodeService::set_tags(const std::vector<TagInfo>& tags) {
    if (PT(PandaNode) node = InspectorService::get_current_node()) {
        while (node->get_num_tags()) {
            node->clear_tag(node->get_tag_key(0));
        }
        for (const auto& [key, value] : tags) {
            node->set_tag(key, value);
        }
    }
}


void PandaNodeService::set_bound_type(BoundingVolume::BoundsType type) {
    if (PT(PandaNode) node = InspectorService::get_current_node()) {
        node->set_bounds_type(type);
    }
}

void PandaNodeService::set_property(const std::string& name,
                                    const Variant& data) {
    
    if (PT(PandaNode) node = InspectorService::get_current_node()) {
        if (const PropertyBase* prop = find_property(node, name)) {
            prop->set_value(node, data);
        }
    }
}

Variant PandaNodeService::get_property(const std::string& name) {
    
    if (PT(PandaNode) node = InspectorService::get_current_node()) {
        if (const PropertyBase* prop = find_property(node, name)) {
            return prop->get_value(node);
        }
    }
    return Variant(); // did not found property with that name
}

