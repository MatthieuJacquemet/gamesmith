// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <config_gdk.h>
#include <eventDispatcher.h>
#include <internalName.h>
#include <lightMutexHolder.h>
#include <panda3d/mouseButton.h>
#include <panda3d/shader.h>
#include <panda3d/cardMaker.h>
#include <panda3d/geomLinestrips.h>
#include <panda3d/geomVertexData.h>
#include <panda3d/geomVertexWriter.h>
#include <panda3d/graphicsWindow.h>
#include <panda3d/stencilAttrib.h>
#include <panda3d/depthTestAttrib.h>
#include <panda3d/depthWriteAttrib.h>
#include <panda3d/colorWriteAttrib.h>
#include <panda3d/scissorAttrib.h>
#include <panda3d/boundingHexahedron.h>
#include <panda3d/boundingLine.h>
#include <panda3d/occlusionQueryContext.h>
#include <panda3d/cullHandler.h>
#include <panda3d/antialiasAttrib.h>

#include <algorithm>
#include <optional>
#include <renderPipeline.h>
#include <samplerState.h>
#include <sceneData.h>
#include <render_utils.h>
#include <cullHandlerHook.h>
#include <renderComponent.h>
#include <weakCollections.h>
#include <renderTarget.h>

#include "iostream_ops.h"
#include "inputEventListener.h"
#include "serviceBase.h"
#include "viewportService.h"
#include "selectionTraverser.h"
#include "selectTool.h"
#include "renderOverlayLayer.h"


const CPT_InternalName stancil_name = "u_stencil";


static CPT(RenderAttrib) make_stencil_attrib(unsigned int stats[]) {
    using CmpFn = RenderAttrib::PandaCompareFunc;
    using SO = StencilAttrib::StencilOperation;

    return StencilAttrib::make_2_sided_with_clear(true, true,
        CmpFn(stats[StencilAttrib::SRS_front_comparison_function]),
        SO(stats[StencilAttrib::SRS_front_stencil_fail_operation]),
        SO(stats[StencilAttrib::SRS_front_stencil_pass_z_fail_operation]),
        SO(stats[StencilAttrib::SRS_front_stencil_pass_z_pass_operation]),
        stats[StencilAttrib::SRS_reference],
        stats[StencilAttrib::SRS_read_mask],
        stats[StencilAttrib::SRS_write_mask],
        CmpFn(stats[StencilAttrib::SRS_back_comparison_function]),
        SO(stats[StencilAttrib::SRS_back_stencil_fail_operation]),
        SO(stats[StencilAttrib::SRS_back_stencil_pass_z_fail_operation]),
        SO(stats[StencilAttrib::SRS_back_stencil_pass_z_pass_operation]),
        stats[StencilAttrib::SRS_clear],
        stats[StencilAttrib::SRS_clear_value]
    );
}


class SelectionCullHandler final :  public PipelineCullTraverser,
                                    private CullHandler {
    
    REGISTER_TYPE("SelectionCullHandler", PipelineCullTraverser);

public:
    SelectionCullHandler(RenderPipeline* pipeline, SelectTool* tool): 
        PipelineCullTraverser(pipeline), _tool(tool),
        _original_cull_traverser(pipeline->get_traverser()) {
    }
    ~SelectionCullHandler() { }

    virtual void set_scene( SceneSetup* scene_setup,
                            GraphicsStateGuardianBase* gsg,
                            bool dr_force) override {
        _actual_cull_handler = get_cull_handler();
        set_cull_handler(this);
        PipelineCullTraverser::set_scene(scene_setup, gsg, dr_force);
    }
    virtual void end_traverse() override {
        set_cull_handler(_actual_cull_handler);
        PipelineCullTraverser::end_traverse();
    }
    virtual void record_object( CullableObject* object,
                                const CullTraverser* trav) override;

    inline PipelineCullTraverser* get_original_traverser() const {
        return _original_cull_traverser;
    }
private:
    SelectTool* _tool;
    CullHandler* _actual_cull_handler = nullptr;
    PT(PipelineCullTraverser) _original_cull_traverser;
};


void SelectionCullHandler::record_object(CullableObject* object,
                                        const CullTraverser* trav) {

    auto* traverser = static_cast<const PipelineCullTraverser*>(trav);
    CullTraverserData* data = traverser->get_current_data();

    using StencilRenderState = StencilAttrib::StencilRenderState;
    using SA = StencilAttrib;

    unsigned int stats[StencilAttrib::SRS_total] = {0};
    const unsigned int selected_bit = 0x80;
    const StencilAttrib* sattr;
    if (object->_state->get_attrib(sattr)) {
        for (int i = 0; i < StencilAttrib::SRS_total; ++i) {
            stats[i] = sattr->get_render_state(StencilRenderState(i));
        }
    }
    if (_tool->is_node_selected(data->node())) {
        stats[SA::SRS_front_comparison_function] = SA::M_always;
        stats[SA::SRS_front_stencil_fail_operation] = SA::SO_replace;
        stats[SA::SRS_front_stencil_pass_z_pass_operation] = SA::SO_replace;
        stats[SA::SRS_front_stencil_pass_z_fail_operation] = SA::SO_replace;

        stats[SA::SRS_back_comparison_function] = SA::M_always;
        stats[SA::SRS_back_stencil_fail_operation] = SA::SO_replace;
        stats[SA::SRS_back_stencil_pass_z_pass_operation] = SA::SO_replace;
        stats[SA::SRS_back_stencil_pass_z_fail_operation] = SA::SO_replace;

        stats[SA::SRS_write_mask] |= selected_bit;
        stats[SA::SRS_reference] |= selected_bit;
    } else {
        // stats[SA::SRS_front_comparison_function] = SA::M_always;
        // stats[SA::SRS_front_stencil_fail_operation] = SA::SO_keep;
        // stats[SA::SRS_front_stencil_pass_z_fail_operation] = SA::SO_keep;
        // stats[SA::SRS_front_stencil_pass_z_pass_operation] = SA::SO_zero;
        // stats[SA::SRS_reference] = selected_bit;
        // stats[SA::SRS_write_mask] = selected_bit;
    }
    object->_state = object->_state->set_attrib(make_stencil_attrib(stats));
    _actual_cull_handler->record_object(object, traverser);
}

DEFINE_TYPEHANDLE(SelectionCullHandler)


struct SelectTool::SelectToolPrivate {

    SelectTool::Mode mode = SelectTool::M_rectangle;
    SelectTool::Operation op = SelectTool::O_replace;
    ShaderState base_state, border_state, outline_state;

    uint64_t token;

    WeakPointerHashSet<PandaNode> selected_nodes;
    struct DragData {
        LVecBase2f start_pos;
    };
    std::optional<DragData> drag_data;
    std::optional<LVector4> selection_request;
    WeakPointerSet<SelectionCullHandler> cull_handlers;
};


CPT(Geom) make_selection_rectangle() {
    CardMaker maker("fullscreen quad");
    maker.set_frame(0.0f, 1.0f, 0.0f, 1.0f);
    return DCAST(GeomNode, maker.generate())->get_geom(0);
}

CPT(Geom) make_selection_outline_rectangle() {

    PT(GeomVertexData) data = new GeomVertexData("rectangle-outline",
                                                GeomVertexFormat::get_v3(),
                                                GeomEnums::UH_static);

    PT(Geom) geom = new Geom(data);

    PT(GeomLinestrips) primitive = new GeomLinestrips(GeomEnums::UH_static);
    primitive->reserve_num_vertices(4);
    
    if (UNLIKELY(!data->reserve_num_rows(4))) {
        return nullptr;
    }
    LVector3 vertices[] = {
        {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f, 0.0f},
        {1.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 1.0f}
    };
    GeomVertexWriter writer(data, InternalName::get_vertex());
    for (const LVector3& vertex : vertices) {
        writer.add_data3f(vertex);
    }
    for (int i = 0; i < 4; ++i) {
        primitive->add_vertices(i, (i + 1) & 0x3);
    }
    primitive->close_primitive(); geom->add_primitive(primitive);
    return geom;
}




SelectTool::SelectTool(uint16_t id) :   InputEventListener(100),
                                        ToolService(id) {
    
    if (ViewportService::get_active_viewport()) viewport_changed(nullptr);

    _d->base_state.set_shader(Shader::load(Shader::SL_GLSL,
        "/data/shader/basic_color_scale_orthographic.vert",
        "/data/shader/basic_color_scale_orthographic.frag"
    ));
    _d->base_state.set_color_scale(LColor(1.0f, 1.0f, 1.0f, 0.1f));
    _d->base_state.set_two_sided(true);
    _d->base_state.set_depth_test(false);
    _d->base_state.set_depth_write(false);
    _d->base_state.set_transparency(TransparencyAttrib::M_alpha);

    _d->border_state.compose(_d->base_state);
    _d->border_state.set_shader(Shader::load(Shader::SL_GLSL,
        "/data/shader/basic_color_scale_orthographic.vert",
        "/data/shader/dashed_lines_color_scale.frag",
        "/data/shader/dashed_lines_color_scale.geom"
    ));
    _d->border_state.set_color_scale(LColor(1.0f, 1.0f, 1.0f, 1.0f));

    _d->outline_state.compose(_d->base_state);
    _d->outline_state.set_shader(Shader::load(Shader::SL_GLSL,
        GDK_SHADER_PATH VERT_SHADER("quad"),
        "/data/shader/selection_outline_color_scale.frag"
    ));
    _d->outline_state.set_color_scale(LColor(1.0f, 1.0f, 0.0f, 1.0f));
    _d->outline_state.set_attrib(AntialiasAttrib::make(AntialiasAttrib::M_multisample |
                                                        AntialiasAttrib::M_better));
    
    _d->token = EventDispatcher::get_global_dispatcher()->add_listener(
        get_selection_updated(), &SelectTool::selection_updated, this
    );
}

SelectTool::~SelectTool() {

    while (!_d->cull_handlers.empty()) {
        auto it = _d->cull_handlers.begin();
        if (PointerTo<SelectionCullHandler> handler = it->lock()) {
            RenderPipeline* pipeline = handler->get_pipeline();
            pipeline->set_traverser(handler->get_original_traverser());
            nassertv(handler->get_ref_count() == 1);
        }
    }
    EventDispatcher::get_global_dispatcher()->remove_listener(
        get_selection_updated(), _d->token
    );
}


void SelectTool::enabled(bool active, uint32_t mode) {
    if (active) {
        LightMutexHolder holder(ToolService::_lock);
        _active_tools.insert(this);
    }
    _d->mode = Mode(mode);
    
    if (ViewportService* viewport = ViewportService::get_active_viewport()) {
        if (RenderPipeline* pipeline = viewport->get_render_pipeline()) {
            setup_pipeline(pipeline);
        }
    }
}


void SelectTool::draw_overlay(RenderOverlayLayer* drawer) {
    
    Thread* thread = Thread::get_current_thread();
    SceneData* data = drawer->get_scene_data();

    if (_d->selection_request.has_value()) {
        occlusion_test(drawer); _d->selection_request.reset();
    }
    if (!_d->selected_nodes.empty()) {
        const ShaderAttrib* sattr;
        SamplerState::FilterType minfilter;
        Texture* texture = nullptr;
        if (LIKELY(_d->outline_state.get_attrib(sattr))) {
            if ((texture = sattr->get_shader_input_texture(stancil_name))) {
                minfilter = texture->get_minfilter();
                texture->set_minfilter(SamplerState::FT_stencil);
            }
        }
        draw_fullscreen_quad(data->get_gsg(), _d->outline_state, thread);
        if (texture != nullptr) {
            texture->set_minfilter(minfilter);
        }
    }
    if (!_d->drag_data.has_value()) {
        return;
    }
    if (_d->mode == Mode::M_rectangle) {
        static CPT(Geom) outline_eom = make_selection_outline_rectangle();
        static CPT(Geom) fill_geom = make_selection_rectangle();

        LVector2 delta =  _d->drag_data->start_pos - get_mouse_position();
        LVector3 scale(delta[0], 1.f, delta[1]);
        LVector3 pos(_d->drag_data->start_pos[0], 1.f,
                    _d->drag_data->start_pos[1]);
        
        auto transform = TransformState::make_pos_hpr_scale(
            pos - scale, LVector3::zero(), scale
        );
        transform = data->get_cs_transform()->compose(transform);
        draw_geom(data->get_gsg(), fill_geom, _d->base_state, transform);
        draw_geom(data->get_gsg(), outline_eom, _d->border_state, transform);
    }
}

void SelectTool::setup_pipeline(RenderPipeline* pipeline) {

    auto* texture = pipeline->get_render_target("org.gdk.rt.depth-stencil");
    SceneData* data = pipeline->get_scene_data();

    texture->set_wrap_u(SamplerState::WM_clamp);
    texture->set_wrap_v(SamplerState::WM_clamp);
    _d->outline_state.set_shader_input(stancil_name, texture);
    _d->outline_state.set_shader_input("u_view_scale", data->_resolution);
    _d->border_state.set_shader_input("u_view_scale", data->_resolution);

    PipelineCullTraverser* trav = pipeline->get_traverser();
    if (!trav->is_exact_type(SelectionCullHandler::get_class_type())) {
        auto* traverser = new SelectionCullHandler(pipeline, this);
        pipeline->set_traverser(_d->cull_handlers.insert(traverser)->p());
    }
}

void SelectTool::viewport_changed(ViewportService* old_viewport) {

    ViewportService* viewport = ViewportService::get_active_viewport();
    if (RenderPipeline* pipeline = viewport->get_render_pipeline()) {
        setup_pipeline(pipeline);
    }
}

SelectTool::SelectedNodes SelectTool::get_selected_nodes() const {
    SelectedNodes nodes;
    for (auto& node_ref : _d->selected_nodes) {
        if (PT(PandaNode) node = node_ref.lock()) {
            nodes.emplace_back(std::move(node));
        }
    }
    return nodes;
}

bool SelectTool::is_node_selected(PandaNode* node) const {
    return _d->selected_nodes.find(node) != _d->selected_nodes.end();
}


static LVector2 get_pixel_size() {
    ViewportService* viewport =ViewportService::get_active_viewport();
    GraphicsOutput* output = viewport->get_framebuffer();

    return LVector2(2.0f / float(output->get_fb_x_size()),
                    2.0f / float(output->get_fb_y_size()));

}

void SelectTool::handle_button_event(const ButtonEvent& event) {

    if (event.get_button() != MouseButton::one()) {

    } else if (event.get_type() == ButtonEvent::Type::T_up &&
            _d->drag_data.has_value()) {
        grab_inputs(false);
        finish_drag_operation();
    } else if (event.get_type() == ButtonEvent::Type::T_down) {
        grab_inputs(true);
        _d->drag_data.emplace();
        _d->drag_data->start_pos = get_mouse_position();
    }
}

void SelectTool::finish_drag_operation() {

    const LVector2& start_pos = _d->drag_data->start_pos;
    const LVector2& end_pos = get_mouse_position();

    LVector4& frame = _d->selection_request.emplace();
    LVector2 pixel_size = get_pixel_size();

    std::tie(frame[0], frame[1]) = std::minmax(start_pos[0], end_pos[0]);
    std::tie(frame[2], frame[3]) = std::minmax(start_pos[1], end_pos[1]);

    frame[1] = std::max(frame[1], frame[0] + pixel_size.get_x());
    frame[3] = std::max(frame[3], frame[2] + pixel_size.get_y());

    _d->drag_data.reset();
}


SceneSetup SelectTool::setup_scene(const SceneSetup* initial_setup) {

    const LVector4& frame = _d->selection_request.value();
    SceneSetup setup(*initial_setup);
    setup.local_object();

    setup.set_initial_state(RenderState::make(
        DepthTestAttrib::make(RenderAttrib::M_less_equal),
        DepthWriteAttrib::make(DepthWriteAttrib::M_off),
        ColorWriteAttrib::make(ColorWriteAttrib::C_off),
        ScissorAttrib::make(frame * 0.5f + LVector4(0.5f)),
        StencilAttrib::make(true,
            RenderAttrib::M_always, StencilAttrib::SO_keep,
            StencilAttrib::SO_keep, StencilAttrib::SO_keep, 1, 1, 0),
        RenderState::get_max_priority()
    ));
    const Lens* lens = initial_setup->get_lens();
    LPoint3 near[4], far[4];
    if (lens->extrude(LPoint2(frame[0], frame[2]), near[0], far[0])) {
        GeometricBoundingVolume* frustum = nullptr;
        
        LVector2 size(frame[0] - frame[0], frame[3] - frame[2]);
        if (size == get_pixel_size()) { // simple click, use line for culling
            frustum = new BoundingLine(near[0], far[0]);
        } 
        if (lens->extrude(LPoint2(frame[1], frame[2]), near[1], far[1]) &&
            lens->extrude(LPoint2(frame[1], frame[3]), near[2], far[2]) &&
            lens->extrude(LPoint2(frame[0], frame[3]), near[3], far[3])) {

            frustum = new BoundingHexahedron(far[0], far[1], far[2], far[3],
                                            near[0],near[1],near[2],near[3]);
        }
        if (LIKELY(frustum != nullptr)) {    
            frustum->xform(setup.get_camera_transform()->get_mat());
            setup.set_view_frustum(frustum);
        }
    }
    return setup;
}

void SelectTool::occlusion_test(RenderOverlayLayer* drawer) {

    GraphicsStateGuardian* gsg = drawer->get_scene_data()->get_gsg();
    SceneSetup setup = setup_scene(drawer->get_scene_data());
    
    // TODO: draw mask geom

    SelectionTraverser traverser(gsg);
    traverser.set_scene(&setup, gsg, false);
    traverser.traverse(setup.get_scene_root());
    traverser.end_traverse();

    std::unordered_set<WPT(PandaNode), wp_hash<PandaNode>> new_selection;
    Operation operation = _d->op;
    if (is_button_pressed(KeyboardButton::shift())) {
        operation = Operation::O_difference;
    }
    for (const auto& object : traverser.get_objects()) {
        object._query->waiting_for_answer();
        if (object._query->get_num_fragments() > 0) {
            switch (operation) {
                case Operation::O_remove: {
                    _d->selected_nodes.insert(object._node);
                    break;
                case Operation::O_add:
                    _d->selected_nodes.erase(object._node);
                    break;
                } case Operation::O_replace:
                    new_selection.insert(object._node);
                    break;
                case Operation::O_intersect:
                    if (_d->selected_nodes.count(object._node)) {
                        new_selection.insert(object._node);
                    }
                    break;
                case Operation::O_difference:
                    if (_d->selected_nodes.count(object._node)) {
                        _d->selected_nodes.erase(object._node);
                    } else {
                        _d->selected_nodes.insert(object._node);
                    }
                default: break;
            }
        }
    }
    if (operation == O_intersect || operation == O_replace) {
        _d->selected_nodes = std::move(new_selection);
    }
    EventDispatcher::get_global_dispatcher()->dispatch_event(
        get_selection_changed(), get_selected_nodes()
    );
}

void SelectTool::selection_updated(const SelectedNodes& selected) {
    
    _d->selected_nodes.clear();
    for (PandaNode* node : selected) {
        _d->selected_nodes.insert(node);
    }
    EventDispatcher::get_global_dispatcher()->dispatch_event(
        get_selection_changed(), get_selected_nodes()
    );
}


TypeHandle _get_type_handle(const SelectTool::SelectedNodes*) {
    return TypeHandle::none();    
}