// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SCENEGRAPHSERVICE_H__
#define __SCENEGRAPHSERVICE_H__

#include "serviceBase.h"
#include <utils.h>
#include <list>
#include <eventDispatcher.h>

class [[meta::service]] SceneGraphService : public ServiceBase {

    REGISTER_SERVICE
public:
    REGISTER_EVENT(node_name_changed, PandaNode*, std::string);

    SceneGraphService(uint16_t id);
    virtual ~SceneGraphService();

    struct [[meta::serialize]] NodeEntry {
        uint64_t id; int32_t classId;
        std::string name;
        std::vector<uint64_t> children;
    };
    using Node = std::optional<::SceneGraphService::NodeEntry>;
    using Graph = std::list<::SceneGraphService::NodeEntry>;

    [[meta::rpc]] ::SceneGraphService::Node get_node(uint64_t id);
    [[meta::rpc]] ::SceneGraphService::Graph get_graph(uint64_t id);

    [[meta::rpc]]
    void rename_node(uint64_t id, const std::string& name);

    [[meta::signal]]
    void node_renamed(uint64_t id, const std::string& name);

    [[meta::rpc]]
    void update_selection(const std::vector<uint64_t>& nodes);

    [[meta::signal]]
    void stale_nodes(const std::vector<uint64_t>& nodes);

    [[meta::signal]]
    void selection_changed(const std::vector<uint64_t>& nodes);
    
    [[meta::signal]]
    void graph_changed(const ::SceneGraphService::Graph& graph);

    static PT(PandaNode) lookup_node(uint64_t id);
    static uint64_t lookup_node(PandaNode* node);

private:
    struct SceneGraphServicePrivate;
    dimpl<SceneGraphServicePrivate> _d;
};

#endif // __SCENEGRAPHSERVICE_H__