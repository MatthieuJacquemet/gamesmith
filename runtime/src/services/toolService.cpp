// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/lightMutexHolder.h>

#include "editTool.h"
#include "toolService.h"


ToolService::ActiveTools ToolService::_active_tools;
LightMutex ToolService::_lock;


ToolService::~ToolService() {
    LightMutexHolder holder(_lock);
    _active_tools.erase(this);
}

const ToolService::ActiveTools& ToolService::get_active_tools() noexcept {
    return _active_tools;
}

void ToolService::activate(uint32_t mode) {

    // We iterate over a copy as the active tools set will be modified
    // in the loop
    ActiveTools active_tools = _active_tools;

    for (ToolService* tool : active_tools) {
        if (tool != this) {
            tool->enabled(false, 0);
        }
    }
    this->enabled(true, mode);
}

void ToolService::enabled(bool enabled, uint32_t mode) {
    LightMutexHolder holder(_lock);
    if (enabled)    _active_tools.insert(this);
    else            _active_tools.erase(this);
}

void ToolService::draw_overlay(RenderOverlayLayer* drawer) {
    
}

void ToolService::setup_pipeline(RenderPipeline* pipeline) {
    
}