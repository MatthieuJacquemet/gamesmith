// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __TOOLSERVICE_H__
#define __TOOLSERVICE_H__

#include <panda3d/indirectLess.h>

#include <weakCollections.h>
#include "serviceBase.h"
#include "editTool.h"

class RenderOverlayLayer;
class RenderPipeline;
class LightMutex;

class [[meta::service]] ToolService: public ServiceBase {
    
    REGISTER_SERVICE

public:
    inline ToolService(uint16_t id) : ServiceBase(id) {}
    virtual ~ToolService();

    struct Comparator {
        bool operator()(ToolService* a, ToolService* b) const {
            int sort_a = a->get_priority();
            int sort_b = b->get_priority();
            return sort_a == sort_b ? a < b : sort_a < sort_b;
        }
    };
    typedef std::set<ToolService*, Comparator> ActiveTools;

    static const ActiveTools& get_active_tools() noexcept;

    [[meta::rpc]] void activate(uint32_t mode);

    virtual void draw_overlay(RenderOverlayLayer* drawer);
    virtual void setup_pipeline(RenderPipeline* pipeline);
    virtual int get_priority() const { return 0; }

protected:
    virtual void enabled(bool enabled, uint32_t mode);

    static ActiveTools _active_tools;
    static LightMutex _lock;
};

#endif // __TOOLSERVICE_H__