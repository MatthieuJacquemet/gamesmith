// Copyright 2023 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __CONFIGSERVICE_H__
#define __CONFIGSERVICE_H__

#include <panda3d/luse.h>
#include <serviceBase.h>
#include <variant>


class [[meta::service]] ConfigService : public ServiceBase {

    REGISTER_SERVICE

public:
    inline ConfigService(uint16_t id) : ServiceBase(id) {}
    virtual ~ConfigService() = default;


    struct [[meta::serialize]] ValueSpec final {
        ConfigFlags::ValueType value_type;
        std::variant<bool, double, int, int64_t, std::string,
                     std::vector<std::string>, LColor> value;
    };

    struct [[meta::serialize]] ConfigSpec final {
        std::string name;
        std::string description;
        ValueSpec default_value;
    };
    [[meta::rpc]] std::vector<::ConfigService::ConfigSpec> get_config_specs();

    [[meta::rpc]] void set_config_variable(const std::string& name,
                            const ::ConfigService::ValueSpec& value);

};

#endif // __CONFIGSERVICE_H__