// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.


#include <tuple>

#include <panda3d/graphicsPipe.h>

#include <applicationFramework.h>

#include "hardware_info.h"
#include "serviceBase.h"
#include "systemRessources.h"
#include "gpuInformations.h"

#if defined(IS_LINUX) || defined(IS_FREEBSD)

#include <sys/resource.h>  // for getrusage
#include <sys/utsname.h>
#include <unistd.h>        // for getrusage
#include <sys/sysinfo.h>

#endif

static std::unique_ptr<GpuInformations> gpu_info;


static inline DisplayInformation* get_informations() {

    auto* framework = ApplicationFramework::get_global_ptr();
    GraphicsPipe* pipe = framework->get_default_pipe();

    if (LIKELY(pipe != nullptr)) {
        return pipe->get_display_information();
    }
    return static_cast<DisplayInformation*>(nullptr);
}

static void update_gpu_info(DisplayInformation* display_information) {
    gpu_info->update_data(display_information);
}

#if defined(IS_LINUX) || defined(IS_FREEBSD)

static void update_memory_info(DisplayInformation* display_information) {

    struct sysinfo meminfo;
    if (sysinfo(&meminfo) == 0) {
        display_information->_physical_memory = meminfo.totalram;
        display_information->_available_physical_memory = meminfo.freeram;
        display_information->_page_file_size = meminfo.totalswap;
        display_information->_available_page_file_size = meminfo.freeswap;
    }
    static std::ifstream memfile("/proc/self/statm");
    uint64_t vm_size, proc_size;

    memfile >> vm_size >> proc_size;
    memfile.clear();
    memfile.seekg(0);

    int page_size = getpagesize();
    display_information->_process_memory = proc_size * page_size;
    display_information->_process_virtual_memory = vm_size * page_size;
}
#endif


template<SystemRessources::RessourceType Id>
SystemRessources::RessourceData assign_info_impl(DisplayInformation* info) {
    if (auto ressource_getter = get_res<Id>()) {
        return (info->*ressource_getter)();
    }
    throw std::runtime_error("ressource getter not registered: " + std::to_string(Id));
}


template<size_t... Is>
INLINE SystemRessources::RessourceData assign_info(std::index_sequence<Is...>,
                                        int index, DisplayInformation* info) {

    using Handler = SystemRessources::RessourceData (*)(DisplayInformation*);
    constexpr Handler handlers[] = {
        &assign_info_impl<SystemRessources::RessourceType(Is)>...
    };
    if (LIKELY(index < sizeof...(Is))) {
        return handlers[index](info);
    }
    throw std::runtime_error("invalid ressource id: " + std::to_string(index));
}


SystemRessources::RessourceEntries
SystemRessources::querry_entries(RessourceTypesBS types, bool update) {
    DisplayInformation* info = get_informations();
    RessourceEntries entries;

    if (UNLIKELY(info == nullptr)) return entries;
    else if (update) update_ressources(info);

    entries.reserve(types.size());

    uint64_t bits = types.to_ulong();
    int index;
    while ((index = get_lowest_on_bit(bits)) != -1) {
        RessourceData data;
        try {
            data = assign_info(std::make_index_sequence<RT_COUNT>{}, index, info);
        } catch (const std::exception& error) {
            std::cerr << "error [querry_entries]: " << error.what() << "\n";
            continue;
        }
        RessourceType type = RessourceType(index);
        entries.emplace_back(RessourceEntry{type, std::move(data)});
        bits -= (1ull << index);
    }
    return entries;
}


void SystemRessources::update_ressources(DisplayInformation* info) {
    
    if (info != nullptr || (info = get_informations())) {

        size_t max_freq = 0;
        size_t sum_freq = 0;
        for (int procn = 0; procn < info->_num_logical_cpus; procn++) {
            info->update_cpu_frequency(procn);
            max_freq = std::max(max_freq, info->_maximum_cpu_frequency);
            sum_freq += info->_current_cpu_frequency;
        }
        info->_current_cpu_frequency = sum_freq /= info->_num_logical_cpus;
        info->_maximum_cpu_frequency = max_freq;
        
        info->update_gpu_information();
        info->update_memory_information();

#if defined(IS_LINUX) || defined(IS_FREEBSD)
        struct rusage usage;
        if (getrusage(RUSAGE_SELF, &usage)) {
            info->_page_fault_count = usage.ru_majflt;
        }
#endif
    }
}

SystemRessources::SystemRessources(uint16_t id) : ServiceBase(id) {
    
    if (DisplayInformation* info = get_informations()) {
#if defined(IS_LINUX) || defined(IS_FREEBSD)
        info->_update_cpu_frequency_function = update_cpu_frequency;
        info->_get_memory_information_function = update_memory_info;
        utsname buf;
        if (uname(&buf) == 0) {
            sscanf(buf.release, "%d.%d.%d", &info->_os_version_major,
                                            &info->_os_version_minor,
                                            &info->_os_version_build);
        }
        gpu_info.reset(create_gpu_monitor(info));
        if (gpu_info) {
            info->_update_gpu_information_function = update_gpu_info;
        }
#endif
    }
}























//  switch (RessourceType(index)) {
//             CASE_RES(RT_shader_model, shader_model())               
//             CASE_RES(RT_physical_memory, physical_memory())            
//             CASE_RES(RT_avail_physical_memory, physical_memory())       
//             CASE_RES(RT_page_file_size, page_file_size())
//             CASE_RES(RT_avail_page_file_size, available_page_file_size())
//             CASE_RES(RT_process_virtual_memory, process_virtual_memory())
//             CASE_RES(RT_page_fault_count, page_fault_count())
//             CASE_RES(RT_process_memory, process_memory())        
//             CASE_RES(RT_process_peak_memory, peak_process_memory())
//             CASE_RES(RT_page_file_usage, page_file_usage())
//             CASE_RES(RT_page_file_peak_usage, peak_page_file_usage())
//             CASE_RES(RT_memory_load, memory_load())
//             CASE_RES(RT_gpu_vendor_id, vendor_id())
//             CASE_RES(RT_gpu_device_id, device_id())
//             CASE_RES(RT_gpu_driver_product, driver_product())       
//             CASE_RES(RT_gpu_driver_version,driver_version())
//             CASE_RES(RT_gpu_driver_sub_version, driver_sub_version())
//             CASE_RES(RT_gpu_driver_build, driver_build())
//             CASE_RES(RT_gpu_video_memory, video_memory())
//             CASE_RES(RT_gpu_video_memory_usage, video_memory_usage())
//             CASE_RES(RT_gpu_graphics_load, graphics_load())
//             CASE_RES(RT_cpu_vendor_string, cpu_vendor_string())
//             CASE_RES(RT_cpu_brand_string, cpu_brand_string())
//             CASE_RES(RT_cpu_version_information, cpu_version_information())
//             CASE_RES(RT_cpu_brand_index, cpu_brand_index())
//             CASE_RES(RT_cpu_max_frequency, maximum_cpu_frequency())
//             CASE_RES(RT_cpu_cur_frequency, current_cpu_frequency())
//             CASE_RES(RT_cpu_num_cores, num_cpu_cores())
//             CASE_RES(RT_cpu_num_logical_cores, num_logical_cpus())
//             CASE_RES(RT_os_version_major, os_version_major())
//             CASE_RES(RT_os_version_minor, os_version_minor())
//             CASE_RES(RT_os_version_build, os_version_build())
//             default: continue;
//         }

// void generic_read_datagram( SystemRessources::RessourceEntry& entry,
//                             DatagramIterator& scan) {
    
//     entry.type = SystemRessources::RessourceType(scan.get_uint8());
//     std::visit([&scan](auto& data) {
//         generic_read_datagram(data, scan);
//     }, entry.data);
    // switch (SystemRessources::get_res_id(entry.type)) {
    //     case GET_RES_TYPEID(int32_t): entry.data = scan.get_int32)
    //     case GET_RES_TYPEID(uint32_t): entry.data = scan.get_uint32)
    //     case GET_RES_TYPEID(uint64_t): entry.data = scan.get_uint64)
    //     case GET_RES_TYPEID(std::string): entry.data = scan.get_string)
    // }
// }

// void generic_write_datagram(Datagram& datagram,
//                             const SystemRessources::RessourceEntry& entry) {
    
//     datagram.add_uint8(entry.type);
//     std::visit([&datagram](const auto& data) {
//         generic_write_datagram(datagram, data);
//     }, entry.data);

    // switch (SystemRessources::get_res_id(entry.type)) {
    //     case GET_RES_TYPEID(int32_t):
    //         datagram.add_int32(std::get<uint32_t>(entry.data)); break;
    //     case GET_RES_TYPEID(uint32_t):
    //         datagram.add_uint32(std::get<uint32_t>(entry.data)); break;
    //     case GET_RES_TYPEID(uint64_t):
    //         datagram.add_uint64(std::get<uint64_t>(entry.data)); break;
    //     case GET_RES_TYPEID(std::string):
    //         datagram.add_string(std::get<std::string>(entry.data)); break;
    // }
// }