// Copyright 2023 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __VIEWPORTSERVICE_H__
#define __VIEWPORTSERVICE_H__

#include <panda3d/drawMask.h>
#include <eventDispatcher.h>
#include "renderOverlayLayer.h"

#include "serialize_ops.h"
#include "serviceBase.h"

class NavigationCamera;
class RenderPipeline;
class RenderViewLayer;
class GraphicsWindow;
class NodePath;


class [[meta::service]] ViewportService: public ServiceBase {

    REGISTER_SERVICE

public:
    REGISTER_EVENT(active_changed, ViewportService*)
    REGISTER_EVENT(scene_changed, ViewportService*)

    enum ViewLayers { VL_output = 1, VL_overlay, VL_gui };
    
    ViewportService(uint16_t id);
    virtual ~ViewportService();

    [[meta::rpc]] bool load_model(const std::string& path);
    [[meta::rpc]] bool set_pipeline(const std::string& path);
    [[meta::rpc]] void enable_view_layer(uint32_t layer, bool enable);
    [[meta::rpc]] uint64_t create_window(uint64_t parent, int sx, int sy);
    [[meta::rpc]] void enable_overlay_component(
        RenderOverlayLayer::OverlayComponent component, bool enabled);

    [[meta::rpc]] void set_camera_mask(DrawMask mask);
    [[meta::rpc]] DrawMask get_camera_mask();

    GraphicsWindow* get_framebuffer() const;
    NavigationCamera* get_navigation_camera() const;
    RenderPipeline* get_render_pipeline() const;
    NodePath get_scene_nodepath() const;

    static ViewportService* get_active_viewport();
    static TypeHandle get_class_type() { return TypeHandle::none(); }

private:
    void set_root_node(PandaNode* node);
    void add_view_layer(RenderViewLayer* render_layer);

    class ViewportServicePrivate;
    class AsyncModelLoadRequest;

    dimpl<ViewportServicePrivate> _d;
};

#endif // __VIEWPORTSERVICE_H__