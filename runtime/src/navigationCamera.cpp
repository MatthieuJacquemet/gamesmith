// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/nodePath.h>
#include <panda3d/graphicsEngine.h>
#include <panda3d/keyboardButton.h>
#include <panda3d/mouseButton.h>
#include <panda3d/asyncTaskManager.h>
#include <panda3d/mouseAndKeyboard.h>
#include <panda3d/renderBuffer.h>
#include <panda3d/orthographicLens.h>
#include <panda3d/buttonEvent.h>

#include <renderPipeline.h>
#include <renderTarget.h>
#include <scopedReferenceCount.h>

#include "viewportService.h"
#include "keyboardAndMouseHandler.h"
#include "navigationCamera.h"
#include "raycast.h"



DEFINE_TYPEHANDLE(NavigationCamera)



NavigationCamera::NavigationCamera(): Camera("navigation-camera"),
    _hit_point(0.0f, 10.0f, 0.0f) {

}



void NavigationCamera::handle_button_event(const ButtonEvent& event) {

    if (event.get_button() == MouseButton::wheel_up() &&
        event.get_type() == ButtonEvent::Type::T_down) {
        update_hit_point(); do_travelling( 0.8);
    }
    else if (event.get_button() == MouseButton::wheel_down() &&
            event.get_type() == ButtonEvent::Type::T_down) {
        update_hit_point(); do_travelling(-0.8);
    }
    else if (event.get_button() == MouseButton::two()) {
        update_hit_point();
        _prev_mouse.reset();
        if (event.get_type() == ButtonEvent::Type::T_up) {
            _handler = nullptr;
        } else if (is_button_pressed(KeyboardButton::lcontrol())) {
            _handler = &NavigationCamera::do_travelling_mouse;
        } else if (is_button_pressed(KeyboardButton::shift())) {
            _handler = &NavigationCamera::update_pan;
        } else {
            _handler = &NavigationCamera::update_orbit;
        }
        grab_inputs(_handler != nullptr);
    } 
    else if (event.get_button() == KeyboardButton::ascii_key('5') &&
            event.get_type() == ButtonEvent::Type::T_down) {
        toggle_perspective_orthographic();
    }
}

void NavigationCamera::handle_mouse_event(const LVector2& event) {
    if (_handler) (this->*_handler)(event);
}

GraphicsWindow* NavigationCamera::get_window() {
    if (auto* viewport = ViewportService::get_active_viewport()) {
        return viewport->get_framebuffer();
    } else {
        return nullptr;
    }
}

const LVecBase3f& NavigationCamera::update_hit_point() {
    GraphicsWindow* window = get_window();

    if (UNLIKELY(window == nullptr)) return _hit_point;
    
    if (UNLIKELY(!window->get_num_input_devices())) {
        return _hit_point;
    }
    MouseData data = window->get_pointer(0);
    NodePath np(this);
    NodePath root = np.get_top(Thread::get_current_thread());

    if (!data.get_in_window()) return _hit_point;

    LPoint3 pivot_point(get_mouse_position(), -1.0f);
    const Lens* lens = get_lens();

    int y_size = window->get_fb_y_size();
    LPoint3 near, far;
    if (lens->extrude(pivot_point.get_xy(), near, far)) {
        near = root.get_relative_point(np, near);
        far  = root.get_relative_point(np, far);

        if (auto result = raycast(near, far, root)) {
            _hit_point = np.get_relative_point(root, result->hit_point);
        }
    }
    if (lens->is_orthographic()) {
        _hit_point += recompute_orthographic_position(lens);
    }
    return _hit_point;
}


LPoint3 NavigationCamera::compute_mouse_to_view(const LPoint3f& point) const {
    
    const Lens* lens = get_lens();

    LPoint3 final_point;
    if (!lens->extrude_depth(std::move(point), final_point)) {
        return LPoint3(0.0f, 0.0f, 0.0f);
    }
    if (IS_THRESHOLD_EQUAL(final_point.get_y(), lens->get_near(), 1.e-5f)) {

        float correction = _hit_point.get_y() / lens->get_near();
        final_point.set_y(_hit_point.get_y());
        
        if (lens->is_perspective()) {
            final_point.set_x(final_point.get_x() * correction);
            final_point.set_z(final_point.get_z() * correction);
        }
    }
    return final_point;
}

void NavigationCamera::toggle_perspective_orthographic() {

    PointerTo<Lens> new_lens, old_lens = get_lens();

    if (old_lens->is_perspective()) {
        new_lens = new OrthographicLens;
        
        float factor = _hit_point.get_y() / old_lens->get_focal_length();
        new_lens->set_focal_length(1.0f);
        new_lens->set_film_size(old_lens->get_film_size() * factor);
        _hit_point += recompute_orthographic_position(old_lens);
    } else {
        new_lens = new PerspectiveLens;
    
        new_lens->set_fov(60.0f);
        _hit_point += recompute_perspective_position(new_lens);
    }
    new_lens->set_near_far(old_lens->get_near(), old_lens->get_far());
    new_lens->set_aspect_ratio(old_lens->get_aspect_ratio());
    set_lens(new_lens);
}


LVector3 NavigationCamera::recompute_orthographic_position(const Lens* old_lens) {
    
    Thread* current_thread = Thread::get_current_thread();
    NodePath np(this, current_thread);
    NodePath root_np = np.get_top(current_thread);

    float range = old_lens->get_far() - old_lens->get_near();
    float center = range * 0.5f + old_lens->get_near();
    float offset = center - _hit_point.get_y();

    LVector3 vector(0, offset, 0);
    np.set_pos(np.get_pos() - root_np.get_relative_vector(np, vector));
    
    return vector;
}


LVector3 NavigationCamera::recompute_perspective_position(const Lens* new_lens) {
    
    Thread* current_thread = Thread::get_current_thread();
    NodePath np(this, current_thread);
    NodePath root_np = np.get_top(current_thread);
    const Lens* old_lens = get_lens();

    float a = old_lens->get_film_size().get_x();
    float b = new_lens->get_film_size().get_x() / new_lens->get_focal_length();
    float offset =  a / b - _hit_point.get_y();

    LVector3 vector(0, offset, 0);
    np.set_pos(np.get_pos() - root_np.get_relative_vector(np, vector));

    return vector;
}


void NavigationCamera::do_travelling(float alpha) {

    Thread* current_thread = Thread::get_current_thread();
    NodePath np(this, current_thread);
    NodePath root_np = np.get_top(current_thread);
    Lens* lens = get_lens();

    float factor = alpha < 0.0f ? 1.f / -alpha : alpha;
    LVector3 hit_point = _hit_point * (-factor + 1.0f);

    if (lens->is_orthographic()) {
        hit_point.set(hit_point[0], 0.0f, hit_point[2]);
        lens->set_film_size(lens->get_film_size() * factor);
    }
    LVector3 offset = root_np.get_relative_vector(np, hit_point);
    np.set_pos(np.get_pos(root_np) + offset);
}


void NavigationCamera::update_pan(const LVector2& coords) {

    LVecBase2f delta = compute_mouse_move_delta();
    LVector3f vector = compute_mouse_to_view({delta, -1.0f});

    const Lens* lens = get_lens();
    
    if (lens->is_perspective()) {
        vector *= _hit_point.get_y() / vector.get_y();
    }
    vector.set_y(0.0f);

    NodePath np = NodePath::any_path(this);
    NodePath root_np = np.get_top(Thread::get_current_thread());

    LVector3 offset = root_np.get_relative_vector(np, vector);
    np.set_pos(np.get_pos(root_np) - offset);
}


void NavigationCamera::do_travelling_mouse(const LVector2& coords) {

    LVecBase2f delta = compute_mouse_move_delta();
    do_travelling((delta[1] < 0.f ? -1.f : 1.f) / (std::abs(delta[1]) + 1.f));
}


void NavigationCamera::update_orbit(const LVector2& coords) {

    LVecBase2f delta = compute_mouse_move_delta(false) * -0.4f;
    NodePath np = NodePath::any_path(this);
    NodePath root_np = np.get_top(Thread::get_current_thread());

    CPT(TransformState) transform = np.get_net_transform();
    float heading = transform->get_hpr().get_x();

    if (transform->get_mat().get_row(2).get_z() < 0.0f) {
        heading += 180.0f; // flipped
    }
    CPT(TransformState) pivot_tr = TransformState::make_pos_hpr(
        root_np.get_relative_point(np, _hit_point),
        LVecBase3(heading, 0.f, 0.f)
    );
    CPT(TransformState) rot_tr = TransformState::make_hpr(
        LVecBase3(delta.get_x(), delta.get_y(), 0.0f)
    );
    transform = pivot_tr->invert_compose(transform); // cam to pivot
    transform = rot_tr->compose(transform); // apply offset
    transform = pivot_tr->compose(transform); // pivot to cam

    np.set_transform(np.get_top(), transform);
}


inline int wrap_around(int value, int max) {
    return (value + max) % max;
}

LVecBase2f NavigationCamera::compute_mouse_move_delta(bool normalized) {

    if (GraphicsWindow* window = get_window()) {

        MouseData data = window->get_pointer(0);
        LVector2i coord(data._xpos, data._ypos);
    
        if (UNLIKELY(!_prev_mouse.has_value())) {
            _prev_mouse = coord;
        }
        LVecBase2i delta = coord - _prev_mouse.value();

        coord[0] = wrap_around(coord[0], window->get_x_size());
        coord[1] = wrap_around(coord[1], window->get_y_size());
        _prev_mouse = coord;

        if (data._xpos != coord[0] || data._ypos != coord[1]) {
            WindowProperties props;
            props.set_foreground(true);
            window->set_properties_now(props);
            if (!window->move_pointer(0, coord[0], coord[1])) {
                _prev_mouse = LVector2i(data._xpos, data._ypos);
            }
        }
        LVecBase2f result(delta.get_x(), delta.get_y());
        if (normalized) {
            result[0] = result[0] *  2.f / window->get_x_size();
            result[1] = result[1] * -2.f / window->get_y_size();
        }
        return result;
    } else {
        return LVecBase2f(0.0f, 0.0f);
    }
}




