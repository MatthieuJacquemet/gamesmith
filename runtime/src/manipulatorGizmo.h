// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MANIPULATORGIZMO_H__
#define __MANIPULATORGIZMO_H__

#include <optional>
#include <functional>

#include <panda3d/transformState.h>
#include <panda3d/lens.h>
#include <shaderState.h>
#include <color.h>

#include "inputEventListener.h"
#include "operation.h"

typedef std::tuple<CPT(TransformState), Color> AxisData;
typedef std::function<AxisData(uint32_t)> AxisDataGetter;

class SceneData;
class Color;
class ViewportService;

template<class... T> class ProductIterator;
template<class T> class RangeIterator;


void draw_instances(const SceneData* scene_data, CPT(Geom) geom,
                    const RenderState* render_state,
                    const AxisDataGetter& data_getter);


LPoint4 hit_axis_handle(const LPoint3& ray_start, const LPoint3& ray_end,
                        const LMatrix4& mat, int axis, float min, float max);


float get_facing_factor(const SceneData* scene_data, 
                        const TransformState* transform);


class ManipulatorGizmo : public InputEventListener {

public:
    typedef std::function<void (const TransformState*)> TransformCallback;

    ManipulatorGizmo(int priority = 0);
    virtual ~ManipulatorGizmo() = default;

    enum Mode { M_none = -1, M_translate, M_rotate, M_scale, M_unified };
    
    void draw(SceneData* scene_data);

    inline void set_operator(IOperation* operation) {
        _operation = operation;
    }
    inline void set_transform(const TransformState* transform) {
        _transform = transform;
    }
    inline const TransformState* get_transform() const {
        return _transform;
    }
    inline const TransformState* get_grab_transform() const {
        return _grab_data ? _grab_data->transform : nullptr;
    }
    inline void set_mode(Mode mode) { _mode = mode; }
    inline const Mode get_mode() const { return _mode; }

    template<Mode = M_none> class TransformMode {
    public:
        static LPoint4 hit_test(const ManipulatorGizmo* gizmo,
                                const LMatrix4& mat,
                                const LPoint3& ray_start,
                                const LPoint3& ray_end, int axis) {
            return LPoint4::zero();
        }
        static void draw_gizmo( const ManipulatorGizmo* gizmo,
                                const SceneData* scene_data,
                                CPT(TransformState) transforms[],
                                const Color colors[]) {}
    };
    template<class R, Mode mode, class Callback>
    inline static R call_transform_mode(Callback&& callback) {
        return callback(TransformMode<mode>());
    }
    inline void reset_state() noexcept {
        _grab_data.reset(); _hovered_mode = M_none; _hovered_axis = 0;
        grab_inputs(false);
    }
protected:
    typedef ProductIterator<RangeIterator<Mode>,
                RangeIterator<uint32_t>> AxisIterator;

    void viewport_changed(ViewportService* old_viewport) override;
    void handle_button_event(const ButtonEvent& event) override;
    void handle_mouse_event(const LVector2& event) override;

    RangeIterator<Mode> for_each_mode_on() const;
    AxisIterator for_each_axis_on() const;
private:
    std::tuple<LMatrix4, Lens*, PN_float32, LVector2> initialize_data() const;
    struct HitData {
        LPoint4 point; Mode mode; uint32_t axis; PN_float32 scale;
        LMatrix4 trans; CPT(TransformState) transform; 
    };
    std::optional<HitData> hit_test(const LPoint2& mouse) const;

    std::optional<HitData> _grab_data;
    Mode _mode = Mode::M_translate, _hovered_mode = Mode::M_none;
    uint32_t _hovered_axis = 0;
    IOperation* _operation;

    CPT(TransformState) _transform = TransformState::make_identity();
    ShaderState _base_state, _line_state, _rotate_state, _axis_state;
    PTA_float _thickness;
};

#endif // __MANIPULATORGIZMO_H__