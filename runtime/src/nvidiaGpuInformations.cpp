// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifdef HAVE_NVML

#include <iostream>

#include "utils.h"
#include "nvidiaGpuInformations.h"


NvidiaGpuInformations::NvidiaGpuInformations(): _memory_info{0} {
    unsigned int device_count = 0;

    if (!check(nvmlInit(), "Failed to initialize NVML")) {
        
    } else if (!check(nvmlDeviceGetCount(&device_count),
                "Failed to get number of gpu devices")) {
        
    } else if (UNLIKELY(device_count == 0)) {
        std::cout << "No gpu devices found" << std::endl;
    } 
    else if (!check(nvmlDeviceGetHandleByIndex(0, &_device_handle),
                    "Failed to get gpu device handle")) {
        
    } else if (!check(nvmlDeviceGetMemoryInfo(_device_handle, &_memory_info),
                    "Failed to get gpu device memory info")) {
        
    }
}

NvidiaGpuInformations::~NvidiaGpuInformations() {
    if (_device_handle != nullptr) {
        check(nvmlShutdown(), "Failed to shutdown NVML");
    }
}

void NvidiaGpuInformations::update_data(DisplayInformation* display_info) {
    
#ifdef _WIN32
    unsigned int pid = GetCurrentProcessId();
#else
    unsigned int pid = getpid();
#endif
    if (UNLIKELY(_device_handle == nullptr)) {
        // nothing to do, the handle is invlid
    } else if (!nvmlDeviceGetAccountingStats(_device_handle, pid, &_stats)) {;

        display_info->_video_memory = _memory_info.total;

        display_info->_video_memory_usage = 
            _stats.maxMemoryUsage != NVML_VALUE_NOT_AVAILABLE ?
            _stats.maxMemoryUsage : -1;

        display_info->_graphics_load = 
            _stats.gpuUtilization != NVML_VALUE_NOT_AVAILABLE ?
            _stats.gpuUtilization : -1;
        
        return;
    }
    GpuInformations::update_data(display_info);
}

bool NvidiaGpuInformations::check(nvmlReturn_t value, const std::string& msg) {
    if (value == NVML_SUCCESS) {
        return true;
    }
    std::cout << msg << " " << nvmlErrorString(value) << std::endl;
    return false;
}

#endif // HAVE_NVML