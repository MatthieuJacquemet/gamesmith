// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __RENDEROVERLAYLAYER_H__
#define __RENDEROVERLAYLAYER_H__

#include <panda3d/configVariableColor.h>
#include <panda3d/callbackObject.h>
#include <bitset>
#include <array>

#include "meta.h"
#include "renderViewLayer.h"

class GraphicsOutput;
class RenderContext;
class Geom;
class RenderState;

extern ConfigVariableColor x_axis_color;
extern ConfigVariableColor y_axis_color;
extern ConfigVariableColor z_axis_color;


class RenderOverlayLayer : public RenderViewLayer {

    REGISTER_TYPE("RenderOverlayLayer", RenderViewLayer);

public:
    RenderOverlayLayer( ViewportService* viewport_service,
                        const std::string& name = "overlay");
    ~RenderOverlayLayer() = default;

    virtual void draw_callback() override;
    void setup_pipeline(RenderPipeline* pipeline) override;

    enum OverlayComponent {
        OC_floor_grid, OC_axes, OC_active_tool,
        OC_tight_bv, OC_bounds, OC_sel_outline,
        OC_visual_guides, OC_parent_relationship,
        OC_profiling_stats, OC_COUNT
    };
    void set_enabled(OverlayComponent type, bool enabled);
    bool is_enabled(OverlayComponent type) const;

    static std::string component_name(OverlayComponent type);

    static const Geom* get_line_geom();
    static const RenderState* get_line_state();

    struct Component {
        inline Component(RenderOverlayLayer* drawer):
            _drawer(drawer) {}
        virtual ~Component() = default;
        virtual void draw() = 0;
        virtual void setup_pipeline(RenderPipeline* pipeline);
    protected:
        RenderOverlayLayer* const _drawer;
    };
protected:
    std::array<std::unique_ptr<Component>, OC_COUNT> _comps;
};


inline std::ostream& operator <<(std::ostream& os,
                    RenderOverlayLayer::OverlayComponent type) {
    return os << RenderOverlayLayer::component_name(type);
}

template<RenderOverlayLayer::OverlayComponent T>
RenderOverlayLayer::Component* 
create_component(RenderOverlayLayer* drawer) {
    std::cout   << "create_component not implemented for "
                << T << std::endl;
    return nullptr;
}

#define REGISTER_OVERLAY_COMPONENT(TYPE, CLASS)         \
template<> RenderOverlayLayer::Component*               \
create_component<TYPE>(RenderOverlayLayer* drawer) {    \
    return new CLASS(drawer);                           \
}


#endif // __RENDEROVERLAYLAYER_H__