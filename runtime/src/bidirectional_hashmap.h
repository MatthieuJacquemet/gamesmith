// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __BIDIRECTIONAL_HASHMAP_H__
#define __BIDIRECTIONAL_HASHMAP_H__

#include "unordered_map"
#include <pnotify.h>


template <typename Key, typename Value>
class BidirectionalHashMap {

public:
    inline const Value& get_value_ref(const Key& key) const {
        return _forward_map.at(key);
    }
    inline const Key& get_key_ref(const Value& value) const {
        return _reverse_map.at(value);
    }
    Value get_value(const Key& value, const Value& def = Value()) const {
        auto it = _forward_map.find(value);
        return it == _forward_map.end() ? def : it->second;
    }
    Key get_key(const Value& value, const Key& def = Key()) const {
        auto it = _reverse_map.find(value);
        return it == _reverse_map.end() ? def : it->second;
    }

    inline bool contains_key(const Key& key) const {
        return _forward_map.count(key) == 1;
    }
    inline bool contains_value(const Value& value) const {
        return _reverse_map.count(value) == 1;
    }

    inline bool insert(const Key& key, const Value& value) {
        auto it0 = _forward_map.insert(std::make_pair(key, value));
        if (!it0.second) return false;
        auto it1 = _reverse_map.insert(std::make_pair(value, key));
        nassertr(it1.second, false);
        return true;
    }
    inline Value erase(const Key& key, const Value& value) {
        auto it = _forward_map.find(key);
        Value stored_value = value;
        if (it != _forward_map.end()) {
            stored_value = it->second;
            _reverse_map.erase(it->second), _forward_map.erase(it);
        }
        return stored_value;
    }
    inline Key erase(const Value& value, const Key& key = Key()) {
        auto it = _reverse_map.find(value);
        Key stored_key = key;
        if (it != _reverse_map.end()) {
            stored_key = it->second;
            _forward_map.erase(it->second), _reverse_map.erase(it);
        }
        return stored_key;
    }
private:
    std::unordered_map<Key, Value> _forward_map;
    std::unordered_map<Value, Key> _reverse_map;
};

#endif // __BIDIRECTIONAL_HASHMAP_H__