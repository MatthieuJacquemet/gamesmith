// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __RESSOURCE_TYPES_H__
#define __RESSOURCE_TYPES_H__

#include <panda3d/displayInformation.h>
#include "systemRessourceEnums.h"


template<SystemRessourceEnums::RessourceType T>
constexpr auto get_res() { return nullptr; }
    
#define BIND_RES(ID, METHOD) template<> \
constexpr auto get_res<SystemRessourceEnums::ID>() { \
    return &DisplayInformation::METHOD; } 


BIND_RES(RT_shader_model, get_shader_model)
BIND_RES(RT_physical_memory, get_physical_memory)         
BIND_RES(RT_avail_physical_memory, get_physical_memory)
BIND_RES(RT_page_file_size, get_page_file_size)
BIND_RES(RT_avail_page_file_size, get_available_page_file_size)
BIND_RES(RT_process_virtual_memory, get_process_virtual_memory)
BIND_RES(RT_page_fault_count, get_page_fault_count)
BIND_RES(RT_process_memory, get_process_memory)       
BIND_RES(RT_process_peak_memory, get_peak_process_memory)
BIND_RES(RT_page_file_usage, get_page_file_usage)
BIND_RES(RT_page_file_peak_usage, get_peak_page_file_usage)
BIND_RES(RT_memory_load, get_memory_load)
BIND_RES(RT_gpu_vendor_id, get_vendor_id)
BIND_RES(RT_gpu_device_id, get_device_id)
BIND_RES(RT_gpu_driver_product, get_driver_product)  
BIND_RES(RT_gpu_driver_version, get_driver_version)
BIND_RES(RT_gpu_driver_sub_version, get_driver_sub_version)
BIND_RES(RT_gpu_driver_build, get_driver_build)
BIND_RES(RT_gpu_video_memory, get_video_memory)
BIND_RES(RT_gpu_video_memory_usage, get_video_memory_usage)
BIND_RES(RT_gpu_graphics_load, get_graphics_load)
BIND_RES(RT_cpu_vendor_string, get_cpu_vendor_string)
BIND_RES(RT_cpu_brand_string, get_cpu_brand_string)
BIND_RES(RT_cpu_version_information, get_cpu_version_information)
BIND_RES(RT_cpu_brand_index, get_cpu_brand_index)
BIND_RES(RT_cpu_max_frequency, get_maximum_cpu_frequency)
BIND_RES(RT_cpu_cur_frequency, get_current_cpu_frequency)
BIND_RES(RT_cpu_num_cores, get_num_cpu_cores)
BIND_RES(RT_cpu_num_logical_cores, get_num_logical_cpus)
BIND_RES(RT_os_version_major, get_os_version_major)
BIND_RES(RT_os_version_minor, get_os_version_minor)
BIND_RES(RT_os_version_build, get_os_version_build)


#endif // __RESSOURCE_TYPES_H__