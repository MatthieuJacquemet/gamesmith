// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __CUSTOM_TYPES_H__
#define __CUSTOM_TYPES_H__

#include <panda3d/luse.h>
#include <types_ext.h>

REGISTER_TYPE_HANDLE(std::vector<LVecBase3f>)
REGISTER_TYPE_HANDLE(std::vector<uint64_t>)

#endif // __CUSTOM_TYPES_H__