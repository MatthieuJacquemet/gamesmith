// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __EVENTLISTENER_H__
#define __EVENTLISTENER_H__

#include <functional>
#include <map>
#include <string>
#include <eventDispatcher.h>
#include <keyboardAndMouseHandler.h>
#include "viewportService.h"


template<class Dispatcher> class EventListener {

    static_assert(std::is_base_of_v<EventDispatcher, Dispatcher>);

    using Pointer = std::conditional_t<
        std::is_base_of_v<ReferenceCount, Dispatcher>,
        PointerTo<Dispatcher>, Dispatcher*>;

public:
    inline EventListener(Dispatcher* dispatcher):
        _dispatcher(dispatcher) {}
    
    virtual ~EventListener() { clear(); }

    void clear() {
        for (const auto& [name, handler_token] : _handlers) {
            _dispatcher->remove_listener(name, handler_token);
        }
    }
    inline void remove_handler(const std::string& name) {
        auto it = _handlers.find(name);
        if (LIKELY(it != _handlers.end())) {
            _dispatcher->remove_listener(it->first, it->second);
            _handlers.erase(it);
        }
    }
    template<class MethodType, class Object, class... Args>
    void add_handler(const RegisterEvent<Args...>& event,
                    MethodType Object::*function) {
        
        Object* object = static_cast<Object*>(this);
        _handlers.insert(std::make_pair(event.get_name(),
            _dispatcher->add_listener(event, function, object)
        ));
    }
    template<class Callback, class... Args>
    void add_handler(const RegisterEvent<Args...>& event,
                    const Callback& callback) {
        static_assert(std::is_invocable_v<Callback, Args...>);
    
        _handlers.insert(std::make_pair(event.get_name(),
            _dispatcher->add_listener(event, callback)
        ));
    }
protected:
    Pointer _dispatcher;
    std::map<std::string, size_t> _handlers;
};


class InputListener : public EventListener<KeyboardAndMouseHandler> {
public:
    inline InputListener(KeyboardAndMouseHandler* handler):
        EventListener<KeyboardAndMouseHandler>(handler) {}

    virtual ~InputListener() = default;

protected:
    GraphicsWindow* get_window(Thread* current_thread) const;
};


#endif // __EVENTLISTENER_H__