// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.


#include <panda3d/mouseButton.h>
#include <panda3d/graphicsWindow.h>
#include <panda3d/depthTestAttrib.h>
#include <panda3d/depthWriteAttrib.h>
#include <panda3d/clipPlaneAttrib.h>
#include <panda3d/antialiasAttrib.h>
#include <panda3d/shaderAttrib.h>
#include <panda3d/shader.h>
#include <panda3d/rescaleNormalAttrib.h>
#include <panda3d/geomVertexRewriter.h>
#include <panda3d/planeNode.h>

#include <shaderState.h>
#include <render_utils.h>
#include <cone.h>
#include <renderPipeline.h>
#include <color.h>
#include <sceneData.h>
#include <math_utils.h>

#include "viewportService.h"
#include "objectRequestBroker.h"
#include "renderOverlayLayer.h"
#include "runtime_utils.h"
#include "navigationCamera.h"
#include "manipulatorGizmo.h"


ConfigVariableDouble grab_threshold(
    "manipulator-grab-threshold", 10.0,
    PRC_DESC("Maximum distance in pixels between the mouse cursor and the"
            "manipilator grizmo to consider it a grab")
);

ConfigVariableDouble manipulator_scale(
    "manipulator-scale", 80.0,
    PRC_DESC("Size of the manipulator gizmo in pixel")
);

ConfigVariableDouble gizmo_line_thickness(
    "gizmo-line-thickness", 2.0,
    PRC_DESC("Thickness of the gizmo lines in pixels")
);


static CPT_InternalName axis_color_name = "axis_color";
static CPT_InternalName instance_matrix = "instance_matrix";


CPT(GeomVertexArrayFormat) make_instance_format() {

    PT(GeomVertexArrayFormat) new_format = new GeomVertexArrayFormat(
        axis_color_name, 4, GeomEnums::NT_stdfloat, GeomEnums::C_color,
        instance_matrix, 4, GeomEnums::NT_stdfloat, GeomEnums::C_matrix
    );
    new_format->set_divisor(1);
    return GeomVertexArrayFormat::register_format(new_format);
}

PT(GeomVertexArrayData) make_instance_array_data() {
    static CPT(GeomVertexArrayFormat) format = make_instance_format();
    return new GeomVertexArrayData(format, GeomEnums::UH_stream);
}

static GeomVertexArrayData* get_instances_array() {
    static PT(GeomVertexArrayData) data = make_instance_array_data();
    return data;
}


namespace detail {

    template<class Callback, size_t... Is>
    auto call_mode(std::index_sequence<Is...>, Callback&& callback,
                ManipulatorGizmo::Mode mode) {
        
        using Mode = ManipulatorGizmo::Mode;
        using DefMode = typename ManipulatorGizmo::TransformMode<>;
        using RetType = decltype(callback(DefMode()));
    
        constexpr RetType (*callers[])(Callback&&) = {
            ManipulatorGizmo::call_transform_mode<RetType, Mode(Is)>...
        };
        return callers[mode](callback);
    }
}


template<class Callback>
auto call_mode(ManipulatorGizmo::Mode mode, Callback&& callback) {
    constexpr size_t N = size_t(ManipulatorGizmo::M_unified) + 1;
    return detail::call_mode(std::make_index_sequence<N>{}, callback, mode);
}


inline float vector_angle(const LVector3& v1, const LVector3& v2, const LVector3& n) {
    float theta = rad_2_deg(std::acos(dot(normalize(v1), normalize(v2))));
    return dot(n, v1.cross(v2)) < 0.0 ? -theta : theta;
}


float get_facing_factor(const SceneData* scene_data,
                        const TransformState* transform) {
    
    LVector3f axis = normalize(transform->get_mat().get_row3(2));
    LVector3f direction;
    if (scene_data->get_lens()->is_perspective()) {
        direction = normalize(transform->get_pos());
    } else {
        direction = LVector3::forward();
    }
    return inv_lerp(0.0f, 0.1f, 1.0f - std::abs(axis.dot(direction)));
}



void draw_instances(const SceneData* scene_data, CPT(Geom) geom,
                    const RenderState* render_state,
                    const AxisDataGetter& data_getter) {

    Thread* current_thread = Thread::get_current_thread();

    GraphicsStateGuardian* gsg = scene_data->get_gsg();
    GeomVertexArrayData* array = get_instances_array();

    CPT(TransformState) cs_trans = scene_data->get_cs_transform();
    CPT(TransformState) transform = TransformState::make_identity();

    gsg->set_state_and_transform(render_state, transform);

	CPT(GeomVertexData) data = geom->get_vertex_data();
	auto munger = gsg->get_geom_munger(render_state, current_thread);

    PT(GeomVertexData) new_data = new GeomVertexData(*data);
    new_data->insert_array(new_data->get_num_arrays(), array);

    Color colors[3];

    GeomVertexWriter writer(new_data, current_thread);
    writer.set_column(instance_matrix);
    for (uint32_t axis = 0; axis < 3; ++axis) {
        std::tie(transform, colors[axis]) = data_getter(axis);
        writer.add_matrix4f(cs_trans->compose(transform)->get_mat());
    }
    writer.set_column(axis_color_name);
    for (uint32_t axis = 0; axis < 3; ++axis) {
        writer.add_data4f(colors[axis]);
    }
    bool force = !gsg->get_effective_incomplete_render();
    data.swap(new_data);
    if (munger->munge_geom(geom, data, force, current_thread)) {
	    geom->draw(gsg, data, 3, force, current_thread);
    }
}




LPoint4 hit_axis_handle(const LPoint3& ray_start, const LPoint3& ray_end,
                        const LMatrix4& mat, int axis, float min, float max) {
    
    LPoint3 axis_vec = mat.get_row3(axis); // the axis's vector
    LPoint3 position = mat.get_row3(3); // the center of the gozmo

    LVector3 d2 = ray_end - ray_start;
    LVector3 n2 = d2.cross(axis_vec.cross(d2));
    float t = dot(ray_start - position, n2) / dot(axis_vec, n2);

    const LVecBase3f& zero = LVecBase3f::zero();
    return LPoint4(position + axis_vec * std::clamp(t, min ,max), t);
}








ManipulatorGizmo::ManipulatorGizmo(int priority):
    InputEventListener(priority), _thickness(1, 1.0f) {

    get_instances_array()->unclean_set_num_rows(3);

    PT(Shader) base_shader = Shader::load(Shader::SL_GLSL,
        "/data/shader/manipulator_gizmo.vert",
        "/data/shader/manipulator_gizmo.frag"
    );
    PT(Shader) line_shader = Shader::load(Shader::SL_GLSL,
        "/data/shader/manipulator_gizmo.vert",
        "/data/shader/manipulator_gizmo.frag",
        "/data/shader/thick_line.geom"
    );
    _base_state.set_shader(base_shader);
    _base_state.set_depth_test(false);
    _base_state.set_depth_write(false);
    _base_state.set_transparency(TransparencyAttrib::M_alpha);
    _base_state.set_antialias(  AntialiasAttrib::M_multisample |
                                AntialiasAttrib::M_better);

    _line_state.compose(_base_state);
    _line_state.set_shader(line_shader);
    _line_state.set_shader_input("u_thickness", _thickness);

    NodePath np(new PlaneNode("rotate-clip", LPlane(LVector3::back(),
                                                    LPoint3::origin())));

    _rotate_state.compose(_line_state);
    _rotate_state.add_on_clip_plane(std::move(np));
    
    _axis_state.compose(RenderOverlayLayer::get_line_state());
    _axis_state.set_depth_test(false);
}


void ManipulatorGizmo::viewport_changed(ViewportService* old_viewport) {
    reset_state();
}

void ManipulatorGizmo::handle_button_event(const ButtonEvent& event) {

    if (event.get_button() == MouseButton::one()) {

        if (event.get_type() == ButtonEvent::T_down) {
            _grab_data = hit_test(get_mouse_position());
            grab_inputs(_grab_data.has_value());
        }
        else if (event.get_type() == ButtonEvent::T_up) {
            if (_grab_data.has_value() && _operation) {
                _operation->confirm();
            }
            reset_state();
        }
    } else if ( event.get_button() == MouseButton::three() &&
                event.get_type() == ButtonEvent::T_down) {
        if (_grab_data.has_value() && _operation) {
            _operation->cancel();
        }
        reset_state();
    }
}


void ManipulatorGizmo::handle_mouse_event(const LVector2& mouse_pos) {

    std::optional<HitData> hit_data = hit_test(mouse_pos);

    _hovered_mode = hit_data ? hit_data->mode : M_none;
    _hovered_axis = hit_data ? hit_data->axis : 0;

    if (!_grab_data || !hit_data || hit_data->point.is_nan()) {
        return;
    }
    auto& [hit0, mode, axis, scale, trans, _] = _grab_data.value();
    const LPoint4& hit = hit_data->point;
    
    LVector3 prs[3] = {{0.f}, {0.f}, {1.f}};
    float& data = prs[mode][mode == M_rotate ? (axis + 4) % 3 : axis];
    switch (mode) {
        case Mode::M_translate:
            data = (hit.get_w() - hit0.get_w()) * scale; break;
        case Mode::M_scale:
            data = hit.get_w() / hit0.get_w(); break;
        case Mode::M_rotate:
            data = vector_angle(hit0.get_xyz() - trans.get_row3(3),
                                hit.get_xyz(), trans.get_row3(axis));
        default: break;
    }
    if (LIKELY(!std::isnan(data))) {
        _grab_data->transform = TransformState::make_pos_hpr_scale(
            prs[0], prs[1], prs[2]
        );
        if (_operation) _operation->update();
    }
}




void ManipulatorGizmo::draw(SceneData* scene_data) {

    CPT(TransformState) transform = _transform;
    if (_grab_data.has_value()) {
        transform = transform->compose(_grab_data->transform);
    }
    transform = scene_data->get_world_transform()->compose(transform);

    const Lens* lens = scene_data->get_lens();
    const LVector2i& res = scene_data->_resolution[0];

    LVecBase2 size = lens->get_film_size() / lens->get_focal_length();
    float dist_scale = manipulator_scale * size[0] / float(res[0]);

    if (lens->is_perspective()) {
        dist_scale *= transform->get_pos().get_y();
    }
    transform = transform->set_scale(dist_scale);
    // transform = context.get_cs_transform()->compose(transform);

    _thickness.set_element(0, gizmo_line_thickness);
    // assuming the base geometry axis is for the z axis,
    // generate the transform for the x and y axis
    CPT(TransformState) transforms[3] = {
        transform->compose(TransformState::make_hpr({0.f, 0.0f, 90.f})),
        transform->compose(TransformState::make_hpr({0.f, -90.f, 0.f})),
        transform,
    };
    Color colors[3] = { x_axis_color.get_value(),
                        y_axis_color.get_value(),
                        z_axis_color.get_value() };

    _line_state.set_shader_input("u_view_scale", scene_data->_resolution);
    _rotate_state.set_shader_input("u_view_scale", scene_data->_resolution);

    if (_grab_data.has_value()) {
        // TODO: determine correct scale according to lens
        auto trans = transforms[_grab_data->axis]->set_scale(10000.0f);
        _axis_state.set_color_scale(colors[_grab_data->axis]);
        _axis_state.set_shader_input("u_near_far", scene_data->_near_far);
        
        draw_geom(  scene_data->get_gsg(), RenderOverlayLayer::get_line_geom(),
                    _axis_state, scene_data->get_cs_transform()->compose(trans));
    }
    auto for_each_mode_on_range = for_each_mode_on();
    // rotation operation only draw the rotation gizmo
    if (_grab_data && _grab_data->mode == M_rotate) {
        // TODO: draw spetial disc gizmo
        for_each_mode_on_range = RangeIterator(M_rotate, M_scale);
    }
    for (ManipulatorGizmo::Mode mode : for_each_mode_on_range) {
        // first backup the original color before lightening
        Color color(colors[_hovered_axis]);
        if (mode == _hovered_mode) {
            colors[_hovered_axis].set_l(color.get_l() * 1.3f);
        }
        call_mode(mode, [&](auto mode_object) -> void {
            mode_object.draw_gizmo(this, scene_data, transforms, colors);
        });
        colors[_hovered_axis] = color; // recover the original color
    }
}


std::tuple<LMatrix4, Lens*, PN_float32, LVector2>
ManipulatorGizmo::initialize_data() const {
    
    ViewportService* viewport = ViewportService::get_active_viewport();
    
    NavigationCamera* camera = viewport->get_navigation_camera();
    GraphicsWindow* window = viewport->get_framebuffer();
    
    NodePath camera_np = NodePath::any_path(camera);
    Lens* lens = camera->get_lens();
    auto plane = lens->get_film_size() / lens->get_focal_length();

    Thread* current_thread = Thread::get_current_thread();
    NodePath scene_np = camera->get_scene();
    if (scene_np.is_empty()) {
        scene_np = camera_np.get_top(current_thread);
    }
    NodePath scene_root = scene_np.get_parent(current_thread);
    auto viewtrans = scene_root.get_transform(camera_np, current_thread);
    
    auto transform = TransformState::make_pos(_transform->get_pos());
    // transform the manipulator in viewspace
    transform = viewtrans->compose(_transform);
    // bring the transform origin to the unit distance plane of the camera
    // so that the manipulator is always the same size on screen

    LVecBase2 size = lens->get_film_size() / lens->get_focal_length();
    LVector2 screen(window->get_x_size(), window->get_y_size());

    float dist_scale = manipulator_scale * size[0] / screen[0];
    if (lens->is_perspective()) {
        dist_scale *= transform->get_pos().get_y();
    }
    transform = transform->set_scale(dist_scale);
    screen *= 0.5f;

    return std::make_tuple(transform->get_mat(), lens, dist_scale, screen);
}


ManipulatorGizmo::Mode operator +(ManipulatorGizmo::Mode a, int b) {
    return ManipulatorGizmo::Mode(static_cast<int>(a) + b);
}
ManipulatorGizmo::Mode& operator +=(ManipulatorGizmo::Mode& a, int b) {
    return a = a + b;
}
ManipulatorGizmo::Mode& operator ++(ManipulatorGizmo::Mode& a) {
    return a += 1;
}
ManipulatorGizmo::Mode operator -(ManipulatorGizmo::Mode a, int b) {
    return ManipulatorGizmo::Mode(static_cast<int>(a) - b);
}
ManipulatorGizmo::Mode& operator -=(ManipulatorGizmo::Mode& a, int b) {
    return a = a - b;
}
ManipulatorGizmo::Mode& operator --(ManipulatorGizmo::Mode& a) {
    return a -= 1;
}


RangeIterator<ManipulatorGizmo::Mode> ManipulatorGizmo::for_each_mode_on() const {

    using RangeType = RangeIterator<ManipulatorGizmo::Mode>;
    return _mode == M_unified ? RangeType(M_translate, M_unified) :
                                RangeType(_mode, _mode + 1);
}

ManipulatorGizmo::AxisIterator ManipulatorGizmo::for_each_axis_on() const {
    if (_grab_data.has_value()) {
        return ManipulatorGizmo::AxisIterator(
            RangeIterator(_grab_data->mode, _grab_data->mode + 1),
            RangeIterator(_grab_data->axis, _grab_data->axis + 1)
        );
    } else {
        return AxisIterator(for_each_mode_on(), RangeIterator(0u, 3u));
    }
}

std::optional<ManipulatorGizmo::HitData>
ManipulatorGizmo::hit_test(const LPoint2& mouse_pos) const {
    
    auto [transform, lens, scale, screen] = initialize_data();
    const LMatrix4 trans = transform;

    std::optional<HitData> hit_data;
    float threshold_squared = grab_threshold * grab_threshold;

    const auto test_dist = [&]( const LPoint4& hit, const LVector2& screen,
                                const Lens* lens) -> bool {
        LPoint2 hit_screen;
        if (lens->project(hit.get_xyz(), hit_screen)) {
            if (!hit_data || hit[1] < hit_data->point[1]) {
                hit_screen = mul_vec(mouse_pos - hit_screen, screen);
                return hit_screen.length_squared() <= threshold_squared;
            }
        }
        return false;
    };
    LPoint3 ray_start, ray_end;
    if (UNLIKELY(!lens->extrude(mouse_pos, ray_start, ray_end))) {
        return std::optional<ManipulatorGizmo::HitData>{};
    }
    for (auto [mode, axis_index] : for_each_axis_on()) {
        uint32_t axis = axis_index;
        LPoint4 hit = call_mode(mode, [&](auto handler) {
            return handler.hit_test(this, trans, ray_start, ray_end, axis);
        });
        if (!hit.is_nan() && (_grab_data || test_dist(hit, screen, lens))) {
            hit_data = {hit, mode, axis_index, scale, trans,
                        TransformState::make_identity()};
        }
    }
    return hit_data;
}
