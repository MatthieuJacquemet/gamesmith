// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SERIALIZE_OPS_H__
#define __SERIALIZE_OPS_H__

#include <panda3d/datagramIterator.h>
#include <panda3d/datagram.h>
#include <panda3d/drawMask.h>

INLINE void generic_write_datagram(Datagram& dest, DrawMask mask) {
    dest.add_uint32(mask.get_word()); 
}
INLINE void generic_read_datagram(DrawMask& mask, DatagramIterator& scan) {
    mask.set_word(scan.get_uint32());
}

#endif // __SERIALIZE_OPS_H__