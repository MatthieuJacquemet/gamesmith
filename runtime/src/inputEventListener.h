// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __INPUTEVENTDISPATCHER_H__
#define __INPUTEVENTDISPATCHER_H__

#include <panda3d/ordered_vector.h>
#include <panda3d/indirectLess.h>
#include <panda3d/luse.h>
#include <panda3d/buttonHandle.h>
#include <panda3d/bitArray.h>

class GraphicsWindow;
class ButtonEvent;
class ViewportService;
class LightMutex;


class InputEventListener {

public:
    static void dispatch(GraphicsWindow* window);
    inline bool operator<(const InputEventListener& other) const {
        return _priority < other._priority;
    }
    void set_enabled(bool enabled) noexcept;
    inline bool is_enabled() const noexcept {
        return _listeners.count(const_cast<InputEventListener*>(this));
    }

    inline int get_priority() const noexcept { return _priority; }

    virtual void handle_button_event(const ButtonEvent& event) {};
    virtual void handle_mouse_event(const LVector2& event) {}
    virtual void viewport_changed(ViewportService* viewport) {}

protected:
    InputEventListener(int priority = 0);
    virtual ~InputEventListener();

    bool grab_inputs(bool grab) noexcept;

    inline static bool is_button_pressed(ButtonHandle button) {
        return _pressed_buttons.get_bit(button.get_index());
    }
    inline static const LVector2& get_mouse_position() {
        return _prev_pos;
    }
private:
    const int _priority; const uint64_t _token;

    struct Comparator {
        bool operator()(InputEventListener* a, InputEventListener* b) const {
            int sort_a = a->_priority;
            int sort_b = b->_priority;
            return sort_a == sort_b ? a < b : sort_a < sort_b;
        }
    };
    typedef ov_set<InputEventListener*, Comparator> Listeners;
    typedef ov_set<InputEventListener*> InactiveListeners;
    
    static LightMutex _lock;
    static LVector2 _prev_pos;
    static BitArray _pressed_buttons;
    static InactiveListeners _inactive_listeners;
    static Listeners _listeners;
    static InputEventListener* _grabber_listener;
};

#endif // __INPUTEVENTDISPATCHER_H__