// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __CONFIG_AGENT_H__
#define __CONFIG_AGENT_H__

#include <panda3d/notifyCategoryProxy.h>
#include <panda3d/configVariableInt.h>

NotifyCategoryDeclNoExport(runtime_agent);


#ifdef NOTIFY_DEBUG
    // Non-release build:
    #define agent_debug(msg) \
        if (runtime_agent_cat.is_debug()) {\
            runtime_agent_cat->debug() << msg << std::endl; \
        }
#else
    // Release build:
    #define agent_debug(msg) ((void)0);
#endif

#define agent_info(msg) \
  runtime_agent_cat->info() << msg << std::endl

#define agent_warning(msg) \
  runtime_agent_cat->warning() << msg << std::endl

#define agent_error(msg) \
  runtime_agent_cat->error() << msg << std::endl


void init_runtime_agent();

extern ConfigVariableInt runtime_agent_task_sort;
extern ConfigVariableInt runtime_send_timeout;

#endif // __CONFIG_AGENT_H__