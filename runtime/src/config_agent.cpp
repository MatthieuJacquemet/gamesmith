// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <custom_types.h>
#include <panda3d/dconfig.h>

#include "config_agent.h"
#include "config_gdk.h"
#include "macro_utils.h"
#include "navigationCamera.h"
#include "renderOverlayLayer.h"
#include "nvidiaGpuInformations.h"
#include "hardware_info.h"
#include "pandaNodeService.h"

ConfigureDef(config_runtime_agent);

ConfigureFn(config_runtime_agent) {
    init_runtime_agent();
}

NotifyCategoryDef(runtime_agent, "");

void init_runtime_agent() {
    CHECK_INITIALIZED
    init_gdk();
#ifdef HAVE_NVML
    register_gpu_monitor<NvidiaGpuInformations>();
#endif
    NavigationCamera::init_type();
    RenderViewLayer::init_type();
    RenderOverlayLayer::init_type();

    register_legacy_variant_types();
    register_legacy_render_effects();

    Variant::register_type_factory<std::vector<uint64_t>>();
    Variant::register_type_factory<std::vector<LVecBase3f>>();
}


ConfigVariableInt runtime_agent_task_sort("runtime-agent-task-sort", -100,
    PRC_DESC("The sort index of the runtime agent task."));

ConfigVariableInt runtime_send_timeout("runtime-socket-send-timeout", 1000,
    PRC_DESC("timeout in milliseconds used to send message over ipc the "
            "controller node"));

