// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/mouseAndKeyboard.h>
#include "eventListener.h"


GraphicsWindow* InputListener::get_window(Thread* current_thread) const {
    if (UNLIKELY(_dispatcher->get_num_parents(current_thread) == 0)) {
        return nullptr;
    }
    PandaNode* node = _dispatcher->get_parent(0, current_thread);
    if (MouseAndKeyboard* parent = DCAST(MouseAndKeyboard, node)) {
        return parent->get_window();
    }
    return nullptr;
}
