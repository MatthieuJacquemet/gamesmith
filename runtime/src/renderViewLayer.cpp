// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/graphicsStateGuardian.h>
#include <renderPipeline.h>
#include <renderPass.h>

#include <sceneData.h>

#include "viewportService.h"
#include "renderViewLayer.h"


DEFINE_TYPEHANDLE(RenderViewLayer);


RenderViewLayer::RenderViewLayer(ViewportService* viewport,
                                const std::string& name):
    _collector(name), _viewport(viewport) {
}


void RenderViewLayer::do_callback(CallbackData* cbdata) {

    SceneData* scene_data = get_scene_data();
    if (UNLIKELY(scene_data == nullptr)) {
        // no scene data, seems there is not a RenderPipeline yet
    } else if (GraphicsStateGuardian* gsg = scene_data->get_gsg()) {
        RenderScope render_scope(gsg, _collector);

        if (gsg->set_scene(scene_data) && gsg->begin_scene()) {
            draw_callback();
            // We need to call end_scene() after begin_scene() so the
            // the gsg can do finish the rendering of the scene.
            gsg->end_scene();
        }
    }
}

void RenderViewLayer::setup_pipeline(RenderPipeline* pipeline) {
    
}

void RenderViewLayer::setup_display_region(DisplayRegion* dr) {
    
}

SceneData* RenderViewLayer::get_scene_data() const {
    RenderPipeline* pipeline = _viewport->get_render_pipeline();
    return pipeline ? pipeline->get_scene_data() : nullptr;
}
