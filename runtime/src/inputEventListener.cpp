// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <algorithm>
#include <panda3d/dtoolbase.h>
#include <panda3d/graphicsWindow.h>
#include <panda3d/graphicsWindowInputDevice.h>
#include <panda3d/linmath_events.h>
#include <panda3d/buttonEventList.h>

#include <eventDispatcher.h>
#include <math_utils.h>

#include "viewportService.h"
#include "inputEventListener.h"

        
InputEventListener::InactiveListeners InputEventListener::_inactive_listeners;
InputEventListener::Listeners InputEventListener::_listeners;
LVector2 InputEventListener::_prev_pos;
BitArray InputEventListener::_pressed_buttons;
LightMutex InputEventListener::_lock;
InputEventListener* InputEventListener::_grabber_listener = nullptr;


static uint64_t register_listener(InputEventListener* listener) {
    return EventDispatcher::get_global_dispatcher()->add_listener(
        ViewportService::get_active_changed(),
        &InputEventListener::viewport_changed, listener
    );
}

InputEventListener::InputEventListener(int priority):
    _priority(priority), _token(register_listener(this)) {
    LightMutexHolder holder(_lock);
    _listeners.insert(this);
}

InputEventListener::~InputEventListener() {
    LightMutexHolder holder(_lock);
    _listeners.erase(this); _inactive_listeners.erase(this);
    if (_grabber_listener == this) {
        _grabber_listener = nullptr;
    }
    EventDispatcher::get_global_dispatcher()->remove_listener(
        ViewportService::get_active_changed(), _token
    );
}
void InputEventListener::dispatch(GraphicsWindow* window) {
    LightMutexHolder holder(_lock);

    GraphicsWindowInputDevice* device = DCAST(GraphicsWindowInputDevice,
                                            window->get_input_device(0));


    using callback_t = std::function<void(InputEventListener*)>;
    const auto dispatch_event = [](const callback_t& callback) {
        if (_grabber_listener != nullptr) {
            return callback(_grabber_listener);
        }
        std::any_of(_listeners.begin(), _listeners.end(), [&](auto* listener) {
            callback(listener);
            return _grabber_listener != nullptr;
        });
    };
    if (device->has_button_event()) {
        PT(ButtonEventList) event_list = device->get_button_events();
       
        int num_events = event_list->get_num_events();
        for (int i = 0; i < num_events; ++i) {
            const ButtonEvent& event = event_list->get_event(i);
            if (event._type == ButtonEvent::T_down) {
               _pressed_buttons.set_bit(event._button.get_index());
            } 
            else if (event._type == ButtonEvent::T_up) {
                _pressed_buttons.clear_bit(event._button.get_index());
            }
            dispatch_event([&](InputEventListener* listener) {
                listener->handle_button_event(event);
            });
        }
    }
    if (LIKELY(window->has_size() && device->has_pointer())) {
        LVector2i size = window->get_size();
        PointerData mdata = device->get_pointer();

        if (mdata._in_window) {
            LVector2 mouse_pos(mdata._xpos, mdata._ypos);
            mouse_pos[0] = mapf(0.f, size[0], -1.f, 1.f, mouse_pos[0]);
            mouse_pos[1] = mapf(0.f, size[1],  1.f,-1.f, mouse_pos[1]);

            if (_prev_pos != mouse_pos) {
                _prev_pos = mouse_pos;
                dispatch_event([&](InputEventListener* listener) {
                    listener->handle_mouse_event(mouse_pos);
                });
            }
        }
    }
}

void InputEventListener::set_enabled(bool enabled) noexcept {
    if (enabled) {
        _listeners.insert(this); _inactive_listeners.erase(this);
    } else {
        _listeners.erase(this); _inactive_listeners.insert(this);
    }
}

bool InputEventListener::grab_inputs(bool grab) noexcept {
    if (_grabber_listener && _grabber_listener != this) {
        return false;
    }
    _grabber_listener = grab ? this : nullptr; return true;
}