// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __NVIDIAGPUINFORMATIONS_H__
#define __NVIDIAGPUINFORMATIONS_H__

#ifdef HAVE_NVML

#include <nvml.h>
#include "gpuInformations.h"


class NvidiaGpuInformations : public GpuMonitor<0x10de> {

public:
    NvidiaGpuInformations();
    ~NvidiaGpuInformations();

    virtual void update_data(DisplayInformation* info) override;

private:
    static bool check(nvmlReturn_t value, const std::string& msg);

    nvmlDevice_t _device_handle;
    nvmlAccountingStats_t _stats;
    nvmlMemory_t _memory_info;
};

#endif // HAVE_NVML

#endif // __NVIDIAGPUINFORMATIONS_H__