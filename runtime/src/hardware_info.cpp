// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/displayInformation.h>
#include <filesystem>

#include "hardware_info.h"

#if defined(IS_LINUX) || defined(IS_FREEBSD)

#ifdef IS_LINUX
#include <libudev.h>
#endif

static inline std::string cpu_info_path(unsigned int index) {
    return "/sys/devices/system/cpu/cpu" + std::to_string(index) + "/cpufreq/";
}

class CPUInfoFiles {
public:
    static inline int get_freq_info(std::ifstream& stream) {
        if (UNLIKELY(!stream.is_open())) return -1;
        std::string result;
        stream.seekg(0, std::ios::beg);
        std::getline(stream, result);
        return std::stoi(result) * 1000;
    }
    inline void read_freq(DisplayInformation* info) {

        info->_current_cpu_frequency = get_freq_info(_cur_freq_file);
        info->_maximum_cpu_frequency = get_freq_info(_max_freq_file);
    }
    inline CPUInfoFiles(unsigned int cpuid) {
        _cur_freq_file.open(cpu_info_path(cpuid) + "scaling_cur_freq");
        _max_freq_file.open(cpu_info_path(cpuid) + "cpuinfo_max_freq");
    }
private:
    std::ifstream _cur_freq_file, _max_freq_file;
};


int update_cpu_frequency(int procn, DisplayInformation* info) {

    static std::vector<CPUInfoFiles> file_list;

    while (file_list.size() <= procn) {
        file_list.push_back(file_list.size());
    }
    file_list[procn].read_freq(info);
    return true;
}

#endif

std::map<int, std::function<GpuInformations* ()>>& get_factories() {
    static std::map<int, std::function<GpuInformations* ()>> factories;
    return factories;
}


GpuInformations* create_gpu_monitor(DisplayInformation* info) {
#ifdef IS_LINUX
    if (!info->get_vendor_id() || !info->get_device_id()) {
        
        struct udev* dev = udev_new();
        auto enumerate = udev_enumerate_new(dev);
        
        udev_enumerate_add_match_property(enumerate,
                                        "ID_PCI_CLASS_FROM_DATABASE",
                                        "Display controller");
        udev_enumerate_scan_devices(enumerate);

        auto entry = udev_enumerate_get_list_entry(enumerate);
        auto path = udev_list_entry_get_name(entry);
        auto device = udev_device_new_from_syspath(dev, path);
        auto vendor = udev_device_get_sysattr_value(device, "vendor");
        auto product = udev_device_get_sysattr_value(device, "product");

        if (vendor != nullptr) {
            info->_vendor_id = std::stoul(vendor, nullptr, 16);
        }
        if (product != nullptr) {
            info->_device_id = std::stoul(product, nullptr, 16);
        }
        udev_device_unref(device);
        udev_enumerate_unref(enumerate);
        udev_unref(dev);
    }
#endif // IS_LINUX
    auto& factories = get_factories();
    using factories_t = std::remove_reference_t<decltype(factories)>;
    factories_t::const_iterator it;

    if ((it = factories.find(info->_vendor_id)) != factories.end()) {
        return it->second();
    } else {
        std::cout << "Could not find gpu monitor for "
                << std::hex << info->_vendor_id << std::endl;
    }
    return static_cast<GpuInformations*>(nullptr);
}



void register_gpu_monitor(int vendor_id, 
                        const std::function<GpuInformations* ()>& factory) {
    
    get_factories()[vendor_id] = factory;
}