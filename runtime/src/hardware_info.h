

// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __HARDWARE_INFO_H__
#define __HARDWARE_INFO_H__

#include <unordered_map>
#include <functional>

class DisplayInformation;
class GpuInformations;


#if defined(IS_LINUX) || defined(IS_FREEBSD)
int update_cpu_frequency(int procn, DisplayInformation* info);
#endif

void register_gpu_monitor(int vendor_id, 
    const std::function<GpuInformations* ()>& factory);

template<class T>
void register_gpu_monitor() {
    register_gpu_monitor(T::vendor_id, []() { return new T(); });
}

GpuInformations* create_gpu_monitor(DisplayInformation* info);

#endif // __HARDWARE_INFO_H__