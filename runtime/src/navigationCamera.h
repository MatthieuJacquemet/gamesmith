// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __NAVIGATIONCAMERA_H__
#define __NAVIGATIONCAMERA_H__

#include <panda3d/graphicsWindow.h>
#include <panda3d/camera.h>
#include <macro_utils.h>
#include <optional>
#include <keyboardAndMouseHandler.h>

#include "inputEventListener.h"


class NavigationCamera: public Camera, public InputEventListener {

    REGISTER_TYPE("NavigationCamera", Camera)

public:
    NavigationCamera();
    virtual ~NavigationCamera() = default;

    enum TravellingDirection { TD_forward, TD_backward };
    enum NavMode { NM_none, NM_pan, NM_orbit, NM_zoom };

    void toggle_perspective_orthographic();

protected:
    static GraphicsWindow* get_window();
    void handle_button_event(const ButtonEvent& event) override;
    void handle_mouse_event(const LVector2& event) override;

    void do_travelling(float alpha);
    void update_orbit(const LVector2& coords);
    void update_pan(const LVector2& coords);
    void do_travelling_mouse(const LVector2& coords);

    LVecBase2f compute_mouse_move_delta(bool normalized = true);
    const LVecBase3f& update_hit_point();
    LPoint3 compute_mouse_to_view(const LPoint3f& coord) const;

    LVector3 recompute_orthographic_position(const Lens* lens);
    LVector3 recompute_perspective_position(const Lens* lens);

    LVecBase3f _hit_point;
    void (NavigationCamera::*_handler)(const LVector2&) = nullptr;
    std::optional<LVecBase2i> _prev_mouse;
};

#endif // __NAVIGATIONCAMERA_H__