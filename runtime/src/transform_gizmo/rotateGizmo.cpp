// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <cmath>
#include <panda3d/geomLinestrips.h>
#include <panda3d/geomVertexWriter.h>

#include <sceneData.h>
#include <complex>

#include "rotateGizmo.h"

using TransformMode = ManipulatorGizmo::TransformMode<ManipulatorGizmo::M_rotate>;

#define ARC_HANDLE_OVERSHOOT 0.02f


CPT(Geom) TransformMode::make_rotation_handle_geoms() {

    PT(GeomVertexData) wdata = new GeomVertexData("rotate-handle",
                                                GeomVertexFormat::get_v3(),
                                                GeomEnums::UH_static);
    
    PT(Geom) handle_geom = new Geom(wdata);

    if (UNLIKELY(!wdata->reserve_num_rows(64))) {
        return nullptr;
    }
    PT(GeomLinestrips) primitive = new GeomLinestrips(GeomEnums::UH_static);
    primitive->reserve_num_vertices(128);

    std::complex<float> last_vert(1.0f, 0.0f);
    std::complex<float> rotation = std::polar(1.f, M_PIf / 32.f);

    GeomVertexWriter writer(wdata, InternalName::get_vertex());
    for (int i = 0; i < 64; ++i) {
        writer.add_data3(last_vert.real(), last_vert.imag(), 0.f);
        last_vert *= rotation;
    }
    for (int i = 0; i < 64; ++i) {
        primitive->add_vertices(i, (i + 1) & 0x3f);
    }
    primitive->close_primitive(); handle_geom->add_primitive(primitive);

    return handle_geom;
}



LPoint4 TransformMode::hit_test(const ManipulatorGizmo* gizmo,
                                const LMatrix4& mat,
                                const LPoint3& ray_start,
                                const LPoint3& ray_end, int axis) {
        

    bool update = gizmo->_grab_data.has_value();
    LPoint3 axis_vec = mat.get_row3(axis); // the axis's vector
    LPoint3 position = mat.get_row3(3); // the center of the gozmo

    LPlane plane(axis_vec, position);
    LPoint3 intersect;
    if (plane.intersects_line(intersect, ray_start, ray_end)) {
        intersect = normalize(intersect - position);

        if (!update && intersect.get_y() > ARC_HANDLE_OVERSHOOT) {
            // LVector3 tangent = cross(plane.get_normal(), LVector3::forward());
            // intersect = tangent * (tangent.dot(intersect) < 0.f ? -1.f : 1.f);
            auto tangent = cross(plane.get_normal(), LVector3::forward());
            auto bitangent = cross(plane.get_normal(), tangent);
            
            float t = ARC_HANDLE_OVERSHOOT / bitangent.get_y();
            intersect = bitangent * t + tangent * sqrt(1.f - t * t);
        }
        intersect *= axis_vec.length();
        return LPoint4(update ? intersect : intersect + position, 0.f);
    }
    return LPoint4(make_nan(0.0f));
}


void TransformMode::draw_gizmo( const ManipulatorGizmo* gizmo,
                                const SceneData* scene_data,
                                CPT(TransformState) transforms[],
                                const Color colors[]) {

    static CPT(Geom) geom = make_rotation_handle_geoms();
    ShaderState state(gizmo->_rotate_state);
    
    NodePath clip_plane_np = state.get_on_clip_plane(0);
    // here we will make a clip plane facing the camera and passing by the
    // gizmo origine to clip the back half of the rotate gizmo
    if (LIKELY(!clip_plane_np.is_empty())) {
        const auto* view_trans = scene_data->get_camera_transform();
        CPT(TransformState) view_plane = transforms[2];

        float offset = ARC_HANDLE_OVERSHOOT * view_plane->get_scale()[0];
        const LVector3 pos(0.0f, offset, 0.0f);
        
        view_plane = view_plane->set_pos(view_plane->get_pos() + pos);
        view_plane = view_plane->set_scale(LVector3(1.0f));
        view_plane = view_plane->set_hpr(LVector3(0.0f));
        // the view_plane is already in view space, we need to transform it to
        // world space because the gsg expect the plane to be in world space
        clip_plane_np.set_transform(view_trans->compose(view_plane));
        // this is intended only to stale the state so that the gsg
        // is forced to update the plane uniform with the new valeu 
        state.set_depth_test(false);
    }
    const auto get_transform = [&](uint32_t axis) {
        return std::make_tuple(transforms[axis], colors[axis]);
    };
    draw_instances(scene_data, geom, state, get_transform);
}


