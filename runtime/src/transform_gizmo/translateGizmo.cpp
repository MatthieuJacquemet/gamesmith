// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/geomVertexReader.h>
#include <panda3d/geomVertexWriter.h>
#include <panda3d/geomLines.h>

#include <cone.h>

#include "translateGizmo.h"

using TransformMode = ManipulatorGizmo::TransformMode<ManipulatorGizmo::M_translate>;

constexpr const float UNIFIED_BIAS = 1.3f;


std::array<CPT(Geom), 2> TransformMode::make_translate_handle_geoms() {

    PT(GeomVertexData) wdata = new GeomVertexData("translate-handle",
                                                GeomVertexFormat::get_v3(),
                                                GeomEnums::UH_static);
    
    // wdata->insert_array(wdata->get_num_arrays(), get_instances_array());

    PT(Geom) handle_arrow_geom = new Geom(wdata);
    PT(Geom) handle_line_geom  = new Geom(wdata);

    if (UNLIKELY(!wdata->reserve_num_rows(18))) {
        return {nullptr, nullptr};
    }
    const GeomVertexData* rdata = Cone::get_geom_data(2);

    GeomVertexReader reader(rdata, InternalName::get_vertex());
    GeomVertexWriter writer(wdata, InternalName::get_vertex());

    CPT(TransformState) transform = TransformState::make_pos_hpr_scale(
        LVector3(0.f, 0.f, 1.1f), LVector3(0,-90,0), LVector3(0.05f, 0.2f, 0.05f)
    );
    const LMatrix4& mat = transform->get_mat();
    // 16 vertices for circle and 2 for triangle apex and bottom fan center
    for (int i = 0; i < 18; ++i) {
        writer.add_data3f(mat.xform_point(reader.get_data3f()));
    }
    writer.add_data3f(LVector3::zero()); // handle origin
    
    // create handle line
    PT(GeomLines) handle_line = new GeomLines(GeomEnums::UH_static);
    handle_line->add_vertices(1, 18);
    handle_line->close_primitive();

    handle_arrow_geom->add_primitive(Cone::get_primitive(2));
    handle_line_geom->add_primitive(handle_line);

    return {handle_arrow_geom, handle_line_geom};
}


LPoint4 TransformMode::hit_test(const ManipulatorGizmo* gizmo,
                                const LMatrix4& mat,
                                const LPoint3& ray_start,
                                const LPoint3& ray_end, int axis) {
    float min = 0.f, max = 1.f;
    if (!gizmo->_grab_data && gizmo->_mode == Mode::M_unified) {
        min = UNIFIED_BIAS, max = UNIFIED_BIAS;
    }
    return hit_axis_handle(ray_start, ray_end, mat, axis, min, max);
}


void TransformMode::draw_gizmo( const ManipulatorGizmo* gizmo,
                                const SceneData* scene_data,
                                CPT(TransformState) transforms[],
                                const Color colors[]) {

    static std::array<CPT(Geom),2> geoms = make_translate_handle_geoms();

    const auto get_data = [&](uint32_t axis) -> AxisData {
        CPT(TransformState) trans = transforms[axis];
        if (gizmo->_mode == Mode::M_unified) {
            LVector3 offset(0.f, 0.f, UNIFIED_BIAS - 1.f);
            trans = trans->compose(TransformState::make_pos(offset));
        }
        Color color = colors[axis];
        color.set_a(get_facing_factor(scene_data, trans));
        return std::make_tuple(trans, color);
    };
    draw_instances(scene_data, geoms[0], gizmo->_base_state, get_data);
    if (gizmo->_mode != Mode::M_unified) {
        draw_instances(scene_data, geoms[1], gizmo->_line_state, get_data);
    }
}