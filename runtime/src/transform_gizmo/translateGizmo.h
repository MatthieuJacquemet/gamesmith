// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __TRANSLATEGIZMO_H__
#define __TRANSLATEGIZMO_H__

#include "manipulatorGizmo.h"

template<>
class ManipulatorGizmo::TransformMode<ManipulatorGizmo::M_translate> {

public:
    static LPoint4 hit_test(const ManipulatorGizmo* gizmo,
                            const LMatrix4& mat,
                            const LPoint3& ray_start,
                            const LPoint3& ray_end, int axis);
    
    static void draw_gizmo( const ManipulatorGizmo* gizmo,
                            const SceneData* scene_data,
                            CPT(TransformState) transforms[],
                            const Color colors[]);

private:
    static std::array<CPT(Geom), 2> make_translate_handle_geoms();
};

#endif // __TRANSLATEGIZMO_H__