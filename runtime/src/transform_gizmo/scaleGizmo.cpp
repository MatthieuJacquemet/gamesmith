// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/geomVertexWriter.h>
#include <panda3d/geomTriangles.h>
#include <panda3d/geomLines.h>

#include <utils.h>
#include "scaleGizmo.h"


using TransformMode = ManipulatorGizmo::TransformMode<ManipulatorGizmo::M_scale>;

constexpr const float UNIFIED_BIAS = 0.7f;


std::array<CPT(Geom), 2> TransformMode::make_scale_handle_geoms() {

    PT(GeomVertexData) wdata = new GeomVertexData("scale-handle",
                                                GeomVertexFormat::get_v3(),
                                                GeomEnums::UH_static);
    
    // wdata->insert_array(wdata->get_num_arrays(), get_instances_array());
    
    PT(Geom) handle_tips_geom = new Geom(wdata);
    PT(Geom) handle_line_geom = new Geom(wdata);

    if (UNLIKELY(!wdata->reserve_num_rows(8))) {
        return {nullptr, nullptr};
    }
    const LVector3 vertices[8] = {
    // ___________y+__________|__________y-___________
    //      x+    |     x-    |    x+    |     x-
        {1, 1, 1}, {-1, 1, 1}, {1,-1, 1}, {-1,-1, 1},   // top
        {1, 1,-1}, {-1, 1,-1}, {1,-1,-1}, {-1,-1,-1},   // bottom
    };
    const int indices[12][3] = {
        {0,1,2}, {2,1,3},   // top    z+
        {0,4,1}, {4,5,1},   // front  y+
        {2,3,7}, {7,6,2},   // back   y-
        {5,3,1}, {5,7,3},   // left   x-
        {6,0,2}, {6,4,0},   // right  x+
        {4,7,5}, {4,6,7}    // bottom z-
    };
    CPT(TransformState) transform = TransformState::make_pos_hpr_scale(
        LVector3(0.f, 0.f, 1.f), LVector3::zero(), LVector3(0.05f)
    );
    const LMatrix4& mat = transform->get_mat();
    
    PT(GeomTriangles) primitive = new GeomTriangles(GeomEnums::UH_static);
    PT(GeomLines) line_prim = new GeomLines(GeomEnums::UH_static);

    primitive->reserve_num_vertices(3 * 12);
    line_prim->reserve_num_vertices(2);

    GeomVertexWriter writer(wdata, InternalName::get_vertex());
    for (int i = 0; i < 8; ++i) {
        writer.add_data3f(mat.xform_point(vertices[i]));
    }
    writer.add_data3f(LVector3::zero());         // handle line bottom 
    writer.add_data3f(LVector3(0.f, 0.f, 1.0f)); // handle line top

    for (int i = 0; i < 12; ++i) {
        primitive->add_vertices(indices[i][0], indices[i][1], indices[i][2]);
    }
    line_prim->add_vertices(8, 9);

    primitive->close_primitive();
    line_prim->close_primitive();

    handle_tips_geom->add_primitive(primitive);
    handle_line_geom->add_primitive(line_prim);

    return {handle_tips_geom, handle_line_geom};
}


LPoint4 TransformMode::hit_test(const ManipulatorGizmo* gizmo,
                                const LMatrix4& mat,
                                const LPoint3& ray_start,
                                const LPoint3& ray_end, int axis) {
    float max = 1.f;
    if (!gizmo->_grab_data && gizmo->_mode == Mode::M_unified) {
        max = UNIFIED_BIAS;
    }
    return hit_axis_handle(ray_start, ray_end, mat, axis, 0.f, max);
}


void TransformMode::draw_gizmo( const ManipulatorGizmo* gizmo,
                                const SceneData* scene_data,
                                CPT(TransformState) transforms[],
                                const Color colors[]) {

    static std::array<CPT(Geom),2> geoms = make_scale_handle_geoms();
    std::function<CPT(TransformState)(uint32_t)> get_bias;
    LVector3 scale_bias(1.0f);

    if (gizmo->_grab_data) {
        scale_bias = gizmo->_grab_data->transform->get_scale();
    }
    if (gizmo->_mode == Mode::M_unified) {
        scale_bias *= UNIFIED_BIAS;
    }
    const auto get_data = [&](uint32_t axis) -> AxisData {
        auto transform = transforms[axis]->compose(get_bias(axis));
        Color color = colors[axis];
        color.set_a(get_facing_factor(scene_data, transform));
        
        return std::make_tuple(transform, colors[axis]);
    };
    get_bias = [scale_bias](uint32_t axis) {
        LVector3 bias(0.0f, 0.0f, scale_bias.get_cell(axis) - 1.0f);
        return TransformState::make_pos(bias);  
    };
    draw_instances(scene_data, geoms[0], gizmo->_base_state, get_data);
    
    get_bias = [scale_bias](uint32_t axis) {
        LVector3 bias(1.0f, 1.0f, scale_bias.get_cell(axis));
        return TransformState::make_scale(bias);  
    };
    draw_instances(scene_data, geoms[1], gizmo->_line_state, get_data);
}


