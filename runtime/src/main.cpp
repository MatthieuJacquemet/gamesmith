// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <panda3d/asyncTaskManager.h>
#include <panda3d/graphicsPipe.h>
#include <applicationFramework.h>
#include <gamesmith_config.h>
#include <panda3d/pStatClient.h>

#include "config_agent.h"

#include <viewportService.h>


int main(int argc, char* argv[]) {
    init_runtime_agent();

    // PStatClient::connect("localhost");

    auto uri = ExecutionEnvironment::get_environment_variable(IPC_URI_ENV);
    auto* task_mgr = AsyncTaskManager::get_global_ptr();

    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
    ApplicationFramework::get_global_ptr()->init_default_tasks();

    if (UNLIKELY(!broker->connect_interface(uri))) {
        return EXIT_FAILURE;
    } else {
        auto* task = task_mgr->add("runtime-agent", [](AsyncTask*) {
            ObjectRequestBroker::get_global_ptr()->handle_messages();
            return AsyncTask::DS_cont;
        });
        task->set_sort(runtime_agent_task_sort);
        // ViewportService* service = new ViewportService();
        // service->attach_framebuffer();
        return ApplicationFramework::get_global_ptr()->execute();
    }
    return EXIT_SUCCESS;
}