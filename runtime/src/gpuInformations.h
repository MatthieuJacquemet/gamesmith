// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __GPUINFORMATIONS_H__
#define __GPUINFORMATIONS_H__

#include <panda3d/displayInformation.h>
#include <unistd.h>

class DisplayInformation;

class GpuInformations {

public:
    virtual ~GpuInformations() = default;
    virtual void update_data(DisplayInformation* info) {
        info->_video_memory_usage = -1;
        info->_video_memory = -1;
        info->_graphics_load = -1;
    };
    virtual uint16_t get_vendor_id() const = 0;

protected:
    GpuInformations() = default;
};

template<uint16_t VID>
struct GpuMonitor: public GpuInformations {
    enum { vendor_id = VID };
    virtual uint16_t get_vendor_id() const override {
        return vendor_id;
    }
};

#endif // __GPUINFORMATIONS_H__