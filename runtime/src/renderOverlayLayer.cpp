// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <macro_utils.h>
#include <panda3d/displayRegionDrawCallbackData.h>
#include <panda3d/graphicsOutput.h>
#include <panda3d/lineSegs.h>
#include <panda3d/configVariableColor.h>
#include <panda3d/configVariableDouble.h>
#include <panda3d/depthWriteAttrib.h>
#include <panda3d/depthTestAttrib.h>
#include <panda3d/antialiasAttrib.h>
#include <panda3d/colorScaleAttrib.h>

#include <renderPipeline.h>
#include <render_utils.h>
#include <sceneData.h>
#include <renderPass.h>

#include "viewportService.h"
#include "renderOverlayLayer.h"
#include "serviceRef.h"
#include "toolService.h"

class RenderContext;

DEFINE_TYPEHANDLE(RenderOverlayLayer)



ConfigVariableColor grid_color("flood-grid-color",
    LColor(1.0f, 1.0f, 1.0f, 0.2),
    PRC_DESC("Color of the floor grid")
);

ConfigVariableDouble grid_thickness("flood-grid-thickness", 1.0,
    PRC_DESC("Thickness of the floor grid lines in pixels")
);

ConfigVariableColor x_axis_color("x-axis-color",
    LColor(1.0f, 0.2f, 0.32f, 1.0f),
    PRC_DESC("Color of the x axis in the floor grid")
);

ConfigVariableColor y_axis_color("y-axis-color",
    LColor(0.54f, 0.86f, 0.0f, 1.0f),
    PRC_DESC("Color of the y axis in the floor grid")
);

ConfigVariableColor z_axis_color("z-axis-color",
    LColor(0.15f, 0.56f, 1.0f, 1.0f),
    PRC_DESC("Color of the z axis in the floor grid")
);


constexpr const int GRID_RESOLUTION = 64;


template<size_t... Is>
RenderOverlayLayer::Component*
create_component(RenderOverlayLayer::OverlayComponent component_id,
                RenderOverlayLayer* drawer, std::index_sequence<Is...>) {
    
    constexpr RenderOverlayLayer::Component* (*factories[])(RenderOverlayLayer*) = {
        &create_component<RenderOverlayLayer::OverlayComponent(Is)>...
    };
    if (LIKELY(component_id < sizeof...(Is))) {
        return factories[component_id](drawer);
    } else {
        nout << "draw_component: index out of range" << std::endl;
        return nullptr;
    }
}



static CPT(Geom) get_geom_from_line_segs(LineSegs& line_seg) {
    GeomNode temp_node("temp");
    temp_node.local_object();

    line_seg.create(&temp_node);
    return temp_node.get_geom(0);
}


static CPT(Geom) generate_grid_geom(int resolution) {

    LineSegs lines;
    lines.set_color(grid_color.get_value());
    lines.set_thickness(grid_thickness.get_value());

    for (int i = -resolution; i <= resolution; ++i) {
        // add a line on the x axis
        lines.move_to(i, -resolution, 0); lines.draw_to(i, resolution, 0);
        // add a line on the y axis
        lines.move_to(-resolution, i, 0); lines.draw_to(resolution, i, 0);
    }
    return get_geom_from_line_segs(lines);
}

static CPT(Geom) generate_axes_geom(float scale) {

    LineSegs lines;
    lines.set_thickness(grid_thickness.get_value());
    lines.set_color(LColor(1.0f, 1.0f, 1.0f, 1.0));
    lines.move_to(0.0f, 0.0f, -scale);
    lines.draw_to(0.0f, 0.0f,  scale);
    // lines.set_thickness(grid_thickness.get_value());
    // lines.set_color(LColor(1.0f, 1.0f, 1.0f, 0.0));
    // lines.move_to(0.0f, 0.0f, -scale);
    // lines.set_color(LColor(1.0f, 1.0f, 1.0f, 1.0));
    // lines.draw_to(0.0f, 0.0f, 0.0f);
    // lines.set_color(LColor(1.0f, 1.0f, 1.0f, 0.0));
    // lines.draw_to(0.0f, 0.0f,  scale);

    return get_geom_from_line_segs(lines);
}

static CPT(RenderState) lines_render_state() {
    return RenderState::make(
        TransparencyAttrib::make(TransparencyAttrib::M_alpha),
        DepthWriteAttrib::make(DepthWriteAttrib::M_off),
        DepthTestAttrib::make(DepthTestAttrib::M_less),
        AntialiasAttrib::make(  AntialiasAttrib::M_multisample |
                                AntialiasAttrib::M_better)
    );
}

static CPT(RenderState) generate_grid_render_state() {

    PT(Shader) shader = Shader::load(Shader::SL_GLSL,
        "/data/shader/viewport_grid.vert",
        "/data/shader/viewport_grid.frag"
    );
    shader->set_constant("c_grid_resolution", GRID_RESOLUTION);
    return lines_render_state()
        ->set_attrib(ShaderAttrib::make(shader));
}

static CPT(RenderState) generate_axes_render_state() {

    PT(Shader) shader = Shader::load(Shader::SL_GLSL,
        "/data/shader/basic_vert_color_frustum_fade.vert",
        "/data/shader/basic_vert_color_frustum_fade.frag"
    );
    return lines_render_state()
        ->set_attrib(ShaderAttrib::make(shader))
        ->set_attrib(ColorAttrib::make_vertex());
}



RenderOverlayLayer::RenderOverlayLayer( ViewportService* viewport,
                                        const std::string& name):
    RenderViewLayer(viewport, name) {
    
    set_enabled(RenderOverlayLayer::OC_active_tool, true);
    set_enabled(RenderOverlayLayer::OC_floor_grid, true);
    set_enabled(RenderOverlayLayer::OC_axes, true);
}


void RenderOverlayLayer::draw_callback() {
    constexpr auto is = std::make_index_sequence<OC_COUNT>{};
    for (std::unique_ptr<Component>& component : _comps) {
        if (component) component->draw();
    }
}

void RenderOverlayLayer::setup_pipeline(RenderPipeline* pipeline) {
    constexpr auto is = std::make_index_sequence<OC_COUNT>{};
    for (std::unique_ptr<Component>& component : _comps) {
        if (component) component->setup_pipeline(pipeline);
    }
}

void RenderOverlayLayer::set_enabled(OverlayComponent type, bool enabled) {
    constexpr auto is = std::make_index_sequence<OC_COUNT>{};
    std::unique_ptr<Component>& component = _comps[type];

    if (!component && enabled) {
        component.reset(create_component(type, this, is));
        if (component == nullptr) {
            nout << "Unable to create component: " << type << std::endl;
        }
        else if (RenderPipeline* pipeline = _viewport->get_render_pipeline()) {
            component->setup_pipeline(pipeline);
        }
    } else if (component != nullptr && !enabled) {
        component.reset(nullptr);
    }
}

bool RenderOverlayLayer::is_enabled(OverlayComponent type) const {
    return _comps[type] != nullptr;
}

std::string RenderOverlayLayer::component_name(OverlayComponent type) {
    switch (type) {
        case OC_floor_grid:         return "floor grid";
        case OC_active_tool:        return "active tool";
        case OC_tight_bv:           return "tight bounding volume";
        case OC_bounds:             return "bounds";
        case OC_sel_outline:        return "selection outline";
        case OC_visual_guides:      return "visual guides";
        case OC_parent_relationship:return "parent relationship";
        case OC_profiling_stats:    return "profiling stats";
        default:
            std::cout << "unknown component type: " << type << std::endl;
    }
    return "";
}

const Geom* RenderOverlayLayer::get_line_geom() {
    static CPT(Geom) geom = generate_axes_geom(1.0f);
    return geom; 
}

const RenderState* RenderOverlayLayer::get_line_state() {
    static CPT(RenderState) state = generate_axes_render_state();
    return state;
}

void RenderOverlayLayer::
Component::setup_pipeline(RenderPipeline* pipeline) {
    // implement me
}


struct AxisComponent final : public RenderOverlayLayer::Component {
    ShaderState _state;

    AxisComponent(RenderOverlayLayer* drawer) : Component(drawer) {

        static ShaderState base_state(generate_axes_render_state());
        _state = base_state;
    }

    void draw_axis(SceneData* data, int axis, const LColor& color) {

        const Geom* geom = RenderOverlayLayer::get_line_geom();
    
        _state.set_color_scale(color);
        

        LVector3 cam_pos = data->get_camera_transform()->get_pos();
        LVector3 axis_pos = cam_pos;
        axis_pos.set_cell(axis, 0.0f);
        float scale = 50.0f * axis_pos.length();
        // rotate the line according to the axis index
        LVector3 rot(0.0f), pos(0.0f);

        pos.set_cell(axis, cam_pos.get_cell(axis));
        switch (axis) {
            case 0: rot.set_cell(2, 90.0f); break;
            case 1: rot.set_cell(1, 90.0f); break;
            default: break;
        }
        auto transform = data->get_cs_world_transform()->compose(
            TransformState::make_pos_hpr_scale(pos, rot, scale)
        );
    
        draw_geom(data->get_gsg(), geom, _state, transform);
    }

    virtual void draw() override {
        SceneData* data = _drawer->get_scene_data();

        draw_axis(data, 0, x_axis_color.get_value());
        draw_axis(data, 1, y_axis_color.get_value());
        draw_axis(data, 2, z_axis_color.get_value());
    }
    virtual void setup_pipeline(RenderPipeline* pipeline) override {
        SceneData* data = pipeline->get_scene_data();
        _state.set_shader_input("u_near_far", data->_near_far);
    }
};



struct GridComponent final : public RenderOverlayLayer::Component {
    ShaderState _state;
    PTA_float _height;

    GridComponent(RenderOverlayLayer* drawer): Component(drawer),
        _height(1, 0.f) {

        static ShaderState base_state(generate_grid_render_state());
        _state = base_state;
        _state.set_shader_input("u_height", _height);
    }

    void draw_grid_level(SceneData* data, LVector3 position,
                        const Geom* geom, float level) {

        float grid_scale = std::pow(10.0f, ceil(level));
        grid_scale = std::max(grid_scale, 0.001f);
    
        // snap the camera transform to the grid
        position.set_x(ceil(position.get_x() / grid_scale) * grid_scale);
        position.set_y(ceil(position.get_y() / grid_scale) * grid_scale);

        auto transform = TransformState::make_pos_hpr_scale(
            position, LVector3(0.0), LVector3(grid_scale)
        );
        transform = data->get_cs_world_transform()->compose(transform);
        draw_geom(data->get_gsg(), geom, _state, transform);
    }


    virtual void draw() override {
        
        SceneData* data = _drawer->get_scene_data();

        static CPT(Geom) geom = generate_grid_geom(GRID_RESOLUTION);

        const TransformState* cam = data->get_camera_transform();
        LPoint3 position = cam->get_pos();

        _height[0] = std::exchange(position[2], 0.0f);

        if (data->get_lens()->is_orthographic()) {
            
            LVector3 direction = cam->get_mat().get_row3(1);
            
            float alpha = -_height[0] / direction.get_z();
            position[0] += direction.get_x() * alpha;
            position[1] += direction.get_y() * alpha;
            _height[0] = data->get_lens()->get_film_size().length() * 0.5f;
            _height[0] /= direction.get_z();
        }
        float level = std::log10(std::abs(_height[0] / GRID_RESOLUTION));
        _state.set_color(grid_color.get_value());

        draw_grid_level(data, position, geom, level + 0.0f);
        draw_grid_level(data, position, geom, level + 1.0f);
        draw_grid_level(data, position, geom, level + 2.0f);
    }

    virtual void setup_pipeline(RenderPipeline* pipeline) override {
        SceneData* data = pipeline->get_scene_data();
        _state.set_shader_input("u_view_scale",  data->_view_scale);
        _state.set_shader_input("u_near_far",    data->_near_far);
    }
};


struct ActiveToolComponent final : public RenderOverlayLayer::Component {

    ActiveToolComponent(RenderOverlayLayer* drawer): Component(drawer) {}

    virtual void draw() override {
        for (ToolService* tool : ToolService::get_active_tools()) {
            tool->draw_overlay(_drawer);
        }
    }
    virtual void setup_pipeline(RenderPipeline* pipeline) override {
        for (ToolService* tool : ToolService::get_active_tools()) {
            tool->setup_pipeline(pipeline);
        }
    }
};


REGISTER_OVERLAY_COMPONENT(RenderOverlayLayer::OC_floor_grid,   GridComponent);
REGISTER_OVERLAY_COMPONENT(RenderOverlayLayer::OC_axes,         AxisComponent);
REGISTER_OVERLAY_COMPONENT(RenderOverlayLayer::OC_active_tool,  ActiveToolComponent);