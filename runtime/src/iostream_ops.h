// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __IOSTREAM_OPS_H__
#define __IOSTREAM_OPS_H__

#include <iostream>
#include <vector>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>


template<class T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& m) {
    for (const auto& v : m) {
        os << v << " ";
    }
    return os;
}

template<class K, class V, class Comp = std::less<K>>
std::ostream& operator<<(std::ostream& os, const std::map<K, V, Comp>& m) {
    for (const auto& [k, v] : m) {
        os << k << ":" << v << " ";
    }
    return os;
}

template<class K, class V, class H = std::hash<K>, class P = std::equal_to<K>>
std::ostream& operator<<(std::ostream& os, const std::unordered_map<K, V, H, P>& m) {
    for (const auto& [k, v] : m) {
        os << k << ":" << v << " ";
    }
    return os;
}

template<class K, class V, class Comp = std::less<K>>
std::ostream& operator<<(std::ostream& os, const std::set<K, V, Comp>& m) {
    for (const auto& v : m) {
        os << v << " ";
    }
    return os;
}

template<class K, class H = std::hash<K>, class P = std::equal_to<K>>
std::ostream& operator<<(std::ostream& os, const std::unordered_set<K, H, P>& m) {
    for (const auto& v : m) {
        os << v << " ";
    }
    return os;
}


#endif // __IOSTREAM_OPS_H__