// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SYSTEMRESSOURCEENUMS_H__
#define __SYSTEMRESSOURCEENUMS_H__

#include <variant>
#include <panda3d/graphicsStateGuardian.h>
#include "utils.h"

#define GET_RES_TYPEID(TYPE) variant_index<RessourceData, TYPE>()
#define RESSOURCE_TYPE(TYPE) GET_RES_TYPEID(TYPE) | ((__COUNTER__ - _begin_rt) << 2)


constexpr int _begin_rt = __COUNTER__;

class [[meta::reflect]] SystemRessourceEnums {

public:
    using ShaderModel = GraphicsStateGuardian::ShaderModel;
    using RessourceData = std::variant<int32_t, uint32_t, uint64_t,
                                        std::string, ShaderModel>;

    enum [[meta::reflect]] RessourceType {
        RT_shader_model,
        RT_physical_memory, RT_avail_physical_memory,
        RT_page_file_size, RT_avail_page_file_size,
        RT_process_virtual_memory, RT_page_fault_count,
        RT_process_memory, RT_process_peak_memory ,
        RT_page_file_usage, RT_page_file_peak_usage,
        RT_memory_load,
        RT_gpu_vendor_id, RT_gpu_device_id,
        RT_gpu_driver_product, RT_gpu_driver_version, RT_gpu_driver_sub_version,
        RT_gpu_driver_build,
        RT_gpu_video_memory, RT_gpu_video_memory_usage, RT_gpu_graphics_load,
        RT_cpu_vendor_string, RT_cpu_brand_string, RT_cpu_brand_index,
        RT_cpu_version_information,
        RT_cpu_max_frequency, RT_cpu_cur_frequency,   
        RT_cpu_num_cores, RT_cpu_num_logical_cores,
        RT_os_version_major, RT_os_version_minor, RT_os_version_build,
        RT_COUNT
    };

    constexpr static uint8_t get_data_type(RessourceType type) {
        return static_cast<uint8_t>(type & 0x3u);
    }
    constexpr static uint8_t get_res_id(RessourceType type) {
        return static_cast<uint8_t>(type & ~0x3u);
    }
};
#endif // __SYSTEMRESSOURCEENUMS_H__