// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __RUNTIME_UTILS_H__
#define __RUNTIME_UTILS_H__


template<class T> class IntegralIterator {

    static_assert(std::is_enum_v<T> || std::is_integral_v<T>);

public:
    explicit IntegralIterator(T value) : _value(value) {}

    // Increment / decrement operators
    IntegralIterator& operator +=(int n) { _value += n; return *this; }
    IntegralIterator& operator -=(int n) { _value -= n; return *this; }
    IntegralIterator& operator ++() { ++_value; return *this; }
    IntegralIterator& operator --() { --_value; return *this; }
    IntegralIterator operator +(int n) const { return _value + n; }
    IntegralIterator operator -(int n) const { return _value + n; }
    // Dereference operator (*it)
    inline T operator *() const { return _value; }
    inline operator T() const { return _value; }
    // Comparison operators
    bool operator ==(const IntegralIterator& other) const {
        return _value == other._value;
    }
    bool operator !=(const IntegralIterator& other) const {
        return _value != other._value;
    }
private:
    T _value;
};

template<class T=int> class RangeIterator {
public:
    using value_type        = T;
    using iterator          = IntegralIterator<T>;
    using const_iterator    = IntegralIterator<T>;

    RangeIterator(T end) : _start(0), _end(end) {}
    RangeIterator(T start, T end) : _start(start), _end(end) {}

    inline iterator begin() const { return iterator(_start); }
    inline iterator end() const { return iterator(_end); }

private:
    value_type _start, _end;
};


template<class... Conts> class ProductIterator {

public:
    ProductIterator(const Conts&... conts): _conts{conts...} {}

    struct iterator {
        iterator(const std::tuple<Conts...>& containers,
                const typename Conts::iterator&... iterators):
                _conts{containers}, _its{iterators...} {}

        inline iterator& operator++() { advance(); return *this; }
        inline bool operator ==(const iterator& other) const {
            return _its == other._its;
        }
        inline bool operator !=(const iterator& other) const {
            return _its != other._its;
        }
        std::tuple<typename Conts::value_type...> operator *() const {
            return std::apply([](const auto&... args) {
               return std::make_tuple(*args...); 
            }, _its);
        }
    private:
        template<size_t N=0> inline bool advance(size_t n = 1) {
            if constexpr (N >= sizeof...(Conts)) { // end of recursion
                return false;
            } else if (!advance<N + 1>(n)) { // depth first increment
                auto& current_iterator = std::get<N>(_its);
                current_iterator += 1;
                // wrap only non top-level iterators
                if (N && current_iterator >= std::get<N>(_conts).end()) {
                    current_iterator = std::get<N>(_conts).begin();
                    return false;
                }
            }
            return true; // at this point, this level's iterator is valid
        }
        const std::tuple<Conts...>& _conts;
        std::tuple<typename Conts::iterator...> _its;
    };

    inline iterator begin() const {
        return std::apply([this](const auto&... args) {
            return iterator(_conts, args.begin()...);
        }, _conts);
    }
    inline iterator end() const {
        return std::apply([this](const auto& arg, const auto&... args) {
            return iterator(_conts, arg.end(), args.begin()...);
        }, _conts);
    }
private:
    std::tuple<Conts...> _conts;
};


#endif // __RUNTIME_UTILS_H__