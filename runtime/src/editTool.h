// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __EDITTOOL_H__
#define __EDITTOOL_H__

#include <panda3d/referenceCount.h>
#include <functional>


class RenderContext;
class ToolService;

class EditTool: public ReferenceCount {

protected:
    inline EditTool(ToolService* service): _service(service) {}
public:
    ~EditTool() = default;

    virtual void draw_overlay(const RenderContext& context) {}
    virtual void set_active(bool active) {}
    
    struct ToolMode {
        std::string name, description;
        std::string select_hk, invoke_hk;
    };
    struct ToolSpec {
        std::vector<ToolMode> modes;
        std::function<EditTool*(ToolService*)> factory;
    };

    inline static ToolSpec get_spec() { return {}; }

    template<class T>
    static ToolSpec make_spec(const std::vector<ToolMode>& modes) {
        static_assert(std::is_constructible_v<T, ToolService*>,
                    "EditTool constructor must take a ToolService*");
        return ToolSpec{ modes, [](ToolService* service) {
            return new T(service);
        }};
    }
protected:
    ToolService* const _service;
};

#endif // __EDITTOOL_H__