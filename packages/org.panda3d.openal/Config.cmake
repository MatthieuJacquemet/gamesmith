find_package(Panda3D COMPONENT OpenAL REQUIRED)

set(PACKAGE_LIBRARIES ${PACKAGE_LIBRARIES} Panda3D::Core::p3openal_audio)