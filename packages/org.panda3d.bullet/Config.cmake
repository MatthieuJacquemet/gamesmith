find_package(Panda3D COMPONENT Bullet REQUIRED)

set(PACKAGE_LIBRARIES ${PACKAGE_LIBRARIES} Panda3D::Bullet::p3bullet)