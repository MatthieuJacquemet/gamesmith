// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <cppast/cpp_class.hpp>
#include <cppast/cpp_enum.hpp>
#include <cppast/cpp_member_function.hpp>
#include <cppast/cpp_member_variable.hpp>
#include <cppast/cpp_type_alias.hpp>
#include <cppast/cpp_type.hpp>
#include <cppast/parser.hpp>
#include <cppast/visitor.hpp>
#include <cppast/libclang_parser.hpp>

#include <cctype>
#include <regex>
#include <fstream>
#include <algorithm>
#include <filesystem>
#include <iostream>
#include <optional>

#include "jsonQuery.h"
#include "utils.h"
#include "codeGenerator.h"


struct argument_stream;

using transformer_t     = std::function<std::string(const std::string&,int)>;
using meta_generator    = std::function<void (argument_stream&)>;
using string_list       = std::vector<std::string>;

using attrib_list = cppast::cpp_attribute_list;
using func_entity = cppast::cpp_function_base;
using member_func = cppast::cpp_member_function;
using static_func = cppast::cpp_function;
using enum_entity = cppast::cpp_enum;
using prop_entity = cppast::cpp_member_variable;
using aliasentity = cppast::cpp_type_alias;
using entity_kind = cppast::cpp_entity_kind;
using access_spec = cppast::cpp_access_specifier_kind;

inline std::string full_qualified_name(const cppast::cpp_entity& e);


std::tuple<std::string, std::string> parse_definition(const std::string& def) {

    std::size_t index = def.find_first_of('=');

    if (index == std::string::npos) {
        return std::make_tuple(def, "");
    } else {
        return std::make_tuple(def.substr(0, index), def.substr(index + 1));
    }
}


std::string display_name(const cppast::cpp_entity& entity) {

    if (entity.kind() == cppast::cpp_entity_kind::member_function_t) {
        return entity.name() +
               static_cast<const cppast::cpp_member_function&>(entity)
                   .signature();
    } else {
        return entity.name();
    }
}

inline std::string entity_section_name(const cppast::cpp_entity& entity) {
    std::string name = cppast::to_string(entity.kind());
    std::replace(name.begin(), name.end(), ' ', '_');
    
    return name;
}

std::string get_full_qualified_name(const cppast::cpp_entity& entity,
                                    const std::string& name) {

    if(entity.kind() == cppast::cpp_entity_kind::base_class_t) {
        return cppast::to_string(
            static_cast<const cppast::cpp_base_class&>(entity).type());
    }
    else if(entity.parent().has_value() &&
            entity.parent().value().kind() != cppast::cpp_entity_kind::file_t) {
        return full_qualified_name(entity.parent().value()) + "::" + name;
    }
    else {
        return name;
    }
}

inline std::string full_qualified_name(const cppast::cpp_entity& e) {
    return get_full_qualified_name(e, e.name());
}

inline std::string full_qualified_display_name(const cppast::cpp_entity& e) {
    return get_full_qualified_name(e, display_name(e));
}


std::string full_path_without_extension(const std::filesystem::path& p) {
    
    std::filesystem::path full_path = std::filesystem::absolute(p);
    std::string filename = full_path.filename().string();
    std::string extension = full_path.extension().string();
    std::string basename_wo = filename.substr(0, filename.size() - extension.size());
    
    return (full_path.parent_path() / basename_wo).string();
}

std::string basename_without_extension(const std::filesystem::path& p) {
    
    std::string filename = p.filename().string();
    std::string extension = p.extension().string();
    return filename.substr(0, filename.size() - extension.size());
}

int compare_timestamps( const std::filesystem::path& a,
                        const std::filesystem::path& b) {
    std::error_code ec;
    auto time_a = std::filesystem::last_write_time(a, ec);
    if (ec) {
        return 1; // a does not exist or an error occurred
    }
    auto time_b = std::filesystem::last_write_time(b, ec);
    if (ec) {
        return -1; // b does not exist or an error occurred
    }
    return time_a < time_b ? 1 : (time_a > time_b ? -1 : 0);
}


template<class T, class P>
inline type_safe::optional_ref<const T> cast(const P& object) {
    if (object.kind() == T::kind()) {
        return type_safe::ref(static_cast<const T&>(object));
    }
    return type_safe::optional_ref<const T>(nullptr);
}


bool is_unknown_entity(const cppast::cpp_entity& entity) {
    auto parent = entity.parent();

    if(entity.name().empty()) {
        std::cerr << "[warning] Found " << cppast::to_string(entity.kind())
                  << " with empty name";

        if (parent.has_value()) {
            std::cerr << " at " << full_qualified_name(parent.value());
        }
        std::cout << std::endl;
        return true;
    }
    if (parent.has_value() && is_unknown_entity(parent.value())) {
        return true;
    }
    return false;
}


bool is_valid_token(cppast::cpp_token_kind kind) {
    switch (kind) {
        case cppast::cpp_token_kind::int_literal:
        case cppast::cpp_token_kind::float_literal:
        case cppast::cpp_token_kind::char_literal:
        case cppast::cpp_token_kind::string_literal:
            return true;
    }
    return false;
}

CodeGenerator::CodeGenerator(const std::string&             input_filename,
                            const cppast::cpp_standard      cpp_standard,
                            cl::list<std::string>&          output_files,
                            cl::list<std::string>&          template_files,
                            cl::list<std::string>&          include_dirs,
                            cl::list<std::string>&          definitions,
                            cl::list<std::string>&          include_headers,
                            cl::list<std::string>&          custom_flags,
                            const std::string&              clang_binary):
        _input_file(input_filename) {
    
    if (template_files.size() != output_files.size()) {
        std::cerr << "[error] Template and output files must be the same size";
        exit(1);
    }
    for (int i=0; i<output_files.size(); ++i) {
        _entries.emplace_back(ParseEntry{template_files[i], output_files[i]});
    }
    include_headers.push_back(_input_file.filename().string());
    include_headers.push_back("meta.h");

    _data["source_file"] = _input_file.filename().string();
    _data["include_headers"] = std::vector<std::string>(include_headers);

    _config.set_flags(cpp_standard);

    if (!clang_binary.empty() && !_config.set_clang_binary(clang_binary)) {
        std::cerr   << "error configuring libclang parser: Clang binary \""
                    << clang_binary << "\" not found\n";
        std::exit(1);
    }

    std::cout << "parsing file " << input_filename << " "
              << cppast::to_string(cpp_standard) << std::endl;

    // Add definitions to identify that the translation unit
    // is being parsed by tinyrefl-tool
    definitions.addValue("__metaplusplus__");

    std::for_each(definitions.begin(), definitions.end(),
        [this](const std::string& definition) {
            const auto [key, value] = parse_definition(definition);
            _config.define_macro(std::move(key), std::move(value));
    });

    std::for_each(include_dirs.begin(), include_dirs.end(),
        [this](const std::string& include_dir) {
            if (!include_dir.empty()) _config.add_include_dir(include_dir);
    });

    std::for_each(custom_flags.begin(), custom_flags.end(),
        [this](const std::string& custom_flag) {
            _config.enable_feature(custom_flag);
    });
}


bool CodeGenerator::generate() {

    type_safe::optional_ref<const cppast::cpp_file> file_ref;
    cppast::cpp_entity_index index;
    cppast::simple_file_parser<Parser> parser(type_safe::ref(index));

    try {
        file_ref = parser.parse(_input_file.string(), _config);

    } catch(const cppast::libclang_error& error) {
        std::cerr << "[error] " << error.what() << "\n";
        return false;
    }
    if (!file_ref.has_value()) {
        std::cerr << "[error] something went wrong while parsing file\n";
        return false;
    } 
    visite_ast(file_ref.value());
    // std::cout << std::setw(4) << _data << std::endl;
    inja::Environment env = create_environment();

    return std::all_of(_entries.begin(), _entries.end(), [&](auto& entry) {
        return write_output_file(
            entry.output_file,
            env.render_file(entry.template_file.string(), _data)
        );
    });
}


bool CodeGenerator::is_up_to_date() const {
    return std::all_of(_entries.begin(), _entries.end(), [this](auto& entry) {
        return compare_timestamps(_input_file, entry.output_file) > 0;
    });
}


inline bool is_meta_attribute(const cppast::cpp_attribute& attribute) {
    return attribute.scope() && attribute.scope().value() == "meta";
}


inja::json& CodeGenerator::generate_entity( const cppast::cpp_entity& entity,
                                            inja::json& parent_node) {
    
    auto& section_ctn = parent_node[entity_section_name(entity)];
    auto& entity_data = section_ctn.emplace_back();

    auto& attribs_def = entity_data["attributes"] = inja::json::array();        
    entity_data["name"] = entity.name();

    for (const cppast::cpp_attribute& attrib : entity.attributes()) {
        if (UNLIKELY(!is_meta_attribute(attrib))) {
            continue;
        }
        auto& attrib_def = attribs_def.emplace_back();
        attrib_def["name"] = attrib.name();
        auto& args_def = attrib_def["args"] = inja::json::array();

        if (const auto& args_ref = attrib.arguments()) {
            for (const cppast::cpp_token& arg : args_ref.value()) {
                if (is_valid_token(arg.kind)) {
                    args_def.emplace_back(inja::json::parse(arg.spelling));
                }
            }
        }
    }
    return entity_data;
}

void join_list( std::ostringstream& os, const inja::json& list,
                const std::string& separator) {

    std::string sep;
    for (const inja::json& child : list) {
        os << sep;
        if (child.is_string()) os << child.get<std::string>();
        else if (child.is_array()) join_list(os, child, sep);
        else os << child.dump();

        sep = separator;
    }
}

std::string join(const std::vector<std::string>& list,
                const std::string& separator,
                const transformer_t& trans = [](auto& x, int) { return x; }) {
    
    std::ostringstream os;
    std::string sep;
    int count = 0;
    for (const std::string& item : list) {
        os << sep << trans(item, count++);
        sep = separator;
    }
    return os.str();
}

inja::Environment CodeGenerator::create_environment()  {

    inja::Environment environment;

    environment.add_void_callback("error", [](inja::Arguments& args) {
        std::ostringstream msg;
        std::for_each(args.begin(), args.end(), [&](const auto* arg) {
            msg << arg->template get<std::string>() << " ";
        });
        throw std::runtime_error(msg.str());
    });

    environment.add_callback("childHasValue", [](inja::Arguments& args) {
        if (args.size() != 3) {
            throw std::runtime_error("childHasValue require 3 argments "
                    "but only" + std::to_string(args.size()) + " was passed.");
        }
        const inja::json* node  = args[0];
        const std::string key   = args[1]->get<std::string>();
        const std::string value = args[2]->get<std::string>();

        auto it = std::find_if(node->begin(), node->end(), [&](auto& child) {
            inja::json::const_iterator found;
            if ((found = child.find(key)) != child.end()) {
                return found->get<std::string>() == value;
            }
            return false;
        });
        return it != node->end() ? *it : inja::json();
    });

    environment.add_callback("allChildren", [](inja::Arguments& args) {
        if (args.size() != 2) {
            throw std::runtime_error("allChildren require exactly 2 argment "
                        "but " + std::to_string(args.size()) + " was passed.");
        }
        else if (!args[0]->is_array() || !args[1]->is_string()) {
            throw std::runtime_error("all_children requires (array, string).");
        }
        const inja::json* lst = args[0];
        const std::string key = args[1]->get<std::string>();
        inja::json result = inja::json::array();

        std::for_each(lst->begin(), lst->end(), [&](auto& child) -> void {
            inja::json::const_iterator found;
            if ((found = child.find(key)) != child.end()) {
                result.emplace_back(found.value());
            }
        });
        return result;
    });

    environment.add_callback("zip", [](inja::Arguments& args) {
        if (args.size() != 2) {
            throw std::runtime_error("zip require exactly 2 argment "
                        "but " + std::to_string(args.size()) + " was passed.");
        }
        else if (!args[0]->is_array() || !args[1]->is_array()) {
            throw std::runtime_error("zip requires (array, array).");
        }
        const inja::json* lst0 = args[0], *lst1 = args[1];
        inja::json result = inja::json::array();

        auto it0 = lst0->begin(), it1 = lst1->begin();

        for (; it0 != lst0->end() && it1 != lst1->end(); ++it0, ++it1) {
            result.emplace_back(inja::json::array({it0.value(), it1.value()}));
        }
        return result;
    });

    environment.add_callback("joinAll", [](inja::Arguments& args) {
        if (args.size() < 2) {
            throw std::runtime_error("joinAll require at least 2 argments "
                        "but " + std::to_string(args.size()) + " was passed.");
        }
        std::string sep = args[0]->get<std::string>();
        std::ostringstream stream;
        
        std::for_each(std::next(args.begin()), args.end(), [&](auto* arg) {
            join_list(stream, *arg, sep);
        });
        return inja::json(stream.str());
    });

    environment.add_callback("argList", [](inja::Arguments& args) {
        if (args.size() != 1) {
            throw std::runtime_error("argList require exactly 1 argment "
                        "but " + std::to_string(args.size()) + " was passed.");
        }
        else if (!args[0]->is_array()) {
            throw std::runtime_error("argList requires (array).");
        }
        const inja::json* lst = args[0];
        std::ostringstream os;
        std::string sep;
    
        std::for_each(lst->begin(), lst->end(), [&](auto& child) -> void {
            inja::json::const_iterator tit = child.find("type");
            inja::json::const_iterator nit = child.find("name");
            
            if (LIKELY(tit != child.end() && nit != child.end())) {
                os << sep   << tit->get<std::string>() << " " 
                            << nit->get<std::string>();
                sep = ", ";
            }
        });
        return inja::json(os.str());
    });
    
    environment.add_callback("replace", [](inja::Arguments& args) {
        if (args.size() != 3) {
            throw std::runtime_error("replace require exactly 3 argment "
                        "but " + std::to_string(args.size()) + " was passed.");
        }
        else if (!args[0]->is_string() || !args[1]->is_string() ||
                !args[2]->is_string()) {
            throw std::runtime_error("replace requires (string, string, string).");
        }
        std::string text    = args[0]->get<std::string>();
        std::string match   = args[1]->get<std::string>();
        std::string subst   = args[2]->get<std::string>();

        return std::regex_replace(text, std::regex(match), subst);
    });
    
    environment.add_callback("decoratedRecords", [](inja::Arguments& args) {
        if (args.size() < 2 || args.size() > 4) {
            throw std::runtime_error("decoratedRecords require 2 to 4 argments "
                        "but " + std::to_string(args.size()) + " was passed.");
        }
        bool force_display = false, remove_dp_arg = false;
    
        if (args.size() > 2) force_display = args[2]->get<bool>();
        if (args.size() > 3) remove_dp_arg = args[3]->get<bool>();

        const inja::json* lst = args[0];
        std::regex re(args[1]->get<std::string>());

        inja::json result = inja::json::array();

        auto find_attrib = [&](std::string& display, const inja::json& node) {

            auto it = std::find_if(node.begin(), node.end(), [&](auto& attr) {
                const inja::json& tag = attr["name"];
                return std::regex_match(tag.template get<std::string>(), re);
            });
            if (UNLIKELY(it == node.end())) {
                return std::optional<inja::json>{};
            }
            inja::json attrib = *it;
            const inja::json& args = it->operator[]("args");
            if (args.size() && args.begin()->is_string()) {
                display = args.begin()->get<std::string>();
                if (remove_dp_arg) {
                    attrib.erase(attrib.begin());
                }
            }
            else if (force_display) return std::optional<inja::json>{};
    
            return std::optional<inja::json>{attrib};
        };

        std::for_each(lst->begin(), lst->end(), [&](auto& child) -> void {
            inja::json::const_iterator found0;
            inja::json::const_iterator found1;
            if ((found0 = child.find("attributes")) != child.end() &&
                (found1 = child.find("name"))       != child.end()) {
                
                std::string display = found1->get<std::string>();

                if (auto attrib = find_attrib(display, found0.value())) {
                    auto& new_entry = result.emplace_back(child);
                    
                    new_entry["_display"] = std::move(display);
                    new_entry["_tag"] = std::move(attrib.value());
                }
            }
        }); 
        return result;  
    });


    environment.add_callback("findObjects", [](inja::Arguments& args) {
        if (args.size() < 2 || args.size() > 3) {
            throw std::runtime_error("findObjects requires 2 to 3 argments "
                        "but " + std::to_string(args.size()) + " was passed.");
        }
        else if (!args[1]->is_string() || args.size() == 3
                && !args[2]->is_number_integer()) {
            throw std::runtime_error("findObjects requires (object, string [,int]).");
        }
        inja::json result = inja::json::array();
        std::string query = args[1]->get<std::string>();
    
        int max = args.size() == 3 ? args[2]->get<int>() : 1;
    
        json_query(*args[0], query, [&](const inja::json& object) {
            result.emplace_back(object);
            return result.size() < max;
        });
        return result;
    });

    environment.add_callback("camelCase", [](inja::Arguments& args) {
        if (args.size() != 1) {
            throw std::runtime_error("camelCase requires exactly 1 argment "
                        "but " + std::to_string(args.size()) + " was passed.");
        }
        else if (!args[0]->is_string()) {
            throw std::runtime_error("camelCase requires (string).");
        }
        std::string str = args[0]->get<std::string>();
        std::size_t prev = 0, pos;
        std::vector<std::string> words;

        while ((pos = str.find_first_of(" _-", prev)) != std::string::npos) {
            if (pos > prev) {
                words.emplace_back(str.substr(prev, pos - prev));
            }
            prev = pos + 1;
        }
        if (prev < str.length()) {
            words.emplace_back(str.substr(prev, std::string::npos));
        }
        return join(words, "", [](std::string word, int n) {
            word[0] = n ? std::toupper(word[0]) : std::tolower(word[0]);
            return word;
        });
    });
    // environment.add_callback("findAttribute", [](inja::Arguments& args) {
    //     if (args.size() != 3) {
    //         throw std::runtime_error("findAttribute requires exactly 3 argments "
    //                     "but " + std::to_string(args.size()) + " was passed.");
    //     }
    //     else if (!args[0]->is_array() && !args[1]->is_string()) {
    //         throw std::runtime_error("findAttribute requires (array, string, string).");
    //     }
    //     std::string attrib_name = args[1]->get<std::string>();
    //     std::string result_path = args[2]->get<std::string>();
    //     const char* path = "base/*/attributes/*/name";

    //     auto r = find_objects(*args[0], path, 1, [&](auto&_, const inja::json& n) {
    //         return n.get<std::string>() == attrib_name;
    //     });
    //     if (r.size() == 1) {
    //         if ((r = find_objects(*r[0][1], result_path)).size()) {
    //             return *r[0].back();
    //         }
    //     }
    //     return inja::json();
    // });

    environment.set_lstrip_blocks(true);
    environment.set_trim_blocks(true);

    return environment;
}


inja::json& CodeGenerator::generate_type(const cppast::cpp_entity& entity,
                                        inja::json& parent) {

    inja::json& meta_type = generate_entity(entity, parent);
    meta_type["fqn"] = full_qualified_name(entity);
    
    return meta_type;
}


void CodeGenerator::generate_class(const cppast::cpp_class& entity) {
    
    if (!add_entity(entity)) {
        return;
    }
    inja::json& meta_class = generate_type(entity, _data);

    auto& props_def = meta_class["member_variable"] = inja::json::array();
    auto& funcs_def = meta_class["member_function"] = inja::json::array();
    auto& enums_def = meta_class["enum"]            = inja::json::array();
    auto& bases_def = meta_class["base"]            = inja::json::array();
    auto& alias_def = meta_class["alias"]           = inja::json::array();

    for (const cppast::cpp_base_class& base : entity.bases()) {
        bases_def.emplace_back(full_qualified_name(base));
    }
    cppast::visit(entity, [&](const auto& child, const auto& info) {
        generate_member(entity, child, info, meta_class);
    });
}


void CodeGenerator::generate_enum(const cppast::cpp_enum& entity) {

    if (!add_entity(entity)) {
        return;
    }
    inja::json& meta_enum = generate_type(entity, _data);
    auto& values_def = meta_enum["values"];

    for (const cppast::cpp_enum_value& enum_value : entity) {
        if (!is_unknown_entity(enum_value)) {
            values_def.push_back(enum_value.name());
        }
    }
}

template<class func_type>
void CodeGenerator::register_func(const func_type& entity, inja::json& data) {
    static_assert(std::is_base_of_v<func_entity, func_type>);

    auto& function_def = generate_entity(entity, data);

    auto& params    = function_def["arguments"] = inja::json::array();
    auto& qualifier = function_def["qualifier"] = inja::json::array();

    function_def["return_type"] = cppast::to_string(entity.return_type());
    function_def["signature"] = entity.signature();

    for (const auto& parameter : entity.parameters()) {
        auto& parameter_def = params.emplace_back();
        parameter_def["name"] = parameter.name();
        parameter_def["type"] = cppast::to_string(parameter.type());
    }
    if constexpr (std::is_same_v<func_type, cppast::cpp_member_function>) {
        
        if (cppast::is_const(entity.cv_qualifier())) {
            qualifier.emplace_back("const");
        } if (cppast::is_volatile(entity.cv_qualifier())) {
            qualifier.emplace_back("volatile");
        }
        if (entity.ref_qualifier() == cppast::cpp_ref_lvalue) {
            qualifier.emplace_back("&");
        } else if (entity.ref_qualifier() == cppast::cpp_ref_rvalue) {
            qualifier.emplace_back("&&");
        }
    }
}

void CodeGenerator::register_enum(  const cppast::cpp_enum& entity,
                                    inja::json& data) {
    data["enum"].emplace_back(full_qualified_name(entity));
}

void CodeGenerator::register_vars(  const cppast::cpp_member_variable& entity,
                                    inja::json& data) {
    
    inja::json& var = generate_entity(entity, data);
    var["type"] = cppast::to_string(entity.type());
}

void CodeGenerator::register_alias( const cppast::cpp_type_alias& entity,
                                    inja::json& data) {
    
    inja::json& alias = data["alias"].emplace_back();
    alias["name"] = entity.name();
    alias["underlying_type"] = cppast::to_string(entity.underlying_type());
}


void CodeGenerator::generate_member(const cppast::cpp_entity& entity,
                                    const cppast::cpp_entity& child,
                                    const cppast::visitor_info& info,
                                    inja::json& parent_node) {
    
    if (info.access != cppast::cpp_access_specifier_kind::cpp_public ||
                info.is_old_entity() || cppast::is_templated(child) ||
                child.parent() != entity || is_unknown_entity(child)) {
        return;
    }
    switch (child.kind()) {
        case cppast::cpp_entity_kind::member_variable_t:
            register_vars((const prop_entity&)child, parent_node); break;
        case cppast::cpp_entity_kind::enum_t:
            register_enum((const enum_entity&)child, parent_node); break;
        case cppast::cpp_entity_kind::member_function_t:
            register_func((const member_func&)child, parent_node); break;
        case cppast::cpp_entity_kind::function_t:
            register_func((const static_func&)child, parent_node); break;
        case cppast::cpp_entity_kind::type_alias_t:
            register_alias((const aliasentity&)child, parent_node); break;
        default: break;
    }
}


void CodeGenerator::visite_ast(const cppast::cpp_file& ast_root) {

    _data["enum"]   = inja::json::array();
    _data["class"]  = inja::json::array();

    cppast::visit(ast_root,
    [this](const cppast::cpp_entity& e) {
        return !cppast::is_templated(e) && cppast::is_definition(e);
    },
    [this](const cppast::cpp_entity& entity, const cppast::visitor_info& info) { 

        if (!info.is_new_entity() || info.access != cppast::cpp_public &&
            is_unknown_entity(entity)) {
            return;
        }
        switch(entity.kind()) {
            case cppast::cpp_entity_kind::class_t:
                generate_class((const cppast::cpp_class&)entity); break;
            case cppast::cpp_entity_kind::enum_t:
                generate_enum((const cppast::cpp_enum&)entity); break;
            default: break;
        }
    });
}


bool CodeGenerator::add_entity(const cppast::cpp_entity& entity) {
    return _entites.insert(full_qualified_name(entity)).second;
}


bool CodeGenerator::write_output_file(  const std::filesystem::path& path,
                                        const std::string& output_data) {
    std::ofstream file_stream;
    try {
        std::filesystem::create_directories(path.parent_path());
    }
    catch (const std::system_error& error) {
        if (error.code().value() != int(std::errc::file_exists)) {
            std::cerr << "[error] while creating dir " << error.what() << "\n";
            return false;
        }
    }
    std::ofstream file_output(path);
    return (file_output << std::setw(4) << output_data).good();
}









// bool is_subclass(const cppast::cpp_class& cls, const std::string& name) const {

//     for (const auto& base: cls.bases()) {
//         if (auto cls_ref = cppast::get_class(_index, base)) {
//             if (is_subclass(cls_ref.value(), name)) {
//                 return true;
//             }
//         }
//     }
//     return false;
// }

// bool is_subclass(const cppast::cpp_base_class& cls, const std::string& name) {
    
//     std::cout << "testing " << cls.name() << std::endl;
//     if (!cls.scope_name() && cls.name() == name) {
//         return true;
//     }
//     for (const auto& base: cls.bases()) {
//         if (auto cls_ref = cppast::get_class(_index, base)) {
//             if (is_subclass(cls_ref.value(), name)) {
//                 return true;
//             }
//         }
//     }
//     return false;
// }


// bool CodeGenerator::has_type_handle(const cppast::cpp_class& cls) const {
//     bool result = false;
    
//     cppast::visit(cls, [&result](   const cppast::cpp_entity& child,
//                                     const cppast::visitor_info& info) {
        
//         if (const auto fn = cast<cppast::cpp_function>(child)) {
//             if (info.access == access_spec::cpp_public  &&
//                 fn.value().name() == "get_class_type"   &&
//                 is_type_named(fn.value().return_type(), "TypeHandle")) {
                
//                 result = true;
//             }
//         }
//     });
//     return result;
// }
