// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <type_safe/optional_ref.hpp>
#include <regex>
#include "jsonQuery.h"



std::vector<std::string> split_string(const std::string& input) {
    std::vector<std::string> result(1);

    bool is_escaped = false, in_quotes = false, in_double_quotes = false;

    for (char c : input) {
        if (c == '\\') {
            is_escaped = !is_escaped;
        } else if (c == '.' && !is_escaped && !in_quotes && !in_double_quotes) {
            result.emplace_back();
        } else {
            result.back() += c;
            is_escaped = false;

            if (c == '\'') {
                in_quotes = !in_quotes;
            } else if (c == '\"') {
                in_double_quotes = !in_double_quotes;
            }
        }
    }
    return result;
}

bool _find_objects( const std::vector<std::string>::const_iterator it,
                    const std::vector<std::string>& exp,
                    const inja::json& object, const callback_t& callback);


void json_query(const inja::json& object, const std::string& path,
                const callback_t& callback) {

    std::vector<std::string> components = split_string(path);
    _find_objects(components.begin(), components, object, callback); 
}



bool _find_objects( const std::vector<std::string>::const_iterator it,
                    const std::vector<std::string>& exp,
                    const inja::json& object, const callback_t& callback) {
    

    static const std::regex filter_expr(R"(^(\w+)\[(.+)\]$)");
    static const std::regex indices_expr(R"(^(-?\d+)(?:,(-?\d+))*$)");
    static const std::regex slice_expr(R"(^(-?\d*):(-?\d*):?(-?\d*)$)");
    static const std::regex pred_expr(R"(^\?\(([^=<>!]+)(?:([=<>!]=?)(.+))?\)$)");
    static const std::regex digit_expr(R"(-?\d+.?\d*)");
    
    using pred_t = std::function<bool(const inja::json&, const inja::json&)>;
    static const std::unordered_map<std::string, pred_t> predicates = {
        {"==", [](const auto& a, const auto& b) { return a == b; }},
        {"=",  [](const auto& a, const auto& b) { return a == b; }},
        {"!=", [](const auto& a, const auto& b) { return a != b; }},
        {"<",  [](const auto& a, const auto& b) { return a <  b; }},
        {">",  [](const auto& a, const auto& b) { return a >  b; }},
        {"<=", [](const auto& a, const auto& b) { return a <= b; }},
        {">=", [](const auto& a, const auto& b) { return a >= b; }},
    };
    inja::json::const_iterator found;
    const inja::json* child = nullptr;
    std::string filter_str;
    std::smatch matches;

    const auto get_index = [&](int group, int index = 0, bool wrap = true) {
        if (group < matches.size()) {
            std::string str = matches.str(group);
            if (!str.empty()) {
                index = std::stoi(str);
            }
        }
        int size = int(child->size());
        return !wrap ? index : (index < 0 ? std::max(size + index, 0) :
                                            std::min(index, size));
    };

    const auto traverse = [&](std::vector<std::string>::const_iterator it,
                        type_safe::optional_ref<const inja::json> obj = {}) {
        
        const inja::json& data = obj.value_or(object);
        if (data.is_structured()) {
            return std::all_of(data.begin(), data.end(), [&](auto& item) {
                return _find_objects(it, exp, item, callback);
            });
        }
        return true;
    };

    if (it == exp.cend())               return callback(object);
    else if (*it == "*" || *it == "**") return traverse(std::next(it));

    else if (!std::regex_match(*it, matches, filter_expr)) {
        if (object.is_object() && object.contains(*it)) {
            return _find_objects(std::next(it), exp, object[*it], callback);
        } else {
            goto rec;
        }
    } else if (!object.is_object() || !object.contains(matches.str(1))) {
        goto rec;

    } else if (!(child = &object[matches.str(1)])->is_array()) {
        // nothing to do for non-array children
    } else if ((filter_str = matches.str(2)) == "*") {
        return traverse(std::next(it), *child);
    } 
    else if (std::regex_match(filter_str, matches, indices_expr)) {
        for (int i = 0; i < matches.size(); ++i) {
            if (matches.str(i + 1).empty()) {
                break;
            }
            int id = get_index(i + 1);
            if (!_find_objects(std::next(it), exp, (*child)[id], callback)) {
                return false;
            }
        }
    } else if (std::regex_match(filter_str, matches, slice_expr)) {
        
        int start   = get_index(1, 0);
        int stop    = get_index(2, object.size());
        int step    = get_index(3, 1, false);

        for (int i = std::min(start, stop); i < stop; i+=step) {
            if (!_find_objects(std::next(it), exp, (*child)[i], callback)) {
                return false;
            }
        }
    } else if (std::regex_match(filter_str, matches, pred_expr)) {
        const std::string path = matches.str(1);
        inja::json operand;
        pred_t predicate;
        if (matches.size() >= 3) {
            auto found = predicates.find(matches.str(2));
            if (found != predicates.cend()) {
                std::string value = matches.str(3);
                if (!std::regex_match(value, digit_expr)) {
                    value = '"' + value + '"';
                }
                predicate = found->second;
                operand = inja::json::parse(value);
            } else {
                std::cerr << "Invalid operator: " << filter_str << std::endl;
            }
        }
        bool cont = true;
        for (auto it1 = child->begin(); it1 != child->end() && cont; ++it1) {
            json_query(*it1, path, [&](const inja::json& obj) {
                if (!predicate || predicate(obj, operand)) {
                    cont = _find_objects(std::next(it), exp, *it1, callback);
                }
                return false;
            });
        }
    } else {
rec:    if (it != exp.cbegin() && *std::prev(it) == "**") {
            return traverse(it);
        }
    }
    return true; // continue to search
}
