// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __CODEGENERATOR_H__
#define __CODEGENERATOR_H__

#include <llvm/Support/CommandLine.h>
#include <cppast/cpp_entity.hpp>
#include <cppast/cpp_entity_index.hpp>
#include <cppast/cpp_member_variable.hpp>
#include <cppast/parser.hpp>
#include <cppast/libclang_parser.hpp>

#include <inja/inja.hpp>

#include <filesystem>
#include <unordered_set>

namespace cl = llvm::cl;


class CodeGenerator {

public:
    CodeGenerator(  const std::string&              input_file,
                    cppast::cpp_standard            cpp_standard,
                    cl::list<std::string>&          output_files,
                    cl::list<std::string>&          template_files,
                    cl::list<std::string>&          include_dirs,
                    cl::list<std::string>&          include_header,
                    cl::list<std::string>&          definitions,
                    cl::list<std::string>&          custom_flags,
                    const std::string&              clang_binary);

    ~CodeGenerator() = default;

    bool generate();
    bool is_up_to_date() const;

private:
    struct ClassContext {
        const cppast::cpp_class& entity;

        std::vector<std::string> props;
        std::vector<std::string> bases;
        std::vector<std::string> enums;
        std::vector<std::string> funcs;
    };

    void visite_ast(const cppast::cpp_file& ast_root);


    void generate_entity(const cppast::cpp_entity& entity,
                        const cppast::visitor_info& info);

    void generate_member(   const cppast::cpp_entity& entity,
                            const cppast::cpp_entity& child,
                            const cppast::visitor_info& info,
                            inja::json& data);

    void generate_class(const cppast::cpp_class& entity);
    void generate_enum(const cppast::cpp_enum& entity);

    void register_enum( const cppast::cpp_enum& entity,
                        inja::json& data);

    void register_vars( const cppast::cpp_member_variable& entity,
                        inja::json& data);

    void register_alias(const cppast::cpp_type_alias& entity,
                        inja::json& data);

    template<class func_type>
    void register_func(const func_type& entity, inja::json& data);

    bool add_entity(const cppast::cpp_entity& entity);
    void render_template(const std::filesystem::path& template_path);

    bool write_output_file( const std::filesystem::path& path,
                            const std::string& output_data);

    inja::json& generate_type(const cppast::cpp_entity& entity,
                            inja::json& parent);

    inja::json& generate_entity(const cppast::cpp_entity& entity,
                                inja::json& parent_node);

    inja::Environment create_environment();

    typedef cppast::libclang_parser Parser;
    typedef std::unordered_set<std::string> Entites;
    typedef std::unordered_set<std::string> PropClasses;

    struct ParseEntry {
        std::filesystem::path template_file;
        std::filesystem::path output_file;
    };
    std::vector<ParseEntry>     _entries;

    std::filesystem::path       _input_file;
    Parser::config              _config;
    Entites                     _entites;
    PropClasses                 _prop_classes;
    inja::json                  _data;
};

#endif // __CODEGENERATOR_H__