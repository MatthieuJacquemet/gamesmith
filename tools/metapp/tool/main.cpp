// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <cassert>

#include <cppast/compile_config.hpp>

#include <fmt/format.h>
#include <fmt/ostream.h>
#include <fstream>
#include <functional>
#include <iostream>
#include <llvm/Support/CommandLine.h>
#include <regex>
#include <sstream>
#include <string>
#include <unordered_set>

#include "codeGenerator.h"


int main(int argc, char** argv) {

    cl::opt<std::string> input {
        "s", cl::Prefix, cl::ValueRequired, cl::desc("source file")};
    cl::list<std::string> outputs {
        "o", cl::Prefix, cl::ValueRequired, cl::desc("output files")};
    cl::list<std::string> template_files {
        "t", cl::Prefix, cl::ValueRequired, cl::desc("template files")};
    cl::list<std::string> includes {
        "I", cl::Prefix, cl::ValueOptional, cl::desc("Include directories")};
    cl::list<std::string> headers {
        "i", cl::Prefix, cl::ValueOptional, cl::desc("Include headers")};
    cl::list<std::string> definitions {
        "D", cl::Prefix, cl::ValueOptional, cl::desc("Compile definitions")};
    cl::opt<cppast::cpp_standard> stdversion {
        "std",
        cl::desc("C++ standard"),
        cl::values(
            clEnumValN(cppast::cpp_standard::cpp_98, "c++98", "C++ 1998 standard"),
            clEnumValN(cppast::cpp_standard::cpp_03, "c++03", "C++ 2003 standard"),
            clEnumValN(cppast::cpp_standard::cpp_11, "c++11", "C++ 2011 standard"),
            clEnumValN(cppast::cpp_standard::cpp_14, "c++14", "C++ 2014 standard"),
            clEnumValN(cppast::cpp_standard::cpp_1z, "c++17", "C++ 2017 standard"),
            clEnumValN(cppast::cpp_standard::cpp_20, "c++20", "C++ 2020 standard")
        )};
    cl::opt<std::string>  clang_binary{
        "b", cl::Prefix, cl::ValueOptional, 
        cl::desc("clang++ binary. If not given, meta++ will search in PATH")};

    cl::list<std::string> custom_flags{cl::Sink,
                                       cl::desc("Custom compiler flags")};


    

    if (cl::ParseCommandLineOptions(argc, argv, "meta++ codegen tool")) {
        
        CodeGenerator generator(input, stdversion, outputs, template_files,
                                includes, definitions, headers, custom_flags,
                                clang_binary);

        if ((generator.is_up_to_date() && false) || generator.generate()) {
            return EXIT_SUCCESS;
        }
    }
    return EXIT_FAILURE;
}




