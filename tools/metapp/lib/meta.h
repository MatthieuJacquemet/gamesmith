// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __META_H__
#define __META_H__

#include <panda3d/dtoolbase.h>
#include "metaClass.h"
#include "metaEnum.h"
#include "macro_utils.h"

#include "utils.h"

// void CHECK_MEM(T, init_type)::init_type() {  get_metatype<T>()->init_type(); }                    

// TypeHandle T::get_type() const { return get_class_type(); }                 
// TypeHandle T::get_class_type() { return get_metatype<T>()->get_handle(); }  
// TypeHandle T::force_init_type() { init_type(); return get_class_type(); }



#define GET_META_TYPE(FQN) get_metatype<FQN>()

#define META_MEMBER(NAME) &_current_type::NAME

#define META_BASES(...) meta::filter<MetaClassBase::Bases>(__VA_ARGS__)
#define META_ENUMS(...) meta::filter<MetaClassBase::Enums>(__VA_ARGS__)
#define META_PROPS(...) meta::filter<MetaClassBase::Props>(__VA_ARGS__)

#define ENUM_VALUES(...) MetaEnumBase::EnumValues{__VA_ARGS__}


#define _rename(NEW_NAME, OLD_NAME, ...) NEW_NAME, __VA_ARGS__

#define _reflect(NAME, TYPE, ...)                                           \
    template<> const MetaType<TYPE>* get_metatype<TYPE>() {                 \
        using _current_type = TYPE;                                         \
        static const MetaType<TYPE> metaobject(NAME, __VA_ARGS__);          \
        return &metaobject;                                                 \
    }

#define _call(NAME, ...) NAME(__VA_ARGS__)


#define __meta__enum(NAME, VALUE)           EnumValueSpec(NAME, VALUE)
#define __meta__rename(...)                 _call(_rename,  __VA_ARGS__)
#define __meta__reflect(...)                _call(_reflect, __VA_ARGS__)
#define __meta__property(NAME, MEMBER)      new Property(NAME, MEMBER)
#define __meta__range(MIN, MAX, NAME, MEM)  new Range( _mem_type(MEM)(MIN), \
                                                       _mem_type(MEM)(MAX), \
                                                       NAME, MEM)


#define _decl__meta__get_class_type(_0, _1, CLS)                            \
TypeHandle CLS::get_class_type() {                                          \
    const MetaClass<CLS>* metatype = get_metatype<CLS>();                   \
    nassertr(metatype != nullptr, TypeHandle::none())                       \
    return metatype->get_handle();                                          \
}

#define _decl__meta__force_init_type(_0, _1, CLS)                           \
TypeHandle CLS::force_init_type() {                                         \
    const MetaClass<CLS>* metatype = get_metatype<CLS>();                   \
    nassertr(metatype != nullptr, TypeHandle::none())                       \
    return metatype->init_type();                                           \
}

#define REGISTER_CLASS                                                      \
public:                                                                     \
    [[meta::get_class_type]] static TypeHandle get_class_type();            \
    [[meta::force_init_type]] virtual TypeHandle force_init_type();         \
    virtual TypeHandle get_type() const { return get_class_type(); }        \
private:

#define _mem_type(MEM) decltype(meta::get_member_type(MEM))



namespace meta {


template<class T>
using is_ptr_container = std::is_pointer<typename T::value_type>;

template<class From, class To>
using is_castable = std::is_convertible<From, typename To::value_type>;

template<class T>
using base_type = std::remove_cv_t<std::decay_t<std::remove_pointer_t<T>>>;

template<class C, typename T>
T get_member_type(T C::*);

template<class Container>
inline Container filter(Container ctn) {
    std::remove_if(ctn.begin(), ctn.end(), [](auto* value) {
        return value == nullptr;
    });
    return ctn;
}

}

template<class Type> TypeHandle get_type_handle_impl() {
    if (auto* meta_type = get_metatype<Type>()) {
        return meta_type->get_handle();
    }
    return TypeHandle::none();
}


#endif // __META_H__