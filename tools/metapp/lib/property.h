// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __PROPERTY_H__
#define __PROPERTY_H__

#include <panda3d/bamWriter.h>
#include <macro_utils.h>
#include <variant.h>

#include "metaTypeRegistry.h"
#include "serializerRegistry.h"
#include "metaEnum.h"


class MetaClassBase;


class PropertyBase {

protected:
    PropertyBase(const char* name) : _name(name) {}

public:
    virtual ~PropertyBase() = default;

    enum Attribute {
        A_enum_values, A_readonly, A_enum_flags, A_no_alpha,
        A_add, A_remove, A_base_user = 0x80
    };
    using Attributes = std::map<Attribute, Variant>;

    inline const char* get_name() const { return _name; }

    virtual TypeHandle get_type() const = 0;
    virtual Variant get_value(const TypedObject* object) const = 0;
    virtual void set_value(TypedObject* object, const Variant& value) const = 0;
    virtual Attributes get_attributes() const { return {}; }

private:
    DISABLE_COPY_MOVE(PropertyBase)

    const char* const _name;
};



template<class Type>
class Property: public PropertyBase {

public:
    template<class Object>
    inline Property(const char* name, Type Object::*member);

    inline Type& get_value_ref(TypedObject* object) const;
    inline const Type& get_value_ref(const TypedObject* object) const;

    TypeHandle get_type() const override { return get_type_handle(Type); }
    Variant get_value(const TypedObject* object) const override;
    void set_value(TypedObject* object, const Variant& value) const override;
    Attributes get_attributes() const override;

private:
    const std::function<Type& (TypedObject*)> _member_getter;
};

template<class Type>
class Range final : public Property<Type> {
public:
    template<class Object>
    inline Range(Type min, Type max, const char* name, Type Object::*member);

    enum {
        A_min_value = PropertyBase::Attribute::A_base_user + 1,
        A_max_value
    };
    PropertyBase::Attributes get_attributes() const override {
        return {{A_min_value, _min}, {A_max_value, _max}};
    }
private:
    Type _min, _max;
};

template<class Tuple, size_t Id>
using TypeAt = std::remove_reference_t<decltype(std::get<Id>(std::declval<Tuple>()))>;

template<class Type, size_t... Is>
inline void register_types_impl(std::index_sequence<Is...>) {
    (TypeAt<serializer_types, Is>::template register_type<Type>(), ...);
}

template<class Type>
inline void register_types() {
    constexpr size_t tuple_size = std::tuple_size_v<serializer_types>;
    register_types_impl<Type>(std::make_index_sequence<tuple_size>{});
}

template<class Type>
template<class Object>
inline Property<Type>::Property(const char* name, Type Object::*member):
    PropertyBase(name),
    _member_getter([member](TypedObject* obj) -> Type& {
        return static_cast<Object*>(obj)->*member;
    }) {
    register_types<Type>();
}

template<class Type>
inline Type& Property<Type>::get_value_ref(TypedObject* object) const {
    return _member_getter(object);
}

template<class Type>
inline const Type& Property<Type>::get_value_ref(const TypedObject* object) const {
    return _member_getter(const_cast<TypedObject*>(object));
}

template<class Type>
inline Variant Property<Type>::get_value(const TypedObject* object) const {
    return _member_getter(object);
}

template<class Type>
void Property<Type>::set_value(TypedObject* object, const Variant& value) const {
    _member_getter(object) = value.get_value<Type>();
}

template<class Type>
PropertyBase::Attributes Property<Type>::get_attributes() const {
    PropertyBase::Attributes attributes;
    using names_t = std::vector<std::string>;
    using values_t = std::vector<int64_t>;

    if constexpr (std::is_enum_v<Type>) {
        if (MetaEnumBase* meta = MetaTypeRegistry::get_type<Type>()) {
            using list_t = std::vector<std::pair<std::string, int64_t>>;
            auto& attrib = attributes[Attribute::A_enum_values];

            list_t& items = attrib.emplace<list_t>();
            items.reserve(meta->get_values().size());

            for (const auto& spec : meta->get_values()) {
                items.emplace_back(spec.get_name(), spec.get_value());
            }
        }
    }
    return attributes;
}

template<class Type>
template<class Object>
inline Range<Type>::Range(Type min, Type max, const char* name, Type Object::*member):
    Property<Type>(name, member),
    _min(min), _max(max) {
    
}

#endif // __PROPERTY_H__