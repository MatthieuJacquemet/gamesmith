// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __METATYPEREGISTRY_H__
#define __METATYPEREGISTRY_H__

#include <panda3d/typeHandle.h>
#include <types_ext.h>
#include <unordered_map>
#include <macro_utils.h>
#include <typeHandler.h>

class MetaEnumBase;
class MetaClassBase;
class AbstractType;

template<class T> class MetaClass;
template<class T> class MetaEnum;

template<class T, class R = void>
using if_enum = std::enable_if_t<std::is_enum_v<T>, R>;

template<class T, class R = void>
using if_not_enum = std::enable_if_t<!std::is_enum_v<T>, R>;

template<class T>
using MetaType = std::conditional_t<std::is_enum_v<T>, MetaEnum<T>, MetaClass<T>>;



template<class T> inline const MetaType<T>* get_metatype() {
    return nullptr;
}



class AbstractType {
public:
    inline const char* get_name() const { return _name; }
    inline TypeHandle get_handle() const { return _type_handle; };

    virtual const MetaEnumBase* as_enum() const;
    virtual const MetaClassBase* as_class() const;
    virtual TypeHandle init_type() const = 0;

protected:
    inline AbstractType(const char* name):
        _name(name),
        _type_handle(TypeHandle::none()) {
    }

    DISABLE_COPY_MOVE(AbstractType)
    const char* const _name;
    mutable TypeHandle _type_handle;
};



class MetaTypeRegistry {
public:
    static inline const MetaClassBase* get_meta_class(TypeHandle handle);
    static inline const MetaEnumBase* get_meta_enum(TypeHandle handle);
    
    static inline void register_type(TypeHandle handle,
                                    const AbstractType* type);

    template<class Type>
    static constexpr const MetaType<Type>* get_type() {
        return static_cast<const MetaType<Type>*>(get_metatype<Type>());
    }
private:
    MetaTypeRegistry() = delete;

    typedef TypeHandler<const AbstractType*> TypeMapping;
    static TypeMapping _mapping;
};



inline const MetaClassBase* MetaTypeRegistry::get_meta_class(TypeHandle handle) {
    
    if (const auto* type = _mapping.get_handler(handle)) {
        return type->as_class();
    } else {
        return static_cast<MetaClassBase*>(nullptr);
    }
}

inline const MetaEnumBase* MetaTypeRegistry::get_meta_enum(TypeHandle handle) {
    
    if (const auto* abstract_type = _mapping.get_handler(handle)) {
        return abstract_type->as_enum();
    } else {
        return static_cast<MetaEnumBase*>(nullptr);
    }
}

inline void MetaTypeRegistry::register_type(TypeHandle handle,
                                            const AbstractType* type) {
    _mapping.set_handler(handle, type);
}


#endif // __METATYPEREGISTRY_H__