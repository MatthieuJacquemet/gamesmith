// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of gdk.
// 
// gdk is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// gdk is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with gdk.  If not, see <http://www.gnu.org/licenses/>.

#include "variant.h"


void Variant::write_datagram(BamWriter* manager, Datagram& dg) {
    dg.add_int32(_type_handle.get_index());
    if (LIKELY(_value)) _value->write_datagram(dg);
}


void Variant::fillin(DatagramIterator& scan, BamReader* manager) {

    _type_handle = TypeHandle::from_index(scan.get_int32());
    auto& factory = get_factory();

    Factory::const_iterator it = factory.find(_type_handle);
    if (LIKELY(it != factory.end())) {
        _value.reset(it->second(scan));
    } 
    else if (_type_handle != TypeHandle::none()) {
         nassert_raise("No variant factory registered for type index: "
                        + detail::get_type_name(_type_handle));
    }
}



std::ostream& operator<<(std::ostream& os, const Variant& variant) {
    os << variant.get_type();
    return os;
}

void register_legacy_variant_types() {

    Variant::register_type_factory<int8_t>();
    Variant::register_type_factory<int16_t>();
    Variant::register_type_factory<int32_t>();
    Variant::register_type_factory<int64_t>();

    Variant::register_type_factory<uint8_t>();
    Variant::register_type_factory<uint16_t>();
    Variant::register_type_factory<uint32_t>();
    Variant::register_type_factory<uint64_t>();

    Variant::register_type_factory<float>();
    Variant::register_type_factory<double>();
    Variant::register_type_factory<bool>();
    Variant::register_type_factory<char>();
    Variant::register_type_factory<std::string>();

    Variant::register_type_factory<LVecBase2f>();
    Variant::register_type_factory<LVecBase2d>();
    Variant::register_type_factory<LVecBase2i>();

    Variant::register_type_factory<LVecBase3f>();
    Variant::register_type_factory<LVecBase3d>();
    Variant::register_type_factory<LVecBase3i>();

    Variant::register_type_factory<LVecBase4f>();
    Variant::register_type_factory<LVecBase4d>();
    Variant::register_type_factory<LVecBase4i>();

    Variant::register_type_factory<LMatrix3f>();
    Variant::register_type_factory<LMatrix4f>();
    Variant::register_type_factory<Color>();
}
