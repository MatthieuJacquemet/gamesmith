// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "meta.h"
#include "xmlSerializer.h"

XMLSerializer::AccessorMap XMLSerializer::_accessors;


#define REGISTER_READ_FUNCTION(TYPE, FUNCTION)                          \
inline int read_value(const TiXmlElement* element, TYPE& value) {       \
    return element->FUNCTION("value", &value);                          \
}



XMLSerializer::XMLSerializer(std::iostream& stream):
    _stream(stream) {

}


XMLSerializer::~XMLSerializer() {

}


void XMLSerializer::deserialize(TypedObject* obj, const PropertyBase* prop) {

    const TiXmlElement* element = find_element(prop->get_name());

    if (UNLIKELY(element == nullptr)) {
        return;
    }
    const auto& accessor = _accessors.get_handler(prop->get_type());
    if (accessor.read)  {
        accessor.read(prop, element, obj);
    } else {
        std::cerr << "Cannot find accessor for " << prop->get_name() << std::endl;
    }
}


void XMLSerializer::serialize(const TypedObject* obj, const PropertyBase* prop) {
    
    TiXmlElement* root = _document->RootElement();
    TiXmlElement* node = new TiXmlElement("property");

    const auto& accessor = _accessors.get_handler(prop->get_type());
    if (accessor.write)  {
        node->SetAttribute("name", prop->get_name());
        accessor.write(prop, node, obj);
        root->LinkEndChild(node);
    } else {
        std::cerr << "Cannot find accessor for " << prop->get_name() << std::endl;
    }
}


void XMLSerializer::deserialize_object(TypedObject* object) {
    
    _document.reset(new TiXmlDocument);
    _stream >> *_document;

    if (UNLIKELY(_stream.fail() && !_stream.eof())) {
        std::cerr << "Error parsing XML document" << std::endl;
        _document.reset();
    }
    else if (_document->Error()) {
        std::cerr << "Failled to parse XML file : line"
            << _document->ErrorRow() << " col " << _document->ErrorCol()
            << " : " << _document->ErrorDesc() << std::endl;
    } else {
        SerialInterface::deserialize_object(object);
        _cache.clear();
    }
}


void XMLSerializer::serialize_object(const TypedObject* object) {

    if (UNLIKELY(_stream.fail())) {
        return;
    }
    _document.reset(new TiXmlDocument);

    TiXmlElement* root = new TiXmlElement("object");
    root->SetAttribute("class", object->get_type().get_name());

    _document->LinkEndChild(new TiXmlDeclaration("1.0", "UTF-8", ""));
    _document->LinkEndChild(root);

    SerialInterface::serialize_object(object);

    TiXmlPrinter printer;
    _document->Accept(&printer);

    _stream << printer.CStr();
}


const TiXmlElement* XMLSerializer::find_element(const std::string& name) const {

    Cache::const_iterator it;
    if (LIKELY((it = _cache.find(name)) != _cache.end())) {
        return it->second;
    }
    const TiXmlElement* root = _document->RootElement();
    const TiXmlNode* child_node = nullptr;

    while (child_node = root->IterateChildren(child_node)) {
        const TiXmlElement* element = child_node->ToElement();
        
        if (element == nullptr || element->ValueStr() != "property") {
            continue;
        } else if (const char* ename = element->Attribute("name")) {
            if (name == ename) {
                _cache.insert(std::make_pair(name, element));
                return element;
            }
        } else {
            std::cerr << "Property at line: " << element->Row()
                    << "has no name attribute" << std::endl;
        }
    }
    return static_cast<TiXmlElement*>(nullptr);
}