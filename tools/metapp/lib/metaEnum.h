// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __METAENUM_H__
#define __METAENUM_H__

#include <vector>
#include "metaTypeRegistry.h"



class EnumValueSpec final {
public:
    inline constexpr EnumValueSpec(const char* name, int64_t value):
        _name(name), _value(value) {}

    inline const char* get_name() const { return _name; }
    inline int64_t get_value() const { return _value; }
private:    
    const char* const   _name;
    const int64_t       _value;
};


class MetaEnumBase: public AbstractType {
public:
    typedef std::vector<EnumValueSpec> EnumValues;

    inline MetaEnumBase(const char* name, const EnumValues& values):
        AbstractType(name), _values(values) {}

    const EnumValues& get_values() const { return _values; }

    virtual const MetaEnumBase* as_enum() const override;
private:
    const EnumValues _values;
};


template<class Type>
class MetaEnum final : public MetaEnumBase {
public:
    MetaEnum(const char* name, const EnumValues& values);

    virtual TypeHandle init_type() const override final;
};


template<class Type>
MetaEnum<Type>::MetaEnum(const char* name, const EnumValues& values):
    MetaEnumBase(name, values) {
    MetaEnum<Type>::init_type();
}


template<class Type> TypeHandle MetaEnum<Type>::init_type() const {

    TypeRegistry* registry = TypeRegistry::ptr();

    if (registry->register_type(_type_handle, _name)) {
        MetaTypeRegistry::register_type(_type_handle, this);
    } else {
        std::cerr << "Unable to register type " << _name << std::endl;
    }
    return _type_handle;
}





template<class Type>
inline void get_enum_by_name(const std::string& name, Type& value) {
    using Values = MetaEnumBase::EnumValues;
    
    static_assert(std::is_enum_v<Type>, "Type must ban enumeration");

    if (const MetaEnum<Type>* metaclass = get_metatype<Type>()) {
        const Values& values = metaclass->get_values();

        auto it = std::find_if(values.begin(), values.end(),
            [&name](const EnumValueSpec& spec) {
                return name == spec.get_name();
        });
        if (UNLIKELY(it == values.end())) {
            std::cerr << "No value named " << name << " in "
                        << metaclass->get_name() << std::endl;
        } else {
            value = static_cast<Type>(it->get_value());
        }
    } else {
        std::cerr << "Trying to read unregistered enum" << std::endl;
    }
}


template<class Type>
inline const char* get_enum_by_value(Type value) {
    using Values = MetaEnumBase::EnumValues;
    
    static_assert(std::is_enum_v<Type>, "Type must ban enumeration");

    if (const MetaEnum<Type>* metaclass = get_metatype<Type>()) {
        const Values& values = metaclass->get_values();

        auto it = std::find_if(values.begin(), values.end(),
            [value](const EnumValueSpec& spec) {
                return spec.get_value() == value;
        });
        if (UNLIKELY(it == values.end())) {
            std::cerr << "No enum with value " << value << " in "
                        << metaclass->get_name() << std::endl;
        } else {
            return it->get_name();
        }
    } else {
        std::cerr << "Trying to read unregistered enum" << std::endl;
    }
    return static_cast<const char*>(nullptr);
}


template<class Type>
inline if_enum<Type, std::ostream&> operator << (std::ostream& out,
                                                        Type value) {
    out << get_enum_by_value(value);
    return out;
}

template<class Type>
inline if_enum<Type, std::istream&> operator >> (std::istream& in,
                                                        Type& value) {
    std::string name;
    in >> name;
    get_enum_by_name(name, value);
    return in;
}

#endif // __METAENUM_H__