// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <panda3d/bamWriter.h>
#include <panda3d/bamReader.h>
#include <panda3d/datagramIterator.h>

#include "meta.h"
#include "bamSerializer.h"


BamSerializer::AccessorMap BamSerializer::_accessors;



BamSerializer::BamSerializer(Datagram& datagram):
    _output(datagram), _mode(M_writing) {
    
}

BamSerializer::BamSerializer(DatagramIterator& datagram):
    _input(datagram), _mode(M_reading) {
    
}


void BamSerializer::deserialize(TypedObject* obj, const PropertyBase* prop) {
    nassertv(_mode == M_reading)
    
    const auto& accessor = _accessors.get_handler(prop->get_type());
    if (accessor.read) {
        accessor.read(prop, _input, obj);
    }
}

void BamSerializer::serialize(const TypedObject* obj, const PropertyBase* prop) {
    nassertv(_mode == M_writing)

    const auto& accessor = _accessors.get_handler(prop->get_type());
    if (accessor.write) {
        accessor.write(prop, _output, obj);
    }
}

