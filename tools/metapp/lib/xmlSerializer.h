// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __XMLSERIALIZER_H__
#define __XMLSERIALIZER_H__

#include <tinyxml.h>

#include "serialInterface.h"
#include "utils.h"
#include "typeHandler.h"
#include "metaEnum.h"


class TiXmlDocument;
class TiXmlElement;


class XMLSerializer final : public SerialInterface {

public:
    XMLSerializer(std::iostream& stream);
    ~XMLSerializer();

    void deserialize(TypedObject* obj, const PropertyBase* prop) override;
    void serialize(const TypedObject* obj, const PropertyBase* prop) override;

    void deserialize_object(TypedObject* object) override;
    void serialize_object(const TypedObject* object) override;

    template<class Type>
    static inline void register_type();

private:
    const TiXmlElement* find_element(const std::string& name) const;

    std::unique_ptr<TiXmlDocument>  _document = nullptr;
    std::iostream&                  _stream;

    typedef std::function<void (const PropertyBase*,
                                const TiXmlElement*,
                                const TypedObject*)> RWFunction;
    
    struct Accessor {
        RWFunction  read;
        RWFunction  write;
    };
    typedef std::unordered_map<std::string, const TiXmlElement*> Cache;
    typedef TypeHandler<Accessor> AccessorMap;

    mutable Cache _cache;
    static AccessorMap _accessors;
};

#include "xmlSerializer.T"

#endif // __XMLSERIALIZER_H__