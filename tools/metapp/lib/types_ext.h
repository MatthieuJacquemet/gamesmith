// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of gdk.
// 
// gdk is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// gdk is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with gdk.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __TYPES_EXT_H__
#define __TYPES_EXT_H__

#include <panda3d/register_type.h>

#if defined(__GNUC__) || defined(__clang__) || defined(__INTEL_COMPILER)
    #define NEVER_INLINE __attribute__((noinline))
#elif defined(_MSC_VER)
    #define NEVER_INLINE __declspec(noinline)
#else
    #define NEVER_INLINE
#endif

namespace detail {

    template<class Type>
    TypeHandle get_type_handle_impl(const char* name) {
        static TypeHandle type_handle;
        if (UNLIKELY(type_handle == TypeHandle::none())) {
            register_type(type_handle, name);
        }
        return type_handle;
    }

    template<class Type>
    NEVER_INLINE TypeHandle get_type_handle_proxy() {
        return _get_type_handle((Type*)0);
    }

    extern std::string get_type_name(TypeHandle handle);
}


#define REGISTER_TYPE_HANDLE(TYPE)                               \
template<> INLINE TypeHandle _get_type_handle(const TYPE*) {     \
    return detail::get_type_handle_impl<TYPE>(#TYPE);            \
}


REGISTER_TYPE_HANDLE(int8_t)
REGISTER_TYPE_HANDLE(uint64_t)

#undef get_type_handle
#define get_type_handle(TYPE) detail::get_type_handle_proxy<TYPE>()


#endif // __TYPES_EXT_H__