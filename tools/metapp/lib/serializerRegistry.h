// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __SERIALIZERREGISTRY_H__
#define __SERIALIZERREGISTRY_H__

#include <tuple>
#include <panda3d/lvector2.h>
#include <panda3d/lvector3.h>
#include <panda3d/lvector4.h>

#include "utils.h"
#include "xmlSerializer.h"
#include "bamSerializer.h"


// register new serializers here
using serializer_types = std::tuple<BamSerializer, XMLSerializer>;    

#endif // __SERIALIZERREGISTRY_H__