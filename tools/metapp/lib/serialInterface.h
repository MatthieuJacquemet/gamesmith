// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __SERIALINTERFACE_H__
#define __SERIALINTERFACE_H__

#include <panda3d/typedObject.h>

class MetaClassBase;
class PropertyBase;

class SerialInterface {

public:
    virtual void deserialize_object(TypedObject* object);
    virtual void serialize_object(const TypedObject* object);

protected:
    SerialInterface() = default;
    virtual ~SerialInterface() = default;

    virtual void deserialize(TypedObject* obj, const PropertyBase* prop) = 0;
    virtual void serialize(const TypedObject* obj, const PropertyBase* prop) = 0;
};


#endif // __SERIALINTERFACE_H__