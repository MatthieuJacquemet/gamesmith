// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "meta.h"
#include "serialInterface.h"


void SerialInterface::deserialize_object(TypedObject* object) {

    auto* metaclass = MetaTypeRegistry::get_meta_class(object->get_type());

    if (UNLIKELY(metaclass == nullptr)) {
        std::cerr << "No metaclass registered for " << object->get_type()
                << std::endl;
        return;
    }
    for (const PropertyBase* prop: metaclass->get_properties()) {
        deserialize(object, prop);
    }
}

void SerialInterface::serialize_object(const TypedObject* object) {
    
    auto* metaclass = MetaTypeRegistry::get_meta_class(object->get_type());

    if (UNLIKELY(metaclass == nullptr)) {
        std::cerr << "No metaclass registered for " << object->get_type()
                << std::endl;
        return;
    }
    for (const PropertyBase* prop: metaclass->get_properties()) {
        serialize(object, prop);
    }
}