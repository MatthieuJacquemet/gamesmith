// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <iostream>
#include "metaClass.h"


// REGISTER_POD_META(int)
// REGISTER_POD_META(char)
// REGISTER_POD_META(std::string)


const MetaClassBase* MetaClassBase::as_class() const {
    return this;
}


MetaClassBase::MetaClassBase(   const char* name,
                                const Bases& bases,
                                const Enums& enums,
                                const Props& props):
    AbstractType(name), _bases(bases), _enums(enums), _props(props) {
    
}

MetaClassBase::~MetaClassBase() {
    for (const PropertyBase* prop : _props) {
        delete prop;
    }
}

