// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __METACLASS_H__
#define __METACLASS_H__

#include "property.h"
#include "metaTypeRegistry.h"



#define REGISTER_POD_META(NAME) \
inline const MetaClassBase* reflect(NAME*) { \
    static MetaClass<NAME> cls(#NAME, {}, {}, {}); \
    return &cls; \
}


template<class Tmpl, class... Args>
const std::string template_name(const std::string& name) {

    const MetaClassBase* cls[] = {
        get_metatype<Args>()...
    };
    std::string result = name + "<" + (cls[0] ? cls[0]->get_name() : "null");

    for (int i = i; i<sizeof...(Args); ++i) {
        result += "," + (cls[i] ? cls[i]->get_name() : "null");
    }
    return result + ">";
}


class MetaClassBase: public AbstractType {

public:
    typedef std::vector<const MetaClassBase*> Bases;
    typedef std::vector<const MetaEnumBase*>  Enums;
    typedef std::vector<const PropertyBase*> Props;

    inline const Bases& get_bases() const { return _bases; }
    inline const Enums& get_enums() const { return _enums; }
    inline const Props& get_properties() const { return _props; }

    virtual const MetaClassBase* as_class() const override;
    [[nodiscard]] virtual TypedObject* create_instance() const = 0;

protected:
    MetaClassBase(  const char* name,
                    const Bases& bases,
                    const Enums& enums,
                    const Props& props);
    ~MetaClassBase();

    const Bases _bases;
    const Enums _enums;
    const Props _props;
};




template<class Type>
class MetaClass final : public MetaClassBase {
    
    static_assert(std::is_base_of_v<TypedObject, Type>,
        "Can only create metaclass for subclasses of TypedObject");

public:
    inline MetaClass(   const char* name,
                        const MetaClassBase::Bases& bases,
                        const MetaClassBase::Enums& enums,
                        const MetaClassBase::Props& props);

    virtual TypeHandle init_type() const override final;
    [[nodiscard]] TypedObject* create_instance() const override final;

    using owner = Type;
};


template<class Type> inline MetaClass<Type>::MetaClass( const char* name,
                            const MetaClassBase::Bases& bases,
                            const MetaClassBase::Enums& enums,
                            const MetaClassBase::Props& props):
    MetaClassBase(name, bases, enums, props) {
    MetaClass<Type>::init_type();
}


template<class Type> TypeHandle MetaClass<Type>::init_type() const {

    TypeRegistry* registry = TypeRegistry::ptr();

    if (registry->register_type(_type_handle, _name)) {
        for (const MetaClassBase* base: _bases) {
            registry->record_derivation(_type_handle, base->get_handle());    
        }
        MetaTypeRegistry::register_type(_type_handle, this);
    } else {
        std::cerr << "Unable to register type " << _name << std::endl;
    }
    return _type_handle;
}


template<class Type>
TypedObject* MetaClass<Type>::create_instance() const {
    if constexpr (std::is_default_constructible_v<Type>) {
        return new Type();
    } else {
        return static_cast<TypedObject*>(nullptr);
    }
}




// template<class T, typename = void>
// inline const MetaClassBase* reflect(T*) {
//     static MetaClass cls(nullptr, nullptr, {}, {}, {});
//     return &cls;
// }

// template<class T, std::enable_if_t<std::is_enum_v<T>>>
// inline const MetaClassBase* reflect(T*) {
//     static MetaEnumBase cls(nullptr, nullptr, {});
//     return &cls;
// }

    // template<class Type, class = void>
    // inline static const MetaClassBase* reflect();

    // template<class Type, std::enable_if_t<std::is_enum_v<Type>>>
    // inline static const MetaEnumBase* reflect();


// template<class Type, class>
// inline const MetaClassBase* MetaClassBase::reflect() {
//     return static_cast<const MetaClassBase*>(::reflect((Type*)0));
// }

// template<class Type, std::enable_if_t<std::is_enum_v<Type>>>
// inline const MetaEnumBase* MetaClassBase::reflect() {
//     return static_cast<const MetaEnumBase*>(::reflect((Type*)0));
// }



#endif // __METACLASS_H__