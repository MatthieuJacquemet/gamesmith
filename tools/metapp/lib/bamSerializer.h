// Copyright 2022 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __BAMSERIALIZER_H__
#define __BAMSERIALIZER_H__

#include <functional>
#include "typeHandler.h"
#include "serialInterface.h"


class BamSerializer : public SerialInterface {

public:
    BamSerializer(Datagram& datagram);
    BamSerializer(DatagramIterator& datagram);

    ~BamSerializer() = default;

    template<class Type>
    inline static void register_type();

protected:
    void deserialize(TypedObject* obj, const PropertyBase* prop) override;
    void serialize(const TypedObject* obj, const PropertyBase* prop) override;

    union {
        std::reference_wrapper<Datagram>            _output;
        std::reference_wrapper<DatagramIterator>    _input;
    };
    enum { M_reading, M_writing } _mode;

    typedef std::function<void (const PropertyBase*,
                                DatagramIterator& ,
                                const TypedObject*)> RFunction;

    typedef std::function<void (const PropertyBase*, Datagram& ,
                                const TypedObject*)> WFunction;
    struct Accessor {
        RFunction  read;
        WFunction  write;
    };
    typedef TypeHandler<Accessor> AccessorMap;
    static AccessorMap _accessors;
};

#include "bamSerializer.T"

#endif // __BAMSERIALIZER_H__