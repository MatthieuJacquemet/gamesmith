// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of gdk.
// 
// gdk is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// gdk is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with gdk.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __VARIANT_H__
#define __VARIANT_H__

#include <panda3d/register_type.h>
#include <panda3d/typeHandle.h>
#include <panda3d/pnotify.h>
#include <panda3d/typedWritable.h>
#include <panda3d/datagramIterator.h>
#include <panda3d/luse.h>
#include <memory>
#include <unordered_map>

#include "utils.h"
#include "color.h"
#include "bamSerializable.h"
#include "types_ext.h"


class Variant final : public TypedWritable {

public:
    struct ValueBase {
        virtual ~ValueBase() = default;
        virtual ValueBase* make_copy() = 0;
        virtual void write_datagram(Datagram& datagram) const = 0;
        virtual void read_datagram(DatagramIterator& scan) = 0;
    };

    typedef ValueBase* (*CreateFunction)(DatagramIterator&);
    typedef hash_map<TypeHandle, CreateFunction> Factory;

    template<class T> struct Value final: ValueBase {
        T _value;

        template<class... Args>
        Value(Args&&... args): _value(std::forward<Args>(args)...) {}

        explicit Value(DatagramIterator& scan) {
            generic_read_datagram(_value, scan);
        }
        ValueBase* make_copy() override { return new Value(_value); }
        void write_datagram(Datagram& datagram) const override {
            generic_write_datagram(datagram, _value);
        }
        void read_datagram(DatagramIterator& scan) override {
            generic_read_datagram(_value, scan);
        }
    };
    
    Variant() = default;
    Variant(const Variant& other) { *this = other; }
    Variant(Variant&& other) { *this = std::move(other); } 

    TypeHandle get_type() const override { return _type_handle; }
    
    void write_datagram(BamWriter* manager, Datagram& dg) override;
    void fillin(DatagramIterator& scan, BamReader* manager) override;

    template<class DataType> Variant(const DataType& value) {
        *this = value;
    }
    template<class DataType> Variant(DataType&& value) {
        *this = std::move(value);
    }
    template<class DataType>
    Variant& operator=(const DataType& value) {
        TypeHandle type_handle = get_type_handle(DataType);
        nassertr(type_handle, *this);
        _type_handle = type_handle;
        _value.reset(new Value<DataType>{value});
        return *this;
    }
    template<class DataType>
    Variant& operator=(DataType&& value) {
        TypeHandle type_handle = get_type_handle(DataType);
        nassertr(type_handle, *this);
        _type_handle = type_handle;
        _value.reset(new Value<DataType>{std::move(value)});
        return *this;
    }
    Variant& operator=(const Variant& other) {
        _type_handle = other._type_handle;
        if (other._value) _value.reset(other._value->make_copy());
        return *this;
    }
    Variant& operator=(Variant&& other) {
        _type_handle = std::exchange(other._type_handle, TypeHandle::none());
        _value = std::move(other._value);
        return *this;
    }
    
    template<class DataType>
    inline const DataType& get_value(const DataType& def = DataType()) const {
        TypeHandle type_handle = get_type_handle(DataType);
        nassertr(type_handle, def);
        if (LIKELY(_type_handle == type_handle)) {
            return static_cast<Value<DataType>*>(_value.get())->_value;
        }
        return def;
    }
    template<class DataType>
    inline DataType& get_value_ref() {
        TypeHandle type_handle = get_type_handle(DataType);
        nassertr(type_handle, get_default<DataType>());
        if (LIKELY(_type_handle == type_handle)) {
            return static_cast<Value<DataType>*>(_value.get())->_value;
        }
        return get_default<DataType>();
    }
    template<class DataType>
    inline DataType&& take_value(const DataType& def = DataType()) {
        TypeHandle type_handle = get_type_handle(DataType);
        nassertr(type_handle, def);
        if (LIKELY(_type_handle == type_handle)) {
            auto* ptr = static_cast<Value<DataType>*>(_value.get());
            DataType value = std::move(ptr->_value);
            _type_handle = TypeHandle::none(); _value.reset();
            return std::move(value);
        }
        return def;
    }
    template<class DataType, class... Args>
    DataType& emplace(Args&&... args) {
        TypeHandle type_handle = get_type_handle(DataType);
        nassertr(type_handle, get_default<DataType>());
        if (LIKELY(_type_handle == type_handle)) {
            _type_handle = type_handle;
            _value.reset(new Value<DataType>(std::forward<Args>(args)...));
        }
        return get_value_ref<DataType>();
    }
    template<class DataType>
    inline static void register_type_factory() {
        TypeHandle type_handle = get_type_handle(DataType);
        nassertv(type_handle);
        auto create = CreateFunction([](DatagramIterator& scan) {
            return static_cast<ValueBase*>(new Value<DataType>(scan));
        });
        get_factory().insert(std::make_pair(type_handle, create));
    }

    static Factory& get_factory() {
        static Factory factory; return factory;
    };

private:
    template<class DataType>
    static DataType& get_default() noexcept {
        static DataType default_value; return default_value;
    }
    TypeHandle _type_handle = TypeHandle::none();
    std::unique_ptr<ValueBase> _value;
};


extern void register_legacy_variant_types();


std::ostream& operator<<(std::ostream& os, const Variant& variant);

#endif // __VARIANT_H__