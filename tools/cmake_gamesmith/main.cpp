// Copyright (C) 2022 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QSettings>
#include <QFileInfo>
#include <iostream>


int main(int argc, char** argv) {

    if (argc == 1) {
        std::cerr << "Invalid number of argument" << std::endl;
        return EXIT_FAILURE;
    }
    QString path(argv[1]);

    if (!QFileInfo::exists(path)) {
        std::cerr << "No such file or directory" << std::endl;
    }
    QSettings settings(path, QSettings::IniFormat);

    for (const QString& key: settings.allKeys()) {
        QString value = settings.value(key).toString();
        std::cout << key.toStdString() << " : " << value.toStdString() << std::endl;;
    }

    return EXIT_SUCCESS;
}