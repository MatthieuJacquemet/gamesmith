
function(load_packages)

    # string(REPLACE ";" " " MODULES ${ARGN})

    # get_property(TARGETS_BEFORE DIRECTORY "${CMAKE_SOURCE_DIR}" PROPERTY IMPORTED_TARGETS)

    # find_package(${PKG_NAME} REQUIRED ${ARGN})

    # get_property(TARGETS_AFTER DIRECTORY "${CMAKE_SOURCE_DIR}" PROPERTY IMPORTED_TARGETS)

    # list(REMOVE_ITEM TARGETS_AFTER ${TARGETS_BEFORE})
    # set(PACKAGE_LIBRARIES ${TARGETS_AFTER} PARENT_SCOPE)


    # Read the JSON file.
    file(READ ${CMAKE_CURRENT_SOURCE_DIR}/manifest.json MANIFEST_STRING)

    string(JSON DEPS GET ${MANIFEST_STRING} dependencies)
    string(JSON NUM_DEPS LENGTH ${DEPS})
    math(EXPR NUM_DEPS "${NUM_DEPS} - 1")

    # Loop through each dependency of the JSON array
    foreach(IDX RANGE ${NUM_DEPS})
        string(JSON PKG_ID MEMBER ${DEPS} ${IDX})    
        string(JSON PKG_VER GET ${DEPS} ${PKG_ID})

    endforeach()


endfunction()