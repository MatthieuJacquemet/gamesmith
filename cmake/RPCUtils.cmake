include(MetaPlusPlus)


macro(get_directories VAR)
    foreach(FILE ${ARGN})
        get_filename_component(DIR ${FILE} DIRECTORY)
        list(APPEND ${VAR} "${DIR}")
    endforeach()
    list(REMOVE_DUPLICATES ${VAR})
endmacro()


function(enable_coroutine target_name)

    if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU") # GCC
        target_compile_options(${target_name} PUBLIC -fcoroutines)

    elseif (CMAKE_CXX_COMPILER_ID STREQUAL "Clang" OR CMAKE_CXX_COMPILER_ID STREQUAL "AppleClang")
        if (MSVC)
            # clang-cl behaves like MSVC and enables coroutines automatically when C++20 is enabled
        else()
            target_compile_options(${target_name} PUBLIC -fcoroutines-ts)
        endif()
    elseif (CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
        # MSVC auto-enables coroutine support when C++20 is enabled
    else()
        message(FATAL_ERROR "Compiler ${CMAKE_CXX_COMPILER_ID} is not currently supported.")
    endif()

endfunction()



function(add_generated_interfaces TARGET PATTERN)

    set(SOURCES ${ARGN})
    list(FILTER SOURCES INCLUDE REGEX ".+\.${PATTERN}$")

    get_directories(GENERATED_SOURCES ${SOURCES})

    target_include_directories(${TARGET} PUBLIC 
        "$<BUILD_INTERFACE:${GENERATED_SOURCES}>"
    )
    add_custom_target(_metaTarget DEPENDS ${SOURCES})
    add_dependencies(${TARGET} _metaTarget)

endfunction()



function(add_generated_files TARGET PATTERN)

    set(SOURCES ${ARGN})
    list(FILTER SOURCES INCLUDE REGEX ".+\.${PATTERN}$")
    string(MD5 METATARGET "${SOURCES}${PATTERN}")

    add_library(${METATARGET} OBJECT ${SOURCES})

    get_directories(GENERATED_SOURCES ${SOURCES})

    set_target_properties(${METATARGET} PROPERTIES LINKER_LANGUAGE CXX)
    get_target_property(INCLUDE_DIRS ${TARGET} INCLUDE_DIRECTORIES)
    get_target_property(COMPILE_FLAG ${TARGET} COMPILE_OPTIONS)
    get_target_property(TARGET_TYPE ${TARGET} TYPE)

    target_include_directories(${METATARGET} PUBLIC
        ${INCLUDE_DIRS} ${GENERATED_SOURCES} ${QtCore_INCLUDE_DIRS}
        ${Qt5Widgets_INCLUDE_DIRS}
    )
    add_dependencies(${TARGET} ${METATARGET})
    target_link_libraries(${TARGET} PRIVATE ${METATARGET})
    target_link_libraries(${METATARGET} PRIVATE metapp)

    if (${TARGET_TYPE} STREQUAL "SHARED_LIBRARY")
        set_property(TARGET ${METATARGET} PROPERTY POSITION_INDEPENDENT_CODE ON)
    endif()
    target_compile_options(${METATARGET} PRIVATE ${COMPILE_FLAG})

endfunction()