

function(read_properties PROP_PATH SECTION_NAME)

    file(READ ${PROP_PATH} PROPS_STR)
    
    string(ASCII 27 Esc)

    # Turn the contents into a list of strings, each ending with an Esc.
    # This allows us to preserve blank lines in the file since CMake
    # automatically prunes empty list items during a foreach loop.
    string(REGEX REPLACE "\r?\n" "${Esc};" PROPS_LINES "${PROPS_STR}")

    foreach(LINE ${PROPS_LINES})
        string(REGEX MATCH "^\\[([a-zA-Z0-9_\-]+)\\]${Esc}$" MATCHED ${LINE})

        if(MATCHED)
            if (NOT SECTION_NAME OR "${CMAKE_MATCH_1}" EQUAL ${SECTION_NAME})
                string(TOUPPER "${CMAKE_MATCH_1}" SECTION)
            endif()
            unset(MATCHED)
        elseif(SECTION)
            string(REGEX MATCH "^([a-zA-Z0-9_\-]+) *= *([^${Esc}]+)" MATCHED ${LINE})
            if (MATCHED)
                string(TOUPPER ${CMAKE_MATCH_1} KEY)
                set(PROP_${SECTION}_${KEY} "${CMAKE_MATCH_2}")
                unset(MATCHED)
            endif()
        endif()
    endforeach()

endfunction()