
get_target_property(METAPP_TEMPLATE_DIR metapp-codegen TEMPLATE_DIR)



macro(reflect_sources _target _generatedFiles)

    unset(_includeDirs)

    cmake_parse_arguments(meta "" "" "TEMPLATES;SOURCES;INCLUDES;HEADERS" ${ARGN})

    if (NOT TARGET ${_target})
        message(FATAL_ERROR "A target must be specified as first parameter")
        return()
    endif()
    if (NOT meta_TEMPLATES)
        set(meta_TEMPLATES ${METAPP_TEMPLATE_DIR}/default.cpp.template)
    endif()

    get_target_property(_include_dirs ${_target} INCLUDE_DIRECTORIES)
    get_target_property(_cxx_standard ${_target} CXX_STANDARD)
    get_target_property(_target_type ${_target} TYPE)
    get_target_property(_compile_options ${_target} COMPILE_OPTIONS)

    set(_compile_flags ${_compile_options})
    list(FILTER _compile_flags INCLUDE REGEX "^-f")

    string(REPLACE "-fcoroutines" "-fcoroutines-ts" _compile_flags "${_compile_flags}")
    string(REPLACE ";" " " _compile_flags "${_compile_flags}")
    string(REGEX REPLACE "(^| )-f" ";" _compile_flags "${_compile_flags}")

    set(_command metapp-codegen -std c++${_cxx_standard})

    set(_include_dirs ${_include_dirs} ${meta_INCLUDES})

    foreach(_include_dir ${_include_dirs})
        list(APPEND _command "-I${_include_dir}")
    endforeach()

    foreach(_template ${meta_TEMPLATES})
        list(APPEND _command "-t${_template}")
    endforeach()

    foreach(_include_header ${meta_HEADERS})
        list(APPEND _command "-i${_include_header}")
    endforeach()

    foreach(includeFile ${meta_SOURCES})
        unset(_generated_files)
        unset(_out)

        file(RELATIVE_PATH _rel_path ${CMAKE_CURRENT_SOURCE_DIR} ${includeFile})
        set(_generated_file ${CMAKE_CURRENT_BINARY_DIR}/meta/${_rel_path})

        foreach(_template ${meta_TEMPLATES})
            get_filename_component(_file_ext ${_template} NAME_WLE)
            string(REGEX REPLACE "\.[hc](pp)?$" ".${_file_ext}" _file ${_generated_file})
            list(APPEND _generated_files ${_file})
        endforeach()

        foreach(_generated_file ${_generated_files})
            list(APPEND _out "-o${_generated_file}")
        endforeach()
    
        add_custom_command(COMMAND ${_command} -s ${includeFile} ${_out} ${_compile_flags}
            OUTPUT ${_generated_files}
            DEPENDS ${includeFile} ${meta_TEMPLATES}
        )
        list(APPEND ${_generatedFiles} ${_generated_files})

        set_property(SOURCE ${_generated_files} PROPERTY SKIP_AUTOGEN ON)
    endforeach()

    set_source_files_properties(${_generatedFiles} PROPERTIES GENERATED 1)

endmacro(reflect_sources)




macro(generate_metalib _target)

    unset(meta_TEMPLATES)
    unset(meta_SUFFIX)
    unset(meta_SOURCES)
    unset(meta_INCLUDES)
    unset(meta_HEADERS)

    cmake_parse_arguments(meta "" "TARGET" "TEMPLATES;SOURCES;INCLUDES;HEADERS" ${ARGN})

    if (NOT meta_TARGET)
        string(MD5 meta_TARGET "${meta_SOURCES}")
    endif()

    reflect_sources(${_target} _generatedFiles ${ARGN})

    add_library(${meta_TARGET} STATIC ${_generatedFiles})
    set_target_properties(${meta_TARGET} PROPERTIES LINKER_LANGUAGE CXX)

    target_include_directories(${meta_TARGET} PRIVATE ${meta_INCLUDES})
    target_link_libraries(${meta_TARGET} PRIVATE metapp)

    add_dependencies(${_target} ${meta_TARGET})
    target_link_libraries(${_target} PRIVATE ${meta_TARGET})


    if (${_target_type} STREQUAL "SHARED_LIBRARY")
        set_property(TARGET ${meta_TARGET} PROPERTY POSITION_INDEPENDENT_CODE ON)
    endif()
    target_compile_options(${meta_TARGET} PRIVATE ${_compile_options})
    target_compile_definitions(${meta_TARGET} PRIVATE METAPLUSPLUS)

    foreach(_outputFile ${_generatedFiles})
        get_filename_component(_srcDir ${_outputFile} DIRECTORY)
        list(APPEND _generatedDirs ${_srcDir})
    endforeach()
    set_target_properties(${meta_TARGET} PROPERTIES GENERATED_OUT_DIRS ${_generatedDirs})

endmacro()