cmake_minimum_required(VERSION 3.12)

project(corba VERSION 1.0 LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

file(GLOB_RECURSE SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/src/**.cpp")

add_library(${PROJECT_NAME} SHARED ${SOURCES})

target_link_libraries(${PROJECT_NAME} PUBLIC metapp)

target_compile_options(${PROJECT_NAME} PRIVATE -fno-rtti -Wno-attributes)

target_include_directories(${PROJECT_NAME} PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/src
    # $<INSTALL_INTERFACE:include/gamesmith/metapp>
)