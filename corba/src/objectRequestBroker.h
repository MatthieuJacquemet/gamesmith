// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __OBJECTREQUESTBROKER_H__
#define __OBJECTREQUESTBROKER_H__

#include <panda3d/datagramIterator.h>
#include <memory>
#include <singleton.h>
#include <functional>
#include <slotPool.h>

#include "future.h"
#include "rpc_message.h"

namespace detail {
    template<class T>
    const char* get_service_name() { assert(false); }
}

class ServiceBase;
class Datagram;
class IMessageTransport;


class ObjectRequestBroker : public Singleton<ObjectRequestBroker> {

public:
    enum State {
        S_offline       = 0x0,
        S_connected     = 0x1,
        S_bound         = 0x2,
        S_ready         = S_bound | S_connected
    };

    using ServiceFactory = std::function<ServiceBase*(uint64_t)>;
    using BackendFactory = std::function<IMessageTransport*()>;
    using ArgsCallback = std::function<void (Datagram&)>;
    using ConnectionCb = std::function<void (bool)>;

    bool send_message(  const rpc::MessageHeader& header,
                        const ArgsCallback& callback);

    void handle_messages();
    void handle_one_message(const Datagram& datagram);

    uint16_t register_service(ServiceBase* service, uint16_t id = 0);
    void release_service(uint16_t service_id);

    int get_transport_handle() const noexcept;
    std::string get_transport_uri() const noexcept;

    template<class Transport>
    inline void register_transport_backend(const std::string& proto) {
        static_assert(std::is_default_constructible_v<Transport>);
        _backend_factories.emplace(proto, [] { return new Transport; });
    }

    template<class Service>
    inline void register_service() {
        if constexpr (std::is_constructible_v<Service, uint16_t>) {
            const char* name = detail::get_service_name<Service>();
            _service_factories.emplace(name, [](uint64_t id) {
                return new Service(id);
            });
        }
    }
    template<class Service>
    inline Service* get_service() noexcept {
        static_assert(std::is_base_of_v<ServiceBase, Service>);
        const char* name = detail::get_service_name<Service>();
        auto found = std::find_if(  _bound_services.begin(),
                                    _bound_services.end(), [name](auto* svc) {
            return svc && svc->is_derived_from(name);        
        });
        if (LIKELY(found != _bound_services.end())) {
            return static_cast<Service*>(*found);
        }
        return static_cast<Service*>(nullptr);
    }
    
    uint64_t register_pending_request();
    void release_pending_request(uint64_t request_id);

    inline ServiceBase* get_controller() const noexcept {
        return _bound_services[0];
    }
    inline ServiceBase* get_service(uint16_t id) noexcept {
        return _bound_services.get(id, nullptr);
    }
    inline Future<DatagramIterator>& get_future(uint16_t id) const {
        return const_cast<Future<DatagramIterator>&>(_pending_requests.get(id));
    }

    void handle_connected(bool connected);
    void client_connected(bool connected);

    bool bind_interface(const std::string& interface_uri);
    bool connect_interface(const std::string& interface_uri);
    void unbind_interface();
    void disconnect_interface();

    inline State get_current_state() const { return _current_state; }
    inline void set_connection_callback(ConnectionCb&& callback) {
        _connection_callback = callback;
    }
private:
    ObjectRequestBroker();
    ~ObjectRequestBroker();
    SINGLETON_DECLARE_CLASS(ObjectRequestBroker);

    std::optional<std::string> initialize_connection(const std::string& uri);

    typedef std::unordered_map<std::string, ServiceFactory> SrvFactories;
    typedef std::unordered_map<std::string, BackendFactory> BckFactories;
    
    std::unique_ptr<IMessageTransport> _transport;
    ConnectionCb _connection_callback;

    SrvFactories _service_factories;
    BckFactories _backend_factories;

    SlotPool<ServiceBase*> _bound_services;
    SlotPool<Future<DatagramIterator>> _pending_requests;

    State _current_state = State::S_offline;

    friend class CORBAService;
};


#endif // __OBJECTREQUESTBROKER_H__