// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __RPC_MESSAGE_H__
#define __RPC_MESSAGE_H__

#include <panda3d/datagram.h>
#include <variant>

#include "macro_utils.h"


namespace rpc {

    enum ErrorStatus { ES_ok = 1, ES_error = 0 };
    struct ServiceCallHeader {
        uint64_t id                     = 0;
        uint16_t service_id             = 0;
        uint16_t derived_id             = 0;
        uint16_t handler_id             = 0;
        bool want_response              = false;
    };

    struct ResponseHeader {
        uint64_t id                     = 0;
        ErrorStatus error_status        = ES_ok;
    };

    struct MessageHeader {
        MessageHeader() = default;
        MessageHeader(const ServiceCallHeader& header): payload(header) {}
        MessageHeader(const ResponseHeader& header): payload(header) {}

        uint32_t payload_length;
        std::variant<ServiceCallHeader, ResponseHeader> payload;   
    };
}


#endif // __RPC_MESSAGE_H__