// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __UNIXDOMAINSOCKETTRANSPORT_H__
#define __UNIXDOMAINSOCKETTRANSPORT_H__

#include "socketTransport.h"

#if defined(__unix__) || defined(__APPLE__) || defined(__FreeBSD__)
    #define IS_POSIX
#endif

#if defined (IS_POSIX)

class UnixDomainSocketTransport : public SocketTransport {

public:
    UnixDomainSocketTransport() = default;
    virtual ~UnixDomainSocketTransport();

    virtual bool bind(const std::string& uri) override;
    virtual bool connect(const std::string& uri) override;
    virtual bool disconnect() override;
    virtual bool unbind() override;

    virtual std::string get_uri() override;

private:
    std::string _current_socket_path;
};

#endif // __UNIXDOMAINSOCKETTRANSPORT_H__

#endif