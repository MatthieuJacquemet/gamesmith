// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SERVICEBASE_H__
#define __SERVICEBASE_H__

#include <panda3d/datagramIterator.h>

#include "rpc_message.h"
#include "objectRequestBroker.h"


#define DEFINE_SERVICE_METHODS(NAME)                                        \
const char* NAME::get_name() const {                                        \
    return detail::get_service_name<NAME>();                                \
}                                                                           \
bool NAME::is_derived_from(const char* cls) const {                         \
    if (strcmp(cls, detail::get_service_name<NAME>()) == 0) {               \
        return true;                                                        \
    } else {                                                                \
        return base_service::is_derived_from(cls);                          \
    }                                                                       \
}

#define REGISTER_DERIVED_SERVICE(BASE)                                      \
public:                                                                     \
    using base_service = BASE;                                              \
    enum { _derived_id = BASE::_derived_id + 1 };                           \
    const std::vector<Handler>& get_handlers(size_t id) const override {    \
        return detail::handlers<std::decay_t<decltype(*this)>>(id);         \
    }                                                                       \
    bool is_derived_from(const char* cls) const override;                   \
    const char* get_name() const override;                                  \
private:

#define REGISTER_SERVICE REGISTER_DERIVED_SERVICE(ServiceBase)




class ServiceBase {

protected:
    uint16_t _id;
public:
    enum { _derived_id = -1 };

    inline ServiceBase(uint16_t id = 0) : _id(id) {}
    virtual ~ServiceBase() = default;
    DISABLE_COPY_MOVE(ServiceBase);

    inline uint16_t get_id() const { return _id; }

    template<uint16_t CLS, uint16_t HID, class... Args>
    void _call_endpoint(const Args&... args) {
        rpc::ServiceCallHeader message {0UL, _id, CLS, HID, false};
        try {
            ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
            broker->send_message(message, [&args...](auto& msg) {
                (generic_write_datagram(msg, args), ...);
            });
        } catch (const std::exception& error) {
            std::cerr << "call_endpoint: " << error.what() << std::endl;
        }
    }
    using Handler = std::function<void (ServiceBase*, Datagram&,
                                        DatagramIterator&)>;


    virtual bool is_derived_from(const char* cls) const { return false; }
    virtual const char* get_name() const = 0;
    virtual const std::vector<Handler>& get_handlers(size_t cls) const {
        return default_handlers();
    }
    
    virtual bool handle_call(const rpc::ServiceCallHeader& header,
                        Datagram& response, DatagramIterator& scan) {
        auto& handlers = get_handlers(header.derived_id);
        if (header.handler_id < handlers.size()) {
            if (const Handler& handler = handlers[header.handler_id]) {
                handler(this, response, scan);
                return true;
            }
        }
        return false;
    }

    template<class R, class T, class... Args>
    static void register_handler(R (T::*func)(Args...), size_t id) {
        static_assert(std::is_base_of_v<ServiceBase, T>);

        std::vector<Handler>& handlers = get_handlers<T>();
        if (id >= handlers.size()) {
            handlers.resize(id + 1);
        }
        handlers[id] = [func](auto* obj, Datagram& out, DatagramIterator& in) {
            std::tuple<std::decay_t<Args>...> args;
            generic_read_datagram(args, in);
            auto callback = [func, obj](Args&&... args) {
                return (static_cast<T*>(obj)->*func)(std::forward<Args>(args)...);
            };
            if constexpr (std::is_void_v<R>) {
                std::apply(callback, std::move(args));
            } else {
                generic_write_datagram(out, std::apply(callback, std::move(args)));
            }
        };
    }
    static const std::vector<Handler>& default_handlers() {
        static std::vector<Handler> handlers;
        return handlers;
    }
    template<class Impl> static std::vector<Handler>& get_handlers() {
        static std::vector<Handler> handlers;
        return handlers;
    }
};


namespace detail {
    template<class T> const char* get_service_name();

    template<class T> constexpr ssize_t service_inheritance_index() {
        if constexpr (std::is_same_v<T, ServiceBase>) {
            return -1;
        } else {
            return service_inheritance_index<typename T::base_service>() + 1;
        }
    }
    template<class T, size_t Id>
    constexpr auto service_at() {
        if constexpr (std::is_same_v<T, ServiceBase>) {
            return static_cast<void*>(nullptr);
        } else if constexpr (Id == T::_derived_id) {
            return static_cast<T*>(nullptr);
        } else {
            return service_at<typename T::base_service, Id>();
        }
    }
    template<class T, size_t Id>
    using service_at_impl = decltype(*service_at<std::decay_t<T>, Id>());
    
    template<class T, size_t Id>
    using service_at_t = std::remove_reference_t<service_at_impl<T, Id>>;

    template<class T, size_t... I>
    constexpr const std::vector<ServiceBase::Handler>&
    handlers_helper(std::index_sequence<I...>, size_t id) {
        using handlers_t = typename ServiceBase::Handler;
        using handlers_getter_t = std::vector<handlers_t>&();

        constexpr handlers_getter_t* handlers_getters[] = {
            ServiceBase::get_handlers<service_at_t<T, I>>...
        };
        return handlers_getters[id]();
    }

    template<class T>
    const std::vector<ServiceBase::Handler>& handlers(size_t cls) {
        constexpr ssize_t this_id = T::_derived_id;
        if (UNLIKELY(cls > this_id)) {
            return ServiceBase::default_handlers();
        }
        return handlers_helper<T>(std::make_index_sequence<this_id + 1>{}, cls);
    }
}


#endif // __SERVICEBASE_H__