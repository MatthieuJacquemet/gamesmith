// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __CORBASERVICE_H__
#define __CORBASERVICE_H__

#include "serviceBase.h"


class CORBAService: public ServiceBase {
    REGISTER_SERVICE
public:
    enum { create_id = 0, release_id = 1 };
    inline CORBAService() : ServiceBase(0) {}
    virtual ~CORBAService() = default;

    bool release_service(uint16_t id);
    bool create_service(uint16_t id, const std::string& name);
};

#endif // __CORBASERVICE_H__