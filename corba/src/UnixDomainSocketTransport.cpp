// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include "UnixDomainSocketTransport.h"

#if defined (IS_POSIX)


#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netdb.h>

#include "objectRequestBroker.h"
    

static sockaddr_un create_socket(const std::string& socket_address_uri) {
    if (socket_address_uri.size() >= 108) {
        throw std::runtime_error("Path too long: " + socket_address_uri);
    }
    sockaddr_un socket;
    socket.sun_family = AF_UNIX;
    strcpy(socket.sun_path, socket_address_uri.c_str());

    return socket;
}



UnixDomainSocketTransport::~UnixDomainSocketTransport() {
    UnixDomainSocketTransport::unbind();
}

bool UnixDomainSocketTransport::disconnect() {
    _current_socket_path.clear();
    return SocketTransport::disconnect();
}

bool UnixDomainSocketTransport::unbind() {
    if (SocketTransport::unbind()) {
        unlink(_current_socket_path.c_str());
        _current_socket_path.clear();
        return true;
    }
    return false;
}

bool UnixDomainSocketTransport::bind(const std::string& uri) {
    sockaddr_un socket;
    try {
        socket = create_socket(uri);
    } catch (const std::exception& e) {
        std::cerr << "(uds) bind failed: " << e.what() << std::endl;
        return false;
    }
    _current_socket_path = uri;
    socklen_t lens = sizeof(socket.sun_family) + uri.size();
    return bind_impl((sockaddr*)&socket, lens, AF_UNIX);
}

bool UnixDomainSocketTransport::connect(const std::string& uri) {
    sockaddr_un socket;
    try {
        socket = create_socket(uri);
    } catch (const std::exception& e) {
        std::cerr << "(uds) connect failed: " << e.what() << std::endl;
        return false;
    }
    _current_socket_path = uri;
    socklen_t lens = sizeof(socket.sun_family) + uri.size();
    return connect_impl((sockaddr*)&socket, lens, AF_UNIX);
}


std::string UnixDomainSocketTransport::get_uri() {
    return "uds://" + _current_socket_path;
}


INITIALIZER(UnixDomainSocketTransport) {
    auto* borker = ObjectRequestBroker::get_global_ptr();
    borker->register_transport_backend<UnixDomainSocketTransport>("uds");
    borker->register_transport_backend<UnixDomainSocketTransport>("ipc"); // for zmq compatibility
}

#endif
