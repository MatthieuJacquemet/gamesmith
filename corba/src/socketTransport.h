// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SOCKETTRANSPORT_H__
#define __SOCKETTRANSPORT_H__

#include "messageTransport.h"
#include <utils.h>

struct sockaddr;


class SocketTransport : public IMessageTransport {

public:
    SocketTransport();
    virtual ~SocketTransport();

    virtual bool send(const Datagram& datagram) override;
    virtual std::optional<Datagram> recv() override;

    virtual bool connect(const std::string& uri) override;
    virtual bool disconnect() override;
    virtual bool bind(const std::string& uri) override;
    virtual bool unbind() override;

    virtual int get_handle() override;
    virtual std::string get_uri() override;

protected:
    void close_sockets();
    
    bool bind_impl(sockaddr* address, socklen_t size, int family);
    bool connect_impl(sockaddr* address, socklen_t size, int family);

private:
    struct SocketTransportPrivate;
    dimpl<SocketTransportPrivate> _d;
};

#endif // __SOCKETTRANSPORT_H__