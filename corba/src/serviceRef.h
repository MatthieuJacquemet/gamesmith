// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SERVICEREF_H__
#define __SERVICEREF_H__

#include <memory>
#include "objectRequestBroker.h"


template<class ServiceType>
class ServiceRef final : public std::weak_ptr<ServiceType> {

    static_assert(std::is_base_of<ServiceBase, ServiceType>::value,
                "ServiceType must inherit from ServiceBase");
public:
    ServiceRef() = default;
    ~ServiceRef() = default;

    [[nodiscard]] std::shared_ptr<ServiceType> acquire() noexcept {
        
        ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
        auto strong_ref = std::weak_ptr<ServiceType>::lock();

        if (UNLIKELY(strong_ref.is_null())) {
            std::weak_ptr<ServiceType>::reassign(
                strong_ref = broker->get_service<ServiceType>()
            );
        }
        return strong_ref;
    }
};
#endif // __SERVICEREF_H__