// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.


#include <panda3d/datagram.h>
#include <panda3d/datagramIterator.h>
#include <regex>

#include "bamSerializer.h"
#include "CORBAService.h"
#include "rpc_message.h"
#include "objectRequestBroker.h"
#include "messageTransport.h"
#include "serviceBase.h"


using ServiceRef = std::shared_ptr<ServiceBase>;

DEFINE_STRUCT_SERIALIZER(5, rpc::ServiceCallHeader)
DEFINE_STRUCT_SERIALIZER(2, rpc::ResponseHeader)
DEFINE_STRUCT_SERIALIZER(2, rpc::MessageHeader)


ObjectRequestBroker::ObjectRequestBroker() {
    _bound_services.acquire_slot(new CORBAService);
}

ObjectRequestBroker::~ObjectRequestBroker() {
    if (_current_state & S_bound && _transport) {
        _transport->unbind();
    }
    delete _bound_services.release_slot(0);
}


void handle_message(const rpc::ServiceCallHeader& header, DatagramIterator& scan) {

    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();

    const auto get_resp_header = [](const Datagram& datagram) {
        return (rpc::ResponseHeader*)((char*)datagram.get_data() + 2 + 4);   
    };
    ServiceBase* service = broker->get_service(header.service_id);
    if (header.want_response) {
        rpc::ResponseHeader response_header{
            .id             = header.id,
            .error_status   = rpc::ES_error
        };
        broker->send_message(response_header, [&](Datagram& datagram) {
            if (service && service->handle_call(header, datagram, scan)) {
                get_resp_header(datagram)->error_status = rpc::ES_ok;
            }
        });
    } else if (LIKELY(service != nullptr)) {
        Datagram datagram;
        service->handle_call(header, datagram, scan);
    }
}

void handle_message(const rpc::ResponseHeader& header, DatagramIterator& scan) {
    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();

    Future<DatagramIterator>& future = broker->get_future(header.id);
    if (UNLIKELY(header.error_status == rpc::ES_error)) {
        future.cancel();
    } else {
        future.set_result(std::move(scan));
    }
}


void ObjectRequestBroker::handle_connected(bool connected) {
    nassertv((_current_state == S_bound &&  connected) ||
             (_current_state == S_ready && !connected));
    
    _current_state = connected ? S_ready : S_bound;

    while (!_pending_requests.empty()) {
        size_t prev = _pending_requests.size();
        _pending_requests.back().cancel();
        if (prev == _pending_requests.size()) {
            _pending_requests.pop_back();
        }
    }
}

void ObjectRequestBroker::client_connected(bool connected) {
    if (_connection_callback) _connection_callback(connected);
}

int ObjectRequestBroker::get_transport_handle() const noexcept {
    return _transport ? _transport->get_handle() : -1;
}

std::string ObjectRequestBroker::get_transport_uri() const noexcept {
    return _transport ? _transport->get_uri() : "";
}

uint64_t ObjectRequestBroker::register_pending_request() {
    return _pending_requests.acquire_slot();
}

void ObjectRequestBroker::release_pending_request(uint64_t request_id) {
    _pending_requests.release_slot(request_id);
}


void ObjectRequestBroker::handle_messages() {
    if (UNLIKELY(_current_state != S_offline && _transport)) {
        while (std::optional<Datagram> message = _transport->recv()) {
            handle_one_message(message.value());
        }
    }
}


void ObjectRequestBroker::handle_one_message(const Datagram& message) {
    
    DatagramIterator scan(message);
    rpc::MessageHeader header;
    generic_read_datagram(header, scan);

    std::visit([&scan](const auto& payload_header) mutable -> void {
        handle_message(payload_header, scan);
    }, header.payload);
}

bool ObjectRequestBroker::send_message( const rpc::MessageHeader& header,
                                        const ArgsCallback& callback) {
    if (UNLIKELY((_current_state & S_connected) == 0)) {
        return false; // always check that the client is still connected
    }
    Datagram datagram;
    generic_write_datagram(datagram, header);
    callback(datagram);
    rpc::MessageHeader* data = (rpc::MessageHeader*)datagram.get_data();
    data->payload_length = datagram.get_length() - 4;

    return _transport->send(datagram);
    return false;
}


bool ObjectRequestBroker::connect_interface(const std::string& uri) {
    nassertr(_current_state == S_offline && !_transport, false)
    
    if (auto location = initialize_connection(uri)) {
        if (_transport->connect(location.value())) {
            _current_state = S_connected;
        }
    }
    return _current_state == S_connected;
}

bool ObjectRequestBroker::bind_interface(const std::string& uri) {
    nassertr(_current_state == S_offline && !_transport, false)
    
    if (auto location = initialize_connection(uri)) {
        if (_transport->bind(location.value())) {
            _current_state = S_bound;
        }
    }
    return _current_state == S_bound;
}

void ObjectRequestBroker::unbind_interface() {
    nassertv((_current_state & S_bound) && _transport)
    _transport->unbind();
    _transport.reset();
    _current_state = S_offline;
}

void ObjectRequestBroker::disconnect_interface() {
    nassertv(_current_state == S_connected && _transport)
    _transport->disconnect();
    _current_state = S_offline;
}

uint16_t ObjectRequestBroker::register_service(ServiceBase* svc, uint16_t id) {
    if (id == 0) {
        id = _bound_services.acquire_slot(svc);
    } else {
        _bound_services.steal_slot(id, svc);
    }
    return id;
}

void ObjectRequestBroker::release_service(uint16_t service_id) {
    _bound_services.release_slot(service_id);
}


std::optional<std::string>
ObjectRequestBroker::initialize_connection(const std::string& uri) {

    std::regex pattern(R"(^(\w+):\/\/(.+))");
    std::smatch match;
    if (std::regex_match(uri, match, pattern)) {
        BckFactories::iterator it = _backend_factories.find(match.str(1));
        
        if (it != _backend_factories.end()) {
            _transport.reset(it->second()); return match.str(2);
        }
    }
    return std::optional<std::string>{};
}
