// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.


#include <panda3d/datagram.h>
#include <regex>
#include <thread>

#if defined(__unix__) || defined(__APPLE__) || defined(__FreeBSD__)
    #define IS_POSIX
#elif defined(_WIN32)
    #define IS_WINDOWS
#endif

#ifdef IS_POSIX
    #include <unistd.h>
    #include <arpa/inet.h>
    #include <sys/socket.h>
    #include <netinet/in.h>
    #include <netdb.h>
#elif defined(IS_WINDOWS)
    #include <winsock2.h>
#endif

#include "objectRequestBroker.h"
#include "socketTransport.h"



struct SocketTransport::SocketTransportPrivate {
#ifdef IS_POSIX
    int server_socket = -1;
    std::atomic<int> client_socket = -1;
    std::thread listen_thread;
#endif
};

static sockaddr_in create_address(const std::string& socket_address_uri) {
    
    std::regex pattern(R"(([^:]+)(?::(\d+))?)");
    std::smatch matches;
    if (!std::regex_match(socket_address_uri, matches, pattern)) {
        throw std::runtime_error("Invalid URI: " + socket_address_uri);
    }
    std::string host_str = matches.str(1), port_str = matches.str(2);

    int port = 0; // Default port value (0 means system will choose a free port)
    if (!port_str.empty()) {
        port = std::stoi(port_str);
        if (port <= 0 || port > 65535) {
            throw std::runtime_error("Invalid port number: " + port_str);
        }
    }
    sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);

    if (host_str == "*") {
        addr.sin_addr.s_addr = INADDR_ANY;
    } else {
#if defined(IS_WINDOWS)
        if (inet_pton(AF_INET, host_str.c_str(), &(addr.sin_addr)) != 1) {
#elif defined(IS_POSIX)
        if (inet_pton(AF_INET, host_str.c_str(), &(addr.sin_addr.s_addr)) != 1) {
#endif
            struct hostent* he = gethostbyname(host_str.c_str());
            if (he == nullptr) {
                throw std::runtime_error("Invalid host: " + host_str);
            }
            memcpy(&addr.sin_addr.s_addr, he->h_addr_list[0], sizeof(in_addr_t));
        }
    }
    return addr;
}


SocketTransport::SocketTransport() {

}

SocketTransport::~SocketTransport() {
    unbind();
}



bool SocketTransport::send(const Datagram& datagram) {
    int fd;
    constexpr int flags = MSG_DONTWAIT;
    
    if ((fd = _d->client_socket.load(std::memory_order_relaxed)) != -1 &&
        ::send(fd, datagram.get_data(), datagram.get_length(), flags) != -1) {
        return true;
    }
    return false;
}

std::optional<Datagram> SocketTransport::recv() {
    std::optional<Datagram> result;
    uint32_t message_size;
    constexpr int flags = MSG_DONTWAIT;
    int fd;
    if ((fd = _d->client_socket.load(std::memory_order_relaxed)) == -1) {
        return std::optional<Datagram>{};
    } 
    else if(LIKELY(::recv(fd, &message_size, 4, flags) == 4)) {
        Datagram datagram;
        datagram.add_uint32(message_size);
        datagram.pad_bytes(message_size);
        char* buffer = (char*)datagram.get_data() + 4;
        
        uint32_t total_received = 0;
        while (total_received < message_size) {
            int bytes_received = ::recv(fd, buffer + total_received, 
                                        message_size - total_received, flags);
            if (bytes_received <= 0) {
                return result;
            }
            total_received += bytes_received;
        }
        result.emplace(std::move(datagram));
    }
    return result;
}

bool SocketTransport::connect(const std::string& socket_uri) {
    sockaddr_in address;
    try {
        address = create_address(socket_uri);
    } catch (const std::exception& e) {
        std::cerr << "SocketTransport::connect: " << e.what() << std::endl;
        return false;
    }
    return connect_impl((sockaddr*)&address, sizeof(address), AF_INET);
}

bool SocketTransport::disconnect() {
    if (UNLIKELY(_d->client_socket == -1)) {
        return false;
    }
    close_sockets();
    return true;
}


void SocketTransport::close_sockets() {
#if defined(IS_WINDOWS)
    shutdown(_d->server_socket, SD_RECEIVE);
    closesocket(_d->client_socket);
    closesocket(_d->socket);
#elif defined(IS_POSIX)
    shutdown(_d->server_socket, SHUT_RD);
    ::close(_d->client_socket);
    ::close(_d->server_socket);
#endif
}

bool SocketTransport::bind_impl(sockaddr* address, socklen_t size, int family) {
    if (UNLIKELY(_d->server_socket != -1)) return false;
#ifdef _WIN32
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
        std::cout << "Failed to initialize Winsock." << std::endl;
        return false;
    }
#endif
    if ((_d->server_socket = socket(family, SOCK_STREAM, 0)) == -1) {
        std::cout << "Failed to create socket." << strerror(errno) << std::endl;
        return false;
    }
    else if (::bind(_d->server_socket, address, size) < 0) {
        std::cerr << "Failed to bind socket. " << strerror(errno) << std::endl;
        return false;
    }
    listen(_d->server_socket, 1);
    
    _d->listen_thread = std::thread([this, size]() mutable {
        int fd;
        sockaddr* socket_info = static_cast<sockaddr*>(alloca(size));
        
        while ((fd = accept(_d->server_socket, socket_info, &size)) != -1) {
            _d->client_socket.store(fd, std::memory_order_relaxed);
            ObjectRequestBroker::get_global_ptr()->client_connected(true);
        }
    });
    return true;
}

bool SocketTransport::connect_impl(sockaddr* address, socklen_t size, int family) {
    if (UNLIKELY(_d->client_socket != -1)) return false;
#ifdef _WIN32
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
        std::cout << "Failed to initialize Winsock." << std::endl;
        return false;
    }
#endif
    if ((_d->client_socket = socket(family, SOCK_STREAM, 0)) == -1) {
        std::cout << "Failed to create socket." << strerror(errno) << std::endl;
        return false;
    }
    else if (::connect(_d->client_socket, address, size) < 0) {
        std::cerr << "Failed to connect socket. " << strerror(errno) << std::endl;
        return false;
    }
    return true;
}

bool SocketTransport::unbind() {
    if (UNLIKELY(_d->server_socket == -1)) {
        return false;
    }
    close_sockets();
    if (_d->listen_thread.joinable()) {
        _d->listen_thread.join();
    }
    _d->server_socket = -1;
    _d->client_socket = -1;
    return true;
}

std::string SocketTransport::get_uri() {
    if (LIKELY(_d->server_socket != -1)) {
        struct sockaddr_in sin;
        socklen_t len = sizeof(sin);
        if (getsockname(_d->server_socket, (sockaddr*)&sin, &len) != -1) {
            // placeholder string
            std::string uri = "tcp://---.---.---.---\0";
            if (inet_ntop(AF_INET, &(sin.sin_addr), uri.data() + 6,
                                                    uri.size() - 6)) {
                uri.resize(strlen(uri.data()));
                return uri + ":" + std::to_string(ntohs(sin.sin_port));
            }
        }
    }
    return std::string();
}

int SocketTransport::get_handle() {
    return _d->client_socket.load(std::memory_order_relaxed);
}

bool SocketTransport::bind(const std::string& socket_uri) {
    sockaddr_in address;
    try {
        address = create_address(socket_uri);
    } catch (const std::exception& e) {
        std::cerr << "SocketTransport::bind: " << e.what() << std::endl;
        return false;
    }
    return bind_impl((sockaddr*)&address, sizeof(address), AF_INET);
}


INITIALIZER(SocketTransport) {
    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
    broker->register_transport_backend<SocketTransport>("tcp");
}