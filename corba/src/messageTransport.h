// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MESSAGETRANSPORT_H__
#define __MESSAGETRANSPORT_H__

#include <string>
#include <optional>

class Datagram;

class IMessageTransport {

public:
    IMessageTransport() = default;
    virtual ~IMessageTransport() = default;

    virtual bool send(const Datagram& datagram) = 0;
    virtual std::optional<Datagram> recv() = 0;

    virtual bool connect(const std::string& uri) = 0;
    virtual bool disconnect() = 0;
    virtual bool bind(const std::string& uri) = 0;
    virtual bool unbind() = 0;

    virtual int get_handle() = 0;
    virtual std::string get_uri() = 0;
};

#endif // __MESSAGETRANSPORT_H__