// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <bamSerializable.h>

#include "objectRequestBroker.h"
#include "CORBAService.h"
#include "serviceBase.h"


using Handler = ServiceBase::Handler;

namespace detail {
template<> const char* get_service_name<CORBAService>() {
    return "CORBAService";
}
}

DEFINE_SERVICE_METHODS(CORBAService)


bool CORBAService::release_service(uint16_t id) {
    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
    assert(id > 0);
    if (!broker->_bound_services.is_slot_free(id)) {
        delete broker->_bound_services.release_slot(id, nullptr);
        return true;
    }
    return false;
}

bool CORBAService::create_service(uint16_t id, const std::string& name) {
    
    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
    auto srv_it = broker->_service_factories.find(name);

    if (UNLIKELY(srv_it == broker->_service_factories.end() ||
                broker->_bound_services.get(id, nullptr))) {
        auto* prev = broker->_bound_services.get(id, nullptr);
        return false;
    } else {
        broker->_bound_services.steal_slot(id, srv_it->second(id));
        return true;
    }
}


INITIALIZER(register_service_CORBAService) {
    ServiceBase::register_handler(&CORBAService::create_service, 0);
    ServiceBase::register_handler(&CORBAService::release_service, 1);
}
