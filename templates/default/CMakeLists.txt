cmake_minimum_required(VERSION 3.0)

include(ProjectProperties.cmake)
read_properties(manifest project)

project(${PROP_PROJECT_NAME} VERSION ${PROP_PROJECT_VERSION})

set(SRC_DIR src)
set(DATA_DIR data)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})


set(INCLUDE_DIRS "")
file(GLOB_RECURSE HEADERS src/*.h)
foreach(HEADER ${HEADERS})
    get_filename_component(INC ${HEADER} PATH)
    set(INCLUDE_DIRS ${INCLUDE_DIRS} ${INC})
endforeach()
list(REMOVE_DUPLICATES INCLUDE_DIRS)


file(GLOB_RECURSE SRCS ${SRC_DIR}/core/**.cpp)


string(REPLACE "-DNDEBUG" ""
    CMAKE_CXX_FLAGS_RELWITHDEBINFO
    "${CMAKE_CXX_FLAGS_RELWITHDEBINFO}"
)


find_package(Bullet REQUIRED)
find_package(Panda3D REQUIRED Core Tools Direct Contrib OpenAL OpenGL Bullet)

set(PANDA_LIBS 
    Panda3D::Core::panda
    Panda3D::Core::pandaexpress
    Panda3D::Core::p3dtool
    Panda3D::Direct::p3direct
    Panda3D::Contrib::p3ai
    Panda3D::Bullet::p3bullet
    Panda3D::OpenAL::p3openal_audio
    Panda3D::OpenGL::pandagl
    Panda3D::Core::pandaphysics
)

add_definitions(-fno-rtti -DTIXML_USE_STL)


if (CMAKE_BUILD_TYPE EQUAL "Release")
    add_executable(${PROJECT_NAME} ${SRCS})
else()
    add_library(${PROJECT_NAME} SHARED ${SRCS})

    add_custom_target(link_data ALL
        COMMAND ${CMAKE_COMMAND} -E create_symlink
        ${CMAKE_CURRENT_SOURCE_DIR}/data
        ${CMAKE_CURRENT_BINARY_DIR}/data
        COMMENT "adding symlink to data"
    )
endif()

target_link_libraries(${PROJECT_NAME} PUBLIC gdk ${PANDA_LIBS})


target_include_directories(${PROJECT_NAME} PUBLIC
    ${INCLUDE_DIRS}
    ${BULLET_INCLUDE_DIRS}
)


set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)