// Copyright (C) 2022 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <mainWindow.h>
#include <editor.h>

#include "KDDockWidgets.h"
#include "assetBrowser.h"



AssetBrowser::AssetBrowser():
    m_browserWidget(new AssetBrowserWidget) {

}


void AssetBrowser::initialize() {

    Editor* editor = Editor::globalInstance();
    MainWindow* window = editor->window();

    window->addEditor(m_browserWidget, KDDockWidgets::Location_OnBottom);
}


AssetBrowserWidget::~AssetBrowserWidget() {

}



void AssetBrowserWidget::serialize(QDataStream& stream) {
    
}

void AssetBrowserWidget::deserialize(QDataStream& stream) {
    
}

AssetBrowserWidget::AssetBrowserWidget() {
    
}
