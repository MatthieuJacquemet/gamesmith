// Copyright (C) 2022 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __ASSETBROWSER_H__
#define __ASSETBROWSER_H__

#include <QWidget>

#include <plugin.h>

#include "editorWidget.h"
#include "editor_defines.h"


class AssetBrowserWidget: public EditorWidget {

    Q_OBJECT

public:
    AssetBrowserWidget();
    virtual ~AssetBrowserWidget();

    void serialize(QDataStream& stream) override;
    void deserialize(QDataStream& stream) override;
};


class AssetBrowser: public QObject, public Plugin {

    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.onyx.Plugin")
    Q_INTERFACES(Plugin)

public:
    AssetBrowser();
    virtual ~AssetBrowser() = default;

    virtual void initialize() override;

protected:
    AssetBrowserWidget* m_browserWidget;
};


#endif // __ASSETBROWSER_H__