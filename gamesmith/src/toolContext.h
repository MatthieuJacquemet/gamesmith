// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __TOOLCONTEXT_H__
#define __TOOLCONTEXT_H__

#include "toolService.gs_rpc_stub.h"
#include <QString>

class ToolContextBase {

protected:
    ToolContextBase(const QString& name, const QString& desc = "");

public:
    virtual ~ToolContextBase();
    virtual QString modeName(uint32_t mode) const = 0;

    inline const QString& name() const { return m_name; }
    inline const QString& desc() const { return m_desc; }

    virtual Task<stub::ToolService*> service() const = 0; 

private:
    const QString m_name, m_desc;
};


template<std::derived_from<stub::ToolService> Service>
class ToolContext: public ToolContextBase, public Service {

public:
    ToolContext(const QString& name, const QString& desc = ""):
        ToolContextBase(name, desc) {}
    virtual ~ToolContext() = default;

    Task<stub::ToolService*> service() const override {
        ToolContext* service = const_cast<ToolContext*>(this);
        co_await service->acquire();
        co_return service;
    }
    QString modeName(uint32_t mode) const override {
        return modeNameImpl(mode);
    }
    QString modeNameImpl(uint32_t mode) const { return ""; }
};

#endif // __TOOLCONTEXT_H__