// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __EDITOR_UTILS_H__
#define __EDITOR_UTILS_H__


#include <QMetaEnum>

#ifdef HAVE_DBUS
#include <QDBusReply>
#include "task.h"
#endif

#include "pandaNodeService.h"


class QDir;

using PropertyWidget = QPair<QWidget*, Variant (*)(QWidget*)>;
using PropListener = std::function<void(const Variant&)>;
using PropFactory = PropertyWidget (*)( const PropertyInfo&,
                                        const PropListener&);



void onInitialized(const std::function<void()>& callback);


#define RESOLVE(CLASS, FUNC, ...) \
    QOverload<__VA_ARGS__>::of(&CLASS::FUNC)

#define ON_EDITOR_CREATED(NAME)                                     \
static void NAME##_impl(Editor* editor);                            \
INITIALIZER_IMPL(NAME) { Editor::onCreated(NAME##_impl); }          \
void NAME##_impl(Editor* editor)

#define ON_INITIALIZE(NAME)                                         \
static void NAME##_impl();                                          \
INITIALIZER_IMPL(NAME) { onInitialized(NAME##_impl); }              \
void NAME##_impl()

template<class T>
T config(const QString& id, const T& def=T());


class QWidget;
typedef QVector<QPair<int, QString>> EnumInfo;


EnumInfo getEnums(const QMetaObject& meta_class, const QString& id);

void copyRecursive(const QDir& src, const QDir& dst);

QString toolTipText(const QString& text);

struct OnlyOnce {
    Q_ALWAYS_INLINE OnlyOnce(const std::function<void()>&& callback) {
        callback();
    }
};

constexpr bool scalingFactorIsSupported(qreal factor) {
#if QT_VERSION < QT_VERSION_CHECK(5, 15, 2)
    // We don't support fractional factors in older Qt.
    const bool isInteger = int(factor) == factor;
    return isInteger;
#else
    Q_UNUSED(factor);
    return true;
#endif
}


QRect clampedTo(QRect rect, const QRect& bounds);

QSize smallIconSize(const QIcon& icon, QWidget* widget);

void registerLegacyPropertieWidgetsFactories();
void clearPropertyWidgetsFactories();

void registerPropertyWidgetFactory(TypeHandle type, PropFactory&& factory);

PropertyWidget createWidgetForProperty( const PropertyInfo& property,
                                        const PropListener& listener);


QColor lColorToQColor(const LColor& color);
LColor qColorToLColor(const QColor& color);

#include "editor_utils.T"

#endif // __EDITOR_UTILS_H__