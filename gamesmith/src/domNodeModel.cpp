// Copyright 2023 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "domNodeModel.h"

#include <QDomNode>
#include <QDomDocument>
#include <QUrl>
#include <QVector>
#include <QSourceLocation>
#include <QVariant>


class DomNode: public QDomNode {
public:
	DomNode(const QDomNode& other): QDomNode(other) {}

	DomNode(QDomNodePrivate* otherImpl): QDomNode(otherImpl) {}

	QDomNodePrivate* getImpl() { return impl; }
};


DomNodeModel::DomNodeModel(const QXmlNamePool& pool, const QDomDocument& doc):
	m_pool(pool), m_doc(doc) {

}

QUrl DomNodeModel::baseUri(const QXmlNodeModelIndex &) const {
	// TODO: Not implemented.
	return QUrl();
}

QXmlNodeModelIndex::DocumentOrder DomNodeModel::compareOrder(
									const QXmlNodeModelIndex & ni1,
									const QXmlNodeModelIndex & ni2 ) const {
	QDomNode n1 = toDomNode(ni1);
	QDomNode n2 = toDomNode(ni2);

	if (n1 == n2) {
		return QXmlNodeModelIndex::Is;
	}
	int l1 = n1.lineNumber();
	int c1 = n1.columnNumber();
	int l2 = n2.lineNumber();
	int c2 = n2.columnNumber();

	if (l1 < l2) {
		return QXmlNodeModelIndex::Precedes;
	}
	if (l1 > l2) {
		return QXmlNodeModelIndex::Follows;
	}
	if (l1 == l2 && c1 < c2) {
		return QXmlNodeModelIndex::Precedes;
	} else {
		return QXmlNodeModelIndex::Follows;
	}
}

QUrl DomNodeModel::documentUri(const QXmlNodeModelIndex&) const {
	// TODO: Not implemented.
	return QUrl();
}

QXmlNodeModelIndex DomNodeModel::elementById(const QXmlName& id) const {
	return fromDomNode(m_doc.elementById(id.toClarkName(m_pool)));
}

QXmlNodeModelIndex::NodeKind DomNodeModel::kind(const QXmlNodeModelIndex& ni) const {
	QDomNode n = toDomNode(ni);
	if (n.isAttr()) {
		return QXmlNodeModelIndex::Attribute;
	} else if (n.isText()) {
		return QXmlNodeModelIndex::Text;
	} else if (n.isComment()) {
		return QXmlNodeModelIndex::Comment;
 	} else if (n.isDocument()) {
		return QXmlNodeModelIndex::Document;
	} else if (n.isElement()) {
		return QXmlNodeModelIndex::Element;
	} else if (n.isProcessingInstruction()) {
		return QXmlNodeModelIndex::ProcessingInstruction;
	}
	return (QXmlNodeModelIndex::NodeKind) 0;
}

QXmlName DomNodeModel::name(const QXmlNodeModelIndex& ni) const {
	QDomNode n = toDomNode(ni);

	if (n.isAttr() || n.isElement() || n.isProcessingInstruction()) {
		return QXmlName(m_pool, n.nodeName(), QString(), QString());
	}
	return QXmlName(m_pool, QString(), QString(), QString());
}

QVector<QXmlName> DomNodeModel::namespaceBindings(const QXmlNodeModelIndex&) const {
	// TODO: Not implemented.
	return QVector<QXmlName>();
}

QVector<QXmlNodeModelIndex> DomNodeModel::nodesByIdref(const QXmlName&) const {
	// TODO: Not implemented.
	return QVector<QXmlNodeModelIndex>();
}

QXmlNodeModelIndex DomNodeModel::root(const QXmlNodeModelIndex& ni) const {
	QDomNode n = toDomNode(ni);
	while (!n.parentNode().isNull()) {
		n = n.parentNode();
	}
	return fromDomNode(n);
}

QSourceLocation	DomNodeModel::sourceLocation(const QXmlNodeModelIndex&) const {
	// TODO: Not implemented.
	return QSourceLocation();
}

QString	DomNodeModel::stringValue(const QXmlNodeModelIndex& ni) const {
	QDomNode n = toDomNode(ni);

	if (n.isProcessingInstruction()) {
		return n.toProcessingInstruction().data();
	} else if (n.isText()) {
		return n.toText().data();
	} else if (n.isComment()) {
		return n.toComment().data();
	} else if (n.isElement()) {
		return n.toElement().text();
	} else if (n.isDocument()) {
		return n.toDocument().documentElement().text();
	} else if (n.isAttr()) {
		return n.toAttr().value();
	}
	return QString();
}

QVariant DomNodeModel::typedValue(const QXmlNodeModelIndex & ni) const {
	return QVariant::fromValue(stringValue(ni));
}

QXmlNodeModelIndex DomNodeModel::fromDomNode(const QDomNode& n) const {
	if (n.isNull()) {
		return QXmlNodeModelIndex();
	}
	return createIndex(DomNode(n).getImpl(), 0);
}

QDomNode DomNodeModel::toDomNode(const QXmlNodeModelIndex& ni) const {
	return DomNode((QDomNodePrivate*)ni.data());
}

DomNodeModel::Path DomNodeModel::path(const QDomNode& n) const {
	Path res;
	QDomNode cur = n;
	while (!cur.isNull()) {
		res.push_back(cur);
		cur = cur.parentNode();
	}
	return res;
}

int DomNodeModel::childIndex(const QDomNode& n) const {

	QDomNodeList children = n.parentNode().childNodes();
	for (int i = 0; i < children.size(); ++i) {
		if (children.at(i) == n) {
			return i;
        }
    }
	return -1;
}

QVector<QXmlNodeModelIndex> DomNodeModel::attributes(const QXmlNodeModelIndex& ni) const {

	QDomElement n = toDomNode(ni).toElement();
	QDomNamedNodeMap attrs = n.attributes();
	QVector<QXmlNodeModelIndex> res;
	for (int i = 0; i < attrs.size(); i++)
	{
		res.push_back(fromDomNode(attrs.item(i)));
	}
	return res;
}

QXmlNodeModelIndex DomNodeModel::nextFromSimpleAxis(SimpleAxis axis,
                                        const QXmlNodeModelIndex & ni) const {

	QDomNode n = toDomNode(ni);
	switch(axis) {
        case Parent:            return fromDomNode(n.parentNode());
        case FirstChild:        return fromDomNode(n.firstChild());
        case PreviousSibling:   return fromDomNode(n.previousSibling());
        case NextSibling:       return fromDomNode(n.nextSibling());
	}

	return QXmlNodeModelIndex();
}