// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include "editor.h"
#include "runtimeManager.h"
#include "toolContext.h"


ToolContextBase::ToolContextBase(const QString& name, const QString& desc):
    m_name(name), m_desc(desc) {
        
    // RuntimeManager* manager = Editor::globalInstance()->runtime();
    // auto signal = &RuntimeManager::clientConnected;

    // m_clientConnected = QObject::connect(manager, signal, [this] {
        
    // });
}

ToolContextBase::~ToolContextBase() {
    // QObject::disconnect(m_clientConnected);
}
