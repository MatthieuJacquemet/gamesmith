// Copyright (C) 2022 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __EDITOR_DEFINES_H__
#define __EDITOR_DEFINES_H__

#include "gamesmith_config.h"

#define APP_ID "org." ORG_NAME "." APP_NAME

#define SERIALIZABLE_IID APP_ID ".serializable"

#define PROJECT_EXT ".gs"
#define LAYOUT_EXT ".lt"

#endif // __EDITOR_DEFINES_H__