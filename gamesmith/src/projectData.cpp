// Copyright (C) 2022 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QDataStream>
#include <QFile>
#include <QDebug>

#include "project.h"
#include "projectData.h"


ProjectData::Objects ProjectData::m_objects;



void ProjectData::serialize(QDataStream& stream) {
    
    quint32 n;
    stream >> n;
    for (quint32 i = 0; i < n; ++i) {
        readSection(stream);
    }
}

void ProjectData::deserialize(QDataStream& stream) {
    stream >> m_name >> m_version >> m_logo;
}


void ProjectData::readSection(QDataStream& stream) {
    
    QtPrivate::StreamStateSaver stateSaver(&stream);
    QString name; quint64 size;
    stream >> name >> size;

    QIODevice* device = stream.device();

    Serializable* object = m_objects.value(name);
    if (object == nullptr) {
        stream.skipRawData(size);
        return;
    }
    qint64 pos = device->pos();

    object->deserialize(stream);

    qint64 diff = device->pos() - pos;
    if (diff > size) {
        qWarning() << "Section" << name << ": read out of range";
    } else if (diff < size) {
        qWarning() << "Section" << name << ": read too few data";
    } else {
        return;
    }
    device->seek(pos + size);
}


void ProjectData::writeSection(const QString& name, QDataStream& stream) {

    QIODevice* device = stream.device();

    stream << name;
    qint64 start = device->pos();
    stream.skipRawData(sizeof(quint64));

    Serializable* object = m_objects.value(name);
    object->serialize(stream);

    qint64 end = device->pos();
    device->seek(start);
    stream << qint64(end - start);
    device->seek(end);
}


void ProjectData::saveData(const QString& path) {

    QFile file(path);

    if (file.open(QIODevice::WriteOnly)) {
        QDataStream stream(&file);
        stream << *this << m_objects.size();

        for (const QString& name: m_objects.keys()) {
            writeSection(name, stream);
        }
    }
}


void ProjectData::add(const QString& id, Serializable* obj) {
    m_objects.insert(id, obj);    
}


// void ProjectData::loadData(const QString& path, int options) {

//     QFile file(path);

//     if (file.open(QIODevice::ReadOnly)) {
//         QDataStream stream(&file);
//         stream >> *this;
//     }
// }


ProjectData::ProjectData():
    m_name("New Project"),
    m_version("1.0"),
    m_logo("qrc:/images/logo.png")
{
    
}
