// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QDebug>
#include "mathExpressionEvaluator.h"


extern "C" {
    int eval_expr(const char* expr, double* result);

    void parse_warning(const char* error) {
        throw std::runtime_error("Error: while parsing expression");
    }
}

double eval_math_expression(const QString& expression) {

    std::string str = expression.toStdString();
    double result;
    if (!eval_expr(str.c_str(), &result)) {
        throw std::runtime_error("Error: while parsing expression");
    }
    return result;
}