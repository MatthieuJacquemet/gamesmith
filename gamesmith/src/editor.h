// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __EDITOR_H__
#define __EDITOR_H__

#include <QApplication>

class MainWindow;
class Project;
class RuntimeManager;
class Config;
class PluginManager;
class Editor;
class EngineMetadata;
class QCommandLineParser;
class QSvgRenderer;
class SvgIconEngine;


struct ConstructCallback {
    ConstructCallback();
    typedef std::function<void (Editor*)> Callback;
    static void onCreated(const Callback& callback);
};


class Editor: public QApplication, public ConstructCallback {

    Q_OBJECT

public:    
    Editor(int& argc, char** argv);
    virtual ~Editor();

    inline static Editor* globalInstance() noexcept {
        return static_cast<Editor*>(QApplication::instance());
    }
    static QString relativePath(const QString& name);

    inline MainWindow* window() const { return m_window; };
    inline Project* project() const { return m_project; };
    inline Config* config() const { return m_config; };
    inline RuntimeManager* runtime() const { return m_runtime; };
    inline EngineMetadata* metadata() const { return m_metadata; };

    void setupCommand(const QCommandLineParser& parser);

    QIcon icon(const QString& uri) const noexcept;

public slots:
    void reloadStyle();

private:
    Q_DISABLE_COPY_MOVE(Editor)

    void initializeProject();

    PluginManager* m_pluginManager  = nullptr;
    Config* m_config                = nullptr;
    Project* m_project              = nullptr;
    MainWindow* m_window            = nullptr;
    RuntimeManager* m_runtime       = nullptr;
    EngineMetadata* m_metadata      = nullptr;
    QSvgRenderer* m_svgRenderer     = nullptr;
    mutable QHash<QString, QIcon> m_icons;
};


#endif // __EDITOR_H__