// Copyright (C) 2021 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __RUNTIMEMANAGER_H__
#define __RUNTIMEMANAGER_H__

#include <objectRequestBroker.h>
#include <QProcess>
#include <QDir>
#include "task.h"

class QFileSystemWatcher;
class QProcess;
class QSocketNotifier;
class QTimer;
class ServiceInterface;


class RuntimeManager : public QProcess {

    Q_OBJECT

public:
    using ArgsCallback = ObjectRequestBroker::ArgsCallback;
    using ServiceFactory = ObjectRequestBroker::ServiceFactory;
    using TypeList = QList<QPair<int32_t, QString>>;
    
    RuntimeManager(QObject* parent = Q_NULLPTR);
    ~RuntimeManager();

    bool setBindAddress(const QString& bindAddress);
    void initialize();

    bool isConnected() const noexcept;
    bool isLocal() const noexcept;

    void registerTransactionAwaiter(std::coroutine_handle<>&& handle);

    int32_t typeId(const QString& name) const;
    QString typeName(int32_t typeId) const;
    bool isSubclassOf(int32_t typeId, int32_t baseId) const;
    const QVector<int32_t>& parents(int32_t typeId) const;
    const QVector<int32_t>& children(int32_t typeId) const;

    TypeList subTypes(const QString& name, bool addBase=false) const;
    void recordTypeHandle(TypeHandle& handle, const QString& name);

    // ServiceInterface* currentTransaction() const;

    // Task<bool> sendMessage( const rpc::MessageHeader& header,
    //                         const ArgsCallback& callback);
    
    // Task<bool> beginTransaction(ServiceInterface* service);
    // void endTransaction();

    // ServiceInterface* createService(const QString& name,
    //                         const ServiceFactory& factory = nullptr);


    // Task<bool> releaseService(ServiceInterface* service, bool await = false);

signals:
    void started(const QString& binding_uri);
    void clientConnected();
    void clientDisconnected();

public slots:
    void terminateApplication();
    void clientDisconnectedImpl();
    void clientConnectedImpl();

private:
    class RuntimeManagerPrivate;
    std::unique_ptr<RuntimeManagerPrivate> d_ptr;
    Q_DECLARE_PRIVATE(RuntimeManager)
};


#endif // __RUNTIMEMANAGER_H__