// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <csignal>
#include <QCommandLineParser>
#include "gamesmith_config.h"
#include "editor.h"
#include "editor_utils.h"


static inline QVector<std::function<void()>>& initializeCallbacks() {
    static QVector<std::function<void()>> callbacks;
    return callbacks;
}


void onInitialized(const std::function<void()>& callback) {
    initializeCallbacks().append(callback);
}



int main(int argc, char** argv) {

	std::signal(SIGINT,  [](int) { QCoreApplication::quit(); });
    std::signal(SIGTERM, [](int) { QCoreApplication::quit(); });

	QLocale::setDefault(QLocale::c());

	Editor::setOrganizationName(ORG_NAME);
	Editor::setApplicationName(APP_NAME);
    Editor::setApplicationVersion(APP_VERSION);

	// Editor::setEffectEnabled(Qt::UI_FadeTooltip, false);
    // Editor::setEffectEnabled(Qt::UI_AnimateMenu, false);
    // Editor::setEffectEnabled(Qt::UI_AnimateTooltip, false);

	for (auto& callback : initializeCallbacks()) {
		callback();
	}
	// Editor::setAttribute(Qt::AA_ShareOpenGLContexts, true);

#ifdef Q_OS_LINUX
	#define DEFAULT_CONFIG_PATH "/etc"
#else
	#define DEFAULT_CONFIG_PATH "/ProgramData/" APP_NAME "/config"
#endif

	QCommandLineParser parser;
    parser.setApplicationDescription(
		QObject::tr("main", "Editor for panda3d game engine")
	);
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addOption(QCommandLineOption {
        {"p","plugin-dirs"},
        QObject::tr("main","Comma separated list of directory to search for plugins."),
        "plugin_dirs"
	});

    parser.addOption(QCommandLineOption(
        "config", "Use the specified configuration file", "file"
        DEFAULT_CONFIG_PATH
    ));
	Editor application(argc, argv);

	parser.process(application);
	application.setupCommand(parser);
	
	return application.exec();
}
