// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QButtonGroup>
#include <QLayout>
#include <QAction>
#include <QDebug>

#include "runtimeManager.h"
#include "toolButton.h"
#include "editor.h"
#include "selectToolsContainer.h"


template<>
QString ToolContext<stub::SelectTool>::modeNameImpl(uint32_t mode) const {
    switch (mode) {
        case ::SelectTool::M_rectangle:   return "rectangle";
        case ::SelectTool::M_circle:      return "circle";
        case ::SelectTool::M_lasso:       return "lasso";
        
        default: qWarning() << "Unknown mode" << mode;
    }
    return QString();
}


SelectToolsContainer::SelectToolsContainer(QWidget* parent):
    ToolBoxContainer(parent),
    m_context(new ContextType("select", "Selection Tools")) {
    

    ToolButton* button = new ToolButton(m_context, "transform");
    globalToolGroup()->addButton(button);

    button->addAction(tr("translate"),    SelectTool::M_rectangle);
    button->addAction(tr("circle"),       SelectTool::M_circle);
    button->addAction(tr("lasso"),        SelectTool::M_lasso);

    layout()->addWidget(button);

    RuntimeManager* mgr = Editor::globalInstance()->runtime();

    // default tool
    connect(mgr, &RuntimeManager::clientConnected, this, [this]() -> Task<> {
        if (auto* service = co_await m_context->service()) {
            service->activate(SelectTool::M_rectangle);
        }
    });
}
