// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __POPUP_H__
#define __POPUP_H__

#include <QWidget>
#include <cstddef>
#include <qdialog.h>
#include <qevent.h>
#include <qnamespace.h>

class QFrame;

constexpr auto default_flags =  Qt::BypassWindowManagerHint | 
                                Qt::WindowStaysOnTopHint |
                                Qt::FramelessWindowHint | Qt::Popup;

class Popup : public QDialog {
    Q_OBJECT
public:
    Popup(QWidget* parent = nullptr, Qt::WindowFlags flags = default_flags);
    virtual ~Popup() = default;

    inline void setParentWidget(QWidget* parent) { m_parent = parent; }
    inline QWidget* parentWidget() const { return m_parent; }
    inline QFrame* frame() const { return m_frame; }
private:
    void initialize();

protected:
    QFrame* m_frame = nullptr;
    QWidget* m_parent = nullptr;
    Popup(  QDialogPrivate& d, QWidget* parrent = nullptr,
            Qt::WindowFlags flags = default_flags);

    void showEvent(QShowEvent* event) override;
    // void focusOutEvent(QFocusEvent* event) override;
    // void changeEvent(QEvent* event) override;
};

#endif // __POPUP_H__