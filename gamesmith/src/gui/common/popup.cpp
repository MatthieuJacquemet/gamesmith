// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QEventLoop>
#include <QPointer>
#include <QWindow>
#include <QScreen>
#include <QLayout>
#include <QFrame>
#include <QEvent>
#include <qboxlayout.h>
#include <qframe.h>
#include "popup.h"


Popup::Popup(QWidget* parent, Qt::WindowFlags flags):
    QDialog(parent, flags) {
    initialize();
}

Popup::Popup(QDialogPrivate& d, QWidget* parent, Qt::WindowFlags flags):
    QDialog(d, parent, flags) {
    initialize();
}

void Popup::initialize() {
    
    setLayout(new QBoxLayout(QBoxLayout::TopToBottom, this));
    layout()->addWidget(m_frame = new QFrame);
    m_frame->setFrameStyle(QFrame::StyledPanel);

    setAttribute(Qt::WA_QuitOnClose, false);
    setAttribute(Qt::WA_TranslucentBackground, true);
}

void Popup::showEvent(QShowEvent* event) {
    QPoint p(pos());

    if (m_parent != nullptr) {
        p = m_parent->mapToGlobal(m_parent->rect().bottomLeft());
        p.rx() += (-width() + m_parent->width()) / 2;

        QWindow* window = m_parent->window()->windowHandle();
        QRect r = window->screen()->geometry();

        if (p.x() + width() > r.x() + r.width()) {
            p.setX(r.x() + r.width() - width());
        }
        if (p.y() + height() > r.y() + r.height()) {
            p.setY(r.y() + r.height() - height());
        }
    }
    move(p);

    QWidget::showEvent(event);
    activateWindow();
}

// void Popup::focusOutEvent(QFocusEvent* event) {
//     done(0);
// }

// void Popup::changeEvent(QEvent* event) {
//     if (event->type() == QEvent::ActivationChange && !isActiveWindow()) {
//         // done(0);
//     }
//     QWidget::changeEvent(event);
// }
