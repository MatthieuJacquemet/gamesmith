// Copyright 2023 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __SPINBOX_H__
#define __SPINBOX_H__

#include <QBoxLayout>
#include <QSpinBox>
#include "doubleSpinBox.h"


template<size_t N, class DataType> class SpinBoxVector;


class IntSpinBox : public QSpinBox {
    Q_OBJECT
public:
    using DataType = int;

    IntSpinBox(QWidget* parent = nullptr);
    virtual ~IntSpinBox() = default;
};


class SpinBoxVectorBase: public QWidget {
    Q_OBJECT
public:
    SpinBoxVectorBase(QBoxLayout::Direction dir, QWidget* parent=nullptr);
    virtual ~SpinBoxVectorBase() = default;

    typedef std::variant<IntSpinBox*, DoubleSpinBox*> SpinBoxVariant;
    virtual SpinBoxVariant getSpinBox(int index) const = 0;
    virtual size_t getSpinBoxCount() const = 0;

    template<class DataType>
    static auto create(int numComponents, QBoxLayout::Direction dir,
                        QWidget* parent = nullptr) {
        constexpr auto is = std::make_index_sequence<4>{};
        return create<DataType>(is, numComponents, dir, parent);
    }
    using ValueType = std::variant<double, int>;

signals:
    void valueChanged(int spinBoxIndex, ValueType value);

private:
    template<class DataType, size_t... Is>
    static inline auto create(std::index_sequence<Is...>, int n,
                            QBoxLayout::Direction dir, QWidget* parent) {
        
        using Factory = SpinBoxVectorBase*(QBoxLayout::Direction, QWidget*);
        constexpr Factory* factories[] = { do_create<DataType, Is>... };
        if (Q_LIKELY(n > 0 && n <= sizeof...(Is))) {
            return factories[n - 1](dir, parent);
        } else {
            qCritical("SpinBoxVectorBase::create: index out of range");
            return static_cast<SpinBoxVectorBase*>(nullptr);
        }
    }
    template<class DataType, size_t N>
    static SpinBoxVectorBase* do_create(QBoxLayout::Direction dir,
                                        QWidget* parent = nullptr) {
        return new SpinBoxVector<N + 1, DataType>(dir, parent);
    }
};

template<class T>
struct SpinBoxType;

template<std::floating_point T>
struct SpinBoxType<T> { using type = DoubleSpinBox; };

template<std::integral T>
struct SpinBoxType<T> { using type = IntSpinBox; };


// template<class T> struct SpinBoxType { using type = void; };

// template<> struct SpinBoxType<double> { using type = DoubleSpinBox; };
// template<> struct SpinBoxType<float> { using type = DoubleSpinBox; };
// template<> struct SpinBoxType<int> { using type = IntSpinBox; };


template<size_t N, class DataType>
class SpinBoxVector: public SpinBoxVectorBase {

public:
    enum { num_components = N };
    using value_type = DataType;

    SpinBoxVector(QBoxLayout::Direction dir, QWidget* parent = nullptr);
    virtual ~SpinBoxVector() = default;

    void setValue(int index, DataType value);
    DataType value(int index) const;
    void setValue(std::initializer_list<DataType> values);
    std::array<DataType, N> values() const;
    SpinBoxVariant getSpinBox(int index) const override;
    size_t getSpinBoxCount() const override;

protected:
    std::array<typename SpinBoxType<DataType>::type*, N> m_spinboxes;
};


typedef SpinBoxVector<1, int> SpinBoxVector1i;
typedef SpinBoxVector<2, int> SpinBoxVector2i;
typedef SpinBoxVector<3, int> SpinBoxVector3i;
typedef SpinBoxVector<4, int> SpinBoxVector4i;

typedef SpinBoxVector<1, double> SpinBoxVector1d;
typedef SpinBoxVector<2, double> SpinBoxVector2d;
typedef SpinBoxVector<3, double> SpinBoxVector3d;
typedef SpinBoxVector<4, double> SpinBoxVector4d;

#endif // __SPINBOX_H__