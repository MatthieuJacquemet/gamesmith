// Copyright 2023 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QWindow>
#include <QTimer>
#include <QMouseEvent>
#include <QApplication>
#include <QPaintEvent>
#include <QPainter>
#include <QAbstractSpinBox>
#include <QDoubleSpinBox>
#include <QDebug>

#include "spinBox.h"
#include "dragLineEdit.h"


DragLineEdit::DragLineEdit(QWidget* widget):
    m_prevMousePos(0, 0),
    m_isDragging(false),
    m_isEditing(false),
    m_valueChanged(false) {

    setAlignment(Qt::AlignCenter);
    setCursor(Qt::SizeHorCursor);

    if (auto* doubleSpinBox = qobject_cast<DoubleSpinBox*>(widget)) {
        m_widget = doubleSpinBox;
        m_initialValue = doubleSpinBox->value();
    } else if (auto* intSpinBox = qobject_cast<IntSpinBox*>(widget)) {
        m_widget = intSpinBox;
        m_initialValue = intSpinBox->value();
    }
    connect(this, &DragLineEdit::editingFinished, [this] {
        clearFocus();
    });
    connect(this, &DragLineEdit::returnPressed, [this] {
        clearFocus();
    });
    QFont font = QApplication::font();
    font.setStyleHint(QFont::Monospace);
    setFont(font);

    
}



void DragLineEdit::focusInEvent(QFocusEvent* event) {

}


void DragLineEdit::focusOutEvent(QFocusEvent* event) {

    setEditing(false);
    QLineEdit::focusOutEvent(event);
}


void DragLineEdit::setEditing(bool editing) {

    if (Q_UNLIKELY(editing == m_isEditing)) {
        return;
    }
    m_isEditing = editing;

    setAlignment(editing ? Qt::AlignLeft | Qt::AlignVCenter : Qt::AlignCenter);
    setCursor(editing ? Qt::IBeamCursor : Qt::SizeHorCursor);

    std::visit([this, editing](auto* widget) {
        if (!m_prefix.has_value()) {
            m_prefix = widget->prefix();
        }
        if (!m_suffix.has_value()) {
            m_suffix = widget->suffix();
        }
        widget->setPrefix(editing ? QString() : m_prefix.value());
        widget->setSuffix(editing ? QString() : m_suffix.value());

        if (editing) {
            setText(QString::number(widget->value()));
        }
    }, m_widget);
    
    if (QWidget* parent = parentWidget()) {
        emit parent->repaint();
    }
}



void DragLineEdit::mousePressEvent(QMouseEvent* event) {

    std::visit([this, event](auto* widget) {
        using WidgetType = std::remove_reference_t<decltype(*widget)>;
        using ValueType = decltype(widget->value());
        if (m_isEditing) {
            QLineEdit::mousePressEvent(event);
        }
        else if (m_isDragging && (event->button() & Qt::RightButton)) {
            clearFocus();
            finishDrag();
            widget->setValue(std::get<ValueType>(m_initialValue));

        } else if (event->button() & Qt::LeftButton) { // init drag

            QApplication::setOverrideCursor(Qt::BlankCursor);
            setContextMenuPolicy(Qt::PreventContextMenu);

            m_prevMousePos = QCursor::pos();
            m_initialValue = widget->value();
            m_isDragging = true;
        }
    }, m_widget);
}


void DragLineEdit::mouseReleaseEvent(QMouseEvent* event) {
    
    if (!m_isDragging || m_isEditing || event->button() != Qt::LeftButton) {
        return QLineEdit::mouseReleaseEvent(event);
    }
    if (m_valueChanged) {
        m_valueChanged = false;
        clearFocus();
    } else {
        setEditing(true);

        QFocusEvent focusEvent(QEvent::FocusIn, Qt::MouseFocusReason);
        QMouseEvent mouseEvent(QEvent::MouseButtonPress,
                                event->pos(), Qt::LeftButton, {}, {});

        QLineEdit::focusInEvent(&focusEvent);
        QLineEdit::mousePressEvent(&mouseEvent);

        // grabMouse();
    }
    finishDrag();
}


void DragLineEdit::mouseMoveEvent(QMouseEvent* event) {
    
    bool isPressed = qApp->mouseButtons() & Qt::LeftButton;

    if ((!m_isDragging && !isPressed) || m_isEditing) {
        return QLineEdit::mouseMoveEvent(event);
    }
    else if (m_isDragging) {
        std::visit([this, event](auto* widget) {

            int dragDist = QCursor::pos().x() - m_prevMousePos.x();
            auto dragMultiplier = widget->singleStep(); 

            if (event->modifiers() & Qt::ShiftModifier) {
                dragMultiplier /= 10;
            }
            widget->setValue(widget->value() + dragDist * dragMultiplier);
        
        }, m_widget);

        QCursor::setPos(m_prevMousePos);
        m_valueChanged = true;
    }
}


void DragLineEdit::finishDrag() {

    QApplication::restoreOverrideCursor();
    QTimer::singleShot(0, [this]() {
        setContextMenuPolicy(Qt::DefaultContextMenu);
    });
    m_isDragging = false;
}

























// template<typename SpinBoxT, typename ValueT>
// SpinBox<SpinBoxT, ValueT>::SpinBox(QWidget* parent):
//     SpinBoxT(parent)
// {

// }


// template<typename SpinBoxT, typename ValueT>
// void SpinBox<SpinBoxT, ValueT>::mouseMoveEvent(QMouseEvent* event) {

//     if (!(event->buttons() & Qt::LeftButton))
//         return SpinBoxT::mouseMoveEvent(event);

//     if (!is_dragging) {
//         start_drag_pos = QCursor::pos();
//         value = double(SpinBoxT::value());
//         is_dragging = true;
//         QApplication::setOverrideCursor(Qt::BlankCursor);
//     }
//     else {
//         int drag_dist = QCursor::pos().x() - start_drag_pos.x();

//         if (drag_dist == 0)
//             return;

//         double m_dragMultiplier = .25 * SpinBoxT::singleStep();

//         if (!(event->modifiers() & Qt::ControlModifier))
//             m_dragMultiplier *= 10.0;

//         value += m_dragMultiplier * drag_dist;

//         SpinBoxT::setValue(ValueT(value));

//         QCursor::setPos(start_drag_pos);
//     }
// }


// template<typename SpinBoxT, typename ValueT>
// void SpinBox<SpinBoxT, ValueT>::mouseReleaseEvent(QMouseEvent* event) {

//     if (!is_dragging || event->button() != Qt::LeftButton)
//         return SpinBoxT::mouseReleaseEvent(event);

//     is_dragging = false;
//     QApplication::restoreOverrideCursor();
// }



