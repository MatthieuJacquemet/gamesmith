// Copyright (C) 2022 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.


#include <QWidget>

#include "flowLayout.h"



FlowLayout::FlowLayout(QWidget *parent, int margin, int hSpacing, int vSpacing)
    : QLayout(parent), m_hSpace(hSpacing), m_vSpace(vSpacing) {
    setContentsMargins(margin, margin, margin, margin);
}


FlowLayout::FlowLayout(int margin, int hSpacing, int vSpacing)
    : m_hSpace(hSpacing), m_vSpace(vSpacing) {
    setContentsMargins(margin, margin, margin, margin);
}


FlowLayout::~FlowLayout() {
    QLayoutItem *item;
    while ((item = takeAt(0))) {
        delete item;
    }
}


int FlowLayout::horizontalSpacing() const {
    if (m_hSpace >= 0) {
        return m_hSpace;
    } else {
        return smartSpacing(QStyle::PM_LayoutHorizontalSpacing);
    }
}


int FlowLayout::verticalSpacing() const {
    if (m_vSpace >= 0) {
        return m_vSpace;
    } else {
        return smartSpacing(QStyle::PM_LayoutVerticalSpacing);
    }
}


void FlowLayout::addItem(QLayoutItem *item) {
    m_itemList.append(item);
}

int FlowLayout::count() const {
    return m_itemList.size();
}

QLayoutItem *FlowLayout::itemAt(int index) const {
    return m_itemList.value(index);
}

QSize FlowLayout::sizeHint() const {
    return minimumSize();
}


Qt::Orientations FlowLayout::expandingDirections() const {
    return { };
}

bool FlowLayout::hasHeightForWidth() const {
    return true;
}


int FlowLayout::heightForWidth(int width) const {

    int height = doLayout(QRect(0, 0, width, 0), true);
    return height;
}


QLayoutItem *FlowLayout::takeAt(int index) {

    if (index >= 0 && index < m_itemList.size()) {
        return m_itemList.takeAt(index);
    }
    return nullptr;
}


void FlowLayout::setGeometry(const QRect &rect) {

    QLayout::setGeometry(rect);
    doLayout(rect, false);
}


QSize FlowLayout::minimumSize() const {

    QSize size;
    for (const QLayoutItem *item : qAsConst(m_itemList)) {
        size = size.expandedTo(item->minimumSize());
    }

    const QMargins margins = contentsMargins();
    size += QSize(margins.left() + margins.right(), margins.top() + margins.bottom());
    return size;
}


int FlowLayout::doLayout(const QRect &rect, bool testOnly) const {

    int left, top, right, bottom;
    getContentsMargins(&left, &top, &right, &bottom);
    QRect effectiveRect = rect.adjusted(+left, +top, -right, -bottom);
    int x = effectiveRect.x();
    int y = effectiveRect.y();
    int lineHeight = 0;

    for (QLayoutItem* item : qAsConst(m_itemList)) {

        const QWidget* wid = item->widget();
        int spaceX = horizontalSpacing();

        if (spaceX == -1) {
            spaceX = wid->style()->layoutSpacing(
                QSizePolicy::PushButton, QSizePolicy::PushButton, Qt::Horizontal
            );
        }
        int spaceY = verticalSpacing();
        if (spaceY == -1) {
            spaceY = wid->style()->layoutSpacing(
                QSizePolicy::PushButton, QSizePolicy::PushButton, Qt::Vertical
            );
        }
        int nextX = x + item->sizeHint().width() + spaceX;

        if (nextX - spaceX > effectiveRect.right() && lineHeight > 0) {
            x = effectiveRect.x();
            y = y + lineHeight + spaceY;
            nextX = x + item->sizeHint().width() + spaceX;
            lineHeight = 0;
        }

        if (!testOnly) {
            item->setGeometry(QRect(QPoint(x, y), item->sizeHint()));
        }
        x = nextX;
        lineHeight = qMax(lineHeight, item->sizeHint().height());
    }
    return y + lineHeight - rect.y() + bottom;
}

int FlowLayout::smartSpacing(QStyle::PixelMetric pm) const {
    return 0;
}