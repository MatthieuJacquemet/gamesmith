// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __WIDGETS_H__
#define __WIDGETS_H__

#include <QLineEdit>
#include <QComboBox>
#include <QSlider>

#include "doubleSpinBox.h"

class QListWidgetItem;
class QListWidget;


class ComboBoxWidget: public QWidget {
    Q_OBJECT
public:
    ComboBoxWidget(const QString& name, QListWidgetItem* item);
    virtual ~ComboBoxWidget() = default;
    
    void initUi();
    QString name() const;

signals:
    void removeItem(QListWidgetItem* item);

private slots:
    void removeRequested();

protected:
    QString m_text;
    QListWidgetItem* m_item;
};


class ListComboBox: public QComboBox {
    Q_OBJECT
public:
    ListComboBox(QWidget* parent = nullptr);
    virtual ~ListComboBox() = default;

    void addItem(const QString& entry);
    void removeItem(QListWidgetItem* item);

protected:
    QListWidget* m_list;

    QStringList itemNames() const;
};


class Slider: public QSlider {
    Q_OBJECT
public:
    Slider(QWidget* parent = nullptr);
    virtual ~Slider() = default;

protected:
    virtual void mouseMoveEvent(QMouseEvent* event) override;
    virtual void mousePressEvent(QMouseEvent* event) override;
    virtual void mouseReleaseEvent(QMouseEvent* event) override;

private:
    bool m_isPressing;
};


class SliderEdit : public DoubleSpinBox {
    Q_OBJECT
public:
    using DataType = double;

    SliderEdit(QWidget* parent = nullptr);
    virtual ~SliderEdit() = default;

    void setMinimum(double minimumValue);
    void setMaximum(double maximumValue);
    void setRange(double min, double max);
};


class ComboBox: public QComboBox {
    Q_OBJECT
public:
    ComboBox(QWidget* parent = nullptr);
    virtual ~ComboBox() = default;

protected:
    virtual void showPopup() override;
};


#endif // __WIDGETS_H__