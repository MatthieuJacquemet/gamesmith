// Copyright 2023 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QBoxLayout>
#include <QSizePolicy>

#include <QFontDatabase>
#include "dragLineEdit.h"
#include "doubleSpinBox.h"
#include "spinBox.h"



SpinBoxVectorBase::SpinBoxVectorBase(QBoxLayout::Direction dir, QWidget* parent):
    QWidget(parent) {

    setLayout(new QBoxLayout(dir));
    layout()->setMargin(0);
    layout()->setSpacing(1);
    
    if (dir == QBoxLayout::LeftToRight) {
        layout()->setAlignment(Qt::AlignVCenter);
    } else if (dir == QBoxLayout::TopToBottom) {
        layout()->setAlignment(Qt::AlignHCenter);
    }
}


IntSpinBox::IntSpinBox(QWidget* parent): QSpinBox(parent) {
    setLineEdit(new DragLineEdit(this));
    setButtonSymbols(QAbstractSpinBox::ButtonSymbols::UpDownArrows);
    setFrame(false);
    setRange(std::numeric_limits<int>::lowest(), std::numeric_limits<int>::max());
    setMinimumWidth(30);
}


template<size_t N, class DataType>
SpinBoxVector<N, DataType>::SpinBoxVector(QBoxLayout::Direction dir, QWidget* parent):
    SpinBoxVectorBase(dir, parent) {

    using SpinBox = typename SpinBoxType<DataType>::type;
    auto signal = QOverload<DataType>::of(&SpinBox::valueChanged);
    for (int i = 0; i < N; ++i) {
        layout()->addWidget(m_spinboxes[i] = new SpinBox);
        connect(m_spinboxes[i], signal, this, [this, i](auto value) {
            emit valueChanged(i, value);
        });
    }
}

template<size_t N, class DataType>
void SpinBoxVector<N, DataType>::setValue(int index, DataType value) {
    m_spinboxes[index]->setValue(value);
}

template<size_t N, class DataType>
DataType SpinBoxVector<N, DataType>::value(int index) const {
    return m_spinboxes[index]->value();
}

template<size_t N, class DataType>
void SpinBoxVector<N, DataType>::setValue(std::initializer_list<DataType> values) {
    Q_ASSERT(values.size() == N);
    auto it = values.begin();
    for (auto* spinbox : m_spinboxes) {
        spinbox->setValue(*(it++));
    }
}
template<size_t N, class DataType>
std::array<DataType, N> SpinBoxVector<N, DataType>::values() const {
    std::array<DataType, N> values;
    for (int index=0; index<m_spinboxes.size(); index++) {
        values[index] = m_spinboxes[index]->value();
    }
    return values;
}

template<size_t N, class DataType>
SpinBoxVectorBase::SpinBoxVariant SpinBoxVector<N, DataType>::getSpinBox(int n) const {
    return m_spinboxes[n];
}

template<size_t N, class DataType>
size_t SpinBoxVector<N, DataType>::getSpinBoxCount() const {
    return m_spinboxes.size();
}

template class SpinBoxVector<1, int>;
template class SpinBoxVector<2, int>;
template class SpinBoxVector<3, int>;
template class SpinBoxVector<4, int>;

template class SpinBoxVector<1, double>;
template class SpinBoxVector<2, double>;
template class SpinBoxVector<3, double>;
template class SpinBoxVector<4, double>;