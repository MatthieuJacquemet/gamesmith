/*
 * Copyright (c) 2013-2021 Victor Wåhlström
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software
 * in a product, an acknowledgment in the product documentation would be
 * appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 */

#include "private/qdialog_p.h"
#include "colorPickerPopup.h"

#include "colorHexEdit.h"
#include "hueSaturationWheel.h"
#include "colorPicker.h"
#include "widgets.h"
#include "styleoptions.h"

#include <QStylePainter>
#include <QApplication>
#include <QButtonGroup>
#include <QScreen>
#include <QDesktopWidget>
#include <QEvent>
#include <QPixmap>
#include <QFrame>
#include <QRadioButton>
#include <QLabel>
#include <QPainter>
#include <QPushButton>
#include <QResizeEvent>
#include <QStackedWidget>
#include <QVBoxLayout>
#include <QtMath>
#include <QPointer>
#include <QDebug>
#include <qdialog.h>
#include <qwidget.h>

#include "editor_utils.h"

#ifdef HAVE_DBUS

#include <QDBusConnection>
#include <QDBusInterface>
#include <serializeTuple.h>

#include "requestinterface.h"
#include "screenshotinterface.h"



typedef std::tuple<double, double, double> RGBColor;
Q_DECLARE_METATYPE(RGBColor)

#endif


constexpr auto winFlags = Qt::Window | Qt::BypassWindowManagerHint | 
                        Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint;



class ColorValueSider final : public QWidget {

    Q_OBJECT

public:
    using PE = QStyle::PrimitiveElement;
    using SE = QStyle::SubElement;
    using CT = QStyle::ContentsType;

    explicit ColorValueSider(QWidget* parent=nullptr): QWidget(parent) {
        setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
        setFocusPolicy(Qt::ClickFocus);
    }
    void updateValue(double value) {
        m_currentValue = value, m_backupValue = value;
        update();
    }
signals:
    void valueChanged(double value);

protected:
    void initStyleOption(StyleOptionColorValueSlider* option) const {
        option->initFrom(this);
        option->value = m_currentValue;
    }
    void paintEvent(QPaintEvent* event) override {
        QStylePainter painter(this);
        StyleOptionColorValueSlider opt;
        initStyleOption(&opt);

        style()->drawPrimitive(PE(Gs::PE_ColorValueSlider),
                                &opt, &painter, this);
    }
    
    void mousePressEvent(QMouseEvent* event) override {
        if (event->buttons() & Qt::RightButton) {
            clearFocus();
            m_currentValue = m_backupValue;
            emit valueChanged(m_currentValue);
        } 
        else if (event->buttons() & Qt::LeftButton) {
            m_backupValue = m_currentValue;
            ColorValueSider::mouseMoveEvent(event);
        }
    }
    void mouseMoveEvent(QMouseEvent* event) override {
        if (hasFocus()) {
            updateValue(event->pos().y());
        }
    }
    QSize sizeHint() const override {
        QSize size = QWidget::sizeHint();
        StyleOptionColorValueSlider opt;
        initStyleOption(&opt);
        return style()->sizeFromContents(CT(Gs::PE_ColorValueSlider),
                                        nullptr, size, this)
                                    .expandedTo(QApplication::globalStrut());       
    }
private:
    void updateValue(int mousePosY) {
        StyleOptionColorValueSlider opt;
        initStyleOption(&opt);
        QRect r = style()->subElementRect(SE(Gs::PE_ColorValueSlider),
                                        &opt, this);

        qreal value = qreal(mousePosY - r.top()) / qreal(r.height());
        m_currentValue = qBound(0.0, 1.0 - value, 1.0);
        if (m_currentValue != opt.value) {
            emit update();
            emit valueChanged(m_currentValue);
        }
    }
    qreal m_currentValue = 0.0f;
    qreal m_backupValue = 0.0f;
};

#include "colorPickerPopup.moc"


class SliderGroup final : public QWidget {

public:
    enum Mode { M_rgb, M_hsv, M_hsl, COUNT };
    enum { NumSliders = 3 };

    SliderGroup(int hintPos, Qt::Key key, const QString& comp0,
                const QString& comp1, const QString& comp2) {

        std::array<const QString*, NumSliders> names{&comp0, &comp1, &comp2};
        setLayout(new QVBoxLayout);
        layout()->setSpacing(1);
        layout()->setMargin(0);
        setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

        QString label;
        for (const QString* name : names) {
            if (label.size() == hintPos) label.append(QChar('&'));
            label.append(name->at(0).toUpper());
        }
        m_radioButton = new QRadioButton(std::move(label));
        m_radioButton->setShortcut(QKeySequence(Qt::AltModifier | key));

        for (int i=0; i<m_sliders.size(); ++i) {
            m_sliders[i] = new SliderEdit;
            m_sliders[i]->setRange(0.0, 1.0);
            m_sliders[i]->setPrefix(QString(names[i]->at(0)) + ": ");
            m_sliders[i]->setToolTip(*names[i]);

            layout()->addWidget(m_sliders[i]);
        }
    }

    void setColor(QColor& color, Mode mode) const {
        void (QColor::*setter)(qreal, qreal, qreal, qreal);
        switch (mode) {
            case Mode::M_rgb: setter = &QColor::setRgbF; break;
            case Mode::M_hsv: setter = &QColor::setHsvF; break;
            case Mode::M_hsl: setter = &QColor::setHslF; break;
            default: break;
        }
        qreal data[NumSliders];
        for (int i=0; i<m_sliders.size(); ++i) {
            data[i] = m_sliders[i]->value();
        }
        (color.*setter)(data[0], data[1], data[2], color.alphaF());
    }
    
    void updateColor(const QColor& color, Mode mode, QWidget* editor) {
        void (QColor::*getter)(qreal*, qreal*, qreal*, qreal*) const;
        switch (mode) {
            case Mode::M_rgb: getter = &QColor::getRgbF; break;
            case Mode::M_hsv: getter = &QColor::getHsvF; break;
            case Mode::M_hsl: getter = &QColor::getHslF; break;
            default: break;
        }
        qreal data[4];
        (color.*getter)(data, data + 1, data + 2, data + 3);
        for (int i=0; i<m_sliders.size(); ++i) {
            // only update the sliders in groups other than the one being edited
            if (m_sliders[i] != editor) {
                QSignalBlocker blocker(m_sliders[i]);
                m_sliders[i]->setValue(data[i]);
            }
        }
    }
    inline SliderEdit* slider(int i) const { return m_sliders[i]; }
    inline QRadioButton* radioButton() const { return m_radioButton; }

private:
    QRadioButton* m_radioButton = nullptr;
    std::array<SliderEdit*, NumSliders> m_sliders;
};


class ColorPickingEventFilter;


class ColorPickerPopupPrivate : public QDialogPrivate {

    Q_DECLARE_PUBLIC(ColorPickerPopup)

public:
    explicit ColorPickerPopupPrivate() = default;


    void syncColors(const QColor& color, QWidget* widget=nullptr) {
        Q_Q(ColorPickerPopup);
        if (widget != value) value->updateValue(color.valueF());
        if (widget != wheel) wheel->updateColor(color);
        if (widget != hex) hex->updateColor(color);

        if (widget != alphaSlider) {
            QSignalBlocker blocker(alphaSlider);
            alphaSlider->setValue(color.alphaF());
        }
        for (int mode=0; mode<SliderGroup::COUNT; ++mode) {
            SliderGroup* group = sliderGroups[mode];
            group->updateColor(color, SliderGroup::Mode(mode), widget);   
        }
        if (Q_LIKELY(this->color != color)) {
            this->color = color;
            emit q->update(); emit q->colorChanged(color);
        }
    }
    ColorHexEdit* hex               = nullptr;
    HueSaturationWheel* wheel       = nullptr;
    QButtonGroup* buttonGroup       = nullptr;
    ColorValueSider* value          = nullptr;
    SliderEdit* alphaSlider         = nullptr;
    QStackedWidget* sliderStack     = nullptr;
    ColorPicker* colorPicker        = nullptr;
    QPushButton* pickScreenButton   = nullptr;
    ColorPickingEventFilter* cpef   = nullptr;
    std::array<SliderGroup*, SliderGroup::COUNT> sliderGroups;
    QColor color                    = Qt::white;
    ColorPicker::EditType editType  = ColorPicker::Float;
};


class ColorPickingEventFilter final : public QObject {

public:
    ColorPickingEventFilter(ColorPickerPopupPrivate* d, QObject *parent):
        QObject(parent), d_ptr(d) {}

#ifdef HAVE_DBUS
    Task<> pickScreenColor() {
        qobject_cast<QDialog*>(d_ptr->q_ptr)->done(0);
    
        static const OnlyOnce register_metatypes([] {
            qDBusRegisterMetaType<RGBColor>();
            qRegisterMetaType<RGBColor>("RGBColor");
        });
        static org::freedesktop::portal::Screenshot screenshot(
            QStringLiteral("org.freedesktop.portal.Desktop"),
            QStringLiteral("/org/freedesktop/portal/desktop"),
            QDBusConnection::sessionBus()
        );
        auto reply = co_await DBusAwaiter(screenshot.PickColor("", {}));
        if (Q_UNLIKELY(!reply.isValid())) {
            qWarning() << "Failed to pick color on screen";
            co_return;
        }
        org::freedesktop::portal::Request request(
            QStringLiteral("org.freedesktop.portal.Desktop"),
            reply.value().path(), QDBusConnection::sessionBus()
        );

        auto [status, response] = co_await SignalAwaiter(&request,
            &org::freedesktop::portal::Request::Response
        );  
        if (Q_LIKELY(status == 0)) {
            auto [r,g,b] = qdbus_cast<RGBColor>(response.value("color"));
            d_ptr->syncColors(QColor::fromRgbF(r,g,b));
        } else {
            qWarning() << "Failed to pick color on screen:" << status;
        }
        co_await DBusAwaiter(request.Close());
    }
#else
    void startPicking();
        QWidget* widget = qobject_cast<QWidget*>(parent());
        beforePickingColor = d_ptr->color;
        widget->installEventFilter(this);

        widget->grabMouse(Qt::CrossCursor);
        widget->grabKeyboard();
        widget->setMouseTracking(true);
        d_ptr->colorPicker->blockSignals(true);
        d_ptr->pickScreenButton->setDisabled(true);
    }

    void releasePicking() {
        QWidget* widget = qobject_cast<QWidget*>(parent());
        widget->removeEventFilter(this);
        widget->releaseMouse();
        widget->releaseKeyboard();
        widget->setMouseTracking(false);
        d_ptr->pickScreenButton->setDisabled(false);
        d_ptr->colorPicker->blockSignals(false);
    }
protected:
    bool eventFilter(QObject *, QEvent *event) override {
        switch (event->type()) {
            case QEvent::MouseButtonPress:
                return handleMousePress(static_cast<QMouseEvent*>(event));
            case QEvent::KeyPress:
                return handleKeyPress(static_cast<QKeyEvent*>(event));
            default:
                break;
        }
        return false;
    }
private:
    bool handleMousePress(QMouseEvent* event) {
        
        if (event->buttons() & Qt::LeftButton) {
            qWarning() << "Left button pressed";
            d_ptr->syncColors(grabScreenColor(QCursor::pos()));
        } 
        else if (event->buttons() & Qt::RightButton) {
            d_ptr->syncColors(beforePickingColor);
        }
        releasePicking();
        return true;
    }
    bool handleMouseMove(QMouseEvent* event) {
        // d_ptr->syncColors(grabScreenColor(QCursor::pos()));
        return true;
    }
    bool handleKeyPress(QKeyEvent* event) {
        if (event->matches(QKeySequence::Cancel)) {
            releasePicking();
            d_ptr->syncColors(beforePickingColor);
        } 
        else if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter) {
            d_ptr->syncColors(grabScreenColor(QCursor::pos()));
            releasePicking();
        }
        event->accept();
        return true;
    }

    QColor grabScreenColor(const QPoint& pos) {
        QDesktopWidget* desktop = QApplication::desktop();
        QScreen* screen = QGuiApplication::screenAt(pos);
        if (screen == nullptr) {
            screen = QGuiApplication::primaryScreen();
        }
        auto pm = screen->grabWindow(desktop->winId(), pos.x(), pos.y(), 1, 1);
        return pm.toImage().pixel(0, 0);
    }
    QColor beforePickingColor;
#endif
    ColorPickerPopupPrivate* d_ptr;
};


ColorPickerPopup::ColorPickerPopup(ColorPicker* colorPicker):
    Popup(*new ColorPickerPopupPrivate) {

    Q_D(ColorPickerPopup);
    d->colorPicker = colorPicker;
    
    d->cpef                     = new ColorPickingEventFilter(d, this);
    d->wheel                    = new HueSaturationWheel;
    d->value                    = new ColorValueSider;
    d->hex                      = new ColorHexEdit;
    d->sliderStack              = new QStackedWidget;
    d->buttonGroup              = new QButtonGroup;
    d->alphaSlider              = new SliderEdit;
    d->pickScreenButton         = new QPushButton;

    QVBoxLayout* layout         = new QVBoxLayout;
    QHBoxLayout* topLayout      = new QHBoxLayout;
    QHBoxLayout* midLayout      = new QHBoxLayout;
    QHBoxLayout* bottomLayout   = new QHBoxLayout;

    m_frame->setLayout(layout);
    layout->setMargin(10);

    topLayout->addWidget(d->hex);
    topLayout->addWidget(d->pickScreenButton);
    topLayout->setAlignment(d->hex, Qt::AlignCenter);

    d->wheel->setToolTip(tr("Hue/Saturation"));
    d->wheel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    midLayout->setContentsMargins(5, 0, 5, 0);
    bottomLayout->setSpacing(1);

    d->value->setToolTip(tr("Value"));

    midLayout->addWidget(d->wheel);
    midLayout->addWidget(d->value);

    d->sliderStack->setCurrentIndex(0);
    d->sliderStack->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);


    d->sliderStack->addWidget(
        d->sliderGroups[SliderGroup::M_rgb] = new SliderGroup(
            0, Qt::Key_1, tr("Red"), tr("Green"), tr("Blue")
        )
    );
    d->sliderStack->addWidget(
        d->sliderGroups[SliderGroup::M_hsv] = new SliderGroup(
            2, Qt::Key_2, tr("Hue"), tr("Saturation"), tr("Value")
        )
    );
    d->sliderStack->addWidget(
        d->sliderGroups[SliderGroup::M_hsl] = new SliderGroup(
            2, Qt::Key_1, tr("Hue"), tr("Saturation"), tr("Lightness")
        )
    );
    d->buttonGroup->setExclusive(true);

    for (int i=0; i<d->sliderGroups.size(); ++i) {
        d->buttonGroup->addButton(d->sliderGroups[i]->radioButton(), i);
        bottomLayout->addWidget(d->sliderGroups[i]->radioButton());
    }
    d->sliderGroups[SliderGroup::M_rgb]->radioButton()->setChecked(true);

    d->alphaSlider->setPrefix(tr("A: "));
    d->alphaSlider->setToolTip(tr("Alpha"));
    d->alphaSlider->setRange(0.0, 1.0);
    d->alphaSlider->setValue(1.0);


    layout->addLayout(topLayout);
    layout->addLayout(midLayout);
    layout->addLayout(bottomLayout);
    layout->addWidget(d->sliderStack);
    layout->addWidget(d->alphaSlider);

    d->pickScreenButton->setFixedSize(20, 20);
    d->pickScreenButton->setIconSize(QSize(14,14));
    d->pickScreenButton->setIcon(
        style()->standardIcon(Gs::sp(Gs::SP_EyeDropper))
    );
    
    connect(d->buttonGroup, &QButtonGroup::idClicked,
            d->sliderStack, &QStackedWidget::setCurrentIndex);

    connect(d->pickScreenButton, &QPushButton::clicked,
            d->cpef, &ColorPickingEventFilter::pickScreenColor);

    auto sig = QOverload<double>::of(&SliderEdit::valueChanged);

    for (int mode = 0; mode < SliderGroup::COUNT; ++mode) {
        const SliderGroup* group = d->sliderGroups[mode];

        for (int i=0; i<SliderGroup::NumSliders; ++i) {
            connect(group->slider(i), sig, [d, i, mode](double value) {
                SliderGroup* group = d->sliderGroups[mode];
                QColor color = d->color;
                group->setColor(color, SliderGroup::Mode(mode));

                d->syncColors(color, group->slider(i));
            });
        }
    }
    connect(d->alphaSlider, sig, [d](double value) {
        QColor color = d->color;
        color.setAlphaF(value);
        d->syncColors(std::move(color), d->alphaSlider);
    });

    connect(d->wheel, &HueSaturationWheel::colorChanged, [d](auto& color) {
        d->syncColors(color, d->wheel);
    });
    connect(d->hex, &ColorHexEdit::colorChanged, [d](auto& color) {
        d->syncColors(color, d->hex);
    });
    connect(d->value, &ColorValueSider::valueChanged, [d](double value) {
        qreal data[4];
        d->color.getHsvF(data, data + 1, data + 2, data + 3);
        data[2] = value;
        d->syncColors(QColor::fromHsvF(data[0], data[1], data[2], data[3]),
                        d->value); 
    });
    
    d->wheel->setColor(colorPicker->color()); // sync color of all child widgets
}

void ColorPickerPopup::setColor(const QColor& color) {
    Q_D(ColorPickerPopup);

    if (d->color != color) {
        d->syncColors(color, nullptr);
        emit colorChanged(d->color);
    }
}

bool ColorPickerPopup::displayAlpha() const {
    Q_D(const ColorPickerPopup);
    return d->hex->displayAlpha();
}

void ColorPickerPopup::setDisplayAlpha(bool visible) {
    Q_D(ColorPickerPopup);

    d->alphaSlider->setVisible(visible);
    d->hex->setDisplayAlpha(visible);
}

ColorPicker::EditType ColorPickerPopup::editType() const {
    Q_D(const ColorPickerPopup);
    return d->editType;
}

void ColorPickerPopup::setEditType(ColorPicker::EditType type) {
    Q_D(ColorPickerPopup);
    d->editType = type;
}









// class ColorPickingPreview : public QLabel {

// public:
//     static ColorPickingPreview& instance() {
//         static ColorPickingPreview singleton;
//         return singleton;
//     }
//     void setCurrentColor(const QColor& color) {
//         m_color = color;
//         repaint();
//     }
// protected:
//     void paintEvent(QPaintEvent* event) override {
//         QPainter painter(this);
//         painter.setPen(Qt::white);
//         painter.setRenderHint(QPainter::Antialiasing);
//         painter.setBrush(m_color);
//         painter.drawEllipse(rect());
//     }
// private:
//     ColorPickingPreview(): QLabel(nullptr, Qt::Widget | Qt::FramelessWindowHint) {
//         setAttribute(Qt::WA_AlwaysStackOnTop);
//         setAttribute(Qt::WA_TranslucentBackground);
//         setAttribute(Qt::WA_X11DoNotAcceptFocus);
//         setAttribute(Qt::WA_InputMethodTransparent);
//         setFocusPolicy(Qt::NoFocus);
//         setFixedSize(16, 16);
//     }
//     QColor m_color;
// };


// //! @cond Doxygen_Suppress
// class ColorQSlider : public QSlider {
// public:
//     explicit ColorQSlider(const QMap<qreal, QColor>& gradients): QSlider(),
//         , m_gradients(gradients)
//         , m_textColor(isBright(gradients.last()) ? Qt::black : Qt::white) {
//         // setSliderComponents(QSlider::SliderComponent::Marker | QSlider::SliderComponent::Text);
//         setAlignment(Qt::AlignRight);
//     }

// protected:
//     void resizeEvent(QResizeEvent* event) {
//         QPalette p = palette();

//         QImage img(event->size(), QImage::Format_ARGB32_Premultiplied);
//         img.fill(0);

//         QRect r = img.rect();
//         QPainter painter(&img);

//         // quint32 size = qMin(r.width(), r.height()) / 2;
//         // drawCheckerboard(painter, r, size);

//         QLinearGradient g;
//         if (orientation() == Qt::Horizontal) {
//             g = QLinearGradient(QPointF(0.0f, 0.0f), QPointF((float)event->size().width(), 0.0f));
//         else {
//             g = QLinearGradient(QPointF(0.0f, (float)event->size().height()), QPointF(0.0f, 0.0f));
//         }
//         auto i = m_gradients.begin();
//         while (i != m_gradients.end()) {
//             g.setColorAt(i.key(), i.value());
//             ++i;
//         }

//         painter.fillRect(r, g);

//         // p.setBrush(QPalette::Base, img);
//         p.setBrush(QPalette::Text, m_textColor);
//         setPalette(p);
//     }

//   private:
//     const QMap<qreal, QColor> m_gradients;
//     const QColor m_textColor;
// };
