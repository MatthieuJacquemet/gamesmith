// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QBoxLayout>
#include <QToolButton>
#include <QStyle>
#include <QDebug>
#include <QMenu>
#include <QPainterPath>
#include <QListWidget>
#include <QListView>
#include <QStyledItemDelegate>
#include <QFontDatabase>

#include "mainWindow.h"
#include "editor.h"
#include "dragLineEdit.h"
#include "widgets.h"


Slider::Slider(QWidget* parent): QSlider(parent),
    m_isPressing(false) {

}

void Slider::mousePressEvent(QMouseEvent* event) {
    QSlider::mousePressEvent(event);
    m_isPressing = true;
}

void Slider::mouseReleaseEvent(QMouseEvent* event) {
    QSlider::mouseReleaseEvent(event);
    m_isPressing = false;
}


void Slider::mouseMoveEvent(QMouseEvent* event) {
    
    if (m_isPressing) {
        QSlider::mouseMoveEvent(event);
    }
}



SliderEdit::SliderEdit(QWidget* parent) : DoubleSpinBox(parent) {
    setLineEdit(new DragLineEdit(this));
    setButtonSymbols(QAbstractSpinBox::NoButtons);
    setFrame(false);
    // setDecimals(16);
    // setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
}

void SliderEdit::setMinimum(double minimumValue) {
    DoubleSpinBox::setMinimum(minimumValue);
    setSingleStep(std::abs(maximum() - minimum()) * 0.01);
}

void SliderEdit::setMaximum(double maximumValue) {
    DoubleSpinBox::setMinimum(maximumValue);
    setSingleStep(std::abs(maximum() - minimum()) * 0.01);
}

void SliderEdit::setRange(double min, double max) {
    DoubleSpinBox::setRange(min, max);
    setSingleStep(std::abs(maximum() - minimum()) * 0.01);
}


ComboBox::ComboBox(QWidget* parent): QComboBox(parent) {
    setView(new QListView);
    setModel(view()->model());
	setItemDelegate(new QStyledItemDelegate(this));

    // setStyleSheet(Editor::globalInstance()->styleSheet());
}



void ComboBox::showPopup() {

    QComboBox::showPopup();

    QWidget* widget = view()->parentWidget();
    Editor* editor = Editor::globalInstance();

    QPoint pos = mapToGlobal(QPoint(0, height()));
    QPoint winPos = editor->window()->pos();
    QSize winSize = editor->window()->size();

    widget->setFixedWidth(width());
    const int margin = 5;

    int maxX = winPos.x() + winSize.width();
    int maxY = winPos.y() + winSize.height();

    int w = widget->width() + margin;
    int h = widget->height() + margin;


    if (pos.x() + margin < winPos.x()) pos.setX(winPos.x() + margin);
    if (pos.y() + margin < winPos.y()) pos.setY(winPos.y() + margin);
    if (pos.x() + w > maxX) pos.setX(maxX - w);
    if (pos.y() + h > maxY) pos.setY(maxY - h);

    widget->move(std::move(pos));
}




ComboBoxWidget::ComboBoxWidget(const QString& name, QListWidgetItem* item):
    m_text(name), m_item(item) {

    initUi();
}


void ComboBoxWidget::initUi() {

    setLayout(new QHBoxLayout(this));
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);

    QToolButton* delBtn = new QToolButton(this);
    delBtn->setAutoRaise(true);
    delBtn->setToolTip(tr("Delete layout"));

    connect(delBtn, &QToolButton::clicked,
            this, &ComboBoxWidget::removeRequested);

    layout()->addItem(new QSpacerItem(0, 0, QSizePolicy::Expanding,
                                            QSizePolicy::Minimum));
    layout()->addWidget(delBtn);
}


QString ComboBoxWidget::name() const {
    return m_text;
}


void ComboBoxWidget::removeRequested() {
    Q_EMIT removeItem(m_item);
}



ListComboBox::ListComboBox(QWidget* parent): QComboBox(parent),
    m_list(new QListWidget(this)) {
    
    setModel(m_list->model());
    setView(m_list);
}


void ListComboBox::addItem(const QString& entry) {

    QListWidgetItem* item = new QListWidgetItem(m_list);
    ComboBoxWidget* iteWidget = new ComboBoxWidget(entry, item);

    connect(iteWidget, SIGNAL(removeItem(QListWidgetItem*)),
            this, SLOT(removeItem(QListWidgetItem*)));

    item->setSizeHint(iteWidget->sizeHint());
    m_list->addItem(item);
    m_list->setItemWidget(item, iteWidget);
}


void ListComboBox::removeItem(QListWidgetItem* item) {

    int row = m_list->row(item);
    delete m_list->takeItem(row);
}

QStringList ListComboBox::itemNames() const {
    
    QStringList names;

    int num = m_list->count();

    for (int i=0; i<num; ++i) {
        QWidget* widget = m_list->itemWidget(m_list->item(i));
        ComboBoxWidget* item = static_cast<ComboBoxWidget*>(widget);
        names.append(item->name());
    }
    return names;
}
