// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <private/qwidget_p.h>
#include <QPushButton>
#include <QBoxLayout>
#include <QToolButton>
#include <QPainter>
#include <QPropertyAnimation>
#include <QGridLayout>
#include <QTimer>

#include "editor_utils.h"
#include "editor.h"
#include "spoiler.h"

#define DEFAULT_ANIMATION_DURATION 0


class Container : public QScrollArea {

public:
    QSize sizeHint() const override {
        QSize size = QScrollArea::sizeHint();
        if (QWidget* widget = this->widget()) {
            size.setHeight(widget->sizeHint().height());
        }
        return size;
    }
    QSize minimumSizeHint() const override {
        return QSize(0, 0);
    }
};


class Spoiler::SpoilerPrivate final : public QWidgetPrivate {

    Q_DECLARE_PUBLIC(Spoiler)
public:
    SpoilerPrivate():   toggleButton(new QToolButton),
                        mainLayout(new QGridLayout),
                        animation(new QPropertyAnimation),
                        contentContainer(new Container),
                        content(nullptr) {
        
        animation->setDuration(DEFAULT_ANIMATION_DURATION);
        animation->setPropertyName("maximumHeight");
        contentContainer->setLayout(new QVBoxLayout);
        
    }
    QWidget*                    content;
    QToolButton*                toggleButton;
    QPropertyAnimation*         animation;
    QGridLayout*                mainLayout;
    QScrollArea*                contentContainer;
    Features                    features;
};


Spoiler::Spoiler(QWidget* parent): Spoiler("", parent) {

}

Spoiler::Spoiler(const QString& title, QWidget* parent):
    QWidget(*new SpoilerPrivate(), parent, Qt::Widget) {
    
    Q_D(Spoiler);

    d->mainLayout->setVerticalSpacing(0);
    d->mainLayout->setContentsMargins(0, 0, 0, 0);
    setLayout(d->mainLayout);

    d->toggleButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    d->toggleButton->setArrowType(Qt::ArrowType::DownArrow);
    d->toggleButton->setCheckable(true);
    d->toggleButton->setChecked(true);

    d->toggleButton->setText(title);
    QPalette palette = d->toggleButton->palette();
    palette.setColor(QPalette::Button, Qt::transparent);
    d->toggleButton->setPalette(palette);

    d->animation->setParent(this);
    d->mainLayout->addWidget(d->toggleButton, 0, 0, 1, 1, Qt::AlignLeft);



    d->animation->setTargetObject(d->contentContainer);
    d->mainLayout->addWidget(d->contentContainer, 1, 0, 1, 3, Qt::AlignTop);
    d->contentContainer->setMinimumHeight(0);
    d->contentContainer->setFrameStyle(QFrame::NoFrame);
    d->contentContainer->setWidgetResizable(true);
    d->contentContainer->viewport()->setAutoFillBackground(false);

    const auto policy = Qt::ScrollBarPolicy::ScrollBarAlwaysOff;
    d->contentContainer->setVerticalScrollBarPolicy(policy);
    d->contentContainer->setHorizontalScrollBarPolicy(policy);
    d->contentContainer->setSizePolicy( QSizePolicy::Expanding,
                                        QSizePolicy::Minimum);



    connect(d->toggleButton, &QToolButton::clicked, [this](bool checked) {
        Q_D(Spoiler);
        d->toggleButton->setArrowType(checked ?
            Qt::ArrowType::DownArrow : Qt::ArrowType::RightArrow
        );
        if (d->content != nullptr) {
            d->animation->setStartValue(
                checked ? 0 : d->contentContainer->height()
            );
            d->animation->setEndValue(
                checked ? d->contentContainer->sizeHint().height() : 0
            );
            d->animation->start();
        }
    });

    connect(d->animation, &QPropertyAnimation::finished, this, [this] {
        Q_D(Spoiler);
        if (d->contentContainer->maximumHeight() != 0) {
            d->contentContainer->setMaximumHeight(QWIDGETSIZE_MAX);
        }
    });

    enableFeatures(Features(Background | RoundedCorner), true);
}

Spoiler::~Spoiler() {
    
}

void Spoiler::setTitle(const QString& title) noexcept {
    Q_D(Spoiler);
    d->toggleButton->setText(title);
}

QString Spoiler::title() const noexcept {
    Q_D(const Spoiler);
    return d->toggleButton->text();
}

void Spoiler::setAnimationDuration(int duration) noexcept {
    Q_D(Spoiler);
    d->animation->setDuration(duration);
}

int Spoiler::animationDuration() const noexcept {
    Q_D(const Spoiler);
    return d->animation->duration();
}

void Spoiler::setContentWidget(QWidget* widget) noexcept {
    Q_D(Spoiler);
    if (QWidget* previous = std::exchange(d->content, widget)) {
        emit previous->deleteLater();
    }
    d->contentContainer->setWidget(widget);
    widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
    widget->installEventFilter(this);

    widget->setAutoFillBackground(false);
}

void Spoiler::paintEvent(QPaintEvent* event) {
    Q_D(Spoiler);
    QPainter p(this);
    if ((d->features & Feature::Background) == 0) {
        //nothing to do
    } else if (d->features & Feature::RoundedCorner) {
        p.setRenderHint(QPainter::Antialiasing);
        QPainterPath path;
        path.addRoundedRect(rect(), 4, 4);
        p.setPen(Qt::NoPen);
        p.fillPath(path, QColor(255, 255, 255, 15));
    }
    else {
        p.fillRect(rect(), QColor(255, 255, 255, 15));
    }
}

bool Spoiler::eventFilter(QObject* object, QEvent* event) {
    if (event->type() == QEvent::Resize) {
        Q_D(Spoiler);
        d->contentContainer->updateGeometry();
    }
    return false;
}

void Spoiler::enableFeatures(Features features, bool enable) {
    Q_D(Spoiler);
    d->features = enable ? d->features | features : d->features & ~features;

    if (features & Feature::CloseButton) {
        if (enable) {
            QPushButton* button = new QPushButton;
            button->setIcon(Editor::globalInstance()->icon("close"));
            button->setIconSize(smallIconSize(button->icon(), this));
            QPalette palette;
            palette.setColor(QPalette::Button, Qt::transparent);
            button->setPalette(palette);

            connect(button, &QPushButton::clicked,
                    this, &Spoiler::closeButtonPressed);

            d->mainLayout->addWidget(button, 0, 2, 1, 1, Qt::AlignRight);
        }
        else if (QLayoutItem* item = d->mainLayout->itemAtPosition(0, 2)) {
            d->mainLayout->removeItem(item);
        }
    }
}