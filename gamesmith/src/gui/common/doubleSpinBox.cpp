// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QStylePainter>
#include "dragLineEdit.h"
#include "mathExpressionEvaluator.h"
#include "doubleSpinBox.h"

template<class T> using limits = std::numeric_limits<T>;

DoubleSpinBox::DoubleSpinBox(QWidget * parent): QDoubleSpinBox(parent),
    m_dispDecimals(2) {
    
    setLineEdit(new DragLineEdit(this));
    setButtonSymbols(QAbstractSpinBox::ButtonSymbols::UpDownArrows);
    setFrame(false);

	setDecimals(2);
	QDoubleSpinBox::setDecimals(1000);

    setRange(limits<double>::lowest(), limits<double>::max());
    setMinimumWidth(30);
}

int DoubleSpinBox::decimals() const {
	return m_dispDecimals;
}


void DoubleSpinBox::setDecimals(int value) {
	m_dispDecimals = value;
}


QString DoubleSpinBox::textFromValue(double value) const {
	return locale().toString(value, 'f', m_dispDecimals);
}


double DoubleSpinBox::valueFromText(const QString &text) const {
    try {
        return eval_math_expression(text);
    } catch (std::exception& e) {
        return value();
    }
}

QValidator::State DoubleSpinBox::validate(QString &text, int &pos) const {
    return QValidator::Acceptable;
}