// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __DOUBLESPINBOX_H__
#define __DOUBLESPINBOX_H__

#include <QDoubleSpinBox>


static bool isIntermediateValueHelper(qint64 num, qint64 minimum, qint64 maximum,
                                    qint64 *match = 0);


class DoubleSpinBox : public QDoubleSpinBox {

	Q_OBJECT

public:
	using DataType = double;

    DoubleSpinBox(QWidget* parent = nullptr);
	virtual ~DoubleSpinBox() = default;

	int decimals() const;
	void setDecimals(int value);

    QString textFromValue(double value ) const override;
    double valueFromText(const QString& text) const override;

private:
	int m_dispDecimals;

private:
	QValidator::State validate(QString& text, int& pos) const override;
};

#endif // __DOUBLESPINBOX_H__