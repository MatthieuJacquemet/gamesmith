// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SPOILER_H__
#define __SPOILER_H__

#include <QWidget>

class QToolButton;
class QFrame;
class QParallelAnimationGroup;
class QGridLayout;
class QScrollArea;


class Spoiler : public QWidget {

    Q_OBJECT
public:
    Spoiler(const QString& title, QWidget *parent = nullptr);
    Spoiler(QWidget* parent = nullptr);

    virtual ~Spoiler();

    enum Feature {
        RoundedCorner = 0x1, Background = 0x2, CloseButton = 0x4
    };
    Q_DECLARE_FLAGS(Features, Feature)

    void setTitle(const QString& title) noexcept;
    QString title() const noexcept;

    void setAnimationDuration(int duration) noexcept;
    int animationDuration() const noexcept;

    void setContentWidget(QWidget* widget) noexcept;
    void enableFeatures(Features features, bool enable=true);

signals:
    void closeButtonPressed();

protected:
    void paintEvent(QPaintEvent* event) override;
    bool eventFilter(QObject* object, QEvent* event) override;

private:
    class SpoilerPrivate;
    Q_DECLARE_PRIVATE(Spoiler);
};

#endif // __SPOILER_H__