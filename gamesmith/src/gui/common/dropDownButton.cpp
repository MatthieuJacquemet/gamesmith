// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QStylePainter>
#include <QMenu>
#include <QStyleOptionComboBox>
#include <QActionGroup>

#include "dropDownButton.h"


DropDownButton::DropDownButton(QWidget* parent): QPushButton(parent),
    m_menu(new QMenu(this)),
    m_group(new QActionGroup(this)) {
    
    setMenu(m_menu);
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);

    connect(m_menu, &QMenu::triggered, this, [this](QAction* action) {
        setIcon(action->icon());
        emit itemChanged(action);
    });
}

void DropDownButton::addItem(const QIcon& icon, const QString& label,
                            const QVariant& data) {
    QAction* action = m_menu->addAction(icon, label);
    action->setCheckable(true);
    m_group->addAction(action);

    if (!data.isNull()) {
        action->setData(data);
    }
    if (m_menu->actions().size() == 1) {
        setIcon(action->icon());
        action->setChecked(true);
    }
}

// void DropDownButton::paintEvent(QPaintEvent* event) {

//     QStylePainter painter(this);
//     QStyleOptionComboBox comboOpt;
//     QStyleOptionButton toolOpt;

//     initStyleOption(&comboOpt);
//     QPushButton::initStyleOption(&toolOpt);

//     painter.drawComplexControl(QStyle::CC_ComboBox, comboOpt);
//     painter.drawControl(QStyle::CE_ComboBoxLabel, comboOpt);
// }

// void DropDownButton::initStyleOption(QStyleOptionComboBox* opt) const {
//     opt->initFrom(this);
//     opt->editable = false;
//     opt->currentText = text();
//     opt->subControls = QStyle::SC_All;
//     opt->iconSize = iconSize();
//     opt->currentIcon = icon();
//     if (isDown()) opt->state |= QStyle::State_Sunken;
//     if (hasFocus()) opt->state |= QStyle::State_HasFocus;
// }

