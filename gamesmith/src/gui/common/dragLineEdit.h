// Copyright 2023 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __DRAGLINEEDIT_H__
#define __DRAGLINEEDIT_H__

#include <QLineEdit>
#include <variant>
#include <optional>

class IntSpinBox;
class DoubleSpinBox;

class DragLineEdit: public QLineEdit {

    Q_OBJECT

public:
    enum ValueType { Int, Double };

    DragLineEdit(QWidget* widget);
    ~DragLineEdit() = default;

    inline bool isEditing() const { return m_isEditing; }
    inline bool isDragging() const { return m_isDragging; }

    void setEditing(bool editing);

protected:
    virtual void mousePressEvent(QMouseEvent* event) override;
    virtual void mouseMoveEvent(QMouseEvent* event) override;
    virtual void mouseReleaseEvent(QMouseEvent* event) override;
    virtual void focusInEvent(QFocusEvent* event) override;
    virtual void focusOutEvent(QFocusEvent* event) override;

    void finishDrag();

    std::variant<IntSpinBox*, DoubleSpinBox*> m_widget;
    std::variant<int, double> m_initialValue;

    bool m_isDragging   : 1 = false;
    bool m_isEditing    : 1 = false;
    bool m_valueChanged : 1 = false;
    QPoint m_prevMousePos;
    std::optional<QString> m_prefix, m_suffix;
};

#endif // __DRAGLINEEDIT_H__