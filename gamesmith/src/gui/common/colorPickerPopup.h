/*
 * Copyright (c) 2013-2021 Victor Wåhlström
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software
 * in a product, an acknowledgment in the product documentation would be
 * appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 */

#ifndef __COLORPICKERPOPUP_H__
#define __COLORPICKERPOPUP_H__

#include <QWidget>

#include "popup.h"
#include "colorPicker.h"

class ColorPickerPopupPrivate;
class ColorPicker;

/**
 * @brief Internal ColorPicker Popup class
 */
class ColorPickerPopup : public Popup {

    Q_OBJECT
    Q_DISABLE_COPY(ColorPickerPopup)

public:
    /**
     * @brief Constructor
     * @param parent parent widget
     */
    ColorPickerPopup(ColorPicker* colorPicker);
    virtual ~ColorPickerPopup() = default;

    /**
     * @brief Show or hide the alpha channel
     * @param visible true if alpha channel should be visible
     */
    void setDisplayAlpha(bool visible);

    /**
     * @brief Get the display status of the alpha channel
     * @return true if alpha channel is displayed in the widget
     */
    bool displayAlpha() const;

    /**
     * @brief Set the type used when directly editing values
     * @param type The type used by the widget
     */
    void setEditType(ColorPicker::EditType type);

    /**
     * @brief Get the type used when directly editing values
     * @return The type used when directly editing values
     */
    ColorPicker::EditType editType() const;

    /**
     * @brief Set color
     * @param color The new color
     */
    void setColor(const QColor& color);

signals:
    /**
     * @brief Emitted when the color has changed
     * @param color new color
     */
    void colorChanged(const QColor& color);

private:
    Q_DECLARE_PRIVATE(ColorPickerPopup)
};

#endif // __COLORPICKERPOPUP_H__
