/*
 * Copyright (c) 2013-2021 Victor Wåhlström
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software
 * in a product, an acknowledgment in the product documentation would be
 * appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 */

#include <private/qwidget_p.h>
#include "hueSaturationWheel.h"

#include <QMouseEvent>
#include <QPainter>
#include <QPainterPath>

static QRect fittedSquare(const QRect& rect) {

    QRect square(rect);

    int w = square.width();
    int h = square.height();
    if (w > h) {
        int offset = (w - h) / 2;
        square.setWidth(h);
        square.moveLeft(square.left() + offset);
    }
    else {
        int offset = (h - w) / 2;
        square.setHeight(w);
        square.moveTop(square.top() + offset);
    }
    return square;
}

//! @cond Doxygen_Suppress
class HueSaturationWheelPrivate : public QWidgetPrivate {

public:
    explicit HueSaturationWheelPrivate() = default;

    void updateColor(const QPointF& pos);
    void updateMarkerPos();
    void rebuildColorWheel();

    QColor color = Qt::white;
    QColor prevColor = Qt::white;
    QImage wheelImg;
    QPointF markerPos = QPointF(0, 0);

private:
    Q_DECLARE_PUBLIC(HueSaturationWheel)
};


void HueSaturationWheelPrivate::updateMarkerPos() {
    Q_Q(HueSaturationWheel);

    QRect square = fittedSquare(q->rect());
    qreal radius = square.width() * 0.5;
    qreal h      = color.hsvHueF();
    qreal s      = color.hsvSaturationF();

    qreal distance = s * radius;

    QPoint center = square.center();
    QLineF line(center.x(), center.y(), center.x(), center.y() + distance);
    line.setAngle(360.0 - h * 360.0 - 90.0);
    markerPos = line.p2();
}

void HueSaturationWheelPrivate::rebuildColorWheel() {
    Q_Q(HueSaturationWheel);

    QRect square = fittedSquare(q->rect());
    wheelImg   = QImage(square.size(), QImage::Format_ARGB32_Premultiplied);
    wheelImg.fill(0);

    QRect r = wheelImg.rect();
    QPainter painter(&wheelImg);

    painter.save();
    painter.setRenderHint(QPainter::Antialiasing);
    QPen pen;
    pen.setColor(Qt::transparent);
    painter.setPen(pen);
    painter.setBrush(Qt::transparent);
    painter.drawRect(r);

    QPainterPath path;
    path.addEllipse(r);

    painter.setClipPath(path);

    // Calculate hue, value
    QConicalGradient hue(r.center(), -90.0);
    QColor color;
    qreal step = 0.0;
    qreal val  = this->color.valueF();

    while (step < 1.0) {
        color.setHsvF(1.0 - step, 1.0, val);
        hue.setColorAt(step, color);
        step += 0.1;
    }
    painter.fillPath(path, hue);

    // Calculate saturation. May not be pixel perfect
    qreal radius = r.width() * 0.5;
    QRadialGradient sat(r.center(), radius);
    color.setRgbF(val, val, val, 1.0);
    sat.setColorAt(0, color);
    color.setRgbF(val, val, val, 0.0);
    sat.setColorAt(1, color);
    painter.fillPath(path, sat);
    painter.restore();

    q->update();
}

void HueSaturationWheelPrivate::updateColor(const QPointF& pos) {
    Q_Q(HueSaturationWheel);

    QRect square = fittedSquare(q->rect());

    qreal radius = square.width() * 0.5;
    QLineF line(square.center(), pos);
    qreal distance = qBound(0.0, line.length(), radius);
    line.setAngle(line.angle() + 90.0);
    qreal h = (360.0 - line.angle()) / 360.0;
    qreal s = distance / radius;
    qreal v = color.valueF();
    qreal a = color.alphaF();
    color.setHsvF(h, s, v, a);
}
//! @endcond

HueSaturationWheel::HueSaturationWheel(QWidget* parent):
    QWidget(*new HueSaturationWheelPrivate, parent, {}) {
    
    setFocusPolicy(Qt::ClickFocus);
    setMinimumSize(10, 10);
}

HueSaturationWheel::~HueSaturationWheel() {
}

void HueSaturationWheel::updateColor(const QColor& color) {
    Q_D(HueSaturationWheel);

    if (color.rgb() == d->color.rgb()) {
        // alpha may have changed
        if (d->color != color) {
            d->color = color, d->prevColor = color;
            update();
        }
        return;
    }
    int old_value = d->color.value();

    QRect square = fittedSquare(rect());
    qreal radius = square.width() * 0.5;
    qreal h      = color.hsvHueF();
    qreal s      = color.hsvSaturationF();
    d->color.setRgba(color.rgba());

    qreal distance = s * radius;
    QPoint center  = square.center();

    QLineF line(center.x(), center.y(), center.x(), center.y() + distance);
    line.setAngle(360.0 - h * 360.0 - 90.0);

    d->updateColor(line.p2());
    d->updateMarkerPos();
    if (old_value != color.value()) {
        d->rebuildColorWheel();
    }

    update();
}

void HueSaturationWheel::setColor(const QColor& color) {
    Q_D(HueSaturationWheel);
    if (d->color != color) {
        updateColor(color);
        emit colorChanged(d->color);
    }
}

void HueSaturationWheel::resizeEvent(QResizeEvent* event) {
    Q_D(HueSaturationWheel);

    d->rebuildColorWheel();
    d->updateMarkerPos();
    QWidget::resizeEvent(event);
}

void HueSaturationWheel::mousePressEvent(QMouseEvent* event) {
    Q_D(HueSaturationWheel);

    if (event->buttons() & Qt::RightButton) {
        clearFocus();
        d->color = d->prevColor;
        d->updateMarkerPos();
        emit colorChanged(d->color);
    } 
    else if (event->buttons() & Qt::LeftButton) {
        d->prevColor = d->color;
        HueSaturationWheel::mouseMoveEvent(event);
    }
}

void HueSaturationWheel::mouseMoveEvent(QMouseEvent* event) {
    Q_D(HueSaturationWheel);
    if (hasFocus()) {
        d->updateColor(event->pos());
        d->updateMarkerPos();
        emit colorChanged(d->color);
    }
}


void HueSaturationWheel::paintEvent(QPaintEvent*) {
    Q_D(HueSaturationWheel);

    QPainter painter(this);
    painter.save();
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setClipRect(rect());

    QRect square = fittedSquare(rect());
    painter.drawImage(square, d->wheelImg);

    QPen pen;
    QColor marker_color = d->color.valueF() > 0.5 ? Qt::black : Qt::white;
    pen.setColor(marker_color);

    painter.setPen(pen);
    QRectF marker(d->markerPos.x() - 2, d->markerPos.y() - 2, 5, 5);
    // arcs are specified in 1/16 degrees; draw a full circle
    painter.drawArc(marker, 0, 360 * 16);

    painter.restore();
}
