/*
 * Copyright (c) 2013-2021 Victor Wåhlström
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software
 * in a product, an acknowledgment in the product documentation would be
 * appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 */

#include <private/qwidget_p.h>
#include <QApplication>
#include <QStylePainter>
#include "colorHexEdit.h"
#include "colorPickerPopup.h"
#include "hueSaturationWheel.h"
#include "colorPicker.h"
#include "styleoptions.h"

#include <QFontDatabase>
#include <QHBoxLayout>
#include <qnamespace.h>

//! @cond Doxygen_Suppress

class ColorPickerPrivate : public QWidgetPrivate {

    Q_DECLARE_PUBLIC(ColorPicker)

public:
    explicit ColorPickerPrivate();

    ColorPickerPopup* popup;
    QColor color;
    ColorPicker::EditType editType;
    bool    displayAlpha : 1;
};

ColorPickerPrivate::ColorPickerPrivate():
    popup(nullptr),
    color(Qt::white),
    editType(ColorPicker::Float),
    displayAlpha(true) {}

//! @endcond




ColorPicker::ColorPicker(QWidget* parent):
    QWidget(*new ColorPickerPrivate(), parent, {}) {
    // set default color and sync child widgets
    setColor(QColor(255, 255, 255, 255));
    // setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
}

ColorPicker::~ColorPicker() {
    Q_D(ColorPicker);

    if (d->popup) {
        delete d->popup;
    }
}

void ColorPicker::updateColor(const QColor& color) {
    Q_D(ColorPicker);

    if (Q_LIKELY(d->color != color)) {
        d->color = color;
        update();
    }
}

void ColorPicker::setColor(const QColor& color) {
    Q_D(ColorPicker);

    if (d->color != color) {
        updateColor(color);
        emit colorChanged(d->color);
    }
}

void ColorPicker::setDisplayAlpha(bool visible) {
    Q_D(ColorPicker);

    d->displayAlpha = visible;
    if (d->popup) {
        d->popup->setDisplayAlpha(visible);
    }
}

bool ColorPicker::displayAlpha() const {
    Q_D(const ColorPicker);
    return d->displayAlpha;
}

void ColorPicker::setEditType(ColorPicker::EditType type) {
    Q_D(ColorPicker);

    d->editType = type;
    if (d->popup) {
        d->popup->setEditType(type);
    }
}

QColor ColorPicker::color() const {
    Q_D(const ColorPicker);
    return d->color;
}

ColorPicker::EditType ColorPicker::editType() const {
    Q_D(const ColorPicker);
    return d->editType;
}


void ColorPicker::paintEvent(QPaintEvent* event) {
    QStylePainter painter(this);
    StyleOptionColorPicker option;
    initStyleOption(&option);

    painter.drawPrimitive(QStyle::PrimitiveElement(Gs::PE_ColorPicker), option);
}


void ColorPicker::mousePressEvent(QMouseEvent* event) {

    if ((event->buttons() & Qt::LeftButton) == 0) {
        return;
    }
    Q_D(ColorPicker);

    if (!d->popup) {
        d->popup = new ColorPickerPopup(this);
        d->popup->setParentWidget(this);
        d->popup->setMinimumSize(210, 320);

        d->popup->setDisplayAlpha(d->displayAlpha);
        d->popup->setEditType(d->editType);
        d->popup->setFont(this->font());
        d->popup->setColor(d->color);

        connect(d->popup, &ColorPickerPopup::colorChanged,
                this, &ColorPicker::updateColor);
        connect(d->popup, &ColorPickerPopup::colorChanged,
                this, &ColorPicker::colorChanged);
    }
    d->popup->open();
}


void ColorPicker::initStyleOption(StyleOptionColorPicker* option) const {
    Q_D(const ColorPicker);
    option->initFrom(this);
    option->color = d->color;
}


QSize ColorPicker::sizeHint() const  {
    ensurePolished();
    QSize hint = QWidget::sizeHint();

    StyleOptionColorPicker opt;
    initStyleOption(&opt);
    auto type = QStyle::ContentsType(Gs::PE_ColorPicker);

    return style()->sizeFromContents(type, &opt, hint, this)
                    .expandedTo(QApplication::globalStrut());
}