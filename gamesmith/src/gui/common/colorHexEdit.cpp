/*
 * Copyright (c) 2013-2021 Victor Wåhlström
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software
 * in a product, an acknowledgment in the product documentation would be
 * appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 */

#include <QApplication>
#include <QFontDatabase>
#include <QSize>
#include <QString>
#include <QColor>
#include <QFont>
#include <QFontMetrics>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QStyle>
#include <QStyleOptionFrame>
#include <qcoreapplication.h>

#include "colorHexEdit.h"


static QString colorToString(const QColor& color, bool include_alpha) {
    QLatin1Char fill('0');
    if (include_alpha) {
        return QString("#%1%2%3%4")
            .arg(color.alpha(), 2, 16, fill)
            .arg(color.red(), 2, 16, fill)
            .arg(color.green(), 2, 16, fill)
            .arg(color.blue(), 2, 16, fill)
            .toUpper();
    }

    return QString("#%1%2%3")
        .arg(color.red(), 2, 16, fill)
        .arg(color.green(), 2, 16, fill)
        .arg(color.blue(), 2, 16, fill)
        .toUpper();
}

//! @cond Doxygen_Suppress
class ColorHexEditPrivate {

public:
    explicit ColorHexEditPrivate(ColorHexEdit* hex_edit);

    int editWidth() const;
    void refresh();

    QColor color = Qt::white;
    bool displayAlpha : 1;
    bool modified : 1;

private:
    ColorHexEdit* q_ptr;
    Q_DECLARE_PUBLIC(ColorHexEdit)
};


ColorHexEditPrivate::ColorHexEditPrivate(ColorHexEdit* hexEdit):
    displayAlpha(true),
    modified(false),
    q_ptr(hexEdit) {

}


int ColorHexEditPrivate::editWidth() const {
    Q_Q(const ColorHexEdit);

    QFontMetrics fm(q->fontMetrics());
    const QStyle* style = q->style();
    QStyleOptionFrame option;
    option.initFrom(q);
    int width = fm.averageCharWidth() * q->maxLength();
    width += style->pixelMetric(QStyle::PM_DefaultFrameWidth, &option, q) * 2;
    // input mask makes the cursor about 5 pixels wide when no character is selected
    // this is not reflected by QStyle::PM_TextCursorWidth. Qt Bug?
    // width += style->pixelMetric(QStyle::PM_TextCursorWidth, &option, m_LineEdit) * 2;
    // increase width by one character to compensate for now
    width += fm.averageCharWidth();
    int height = fm.height();

    QSize size(width, height);
    size = style->sizeFromContents(QStyle::CT_LineEdit, &option, size, q);

    return size.width();
}

void ColorHexEditPrivate::refresh() {
    Q_Q(ColorHexEdit);

    if (displayAlpha) {
        q->setMaxLength(9);
        q->setInputMask("\\#>HHHHHHHH");
        q->setPlaceholderText("#AARRGGBB");
        q->setToolTip(q->tr("A hexadecimal value on the form #AARRGGBB:\n\n"
                            "AA = alpha\nRR = red\nGG = green\nBB = blue"));
    }
    else {
        q->setMaxLength(7);
        q->setInputMask("#>HHHHHH");
        q->setPlaceholderText("#RRGGBB");
        q->setToolTip(q->tr("A hexadecimal value on the form RRGGBB:\n\n"
                            "RR = red\nGG = green\nBB = blue"));
    }

    q->setAlignment(Qt::AlignCenter);

    q->setFixedWidth(editWidth());
    q->setText(colorToString(color, q->displayAlpha()));
}

//! @endcond

ColorHexEdit::ColorHexEdit(QWidget* parent):
    QLineEdit(parent),
    d_ptr(new ColorHexEditPrivate(this)) {
    
    connect(this, &QLineEdit::editingFinished, [this] {
        Q_D(ColorHexEdit);
        if (d->modified) {
            d->modified = false;
            emit colorChanged(d->color);
        }
    });

    connect(this, &QLineEdit::textEdited, [this](const QString& text) {
        Q_D(ColorHexEdit);

        if (text.size() < maxLength()) {
            int pos = cursorPosition();
            insert("0");
            setCursorPosition(pos);
        }
        if (this->text() == colorToString(d->color, displayAlpha())) {
            return;
        }
        d->modified = true;
        d->color.setNamedColor(text);
        emit colorChanged(d->color);
    });

    QFont font = QApplication::font();
    font.setStyleHint(QFont::TypeWriter);
    font.setWeight(QFont::ExtraBold);
    font.setStyleStrategy(QFont::ForceOutline);
    setFont(font);
}

ColorHexEdit::~ColorHexEdit() {
    delete d_ptr;
}

void ColorHexEdit::showEvent(QShowEvent* event) {
    Q_D(ColorHexEdit);
    // refresh the UI here as fonts, styles, and other things affecting this widget may have changed
    d->refresh();
    QWidget::showEvent(event);
}

void ColorHexEdit::updateColor(const QColor& color) {
    Q_D(ColorHexEdit);
    if (d->color != color) {
        setText(colorToString(color, displayAlpha()));
        d->color = color;
        update();
    }
}

void ColorHexEdit::setDisplayAlpha(bool visible) {
    Q_D(ColorHexEdit);
    d->displayAlpha = visible;
    d->refresh();
}

bool ColorHexEdit::displayAlpha() const {
    Q_D(const ColorHexEdit);
    return d->displayAlpha;
}
