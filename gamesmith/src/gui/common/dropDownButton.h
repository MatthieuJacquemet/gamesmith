// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __DROPDOWNBUTTON_H__
#define __DROPDOWNBUTTON_H__

#include <QVariant>
#include <QPushButton>

class QStyleOptionComboBox;
class QActionGroup;

class DropDownButton : public QPushButton {

    Q_OBJECT
public:
    DropDownButton(QWidget* parent = nullptr);
    virtual ~DropDownButton() = default;

    void addItem(const QIcon& icon, const QString& label,
                const QVariant& data = QVariant());
signals:
    void itemChanged(QAction* item);

protected:
    QMenu* m_menu;
    QActionGroup* m_group;

    // void paintEvent(QPaintEvent* event) override;
    // void initStyleOption(QStyleOptionComboBox* opt) const;
};

#endif // __DROPDOWNBUTTON_H__