// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QStyleOption>
#include <QButtonGroup>
#include <QApplication>
#include <QDebug>
#include <QTimer>
#include <QLayoutItem>
#include <QBoxLayout>
#include <QAction>
#include <optional>
#include <QMenu>

#include <private/qwidget_p.h>

#ifdef Q_OS_MACOS
#include <qpa/qplatformnativeinterface.h>
#endif

#include "editor.h"
#include "editor_utils.h"
#include "styleoptions.h"
#include "runtimeManager.h"
#include "task.h"
#include "toolBoxContainer.h"
#include "toolButton.h"
#include "toolContext.h"

#include <QToolBar>


#define POPUP_TIMER_INTERVAL 500


class ToolBoxContainerPrivate : public QWidgetPrivate {
    Q_DECLARE_PUBLIC(ToolBoxContainer)
public:
    ToolBoxContainerPrivate() = default;

    void init();
    void toggleView(bool enabled);

    bool movable                        = true;
    bool floatable                      = true;
    Qt::Orientation orientation         = Qt::Horizontal;
    Qt::ToolButtonStyle toolButtonStyle = Qt::ToolButtonIconOnly;

    QAction* toggleViewAction           = nullptr;

    struct DragState {
        QPoint pressPos;
        bool dragging;
        bool moving;
        QLayoutItem* widgetItem;
    };
    std::optional<DragState> state;
    QBasicTimer waitForPopupTimer;
    // QWidget* container;

    bool mousePressEvent(QMouseEvent* e);
    bool mouseReleaseEvent(QMouseEvent* e);
    bool mouseMoveEvent(QMouseEvent* e);

    void updateWindowFlags(bool floating, bool unplug = false);
    void setWindowState(bool floating, bool unplug = false,
                        const QRect& rect = QRect());

    void startDrag(bool moving = false);
    void endDrag();

    void unplug();
    void plug();

    bool insideHandle(const QPoint& pos) const;


    Task<> clientConnected();
};


void ToolBoxContainerPrivate::init() {
    Q_Q(ToolBoxContainer);
    q->setSizePolicy(QSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding));
    q->setBackgroundRole(QPalette::Button);
    q->setAttribute(Qt::WA_Hover);
    q->setAttribute(Qt::WA_X11NetWmWindowTypeToolBar);

    RuntimeManager* runtime = Editor::globalInstance()->runtime();
    
    // container = new QWidget;
    // qobject_cast<QToolBar*>(q->parentWidget())->addWidget(container);
    int handleSize = q->style()->pixelMetric(QStyle::PM_ToolBarHandleExtent);

    layout = new QBoxLayout(QBoxLayout::LeftToRight, q);
    layout->setMargin(0);
    layout->setSpacing(1);
    layout->addItem(new QSpacerItem(handleSize, 0));
    q->setLayout(layout);

    toggleViewAction = new QAction(q);
    toggleViewAction->setCheckable(true);
    q->setMovable(q->style()->styleHint(QStyle::SH_ToolBar_Movable, 0, q));

    connect(toggleViewAction, &QAction::triggered,
            this, &ToolBoxContainerPrivate::toggleView);

    connect(runtime, &RuntimeManager::clientConnected,
            this, &ToolBoxContainerPrivate::clientConnected);
}


void ToolBoxContainerPrivate::toggleView(bool enabled) {
    Q_Q(ToolBoxContainer);
    if (enabled == q->isHidden()) {
        if (enabled) q->show();
        else q->close();
    }
}


void ToolBoxContainerPrivate::updateWindowFlags(bool floating, bool unplug) {
    Q_Q(ToolBoxContainer);
    Qt::WindowFlags flags = floating ? Qt::Tool : Qt::Widget;

    flags |= Qt::FramelessWindowHint;

    if (unplug) {
        flags |= Qt::X11BypassWindowManagerHint;
    }
    q->setWindowFlags(flags);
}


void ToolBoxContainerPrivate::setWindowState(bool floating, bool unplug, const QRect& rect) {
    Q_Q(ToolBoxContainer);
    bool visible        = !q->isHidden();
    bool wasFloating    = q->isFloating();

    q->hide();
    updateWindowFlags(floating, unplug);

    if (floating != wasFloating)// layout->checkUsePopupMenu();
    if (!rect.isNull()) q->setGeometry(rect);
    if (visible) q->show();
    if (floating != wasFloating) emit q->topLevelChanged(floating);

}


void ToolBoxContainerPrivate::startDrag(bool moving) {
    Q_Q(ToolBoxContainer);

    Q_ASSERT(state.has_value());

    if ((moving && state->moving) || state->dragging) {
        return;
    }
    if (!moving) {
        unplug();
    }
    state->dragging = !moving;
    state->moving   =  moving;
}


void ToolBoxContainerPrivate::endDrag() {
    Q_Q(ToolBoxContainer);
    Q_ASSERT(state.has_value());

    q->releaseMouse();

    if (state->dragging) {
        Q_ASSERT(layout != nullptr);

        // if (!layout->plug(state->widgetItem)) {
        //     if (q->isFloatable()) {
        //         layout->restore();
        //         setWindowState(true); // gets rid of the X11BypassWindowManager window flag
        //                               // and activates the resizer
        //         q->activateWindow();
        //     } else {
        //         layout->revert(state->widgetItem);
        //     }
        // }
    }
    state.reset();
}

static QPoint pointToDirection(const QWidget* widget, const QPoint& pos) {
    QPoint rtl(widget->width() - pos.x(), pos.y());
    return widget->isRightToLeft() ? rtl : pos;
}

bool ToolBoxContainerPrivate::mousePressEvent(QMouseEvent *event) {
    Q_Q(ToolBoxContainer);

    if (!insideHandle(event->pos())) {
        return false;
    }
    else if (event->button() != Qt::LeftButton) {
        return true;
    }
    else if (!q->isMovable()) {
        return true;
    }
     if (Q_UNLIKELY(state.has_value())) {
        return true;
    }
    state = DragState {
        .pressPos   = pointToDirection(q, event->pos()),
        .dragging   = false,
        .moving     = false,
        .widgetItem = nullptr
    };
    return true;
}

bool ToolBoxContainerPrivate::mouseReleaseEvent(QMouseEvent*) {
    if (state.has_value()) {
        endDrag();
        return true;
    } else {
        return false;
    }
}

bool ToolBoxContainerPrivate::mouseMoveEvent(QMouseEvent *event) {
    Q_Q(ToolBoxContainer);

    if (!state.has_value()) {
        return false;
    }
    int dist = (event->pos() - state->pressPos).manhattanLength();
    if (dist > QApplication::startDragDistance()) {

        const bool wasDragging = state->dragging;
        const bool moving = !q->isWindow() && (orientation == Qt::Vertical ?
            event->x() >= 0 && event->x() < q->width() :
            event->y() >= 0 && event->y() < q->height());

        startDrag(moving);
        if (!moving && !wasDragging) {
            q->grabMouse();
        }
    }
    if (state->dragging) {
        QPoint pos = event->globalPos();
        if (q->isLeftToRight()) {
            pos -= state->pressPos;
        } else {
            pos += QPoint(state->pressPos.x() - q->width(), -state->pressPos.y());
        }
        q->move(pos);
        // layout->hover(state->widgetItem, event->globalPos());
    } 
    else if (state->moving) {

        const QPoint globalPressPos =
            q->mapToGlobal(pointToDirection(q, state->pressPos));
        int pos = 0;

        QPoint delta = event->globalPos() - globalPressPos;
        if (orientation == Qt::Vertical) {
            pos = q->y() + delta.y();
        } else {
            if (q->isRightToLeft()) {
                // pos = win->width() - q->width() - q->x()  - delta.x();
            } else {
                // pos = q->x() + delta.x();
            }
        }

        // layout->moveToolBar(q, pos);
    }
    return true;
}

void ToolBoxContainerPrivate::unplug() {
    Q_Q(ToolBoxContainer);
    QRect r = q->geometry();
    r.moveTopLeft(q->mapToGlobal(QPoint(0, 0)));
    setWindowState(true, true, r);
}

void ToolBoxContainerPrivate::plug() {
    Q_Q(ToolBoxContainer);
    setWindowState(false, false, q->geometry());
}

bool ToolBoxContainerPrivate::insideHandle(const QPoint& pos) const {
    Q_Q(const ToolBoxContainer);
    QStyleOptionToolBar opt;
    q->initStyleOption(&opt);
    QRect r = q->style()->subElementRect(QStyle::SE_ToolBarHandle, &opt, q);
    return r.contains(pos);
}


Task<> ToolBoxContainerPrivate::clientConnected() {
    Q_Q(ToolBoxContainer);

    // if (auto toolsvc = co_await service.acquire()) {
        
    //     for (auto& [id, name] : enumerate(co_await toolsvc->get_tools())) {
    //         auto type = QStyle::StandardPixmap(Gs::SP_ToolBase + id);

    //         QToolButton* button = new QToolButton;

    //         button->setToolButtonStyle(Qt::ToolButtonIconOnly);
    //         button->setObjectName(QString::fromStdString(name));
    //         button->setText(button->text());
    //         button->setIcon(q->style()->standardIcon(type, 0, button));

    //         QObject::connect(button, &QToolButton::triggered,
    //             [id, this](QAction*) -> Task<> {
    //                 if (auto toolsvc = co_await service.acquire())
    //                     co_await service->set_tool(id);
    //             }
    //         );
    //         q->layout()->addWidget(button);
    //     }
    // }
    co_return;
}

ToolBoxContainer::ToolBoxContainer(QWidget *parent): QWidget(*new ToolBoxContainerPrivate, parent, {}) {
    Q_D(ToolBoxContainer);
    d->init();
}


void ToolBoxContainer::setMovable(bool movable) {
    Q_D(ToolBoxContainer);
    if (!movable == !d->movable) {
        return;
    }
    d->movable = movable;
    d->layout->invalidate();
    emit movableChanged(d->movable);
}

bool ToolBoxContainer::isMovable() const {
    Q_D(const ToolBoxContainer);
    return d->movable;
}

bool ToolBoxContainer::isFloatable() const {
    Q_D(const ToolBoxContainer);
    return d->floatable;
}

void ToolBoxContainer::setFloatable(bool floatable) {
    Q_D(ToolBoxContainer);
    d->floatable = floatable;
}

bool ToolBoxContainer::isFloating() const {
    return isWindow();
}

void ToolBoxContainer::setOrientation(Qt::Orientation orientation) {
    Q_D(ToolBoxContainer);
    if (orientation == d->orientation) {
        return;
    }
    d->orientation = orientation;

    if (orientation == Qt::Vertical) {
        setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    } else {
        setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    }
    d->layout->invalidate();
    d->layout->activate();

    emit orientationChanged(d->orientation);
}


Qt::Orientation ToolBoxContainer::orientation() const {
    Q_D(const ToolBoxContainer);
    return d->orientation;
}


void ToolBoxContainer::changeEvent(QEvent* event) {
    Q_D(ToolBoxContainer);
    switch (event->type()) {
        case QEvent::StyleChange:
            d->layout->invalidate();
            break;
        case QEvent::LayoutDirectionChange:
            d->layout->invalidate();
            break;
        default:
            break;
    }
    QWidget::changeEvent(event);
}


void ToolBoxContainer::paintEvent(QPaintEvent* event) {
    Q_D(ToolBoxContainer);

    QStyle* style = this->style();
    QPainter p(this);
    QStyleOptionToolBar opt;
    initStyleOption(&opt);

    if (isWindow()) {
        p.fillRect(opt.rect, palette().window());
        style->drawControl(QStyle::CE_ToolBar, &opt, &p, this);
        style->drawPrimitive(QStyle::PE_FrameMenu, &opt, &p, this);
    } else {
        style->drawControl(QStyle::CE_ToolBar, &opt, &p, this);
    }

    opt.rect = style->subElementRect(QStyle::SE_ToolBarHandle, &opt, this);
    if (opt.rect.isValid()) {
        style->drawPrimitive(QStyle::PE_IndicatorToolBarHandle, &opt, &p, this);
    }
}


static bool waitForPopup(ToolBoxContainer* toolBar, QWidget* popup) {

    if (popup == nullptr || popup->isHidden())
        return false;

    for (QWidget* w = popup; w != nullptr; w = w->parentWidget()) {
        if (w == toolBar) {
            return true;
        }
    }
    QMenu* menu = qobject_cast<QMenu*>(popup);
    if (menu == nullptr) {
        return false;
    }
    QAction* action = menu->menuAction();
    QList<QWidget*> widgets = action->associatedWidgets();
    for (int i = 0; i < widgets.count(); ++i) {
        if (waitForPopup(toolBar, widgets.at(i))) {
            return true;
        }
    }
    return false;
}

#ifdef Q_OS_MACOS
static void enableMacToolBar(ToolBoxContainer *toolbar, bool enable) {
    auto* nativeInterface = QApplication::platformNativeInterface();
    if (nativeInterface == nullptr) {
        return;
    }
    QPlatformNativeInterface::NativeResourceForIntegrationFunction function =
        nativeInterface->nativeResourceFunctionForIntegration("setContentBorderAreaEnabled");
    if (function == nullptr) {
        return; // Not Cocoa platform plugin.
    }
    typedef void (*SetContentBorderAreaEnabledFunction)(
        QWindow *window, void *identifier, bool enabled
    );
    (reinterpret_cast<SetContentBorderAreaEnabledFunction>(function))(
        toolbar->window()->windowHandle(), toolbar, enable
    );
}
#endif

bool ToolBoxContainer::event(QEvent *event) {
    Q_D(ToolBoxContainer);

    switch (event->type()) {
        // case QEvent::Timer:
        //     if (d->waitForPopupTimer.timerId() == 
        //             static_cast<QTimerEvent*>(event)->timerId()) {
                
        //         QWidget* w = QApplication::activePopupWidget();
        //         if (!waitForPopup(this, w)) {
        //             d->waitForPopupTimer.stop();
        //         }
        //     }
        //     break;
        case QEvent::Hide:
            if (!isHidden()) break;
            Q_FALLTHROUGH();
        case QEvent::Show:
            d->toggleViewAction->setChecked(event->type() == QEvent::Show);
#ifdef Q_OS_MACOS
            enableMacToolBar(this, event->type() == QEvent::Show);
#endif
            emit visibilityChanged(event->type() == QEvent::Show);
            break;
        case QEvent::ParentChange:
            // d->layout->checkUsePopupMenu();
            break;

        case QEvent::MouseButtonPress:
            if (d->mousePressEvent(static_cast<QMouseEvent*>(event))) {
                return true;
            }
            break;
        case QEvent::MouseButtonRelease:
            if (d->mouseReleaseEvent(static_cast<QMouseEvent*>(event))) {
                return true;
            }
            break;
        case QEvent::HoverEnter:
        case QEvent::HoverLeave:
            return true;
        case QEvent::HoverMove:
#ifndef QT_NO_CURSOR
            if (d->insideHandle(static_cast<QHoverEvent*>(event)->pos())) {
                setCursor(Qt::SizeAllCursor);
            } else {
                unsetCursor();
            }
#endif
            break;
        case QEvent::MouseMove:
            if (d->mouseMoveEvent(static_cast<QMouseEvent*>(event))) {
                return true;
            }
            break;
        case QEvent::Leave:
            if (d->state.has_value() && d->state->dragging) {
    #ifdef Q_OS_WIN
                // This is a workaround for loosing the mouse on Vista.
                QPoint pos = QCursor::pos();
                QMouseEvent fake(
                    QEvent::MouseMove,
                    mapFromGlobal(pos), pos, Qt::NoButton,
                    QGuiApplication::mouseButtons(),
                    QGuiApplication::keyboardModifiers()
                );
                d->mouseMoveEvent(&fake);
    #endif
            } else {
                break;
                // QWidget *w = QApplication::activePopupWidget();
                // if (waitForPopup(this, w)) {
                //     d->waitForPopupTimer.start(POPUP_TIMER_INTERVAL, this);
                //     break;
                // }

                // d->waitForPopupTimer.stop();
                // break;
            }
        default:
            break;
    }
    return QWidget::event(event);
}


QAction *ToolBoxContainer::toggleViewAction() const {
    Q_D(const ToolBoxContainer);
    return d->toggleViewAction;
}


void ToolBoxContainer::initStyleOption(QStyleOptionToolBar* option) const {
    Q_D(const ToolBoxContainer);

    if (option == nullptr) {
        return;
    }
    option->initFrom(this);
    if (orientation() == Qt::Horizontal) {
        option->state |= QStyle::State_Horizontal;
    }
    option->lineWidth = style()->pixelMetric(QStyle::PM_ToolBarFrameWidth, 0, this);
    option->features = isMovable() ?  QStyleOptionToolBar::Movable
                                    : QStyleOptionToolBar::None;

    option->toolBarArea = Qt::NoToolBarArea;
}


static QButtonGroup* makeGlobalToolGroup() {
    QButtonGroup* group = new QButtonGroup(Editor::globalInstance());
    group->setExclusive(true);
    auto sig = QOverload<QAbstractButton*>::of(&QButtonGroup::buttonClicked);
    QObject::connect(group, sig, group, [](QAbstractButton* button) -> Task<> {
        if (ToolButton* toolButton = qobject_cast<ToolButton*>(button)) {
            ToolContextBase* context = toolButton->context();
            if (auto tool = co_await context->service()) {
                tool->activate(toolButton->mode());
            }
        }
    });
    return group;
}

QButtonGroup* ToolBoxContainer::globalToolGroup() {
    static QButtonGroup* group = makeGlobalToolGroup();
    return group;
}

// QSize ToolBoxContainer::sizeHint() const {
//     QSize size = QWidget::sizeHint();

//     QStyleOptionToolBar opt;
//     initStyleOption(&opt);
//     QRect r = style()->subElementRect(QStyle::SE_ToolBarHandle, &opt, this);

//     return r.size(); 
// }