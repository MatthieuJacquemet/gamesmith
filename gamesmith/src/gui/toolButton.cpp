// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QResizeEvent>
#include <QDebug>
#include <QAction>
#include <QButtonGroup>

#include "toolContext.h"
#include "editor.h"
#include "toolButton.h"


ToolButton::ToolButton( const QSharedPointer<ToolContextBase>& context,
                        const QString& name, QWidget* parent):
    QToolButton(parent),
    m_context(context),
    m_group(new QActionGroup(this)) {
    
    setToolButtonStyle(Qt::ToolButtonIconOnly);
    setText(name);
    setCheckable(true);

    connect(this, &QToolButton::triggered, this, [this](QAction* action) {
        m_mode = action->data().toUInt();
        setIcon(action->icon());
        if (QButtonGroup* buttonGroup = group()) {
            emit buttonGroup->buttonClicked(this);
        }
    });
}

void ToolButton::addAction(const QString& text, uint32_t mode) {
    QAction* action = new QAction(text);
    QString name = m_context->modeName(mode);
    action->setIcon(Editor::globalInstance()->icon("Tool_" + name));
    action->setData(mode);
    action->setCheckable(true);
    m_group->addAction(action);
    QToolButton::addAction(action);

    if (Q_UNLIKELY(!m_mode.has_value())) {
        action->trigger();
    }
    setPopupMode(QToolButton::MenuButtonPopup);
}

void ToolButton::setMode(uint32_t mode) {
    m_mode = mode;
    QString name = m_context->modeName(mode);
    setIcon(Editor::globalInstance()->icon("Tool_" + name));
}


// QSize ToolButton::sizeHint() const {
//     QSize size = QToolButton::sizeHint();
//     size.setHeight(qMax(size.width(), size.height()));
//     size.setWidth(qMax(size.width(), size.height()));
//     return size;
// }

// void ToolButton::resizeEvent(QResizeEvent *event) {
//     QSize size = event->size();
    
//     size.setHeight(qMax(size.width(), size.height()));
//     size.setWidth(qMax(size.width(), size.height()));
    
//     resize(size);
// }
