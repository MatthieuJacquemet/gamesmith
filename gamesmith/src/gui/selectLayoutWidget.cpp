// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QMenu>
#include <QTimer>
#include <QInputDialog>
#include <QStandardPaths>
#include <QFileDialog>
#include <QActionGroup>
#include <QDebug>
#include <QStylePainter>
#include <QStyleOptionToolButton>

#include "editor.h"
#include "mainWindow.h"
#include "utils.h"
#include "editor_defines.h"
#include "gui_utils.h"
#include "selectLayoutWidget.h"




Layout::Layout(SelectLayoutWidget* widget, const QString& name):
	QAction(name),
    m_widget(widget),
	m_remove(widget->m_removeMenu->addAction(name)) {

    setCheckable(true);

	connect(m_remove, &QAction::triggered, [this] {
        if (isChecked()) {
            checkNext();
        }
        delete this;
	    m_widget->enableFirst(false);
    });

    connect(this, &QAction::toggled, [this](bool checked) {
        // ads::CDockManager* mgr = dockManager();
        if (checked) {
            m_widget->setText(text());
            // if (!QAction::data().isNull()) {
            //     mgr->restoreState(data());
            // }
        } else {
            // setData(mgr->saveState());
        }
    });

    widget->menu()->insertAction(widget->m_separator, this);
    widget->m_layouts->addAction(this);
}


void Layout::checkNext() {

    QList<QAction*> actions = m_widget->m_layouts->actions();

    if (actions.size() > 1) {
        int index = actions.indexOf(this);
        Q_ASSERT(index != -1);

        actions[index ? index - 1 : 1]->setChecked(true);
    }
}


void Layout::serialize(QDataStream& stream) {
    stream << text() << data();
}


void Layout::deserialize(QDataStream& stream) {

    QString name;
    QByteArray data;
    stream >> name >> data;

    name = m_widget->getUnique(name);

    setText(name);
    m_remove->setText(name);
    setData(std::move(data));
}


Layout::~Layout() {
	m_widget->m_removeMenu->removeAction(m_remove);
}

QByteArray Layout::data() const {
	return QAction::data().toByteArray();
}

void Layout::setEnabled(bool enabled) {
	m_remove->setEnabled(enabled);
}





SelectLayoutWidget::SelectLayoutWidget(QWidget* parent):
    QPushButton(parent),
	m_layouts(new QActionGroup(this)),
    m_smenu(new QMenu(tr("Layouts"), this)) {
    
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    
    m_removeMenu    = m_smenu->addMenu(tr("delete layout")),
    m_saveAction    = m_smenu->addAction(tr("save current layout")),
    m_saveToFile    = m_smenu->addAction(tr("save layout to file")),
    m_loadFromFile  = m_smenu->addAction(tr("load layout from file")),
    m_separator     = m_smenu->addSeparator();

    m_layouts->setExclusive(true);
    setMinimumWidth(100);

	setToolTip(tr("Layout"));
	show();

    connect(this, &QPushButton::pressed, this, [this] {
        m_smenu->popup(mapToGlobal(QPoint(0, height())));
    });
    connect(m_smenu, &QMenu::aboutToHide, this, [this] {
        setDown(false);
    });

	connect(m_saveAction, &QAction::triggered,
            this, &SelectLayoutWidget::addLayout);

	connect(m_saveToFile, &QAction::triggered,
            this, &SelectLayoutWidget::saveLayout);

	connect(m_loadFromFile, &QAction::triggered,
            this, &SelectLayoutWidget::loadLayout);

	QTimer::singleShot(0, [this] {
        Layout* layout = new Layout(this, tr("default"));
        layout->setChecked(true);
        layout->setEnabled(false);
    });
}

// void SelectLayoutWidget::showPopup() {
//     // QMenu* menu = new QMenu(this);

//     // QAction* action1 = new QAction("Action 1", menu);
//     // connect(action1, &QAction::triggered, [=]() {
//     //     qDebug() << "Action 1 triggered";
//     // });
//     // menu->addAction(action1);

//     // QAction* action2 = new QAction("Action 2", menu);
//     // connect(action2, &QAction::triggered, [=]() {
//     //     qDebug() << "Action 2 triggered";
//     // });
//     // menu->addAction(action2);

//     m_smenu->popup(mapToGlobal(QPoint(0, height())));
// }


int SelectLayoutWidget::enableFirst(bool enable) {

	QList<QAction*> list = m_layouts->actions();
    int size = list.size();

	if (Q_UNLIKELY(size == 1)) {
		qobject_cast<Layout*>(list[0])->setEnabled(enable);
    }
    return size;
}


void SelectLayoutWidget::serialize(QDataStream& stream)  {

	Layout* current_layout = current();

    if (current_layout != Q_NULLPTR) {
        // current_layout->setData(dockManager()->saveState());
    }
    QList<QAction*> actions = m_layouts->actions();
    
    stream << actions.size() << actions.indexOf(current_layout);

	for (QAction* action : actions) {
        Layout* layout = qobject_cast<Layout*>(action);
		layout->serialize(stream);
	}
}


void SelectLayoutWidget::deserialize(QDataStream& stream) {
    
    for (QAction* action : m_layouts->actions()) {
        delete action;
    }
    int count, index;
	stream >> count >> index;

    for (int i=0; i<count; ++i) {
        Layout* layout = new Layout(this);
        layout->deserialize(stream);

        if (Q_UNLIKELY(i == index)) {
            layout->setChecked(true);
        }
    }
    enableFirst(false);
}

void SelectLayoutWidget::initialize() {
    MainWindow* window = Editor::globalInstance()->window();
	window->menuView->addMenu(menu());
}


void SelectLayoutWidget::paintEvent(QPaintEvent* event) {
    // QPushButton::paintEvent(event);

    // QStyleOptionButton option;
    // initStyleOption(&option);

    // QPainter painter(this);
    // painter.setRenderHint(QPainter::Antialiasing, true);
    // style()->drawPrimitive(QStyle::PE_IndicatorArrowDown, &option, &painter, this);

    QStylePainter painter(this);
    QStyleOptionComboBox comboOpt;
    QStyleOptionButton toolOpt;

    initStyleOption(&comboOpt);
    QPushButton::initStyleOption(&toolOpt);

    painter.drawComplexControl(QStyle::CC_ComboBox, comboOpt);
    painter.drawControl(QStyle::CE_PushButtonLabel, toolOpt);
}

void SelectLayoutWidget::initStyleOption(QStyleOptionComboBox* opt) const {
    opt->initFrom(this);
    opt->editable = false;
    opt->currentText = text();
    opt->subControls = QStyle::SC_All;
    if (isDown()) opt->state |= QStyle::State_Sunken;
    if (hasFocus()) opt->state |= QStyle::State_HasFocus;
}


void SelectLayoutWidget::addLayout() {

	QString new_name = getUnique(tr("Layout"));
	QString name;
	bool ok = false;

 	do {
        name = QInputDialog::getText(nullptr, tr("New Layout"), "",
								QLineEdit::Normal, new_name, &ok);
		if (!ok) return;

	} while (name.isEmpty());

    int count = enableFirst(true);

	Layout* layout = new Layout(this, name);
    layout->setChecked(true);

    if (count == 0) {
        layout->setEnabled(false);
    }
}


void SelectLayoutWidget::saveLayout() {
    
    static QDir dir = QStandardPaths::writableLocation(
        QStandardPaths::DocumentsLocation
    );
    Layout* layout = current();
    // layout->setData(dockManager()->saveState());

    QString filename = QFileDialog::getSaveFileName(Q_NULLPTR,
        tr("Save layout as"),
        dir.absoluteFilePath(layout->text()),
        tr("GameSmith layout file (*" LAYOUT_EXT ")")
    );
    if (filename.isEmpty()) {
        return;
    } else if (!filename.endsWith(LAYOUT_EXT)) {
        filename += LAYOUT_EXT;
    }
    QFile file(filename);
    
    if (file.open(QIODevice::WriteOnly)) {
        QDataStream stream(&file);
        layout->serialize(stream);
    }
}


void SelectLayoutWidget::loadLayout() {

    static QString dir = QStandardPaths::writableLocation(
        QStandardPaths::DocumentsLocation
    );

    QString filename = QFileDialog::getOpenFileName(Q_NULLPTR,
        tr("Load layout"), dir,
        tr("GameSmith layout file (*" LAYOUT_EXT ")")
    );
    QFile file(filename);

    if (file.open(QIODevice::ReadOnly)) {
        QDataStream stream(&file);
        Layout* layout = new Layout(this);
        layout->deserialize(stream);
        layout->setChecked(true);
    }
}


QString SelectLayoutWidget::getUnique(const QString& name) const {
	
	QString newName = name;
	QStringList names;
    QList<QAction*> actions = m_layouts->actions();

	QString fmt("%1.%2");
	int i = 0;

	for (QAction* action : actions) {
		names.append(action->text());
    }
	while (names.contains(newName)) {
		newName = fmt.arg(name).arg(i++, 3, 10, QChar('0'));
    }
	return newName;
}


Layout* SelectLayoutWidget::current() const {
    return qobject_cast<Layout*>(m_layouts->checkedAction());
}

