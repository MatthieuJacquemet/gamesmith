/**
 * Copyright (C) 2022 Matthieu Jacquemet
 * 
 * This file is part of GameSmith.
 * 
 * GameSmith is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * GameSmith is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __EDITORWIDGET_H__
#define __EDITORWIDGET_H__

#include <QWidget>
#include "serializable.h"

namespace KDDockWidgets { class DockWidget; }

class EditorWidget: public QWidget, public Serializable {

    Q_OBJECT
    Q_INTERFACES(Serializable)

public:
    enum OpenFlag {
        OpenEmbededCentral, OpenNewWindow, DontOpen
    };
    virtual ~EditorWidget();

    void setLabel(const QString& label);
    QString label() const;

    void serialize(QDataStream& stream) override;
    void deserialize(QDataStream& stream) override;

    void focus(OpenFlag flag = OpenFlag::DontOpen);

public slots:
    virtual void runtimeConnected() {}
    virtual void runtimeDisconnected() {}
    virtual void runtimeStarted() {}

protected:
    EditorWidget(const QString& label="");

    KDDockWidgets::DockWidget* getDockWidget() const;

private:
    QString m_label;
};

#endif // __EDITORWIDGET_H__