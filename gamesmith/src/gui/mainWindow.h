// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MAINWINDOW_H__
#define __MAINWINDOW_H__


#include <QMainWindow>
#include <QAction>
#include <kddockwidgets/MainWindow.h>
#include <iostream>
#include "KDDockWidgets.h"
#include "serializable.h"
#include "ui_interface.h"


class MainWindowPrivate;
class StatusBar;
class SelectLayoutWidget;
class EditorWidget;


class MainWindow : 	public KDDockWidgets::MainWindow,
					public Ui::MainWindow, public Serializable {
	Q_OBJECT
    Q_INTERFACES(Serializable)

public:
	static MainWindow* createWindow();
	virtual ~MainWindow() = default;

	using DockWidgetBase = KDDockWidgets::DockWidgetBase;
	using Location = KDDockWidgets::Location;
	using DockArea = KDDockWidgets::DockWidget;

	void createActions();
	void createShortcuts();
	void initialize();

	void serialize(QDataStream& stream) override;
    void deserialize(QDataStream& stream) override;

	DockArea* addEditor(EditorWidget* editorWidget,
						Location location = Location::Location_OnLeft,
						DockWidgetBase* relativeTo = nullptr,
						KDDockWidgets::InitialOption options = {});

	// add as tabbed
	DockArea* addEditor(EditorWidget* editor, DockWidgetBase* parent);

	DockArea* createEditor(EditorWidget* editor);

	template<std::derived_from<EditorWidget> Type>
    Type* getEditor() const {
		const QMetaObject& metaObject = Type::staticMetaObject;
        return qobject_cast<Type*>(getEditor(metaObject.className()));
    }

    EditorWidget* getEditor(const QString& className) const;
    void registerEditor(EditorWidget* editor);

	void loadStyleSheet(const QString& styleSheetPath);

signals:
	void closing();

private slots:
	void toggleFullscreen(bool active);

protected:
	MainWindow();

	virtual void closeEvent(QCloseEvent* event) override;
	virtual bool eventFilter(QObject* obj, QEvent* ev) override;

private:
	void setupUI();

	StatusBar* m_statusBar;
	SelectLayoutWidget* m_selectLayout;
};




#endif // __MAINWINDOW_H__