// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __TOOLBAR_H__
#define __TOOLBAR_H__

#include <QWidget>

class ToolBoxContainerPrivate;
class QStyleOptionToolBar;

class ToolBoxContainer: public QWidget {

    Q_OBJECT

public:
    ToolBoxContainer(QWidget* parent = nullptr);
    virtual ~ToolBoxContainer() = default;

    Qt::Orientation orientation() const;
    void setOrientation(Qt::Orientation orientation);

    bool isMovable() const;
    void setMovable(bool movable);

    bool isFloatable() const;
    void setFloatable(bool movable);

    bool isFloating() const;

    QAction* toggleViewAction() const;

    static QButtonGroup* globalToolGroup();

signals:
    void visibilityChanged(bool visible);
    void topLevelChanged(bool floating);
    void movableChanged(bool movable);
    void orientationChanged(Qt::Orientation orientation);

protected:
    bool event(QEvent* event) override;
    void paintEvent(QPaintEvent* event) override;
    void changeEvent(QEvent* event) override;

    void initStyleOption(QStyleOptionToolBar* option) const;
    // QSize sizeHint() const override;

private:
    Q_DECLARE_PRIVATE(ToolBoxContainer)
};


#endif // __TOOLBAR_H__