// Copyright (C) 2022 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include "templateView.h"




ProjectTemplate::ProjectTemplate(const QDir& path, QWidget* parent):
    QLabel(parent),
    m_path(path.dirName())
{    
    // m_data.loadData(path.absolutePath(), ProjectData::HeaderOnly);
    // setText(m_data.name());

    // QPixmap pixmap(m_data.logo());
    // setPixmap(pixmap);
}


QDir ProjectTemplate::dir() const {
    return QDir();
}


// void ProjectTemplate::instantiate(const QDir& dir) {

//     copyRecursive(m_path, dir);
// }


void ProjectTemplate::focusInEvent(QFocusEvent* event) {

    QLabel::focusInEvent(event);
    Q_EMIT focused();
}


TemplateView::TemplateView(QWidget* parent):
    QWidget(parent),
    m_current(nullptr) {
    
}


void TemplateView::addTemplate(const QDir& path) {
    
    ProjectTemplate* tmp = new ProjectTemplate(path, this);
    connect(tmp, &ProjectTemplate::focused, [=, this] {
        m_current = tmp;
    });
}


ProjectTemplate* TemplateView::currentTemplate() const {
    return m_current;
}
