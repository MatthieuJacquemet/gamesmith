// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QButtonGroup>
#include <QLayout>
#include <QDebug>
#include <QComboBox>
#include <QAction>
#include <qnamespace.h>
#include <qpalette.h>

#include "dropDownButton.h"
#include "editor.h"
#include "toolButton.h"
#include "transformTools.h"

template<>
QString ToolContext<stub::TransformTool>::modeNameImpl(uint32_t mode) const {
    switch (mode) {
        case ::TransformTool::M_translate: return "translate";
        case ::TransformTool::M_rotate: return "rotate";
        case ::TransformTool::M_scale: return "scale";
        case ::TransformTool::M_unified: return "unified";
        
        default: qWarning() << "Unknown mode" << mode;
    }
    return QString();
}

inline TransformTool::Space getData(QComboBox* combo, int index) {
    return TransformTool::Space(combo->itemData(index).toUInt());
};


TransformTools::TransformTools(QWidget* parent): ToolBoxContainer(parent),
    m_context(new ContextType("transform", "Transform Tools")) {
    
    QBoxLayout* buttonsLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    QBoxLayout* configLayout = new QBoxLayout(QBoxLayout::LeftToRight);
    configLayout->setMargin(0);
    buttonsLayout->setMargin(0);

    QBoxLayout* layout = qobject_cast<QBoxLayout*>(this->layout());

    layout->addLayout(buttonsLayout);
    layout->addSpacing(8);
    layout->addLayout(configLayout);
    

    auto addWidget = [this, buttonsLayout](const QString& name, uint32_t mode) {
        ToolButton* button = new ToolButton(m_context, name);
        button->setMode(mode);
        globalToolGroup()->addButton(button);
        buttonsLayout->addWidget(button);
    };
    auto addItem = [this](  std::variant<QComboBox*, DropDownButton*> widget,
                            const char* label, uint32_t data) {
        std::visit([label, data](auto* widget) {
            Editor* editor = Editor::globalInstance();
            QString name(label);
            widget->addItem(editor->icon(name.replace(' ', '_')), tr(label), data);
        }, widget);
    };

    addWidget(tr("translate"),  TransformTool::M_translate);
    addWidget(tr("scale"),      TransformTool::M_scale);
    addWidget(tr("rotate"),     TransformTool::M_rotate);
    addWidget(tr("unified"),    TransformTool::M_unified);

    QComboBox* selectSpace = new QComboBox;
    DropDownButton* selectPivot = new DropDownButton;

    selectSpace->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    selectPivot->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    
    selectPivot->setProperty("hasFrame", false);
    selectSpace->setProperty("hasFrame", false);

    addItem(selectSpace, "Local", TransformTool::S_local);
    addItem(selectSpace, "Global", TransformTool::S_global);
    addItem(selectSpace, "View", TransformTool::S_view);

    addItem(selectPivot, "Bounding Box Center", TransformTool::P_bounding_box_center);
    addItem(selectPivot, "Individual Origins", TransformTool::P_individual_origins);
    addItem(selectPivot, "Median Point", TransformTool::P_median);

    auto sig0 = QOverload<int>::of(&QComboBox::currentIndexChanged);
    connect(selectSpace, sig0, this, [this, selectSpace](int index) -> Task<> {
        if (Q_LIKELY(co_await m_context->acquire())) {
            m_context->set_space(getData(selectSpace, index));
        }
    });

    auto sig1 = &DropDownButton::itemChanged;
    connect(selectPivot, sig1, this, [this, selectPivot](QAction* a) -> Task<> {
        if (Q_LIKELY(co_await m_context->acquire())) {
            m_context->set_pivot(TransformTool::Pivot(a->data().toUInt()));
        }
    });
    configLayout->addWidget(selectSpace);
    configLayout->addWidget(selectPivot);
}
