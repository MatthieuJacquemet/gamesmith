// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.


#ifndef __DOCKINGOVERLAY_H__
#define __DOCKINGOVERLAY_H__

#include <kddockwidgets/private/DropArea_p.h>
#include <optional>

class QPropertyAnimation;
class QRubberBand;

class DockingOverlay : public KDDockWidgets::DropIndicatorOverlayInterface {

    Q_OBJECT

public:
    explicit DockingOverlay(KDDockWidgets::DropArea *dropArea);
    ~DockingOverlay() = default;
    QRubberBand* m_rubberBand;
    KDDockWidgets::DropLocation hover_impl(QPoint globalPos) override;

    KDDockWidgets::DropLocation dropLocationForPos(const QPoint& pos);

protected:
    QPoint posForIndicator(KDDockWidgets::DropLocation) const override;
    virtual void updateVisibility() override;

private:
    typedef QHash<KDDockWidgets::DropLocation, QPolygon> Segments;
    QPolygon m_poly;
    
    Segments computeSegments(const QRect& r, bool inner,
                            bool offset=false) const;

    static KDDockWidgets::Location toMultiSplitterLocation(
                            KDDockWidgets::DropLocation location);

    QRect geometryForRubberband(QRect localRect) const;
};

#endif // __DOCKINGOVERLAY_H__