// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __STYLEOPTIONS_H__
#define __STYLEOPTIONS_H__

#include <QStyleOption>
#include <QStyle>

#define GS_STYLE_BASE(QSTYLE_BASE) QSTYLE_BASE + 64

namespace Gs {

    enum PrimitiveElement {
        PE_ColorPicker      = GS_STYLE_BASE(QStyle::PE_CustomBase),
        PE_ColorValueSlider,
        PE_DockWidget,
    };
    enum StandardPixmap {
        SP_EyeDropper       = GS_STYLE_BASE(QStyle::SP_CustomBase),
        SP_ToolBase,
        SP_DockWidgetBase   = GS_STYLE_BASE(SP_ToolBase),
        SP_DockWidgetEnd    = SP_DockWidgetBase + 7,
    };
    enum PixelMetric {
        PM_ToolTipMinWidth  = GS_STYLE_BASE(QStyle::PM_CustomBase),
    };


    constexpr QStyle::PixelMetric pm(PixelMetric element) {
        return QStyle::PixelMetric(element);
    }
    constexpr QStyle::PrimitiveElement pe(PrimitiveElement element) {
        return QStyle::PrimitiveElement(element);
    }
    constexpr QStyle::StandardPixmap sp(StandardPixmap element) {
        return QStyle::StandardPixmap(element);
    }

    extern QString peName(Gs::PrimitiveElement element);
    extern QString spName(Gs::StandardPixmap element);
    extern QString pmName(Gs::PixelMetric element);
};


template<size_t ID, int Version = 1>
struct CustomStyleOption : public QStyleOption {
    enum StyleOptionType { Type = GS_STYLE_BASE(SO_CustomBase) + ID};
    inline CustomStyleOption(): QStyleOption(Version, Type) {}
};

struct StyleOptionColorPicker: CustomStyleOption<0> {
    QColor color;
};

struct StyleOptionColorValueSlider: CustomStyleOption<1> {
    qreal value;
};

#endif // __STYLEOPTIONS_H__