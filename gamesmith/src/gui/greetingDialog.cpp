// Copyright (C) 2022 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QStandardPaths>
#include <QFileDialog>
#include <QDir>
#include <QDebug>
#include <QGridLayout>
#include <QStandardPaths>

#include "projectData.h"
#include "editor_defines.h"
#include "flowLayout.h"
#include "editor_utils.h"
#include "greetingDialog.h"






GreetingDialog::GreetingDialog() {

    setupUi(this);
    loadTemplates();

    using QSP = QStandardPaths;

    m_defaultDir.setPath(config<QString>("project/default_dir",
        QSP::writableLocation(QSP::DocumentsLocation)
    ));
    directory->setText(m_defaultDir.absolutePath());
    template_view->setLayout(new FlowLayout);
}


QDir GreetingDialog::openedProject() const {
    return m_openedProject;
}


void GreetingDialog::selectDirectory() {
    
    QDir dir = directory->text();

    if (!dir.exists()) {
        dir = m_defaultDir;
    }
    QString path = QFileDialog::getExistingDirectory(nullptr,
        "Open Project Parent Directory", dir.absolutePath()
    );
    if (!path.isEmpty()) {
        directory->setText(path);
    }
}


void GreetingDialog::moreTemplates() {
    
}

void GreetingDialog::createNew() {

    ProjectTemplate* tmp = template_view->currentTemplate();
    if (tmp != nullptr) {
        // tmp->instantiate(directory->text());
    }
    accept();
}


void GreetingDialog::loadTemplates() {
    
    static QString default_dir = QStandardPaths::locate(
        QStandardPaths::AppDataLocation,
        "templates",
        QStandardPaths::LocateDirectory
    );

    QDir dir = config<QString>("core/temlpates", default_dir);

    if (Q_UNLIKELY(dir.isEmpty())) {
        return;
    }
    
    for (const QString& item: dir.entryList(QDir::Dirs)) {
        const QDir dir_path = dir.absoluteFilePath(item);
        
        if (dir_path.exists("project" PROJECT_EXT)) {
            QString path = dir_path.absoluteFilePath("project" PROJECT_EXT);
            template_view->addTemplate(path);
        } else {
            qWarning() << "Found template without manifest.json at" 
                << dir_path.absolutePath();
        }
    }
}
