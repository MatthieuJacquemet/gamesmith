// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QToolButton>
#include <QMetaEnum>
#include "styleoptions.h"
#include <KDDockWidgets.h>

#define ENUM_CASE(NAME) case Gs::NAME: return #NAME;

namespace Gs {
    
    QString peName(Gs::PrimitiveElement element) {
        switch (element) {
            ENUM_CASE(PE_ColorPicker)
            ENUM_CASE(PE_ColorValueSlider)
            ENUM_CASE(PE_DockWidget)
            default: break;
        };
        auto metaEnum = QMetaEnum::fromType<QStyle::PrimitiveElement>();
        return metaEnum.valueToKey(element);
    }

    QString pmName(Gs::PixelMetric element) {
        switch (element) {
            ENUM_CASE(PM_ToolTipMinWidth)
            default: break;
        };
        auto metaEnum = QMetaEnum::fromType<QStyle::PixelMetric>();
        return metaEnum.valueToKey(element);
    }
    
    QString spName(Gs::StandardPixmap element) {
        QMetaEnum metaEnum = QMetaEnum::fromType<QStyle::StandardPixmap>();
        Gs::StandardPixmap base = StandardPixmap(0);
        switch (element) {
            ENUM_CASE(SP_EyeDropper)
            default:
            if (int(element) < QStyle::SP_CustomBase) {
                break; // nothing spetial, using StandardPixmap
            } else if (element < Gs::SP_DockWidgetEnd) {
                base = SP_DockWidgetBase;
                metaEnum = QMetaEnum::fromType<KDDockWidgets::TitleBarButtonType>();
            }
            break;
        };
        return metaEnum.valueToKey(element - base);
    }
}