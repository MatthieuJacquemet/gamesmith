// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __SELECTLAYOUTWIDGET_H__
#define __SELECTLAYOUTWIDGET_H__

#include <QPushButton>
#include <QAction>

#include "serializable.h"


class QStyleOptionComboBox;
class QActionGroup;
class Layout;

class SelectLayoutWidget: public QPushButton, public Serializable {

	Q_OBJECT
    Q_INTERFACES(Serializable)

public:
	SelectLayoutWidget(QWidget* parent=nullptr);
	virtual ~SelectLayoutWidget() = default;

    void serialize(QDataStream& stream) override;
    void deserialize(QDataStream& stream) override;
	void initialize();

	inline QMenu* menu() const { return m_smenu; }

protected:
	void paintEvent(QPaintEvent* event) override;
	void initStyleOption(QStyleOptionComboBox* opt) const;

private Q_SLOTS:
	void addLayout();
	void saveLayout();
	void loadLayout();

private:
	QString getUnique(const QString& name) const;
	Layout* current() const;
	int enableFirst(bool enable);

	QMenu* m_smenu, *m_removeMenu;
	QAction* m_saveAction, *m_saveToFile, *m_loadFromFile, *m_separator;

	mutable QActionGroup* m_layouts;

	friend class Layout;
};




class Layout: public QAction, public Serializable {

    Q_OBJECT
    Q_INTERFACES(Serializable)

public:
    Layout(SelectLayoutWidget* widget, const QString& name="");
    virtual ~Layout();

    QByteArray data() const;
    void setEnabled(bool enabled);
	void checkNext();

    void serialize(QDataStream& stream) override;
    void deserialize(QDataStream& stream) override;

private:
	SelectLayoutWidget* m_widget;
    QAction* m_remove;
};


#endif // __SELECTLAYOUTWIDGET_H__