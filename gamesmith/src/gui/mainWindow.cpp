// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QCloseEvent>
#include <QMessageBox>
#include <QSettings>
#include <QFileSystemWatcher>
#include <QStyle>
#include <QLayout>
#include <QStylePainter>
#include <QStyleOptionTabBarBase>
#include <QTimer>

#include <kddockwidgets/DockWidget.h>
#include <kddockwidgets/Config.h>
#include <kddockwidgets/FrameworkWidgetFactory.h>
#include <kddockwidgets/private/widgets/FrameWidget_p.h>
#include <kddockwidgets/private/widgets/TabWidgetWidget_p.h>

#include "KDDockWidgets.h"
#include "dockingOverlay.h"

#include "editorWidget.h"
#include "gui_utils.h"
#include "editor.h"
#include "selectLayoutWidget.h"
#include "statusBar.h"
#include "project.h"
#include "projectData.h"
#include "mainWindow.h"
#include "dockingTabBarButton.h"
#include "tabWidgetProxyStyle.h"

#include "styleoptions.h"
#include "configEditor.h"
#include "viewportEditor.h"
#include "runtimeEditor.h"
#include "outlinerEditor.h"
#include "transformTools.h"
#include "inspectorEditor.h"
#include "selectToolsContainer.h"


constexpr auto dockopts = KDDockWidgets::MainWindowOption_None;


MainWindow::MainWindow(): KDDockWidgets::MainWindow("main-window", dockopts), 
	m_statusBar(new StatusBar(this)),
	m_selectLayout(new SelectLayoutWidget(this)) {

	Editor::globalInstance()->installEventFilter(this);
	setCenterWidgetMargins(QMargins(5,0,5,0));
}


void MainWindow::initialize() {

	showMaximized();
	setupUI();
	createActions();
	createShortcuts();

	// load default layout
	auto* conf = addEditor(	new ConfigEditor, Location::Location_OnRight,
							nullptr, QSize(200, 1000));
	auto* insp = addEditor(new InspectorEditor, conf);
	auto* view = addEditor(new ViewportEditor, Location::Location_OnLeft);
	auto* outl = addEditor(new OutlinerEditor, Location::Location_OnLeft,
							nullptr, QSize(200, 1000));
}


void MainWindow::createActions() {

	Project* project = Editor::globalInstance()->project();

	// connect(actionOpen, &QAction::triggered, project, &Project::loadProject);

	connect(actionSave, &QAction::triggered, project, &Project::save);

	connect(actionSave_As, &QAction::triggered, project, &Project::saveAs);

	connect(actionSave_Copy, &QAction::triggered, project, &Project::saveCopy);

	connect(actionToggle_Fullscreen, &QAction::triggered,
			this, &MainWindow::toggleFullscreen);

	connect(actionShow_StatusBar, &QAction::triggered, m_statusBar,
			&StatusBar::setVisible);
}


void loadShortcut(	QAction* action, const QString& id,
					QList<QKeySequence> shortcuts={}) {

	Editor* editor = Editor::globalInstance();

	QVariant var = editor->config()->value("shortcuts/" + id);
	QStringList keys = var.toStringList();
	
	if (keys.size() > 0) {
		shortcuts.clear();
	}
	for (const QString& key: keys) {
		shortcuts.append(QKeySequence(key));
	}
	action->setShortcuts(shortcuts);
}



void MainWindow::createShortcuts() {

	loadShortcut(actionToggle_Fullscreen, "redo", {QKeySequence::FullScreen});
	loadShortcut(actionOpen, "open-project", {QKeySequence::Open});
	loadShortcut(actionSave, "save-project", {QKeySequence::Save});
	loadShortcut(actionSave_As, "save-project-as", {QKeySequence::SaveAs});
	loadShortcut(actionSave_Screenshot, "screenshot", {QKeySequence::Print});
	loadShortcut(actionQuit, "quit", {QKeySequence::Quit});
	loadShortcut(actionUndo, "undo", {QKeySequence::Undo});
	loadShortcut(actionRedo, "redo", {QKeySequence::Redo});
	loadShortcut(actionSave_Copy, "save-copy");
	loadShortcut(actionOpen_Recent, "open-recent-project");
}



void MainWindow::closeEvent(QCloseEvent* event) {

	emit closing();

	Editor* editor = Editor::globalInstance();
	Project* project = editor->project();

	if (project->modified()) {

		QMessageBox::StandardButton res =
			QMessageBox::question(this, editor->applicationName(),
				tr("Save changed before closing?\n"),
				QMessageBox::Cancel | QMessageBox::Save | QMessageBox::No,
				QMessageBox::Save
			);

		if (res == QMessageBox::Cancel) {
			event->ignore();
			return;
		} else if (res == QMessageBox::Save) {
			project->save();
		}
	}
	event->accept();
	editor->exit();
}

bool MainWindow::eventFilter(QObject* obj, QEvent* ev) {
	
	switch (ev->type()) {
		case QEvent::KeyPress:
		case QEvent::KeyRelease: {
			auto kev = static_cast<QKeyEvent *>(ev);
			if (kev->key() == Qt::Key_Control && qobject_cast<QWindow *>(obj)) {
				const bool hideIndicators = ev->type() == QEvent::KeyPress;
				KDDockWidgets::Config::self().setDropIndicatorsInhibited(hideIndicators);
			}
		}
		break;
		default:
			break;
    }

    return false;
}


void MainWindow::setupUI() {

	// dirty hack to remove the default central widget
	QWidget* centralWidgetTmp = takeCentralWidget();
	Ui::MainWindow::setupUi(this);
	delete takeCentralWidget();
	centralwidget = nullptr;
	QMainWindow::setCentralWidget(centralWidgetTmp);
	
	Editor* editor = Editor::globalInstance();
	Project* project = editor->project();

	setFocusPolicy(Qt::ClickFocus);
	setWindowTitle(project->name());
	setStatusBar(m_statusBar);

	toolBar->setMovable(false);
	toolBar->layout()->setMargin(4);
	// toolBar->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
	// toolBar->setFixedHeight(30);
	// new ToolBar(this);
	toolBar->addWidget(new SelectToolsContainer);
	toolBar->addWidget(new TransformTools);
	toolBar->addWidget(makeSpacer());
	toolBar->addWidget(m_selectLayout);

	actionSave_Copy->setEnabled(false);
	m_statusBar->initialize();
	m_selectLayout->initialize();
}


void MainWindow::toggleFullscreen(bool active) {
	active ? showFullScreen() : showNormal();
}

void MainWindow::serialize(QDataStream& stream) {
	// m_selectLayout->serialize(stream);
}

void MainWindow::deserialize(QDataStream& stream) {
	// m_selectLayout->deserialize(stream);
}


MainWindow::DockArea* MainWindow::addEditor(EditorWidget* editorWidget,
											KDDockWidgets::Location location,
											DockWidgetBase* relativeTo,
											KDDockWidgets::InitialOption opt) {
	
	DockArea* dock = createEditor(editorWidget);
    addDockWidget(dock, location, relativeTo, opt);
	return dock;
}

MainWindow::DockArea* MainWindow::addEditor(EditorWidget* editorWidget,
											DockWidgetBase* parent) {
	DockArea* dock = createEditor(editorWidget);
	parent->addDockWidgetAsTab(dock);
	return dock;
}

MainWindow::DockArea* MainWindow::createEditor(EditorWidget* editorWidget) {

	auto* dock = new KDDockWidgets::DockWidget(editorWidget->objectName());
	dock->setWidget(editorWidget);
	dock->setTitle(editorWidget->label());
    menuView->addAction(dock->toggleAction());
	return dock;
}

void MainWindow::loadStyleSheet(const QString& styleSheetPath)  {

	QFile file(styleSheetPath);

	if (file.open(QIODevice::ReadOnly)) {
		QByteArray data = file.readAll();
		Editor::globalInstance()->setStyleSheet(data); 
	}
	QTimer::singleShot(100, [this] {
		auto children = Editor::globalInstance()->findChildren<QWidget*>(); 
		QStyle* style = Editor::globalInstance()->style();
		for (QWidget* child: children) {
			style->unpolish(child);
			style->polish(child);
		}
		repaint();
		Editor::globalInstance()->processEvents();
	});
}

EditorWidget* MainWindow::getEditor(const QString& className) const {
	
	if (auto* widget = KDDockWidgets::DockWidget::byName(className)) {
    	return qobject_cast<EditorWidget*>(widget->widget());
	}
	return static_cast<EditorWidget*>(nullptr);
}



ON_EDITOR_CREATED(main_window) {



	// KDDockWidgets::Config::self().setDropIndicatorsInhibited(true);
	// ads::CDockManager::setConfigFlag(ads::CDockManager::OpaqueSplitterResize);
    // ads::CDockManager::setConfigFlag(ads::CDockManager::XmlCompressionEnabled);
    // ads::CDockManager::setConfigFlag(ads::CDockManager::FocusHighlighting);
	// ads::CDockManager::setConfigFlag(ads::CDockManager::DragPreviewIsDynamic);
	// ads::CDockManager::setConfigFlag(ads::CDockManager::DragPreviewShowsContentPixmap);
	// ads::CDockManager::setConfigFlag(ads::CDockManager::DragPreviewHasWindowFrame);
	// ads::CDockManager::setConfigFlag(ads::CDockManager::FloatingContainerForceNativeTitleBar);
}


MainWindow* MainWindow::createWindow() {
	
	struct MyFactory: public KDDockWidgets::DefaultWidgetFactory {	
		KDDockWidgets::Frame*
		createFrame(KDDockWidgets::QWidgetOrQuick* parent,
					KDDockWidgets::FrameOptions options) const override{
			
			struct MyFrame: public KDDockWidgets::FrameWidget {
				
				MyFrame(KDDockWidgets::QWidgetOrQuick* parent,
						KDDockWidgets::FrameOptions options):
					KDDockWidgets::FrameWidget(parent, options) {}
				
				void paintEvent(QPaintEvent*) override {
					QStylePainter painter(this);
					QStyleOption options;
					options.init(this);
					auto element = QStyle::PrimitiveElement(Gs::PE_DockWidget);
					style()->drawPrimitive(element, &options, &painter, this);
				}
			};
			return new MyFrame(parent, options);
		}
		KDDockWidgets::DropIndicatorOverlayInterface*
		createDropIndicatorOverlay(KDDockWidgets::DropArea* drop) const override{
			return new DockingOverlay(drop);
		}

		QAbstractButton* createTitleBarButton(QWidget *parent,
						KDDockWidgets::TitleBarButtonType type) const override{
			if (parent == nullptr) {
				qWarning() << Q_FUNC_INFO << "Parent not provided";
				return nullptr;
			}
			auto button = new DockingTabBarButton(parent);
			button->setIcon(parent->style()->standardIcon(QStyle::StandardPixmap(
				Gs::SP_DockWidgetBase + static_cast<int>(type)
			)));
			return button;
		}

		KDDockWidgets::TabWidget*
		createTabWidget(KDDockWidgets::Frame* parent,
						KDDockWidgets::TabWidgetOptions) const override {
			
			auto* widget = new KDDockWidgets::TabWidgetWidget(parent);
			widget->setStyle(new TabWidgetProxyStyle(widget->style()));

			return widget;
		}
	};
	KDDockWidgets::Config::Flags flags;
	flags |= KDDockWidgets::Config::Flag_NativeTitleBar;
	flags |= KDDockWidgets::Config::Flag_AlwaysTitleBarWhenFloating;
	flags |= KDDockWidgets::Config::Flag_AlwaysShowTabs;
	flags |= KDDockWidgets::Config::Flag_HideTitleBarWhenTabsVisible;
	flags |= KDDockWidgets::Config::Flag_TabsHaveCloseButton;
	flags |= KDDockWidgets::Config::Flag_ShowButtonsOnTabBarIfTitleBarHidden;
	flags |= KDDockWidgets::Config::Flag_AllowSwitchingTabsViaMenu;
	flags |= KDDockWidgets::Config::Flag_AllowReorderTabs;

	KDDockWidgets::Config::self().setDisabledPaintEvents(
		KDDockWidgets::Config::CustomizableWidget_FloatingWindow
	);
	KDDockWidgets::Config::self().setInternalFlags(
		KDDockWidgets::Config::InternalFlag_TopLevelIndicatorRubberBand
	);
	KDDockWidgets::Config::self().setFlags(flags);
	KDDockWidgets::Config::self().setSeparatorThickness(
		Editor::style()->pixelMetric(QStyle::PM_SplitterWidth)
	);
	KDDockWidgets::Config::self().setFrameworkWidgetFactory(new MyFactory);

	return new MainWindow;
}
