/**
 * Copyright (C) 2022 Matthieu Jacquemet
 * 
 * This file is part of GameSmith.
 * 
 * GameSmith is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * GameSmith is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __TEMPLATEVIEW_H__
#define __TEMPLATEVIEW_H__

#include <QLabel>
#include <QDir>


#include "projectData.h"


class ProjectTemplate: public QLabel {

    Q_OBJECT

public:
    ProjectTemplate(const QDir& dir, QWidget* parent);
    ~ProjectTemplate() = default;

    QDir dir() const;

Q_SIGNALS:
    void focused();

private:
    void focusInEvent(QFocusEvent* event) override;

    ProjectData m_data;
    QDir m_path;
};


class TemplateView: public QWidget {

    Q_OBJECT

public:
    TemplateView(QWidget* parent);
    ~TemplateView() = default;

    void addTemplate(const QDir& path);

    ProjectTemplate* currentTemplate() const;

private:
    ProjectTemplate* m_current;
};

#endif // __TEMPLATEVIEW_H__