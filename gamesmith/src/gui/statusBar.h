// Copyright (C) 2021 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __STATUSBAR_H__
#define __STATUSBAR_H__

#include <QStatusBar>
#include <QLabel>
#include <utils.h>

#include "editor_utils.h"

class QMenu;
class QTimer;

class StatusBar: public QStatusBar {

	Q_OBJECT

public:
	StatusBar(QWidget* window = nullptr);
	virtual ~StatusBar() = default;

	void initialize();
	inline QMenu* menu() const { return m_menu; };

private:
	QMenu* m_menu;
};


#endif // __STATUSBAR_H__