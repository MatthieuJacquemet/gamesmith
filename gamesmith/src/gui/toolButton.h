// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __TOOLBUTTON_H__
#define __TOOLBUTTON_H__

#include <QToolButton>

class ToolContextBase;
class QActionGroup;

class ToolButton : public QToolButton {

    Q_OBJECT

public:
    ToolButton( const QSharedPointer<ToolContextBase>& context,
                const QString& name, QWidget* parent = 0);

    ~ToolButton() = default;

    void addAction(const QString& text, uint32_t mode);

    void setMode(uint32_t mode);
    inline uint32_t mode() const { return m_mode.value_or(0); };
    inline ToolContextBase* context() const {
        return m_context.get();
    }
    // QSize sizeHint() const override;
    // void resizeEvent(QResizeEvent *event) override;

protected:
    QActionGroup* m_group;
    std::optional<uint32_t> m_mode;
    QSharedPointer<ToolContextBase> m_context;
};

#endif // __TOOLBUTTON_H__