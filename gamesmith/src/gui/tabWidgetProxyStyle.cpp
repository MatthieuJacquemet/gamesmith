// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QStyleOptionTabWidgetFrame>
#include <kddockwidgets/DockWidgetBase.h>

#include "editorWidget.h"
#include "tabWidgetProxyStyle.h"



static bool isEditorWidget(const QWidget* widget) {
    using Dock = KDDockWidgets::DockWidgetBase;

    if (auto* tabWidget = qobject_cast<const QTabWidget*>(widget)) {
        if (auto* dock = qobject_cast<Dock*>(tabWidget->currentWidget())) {
            if (auto* dockWidget = dock->widget()) {
                const QMetaObject* metaObject = dockWidget->metaObject();
                return metaObject->inherits(&EditorWidget::staticMetaObject);
            }
        }
    }
    return false;
}


QRect TabWidgetProxyStyle::subElementRect(  QStyle::SubElement element,
                                            const QStyleOption* option,
                                            const QWidget* widget) const {

    if (element != SE_TabWidgetTabPane && element != SE_TabWidgetTabContents ||
        !isEditorWidget(widget)) {
        return QProxyStyle::subElementRect(element, option, widget);
    }
    QRect r;
    if (auto* twf = qstyleoption_cast<const QStyleOptionTabWidgetFrame*>(option)) {
        int space = 0;
        int overlap = 0;
        /* a 2px space between tab page and tab bar
           but no spece between tab pane and tab bar */
        if (element == SE_TabWidgetTabContents && !twf->tabBarSize.isEmpty()) {
            space = 0;
        } else {
            overlap = pixelMetric(PM_TabBarBaseOverlap, twf, widget);
        }
        switch (twf->shape) {
            case QTabBar::RoundedNorth:
            case QTabBar::TriangularNorth:
                r = QRect(QPoint(0, qMax(twf->tabBarSize.height() + space - overlap, 0)),
                        QSize(twf->rect.width(),
                                qMin(twf->rect.height() - twf->tabBarSize.height() - space + overlap,
                                    twf->rect.height())));
                break;
            case QTabBar::RoundedSouth:
            case QTabBar::TriangularSouth:
                r = QRect(QPoint(0, 0),
                        QSize(twf->rect.width(),
                                qMin(twf->rect.height() - twf->tabBarSize.height() - space + overlap,
                                    twf->rect.height())));
                break;
            case QTabBar::RoundedEast:
            case QTabBar::TriangularEast:
                r = QRect(QPoint(0, 0),
                        QSize(qMin(twf->rect.width() - twf->tabBarSize.width() - space + overlap,
                                    twf->rect.width()),
                                twf->rect.height()));
                break;
            case QTabBar::RoundedWest:
            case QTabBar::TriangularWest:
                r = QRect(QPoint(twf->tabBarSize.width() + space - overlap, 0),
                        QSize(qMin(twf->rect.width() - twf->tabBarSize.width() - space + overlap,
                                    twf->rect.width()),
                                twf->rect.height()));
        }
    }
    return r.isValid() ? r : QProxyStyle::subElementRect(element,option,widget);
}

// void TabWidgetProxyStyle::drawPrimitive(QStyle::PrimitiveElement element,
//                         const QStyleOption* option, QPainter* painter,
//                         const QWidget* widget) const {
    
//     if (element == PE_FrameTabWidget) {

//     }
//     // QRect bgRect = option->rect.adjusted(1, 1, -1, -1);
//     Ph::paintBorderedRoundRect(painter, option->rect, Ph::TabBarTab_Rounding,
//                                swatch, S_tabFrame, S_tabFrame);
// }
