// Copyright (C) 2021 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QFile>
#include <QTextStream>
#include <QMenu>
#include <QLayout>
#include <QStyle>

#include "objectRequestBroker.h"
#include "runtimeEditor.h"
#include "runtimeManager.h"
#include "mainWindow.h"
#include "gui_utils.h"
#include "editor.h"
#include "statusBar.h"




StatusBar::StatusBar(QWidget* window): QStatusBar(window),
	m_menu(new QMenu(tr("Status Bar"), this)) {

	setContextMenuPolicy(Qt::CustomContextMenu);
	addWidget(makeSpacer());

	// connect(this, &StatusBar::customContextMenuRequested, 
    //     	this, &StatusBar::showContextMenu);
}

void StatusBar::initialize() {

	struct RuntimeConnectionLabel : public QLabel {
	public:
		RuntimeConnectionLabel() {
			Editor* editor = Editor::globalInstance();
			connect(editor->runtime(), &RuntimeManager::clientConnected,
					this, &RuntimeConnectionLabel::clientConnected);
			
			connect(editor->runtime(), &RuntimeManager::clientDisconnected,
					this, &RuntimeConnectionLabel::clientDisconnected);
		}
	protected:
    	virtual void mousePressEvent(QMouseEvent* event) override {
			auto* mainWindow = Editor::globalInstance()->window();

			if (auto* runtime = mainWindow->getEditor<RuntimeEditor>()) {
				runtime->focus(EditorWidget::OpenNewWindow);
			}
		}
	private slots:
		void clientDisconnected() {
			setText(tr("disconnected")); setColor(Qt::red);
		}
		void clientConnected() {
			ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
			setText(QString::fromStdString(broker->get_transport_uri()));
			setColor(style()->standardPalette().color(QPalette::Text));
		}
		void setColor(const QColor& color) {
			QPalette palette = QLabel::palette();
			palette.setColor(QPalette::Text, color);
			QLabel::setPalette(std::move(palette));
		}
	};
	addPermanentWidget(new RuntimeConnectionLabel, 0);

	if (auto* window = qobject_cast<MainWindow*>(parentWidget())) {
		window->menuView->addMenu(m_menu);
	}
}

	// m_timer->setSingleShot(false);
	// m_timer->start(config<int>("core/update-ressources-interval",
	// 							DEFAULT_UPDATE_RESSOURCES_INTERVAL));

