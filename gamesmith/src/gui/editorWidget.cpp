// Copyright (C) 2022 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <kddockwidgets/DockWidget.h>
#include <QTimer>

#include "editorWidget.h"
#include "editor.h"
#include "runtimeManager.h"


EditorWidget::EditorWidget(const QString& label):
    m_label(label) {
    
    setObjectName(metaObject()->className());

    auto* manager = Editor::globalInstance()->runtime();

    connect(manager, &RuntimeManager::clientConnected,
            this, &EditorWidget::runtimeConnected);
    
    connect(manager, &RuntimeManager::clientDisconnected,
            this, &EditorWidget::runtimeDisconnected);

    connect(manager, &RuntimeManager::started,
            this, &EditorWidget::runtimeStarted);

    if (manager->isConnected()) {
        QTimer::singleShot(0, this, &EditorWidget::runtimeConnected);
    }
}

KDDockWidgets::DockWidget* EditorWidget::getDockWidget() const {
    return qobject_cast<KDDockWidgets::DockWidget*>(parentWidget());
}

void EditorWidget::setLabel(const QString& label) {
    m_label = label;
    if (KDDockWidgets::DockWidget* widget = getDockWidget()) {
        widget->setTitle(m_label);
    }
}

QString EditorWidget::label() const {
    return m_label;
}

void EditorWidget::serialize(QDataStream& stream) {
    stream << m_label;
}

void EditorWidget::deserialize(QDataStream& stream) {
    stream >> m_label;
}

void EditorWidget::focus(OpenFlag flag) {
    if (KDDockWidgets::DockWidget* widget = getDockWidget()) {
        emit widget->show();
        if (flag == OpenFlag::OpenNewWindow) {
            emit widget->setFloating(true);
        }
    }
}

EditorWidget::~EditorWidget() {
    
}
