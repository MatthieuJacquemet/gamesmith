// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QStyleOptionRubberBand>
#include <QPropertyAnimation>
#include <QApplication>
#include <QPainter>
#include <QPainterPath>
#include <QRubberBand>
#include <QProxyStyle>

#include <kddockwidgets/Config.h>
#include <kddockwidgets/private/DragController_p.h>
#include <kddockwidgets/FrameworkWidgetFactory.h>

#include "editor.h"
#include "mainWindow.h"
#include "dockingOverlay.h"


const qreal DRAGGED_WINDOW_OPACITY  = 0.4;
const QColor OVERLAY_PEN_COLOR      = QColor(255,255,255);
const int SEGMENT_GIRTH             = 100;



class RubberBandProxyStyle : public QProxyStyle {
public:
    inline RubberBandProxyStyle(QStyle* style): QProxyStyle(style) {}
    virtual ~RubberBandProxyStyle() = default;

    int styleHint(  StyleHint sh, const QStyleOption* opt,
                    const QWidget* w, QStyleHintReturn *shret) const override {
        
        if (sh != QStyle::SH_RubberBand_Mask) {
            return QProxyStyle::styleHint(sh, opt, w, shret);
        }
        if (auto* option = qstyleoption_cast<const QStyleOptionRubberBand*>(opt)) {
            if (auto* mask = qstyleoption_cast<QStyleHintReturnMask*>(shret)) {
                mask->region = option->rect;
                return true;
            }
        }
        return false;
    }
};



class OverlayRubberBand: public QRubberBand {

public:
    OverlayRubberBand(QWidget* parent): QRubberBand(QRubberBand::Rectangle) {
        auto& config = KDDockWidgets::Config::self();
        setStyle(new RubberBandProxyStyle(style()));

        const bool userChoseOpacity = !qIsNaN(config.draggedWindowOpacity());
        if (!userChoseOpacity) {
            config.setDraggedWindowOpacity(DRAGGED_WINDOW_OPACITY);
        }
        setWindowOpacity(config.draggedWindowOpacity());
    }

    virtual void paintEvent(QPaintEvent *event) override {

        QPainter painter(this);
        painter.setRenderHint(QPainter::Antialiasing);

        QPainterPath path;
        path.addRoundedRect(rect(), 5, 5);

        QPen pen(OVERLAY_PEN_COLOR, 1);
        painter.setPen(pen);
        painter.fillPath(path, OVERLAY_PEN_COLOR);
        painter.drawPath(path);
    }
};




DockingOverlay::DockingOverlay(KDDockWidgets::DropArea *dropArea):
    KDDockWidgets::DropIndicatorOverlayInterface(dropArea),
    m_rubberBand(new OverlayRubberBand(dropArea)) {
    
}


KDDockWidgets::DropLocation DockingOverlay::hover_impl(QPoint pt) {

    auto dropLocation = dropLocationForPos(mapFromGlobal(std::move(pt)));
    setCurrentDropLocation(dropLocation);

    KDDockWidgets::Location location = toMultiSplitterLocation(dropLocation);
    KDDockWidgets::Frame* relativeToFrame = nullptr;

    if (QGuiApplication::queryKeyboardModifiers().testFlag(Qt::ControlModifier)) {
        return KDDockWidgets::DropLocation_None;
    }
    switch (dropLocation) {
        case KDDockWidgets::DropLocation_Left:
        case KDDockWidgets::DropLocation_Top:
        case KDDockWidgets::DropLocation_Right:
        case KDDockWidgets::DropLocation_Bottom:
            if (!m_hoveredFrame) {
                qWarning() << "frame is null. location=" << location
                        << "; isHovered=" << isHovered()
                        << "; dropArea->widgets=" << m_dropArea->items();
                Q_ASSERT(false);
                return dropLocation;
            }
            relativeToFrame = m_hoveredFrame;
        default: break;
    }
    QRect frame;
    if (dropLocation == KDDockWidgets::DropLocation_None) {
        m_rubberBand->setVisible(false);
        return KDDockWidgets::DropLocation_None;
    }
    else if (dropLocation == KDDockWidgets::DropLocation_Center) {
        frame = m_hoveredFrame ? m_hoveredFrame->QWidgetAdapter::geometry() : rect();
    } else {
        auto* controller = KDDockWidgets::DragController::instance();
        auto windowBeingDragged = controller->windowBeingDragged();

        frame = m_dropArea->rectForDrop(windowBeingDragged, location,
                                m_dropArea->itemForFrame(relativeToFrame));
    }
    m_rubberBand->setVisible(true);
    m_rubberBand->raise();
    m_rubberBand->setGeometry(geometryForRubberband(std::move(frame)));
    return dropLocation;
}


void DockingOverlay::updateVisibility() {
    m_rubberBand->setVisible(isHovered());
}


QRect DockingOverlay::geometryForRubberband(QRect localRect) const {

    QPoint topLeftLocal = localRect.topLeft();
    QPoint topLeftGlobal = m_dropArea->QWidget::mapToGlobal(topLeftLocal);

    localRect.moveTopLeft(topLeftGlobal);
    return localRect;
}


KDDockWidgets::DropLocation DockingOverlay::dropLocationForPos(const QPoint& pos) {

    const auto outterSegments = computeSegments(rect(), false);
    Segments segments;

    for (auto indicator : { KDDockWidgets::DropLocation_OutterLeft,
                            KDDockWidgets::DropLocation_OutterRight, 
                            KDDockWidgets::DropLocation_OutterTop,
                            KDDockWidgets::DropLocation_OutterBottom }) {
        if (dropIndicatorVisible(indicator)) {
            segments.insert(indicator, outterSegments.value(indicator));
        }
    }
    const bool hasOutter = !segments.isEmpty();
    const bool useOffset = hasOutter;
    const auto inners = computeSegments(hoveredFrameRect(), true, useOffset);

    for (auto indicator : { KDDockWidgets::DropLocation_Left,
                            KDDockWidgets::DropLocation_Top, 
                            KDDockWidgets::DropLocation_Right,
                            KDDockWidgets::DropLocation_Bottom,
                            KDDockWidgets::DropLocation_Center }) {
        if (dropIndicatorVisible(indicator)) {
            segments.insert(indicator, inners.value(indicator));
        }
    }
    QWidget::update();

    for (auto it = segments.cbegin(); it != segments.cend(); ++it) {
        if (it.value().containsPoint(pos, Qt::OddEvenFill)) {
            m_poly = it.value();
            return it.key();
        }
    }
    return KDDockWidgets::DropLocation::DropLocation_None;
}


DockingOverlay::Segments DockingOverlay::computeSegments(const QRect& r,
                                                        bool inner,
                                                        bool useOffset) const {

    const int l     = SEGMENT_GIRTH;
    const int top   = (r.y() == 0 && useOffset) ? l : r.y();
    const int left  = (r.x() == 0 && useOffset) ? l : r.x();
    const int right = (rect().right() == r.right() && useOffset) ?
                        r.right() - l : r.right();
    const int bottom = (rect().bottom() == r.bottom() && useOffset) ?
                        r.bottom() - l : r.bottom();

    const QPoint topLeft        = { left,   top };
    const QPoint topRight       = { right,  top };
    const QPoint bottomLeft     = { left,   bottom };
    const QPoint bottomRight    = { right,  bottom };

    const QVector<QPoint> leftPoints    = { topLeft, bottomLeft,
                                            QPoint(left, bottom) + QPoint(l, -l),
                                            topLeft + QPoint(l, l), topLeft };

    const QVector<QPoint> rightPoints   = { topRight, bottomRight,
                                            bottomRight + QPoint(-l, -l),
                                            topRight + QPoint(-l, l) };

    const QVector<QPoint> topPoints     = { topLeft, topRight,
                                            topRight + QPoint(-l, l),
                                            topLeft + QPoint(l, l) };

    const QVector<QPoint> bottomPoints  = { bottomLeft, bottomRight,
                                            bottomRight  + QPoint(-l, -l),
                                            bottomLeft   + QPoint(l, -l) };

    if (inner) {
        QPolygon bounds = QVector<QPoint> { topLeft     + QPoint( l,  l),
                                            topRight    + QPoint(-l,  l),
                                            bottomRight + QPoint(-l, -l),
                                            bottomLeft  + QPoint( l, -l) };
        
        return {
            { KDDockWidgets::DropLocation_Left,     leftPoints },
            { KDDockWidgets::DropLocation_Top,      topPoints },
            { KDDockWidgets::DropLocation_Right,    rightPoints },
            { KDDockWidgets::DropLocation_Bottom,   bottomPoints },
            { KDDockWidgets::DropLocation_Center,   bounds }
        };
    } else {
        return {
            { KDDockWidgets::DropLocation_OutterLeft,   leftPoints },
            { KDDockWidgets::DropLocation_OutterTop,    topPoints },
            { KDDockWidgets::DropLocation_OutterRight,  rightPoints },
            { KDDockWidgets::DropLocation_OutterBottom, bottomPoints }
        };
    }
}


KDDockWidgets::Location
DockingOverlay::toMultiSplitterLocation(KDDockWidgets::DropLocation location) {
    switch (location) {
        case KDDockWidgets::DropLocation_Left:
            return KDDockWidgets::Location_OnLeft;
        case KDDockWidgets::DropLocation_Top:
            return KDDockWidgets::Location_OnTop;
        case KDDockWidgets::DropLocation_Right:
            return KDDockWidgets::Location_OnRight;
        case KDDockWidgets::DropLocation_Bottom:
            return KDDockWidgets::Location_OnBottom;
        case KDDockWidgets::DropLocation_OutterLeft:
            return KDDockWidgets::Location_OnLeft;
        case KDDockWidgets::DropLocation_OutterTop:
            return KDDockWidgets::Location_OnTop;
        case KDDockWidgets::DropLocation_OutterRight:
            return KDDockWidgets::Location_OnRight;
        case KDDockWidgets::DropLocation_OutterBottom:
            return KDDockWidgets::Location_OnBottom;
        default:
            return KDDockWidgets::Location_None;
    }
}


QPoint DockingOverlay::posForIndicator(KDDockWidgets::DropLocation) const {
    return {};
}
