// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __TABWIDGETPROXYSTYLE_H__
#define __TABWIDGETPROXYSTYLE_H__

#include <QProxyStyle>


class TabWidgetProxyStyle : public QProxyStyle {
public:
    inline TabWidgetProxyStyle(QStyle* style): QProxyStyle(style) {}
    virtual ~TabWidgetProxyStyle() = default;

    QRect subElementRect(QStyle::SubElement subElement,
                        const QStyleOption* option,
                        const QWidget* widget) const override;

    // void drawPrimitive( QStyle::PrimitiveElement element,
    //                     const QStyleOption* option, QPainter* painter,
    //                     const QWidget* widget) const override;
};

#endif // __TABWIDGETPROXYSTYLE_H__