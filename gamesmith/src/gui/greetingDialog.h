/**
 * Copyright (C) 2022 Matthieu Jacquemet
 * 
 * This file is part of GameSmith.
 * 
 * GameSmith is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * GameSmith is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __GREETINGDIALOG_H__
#define __GREETINGDIALOG_H__

#include <QDir>
#include <QDialog>

#include "ui_project.h"


class GreetingDialog:   public QDialog,
                        private Ui::Dialog
{
    Q_OBJECT

public:
    GreetingDialog();
    ~GreetingDialog() = default;

    QDir openedProject() const;

public Q_SLOTS:
    void selectDirectory();
    void moreTemplates();
    void createNew();

private:
    void loadTemplates();

    QDir m_openedProject;
    QDir m_defaultDir;
};

#endif // __GREETINGDIALOG_H__