// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QPainter>
#include <QIcon>
#include <QStyleOption>
#include <QDebug>

#include "editor_utils.h"
#include "dockingTabBarButton.h"


void DockingTabBarButton::paintEvent(QPaintEvent* event) {
    QPainter painter(this);
    QIcon currentIcon = icon();

    QSize iconSize = smallIconSize(currentIcon, this);

    QSize pos = (size() - iconSize) / 2;
    currentIcon.paint(&painter, pos.width(), pos.height(),
                                iconSize.width(), iconSize.height());
}


QSize DockingTabBarButton::sizeHint() const {
    QStyleOption opt;
    opt.initFrom(this);

    const int m = style()->pixelMetric(QStyle::PM_SmallIconSize, &opt, this);
    return QSize(m, m);
}