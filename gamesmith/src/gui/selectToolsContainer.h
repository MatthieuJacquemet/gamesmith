// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SELECTTOOLS_H__
#define __SELECTTOOLS_H__

#include "selectTool.gs_rpc_stub.h"
#include "toolBoxContainer.h"
#include "toolContext.h"


class SelectToolsContainer: public ToolBoxContainer {

    Q_OBJECT

public:
    SelectToolsContainer(QWidget* parent = nullptr);
    virtual ~SelectToolsContainer() = default;

protected:
    using ContextType = ToolContext<stub::SelectTool>;
    QSharedPointer<ContextType> m_context;
};

#endif // __SELECTTOOLS_H__