// Copyright 2023 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __DOMNODEMODEL_H__
#define __DOMNODEMODEL_H__

#include <QAbstractXmlNodeModel>
#include <QXmlNamePool>
#include <QDomDocument>

class DomNodeModel: public QAbstractXmlNodeModel {

public:
	typedef QVector<QDomNode> Path;

public:
	DomNodeModel(const QXmlNamePool& pool, const QDomDocument& document);
    ~DomNodeModel() = default;

	QUrl baseUri(const QXmlNodeModelIndex& n) const override;
	QXmlNodeModelIndex::DocumentOrder compareOrder( const QXmlNodeModelIndex & ni1,
                                                    const QXmlNodeModelIndex & ni2) const override;

	QUrl documentUri(const QXmlNodeModelIndex& n) const override;
	QXmlNodeModelIndex elementById(const QXmlName& id) const override;
	QXmlNodeModelIndex::NodeKind kind(const QXmlNodeModelIndex & ni) const override;
	QXmlName name(const QXmlNodeModelIndex& ni) const override;
	QVector<QXmlName> namespaceBindings(const QXmlNodeModelIndex& n) const override;
	QVector<QXmlNodeModelIndex> nodesByIdref(const QXmlName& idref) const override;
	QXmlNodeModelIndex root(const QXmlNodeModelIndex& n) const override;
	QSourceLocation	sourceLocation(const QXmlNodeModelIndex& index) const;
	QString	stringValue(const QXmlNodeModelIndex& n) const override;
	QVariant typedValue(const QXmlNodeModelIndex & node) const override;

	QXmlNodeModelIndex fromDomNode(const QDomNode& node) const;
	QDomNode toDomNode(const QXmlNodeModelIndex&) const;
	Path path(const QDomNode&) const;
	int childIndex(const QDomNode&) const;

protected:
	QVector<QXmlNodeModelIndex> attributes(const QXmlNodeModelIndex& element) const  override;
	QXmlNodeModelIndex nextFromSimpleAxis(SimpleAxis axis,const QXmlNodeModelIndex& origin) const override;

private:
	mutable QXmlNamePool m_pool;
	mutable QDomDocument m_doc;
};

#endif // __DOMNODEMODEL_H__