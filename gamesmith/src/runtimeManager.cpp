// Copyright (C) 2021 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QSocketNotifier>
#include <QStandardPaths>
#include <QDebug>
#include <QTimer>

#include <memory>
#include <algorithm>
#include <private/qobject_p.h>
#include <custom_types.h>

#include "objectRequestBroker.h"
#include "editor_utils.h"
#include "gamesmith_config.h"

#include "serviceBase.h"
#include "typeRegistryService.gs_rpc_stub.h"
#include "CORBAService.gs_rpc_stub.h"
#include "rpc_message.h"
#include "runtimeManager.h"
#include "serviceInterface.h"
#include "messageAwaitable.h"


namespace detail {
    
    std::string get_type_name(TypeHandle handle) {
        if (Editor* editor = Editor::globalInstance()) {
            RuntimeManager* runtime = editor->runtime();
            return runtime->typeName(handle.get_index()).toStdString();
        }
        return std::string();
    }

    template<class Type>
    inline TypeHandle getTypeHandleHookImpl(const char* name) {
        static TypeHandle type_handle;
        if (UNLIKELY(type_handle == TypeHandle::none())) {
            if (Editor* editor = Editor::globalInstance()) {
                RuntimeManager* runtime = editor->runtime();
                runtime->recordTypeHandle(type_handle, 
                            QString::fromStdString(name));
            }
        }
        return type_handle;
    }

    #define DEFINE_TYPE_HOOK2(TYPE, NAME)                        \
    template<> TypeHandle get_type_handle_proxy<TYPE>() {        \
        return getTypeHandleHookImpl<TYPE>(NAME);                \
    }
    #define DEFINE_TYPE_HOOK(TYPE) DEFINE_TYPE_HOOK2(TYPE, #TYPE)



    DEFINE_TYPE_HOOK2(long,             "long")
    DEFINE_TYPE_HOOK2(int,              "int")
    DEFINE_TYPE_HOOK2(unsigned int,     "uint")
    DEFINE_TYPE_HOOK2(short,            "short")
    DEFINE_TYPE_HOOK2(unsigned short,   "ushort")
    DEFINE_TYPE_HOOK2(char,             "char")
    DEFINE_TYPE_HOOK2(unsigned char,    "uchar")
    DEFINE_TYPE_HOOK2(bool,             "bool")
    DEFINE_TYPE_HOOK2(double,           "double")
    DEFINE_TYPE_HOOK2(float,            "float")
    DEFINE_TYPE_HOOK2(std::string,      "string")
    DEFINE_TYPE_HOOK2(std::wstring,     "wstring")
    
    DEFINE_TYPE_HOOK(int8_t)
    DEFINE_TYPE_HOOK(uint64_t)
    DEFINE_TYPE_HOOK(LVecBase2f)
    DEFINE_TYPE_HOOK(LVecBase2d)
    DEFINE_TYPE_HOOK(LVecBase2i)
    DEFINE_TYPE_HOOK(LVecBase3f)
    DEFINE_TYPE_HOOK(LVecBase3d)
    DEFINE_TYPE_HOOK(LVecBase3i)
    DEFINE_TYPE_HOOK(LVecBase4f)
    DEFINE_TYPE_HOOK(LVecBase4d)
    DEFINE_TYPE_HOOK(LVecBase4i)

}


class RuntimeManager::RuntimeManagerPrivate {

public:
    struct TypeEntry {
        QString name; QVector<int32_t> parents, children;
    };
    QVector<TypeEntry> types;
    QHash<QString, int32_t> typesByName;

    QSocketNotifier* socketNotifier = nullptr;
    QTimer* killTimer;
    stub::TypeRegistryService service;

    QList<std::reference_wrapper<TypeHandle>> typeHandles;
    // QList<std::coroutine_handle<>> pendingRequests;
    // ServiceInterface* currentTransaction;
};


RuntimeManager::RuntimeManager(QObject* parent): QProcess(parent),
    d_ptr(new RuntimeManagerPrivate) {

    Q_D(RuntimeManager);
    d->killTimer = new QTimer(this);

    auto finished = QOverload<int, ExitStatus>::of(&QProcess::finished);
    
    connect(this, finished, [this](int status, QProcess::ExitStatus e) {
        Q_D(RuntimeManager);
        d->killTimer->stop();
        qDebug() << "Runtime exited with status" << status;
    });
    d->killTimer->setSingleShot(true);



    connect(this, &RuntimeManager::clientConnected, this, [this]() -> Task<> {
        using TypeList = QVector<int32_t>;
        Q_D(RuntimeManager);

        if (Q_LIKELY(co_await d->service.acquire())) {
            for (auto& [id, name, pars, chils] : co_await d->service.get_types()) {
                if (id >= d->types.size()) {
                    d->types.resize(id + 1);
                }
                d->types[id] = RuntimeManagerPrivate::TypeEntry {
                    .name = QString::fromStdString(name),
                    .parents = TypeList(pars.begin(), pars.end()),
                    .children = TypeList(chils.begin(), chils.end())
                };
                d->typesByName[d->types[id].name] = id;
            }
	        registerLegacyPropertieWidgetsFactories();
            register_legacy_variant_types();
            Variant::register_type_factory<std::vector<LVecBase3f>>();
            Variant::register_type_factory<std::vector<uint64_t>>();
        }
    });
    connect(d->killTimer, &QTimer::timeout, this, &QProcess::kill);

    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
    broker->set_connection_callback([this](bool connected) {
        QMetaObject::invokeMethod(this, 
            connected ? "clientConnectedImpl" : "clientDisconnectedImpl",
            Qt::QueuedConnection
        );
    });
    setProcessChannelMode(QProcess::ForwardedChannels);
}


RuntimeManager::~RuntimeManager() {
    Q_D(RuntimeManager);

    QProcess::terminate();
    if (!QProcess::waitForFinished(d->killTimer->interval())) {
        QProcess::kill();
    }
    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
    broker->set_connection_callback(nullptr);
    if (broker->get_current_state() & ObjectRequestBroker::S_bound) {
        ObjectRequestBroker::get_global_ptr()->unbind_interface();
    }
}


void RuntimeManager::terminateApplication() { 
    Q_D(RuntimeManager);

    emit QProcess::terminate();
    d->killTimer->start();
}

void RuntimeManager::initialize() {
    Q_D(RuntimeManager);

    QString bindAddress = config<QString>("core/runtime-interface-uri");
    QString runtimePath = config<QString>("core/runtime-path",
                                        DEFAULT_RUNTIME_EXECUTABLE);

    d->killTimer->setInterval(config<int>("core/runtime-terminate-timeout",
                                        DEFAULT_RUNTIME_KILL_TIMEOUT));

    if (bindAddress.isEmpty()) {
        bindAddress = DEFAULT_IPC_BIND_ADDRESS;
        
#if defined(Q_OS_LINUX) || defined(Q_OS_UNIX) || defined(Q_OS_MACOSX)
        QStringList locations = QStandardPaths::standardLocations(
            QStandardPaths::RuntimeLocation
        );
        if (Q_LIKELY(locations.size() > 0)) {
            QDir runtimeDir(locations[0]);
            runtimeDir.mkdir("gamesmith");
            runtimeDir.cd("gamesmith");
            bindAddress = "ipc://" + runtimeDir.absoluteFilePath("socket_" + 
                    QString::number(QCoreApplication::applicationPid()));
        }
    }
#endif // defined(Q_OS_LINUX) || defined(Q_OS_UNIX) || defined(Q_OS_MACOSX)
    if (setBindAddress(bindAddress) && !runtimePath.isEmpty()) {

        if (!QFile::exists(runtimePath)) {
            runtimePath = QStandardPaths::findExecutable(runtimePath);
        }
        if (!runtimePath.isEmpty()) {
            QProcess::setProgram(runtimePath);
            start();
            QProcess::waitForStarted();

            emit started(std::move(bindAddress));
        }
    }
}


bool RuntimeManager::setBindAddress(const QString& bindAddress) {
    Q_D(RuntimeManager);

    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
    bool result;
    if (broker->get_current_state() & ObjectRequestBroker::S_bound) {
        broker->unbind_interface();
    }
    if ((result = broker->bind_interface(bindAddress.toStdString()))) {

        QString actualUri = QString::fromStdString(broker->get_transport_uri());
        
        QProcessEnvironment env(QProcessEnvironment::systemEnvironment());
        env.insert(IPC_URI_ENV, actualUri);
        setProcessEnvironment(std::move(env));
    }
    return result;
}

bool RuntimeManager::isConnected() const noexcept {
    return ObjectRequestBroker::get_global_ptr()->get_current_state() &
            ObjectRequestBroker::S_connected;
}

bool RuntimeManager::isLocal() const noexcept {
    // TODO: implement remote runtime
    return true;
}

int32_t RuntimeManager::typeId(const QString& name) const {
    Q_D(const RuntimeManager);
    return d->typesByName.value(name, 0);
}

QString RuntimeManager::typeName(int32_t typeId) const {
    Q_D(const RuntimeManager);
   return typeId < d->types.count() ? d->types[typeId].name : QString();
}

bool RuntimeManager::isSubclassOf(int32_t typeId, int32_t baseId) const {
    if (typeId == baseId) {
        return true;
    }
    Q_D(const RuntimeManager);
    const auto& parents = d->types[typeId].parents;
    return std::any_of(parents.begin(), parents.end(), [&](int32_t id) {
        return isSubclassOf(id, baseId);
    });
}

const QVector<int32_t>& RuntimeManager::parents(int32_t typeId) const {
    Q_D(const RuntimeManager);
    Q_ASSERT(typeId < d->types.count());
    return d->types[typeId].parents;
}

const QVector<int32_t>& RuntimeManager::children(int32_t typeId) const {
    Q_D(const RuntimeManager);
    Q_ASSERT(typeId < d->types.count());
    return d->types[typeId].children;
}


RuntimeManager::TypeList
RuntimeManager::subTypes(const QString& name, bool includeBase) const {
    Q_D(const RuntimeManager);
    TypeList subTypes;
    std::function<void(int32_t)> addTypes;

    addTypes = [&](int32_t entryId) {
        for (uint32_t subTypeId : d->types[entryId].children) {
            subTypes.append(QPair(subTypeId, d->types[subTypeId].name));
            addTypes(subTypeId);
        }
    };
    if (int32_t typeId = d->typesByName.value(name, 0)) {
        if (includeBase) {
            subTypes.append(QPair(typeId, d->types[typeId].name));
        }
        addTypes(typeId);
    }
    return subTypes;
}


void RuntimeManager::recordTypeHandle(TypeHandle& handle, const QString& name) {
    Q_D(RuntimeManager);
    if (int32_t index = d->typesByName.value(name)) {
        handle = TypeHandle::from_index(index);
        d->typeHandles.append(std::reference_wrapper(handle));
    }
}

void RuntimeManager::clientConnectedImpl() {
    Q_D(RuntimeManager);

    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
    broker->handle_connected(true);

    auto* notifier = new QSocketNotifier(broker->get_transport_handle(),
                                        QSocketNotifier::Read , this);

    delete std::exchange(d->socketNotifier, notifier);

    connect(notifier, &QSocketNotifier::activated, [this] {
        Q_D(RuntimeManager);
        d->socketNotifier->setEnabled(false);
        ObjectRequestBroker::get_global_ptr()->handle_messages();
        d->socketNotifier->setEnabled(true);
    });

    emit clientConnected();
}


void RuntimeManager::clientDisconnectedImpl() {
    Q_D(RuntimeManager);

    ObjectRequestBroker::get_global_ptr()->handle_connected(false);
    // d->currentTransaction = nullptr;

    // reset variant factory
    Variant::get_factory().clear();
    clearPropertyWidgetsFactories();

    for (std::reference_wrapper<TypeHandle>& reference : d->typeHandles) {
        reference.get() = TypeHandle::none();
    }
    d->typeHandles.clear();

    emit clientDisconnected();
}

// Task<ServiceInterface*>
// RuntimeManager::createService(const QString& name, const ServiceFactory& factory) {
//     Q_D(RuntimeManager);
//     ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
//     std::string std_name = name.toStdString();
//     std::shared_ptr<ServiceInterface> interface;

//     uint16_t id = co_await d->corbaService->create_service(std_name);
//     if (LIKELY(id == 0)) {
//         qWarning() << "Remote do not have service " << name;
//     } 
//     else if (factory) {
//         interface.reset(static_cast<ServiceInterface*>(factory(id)));
//     } 
//     else if (const auto& factory = broker->get_factory(std_name)) {
//         interface.reset(static_cast<ServiceInterface*>(factory(id)));
//     } else {
//         qWarning() << "Unknown service named: " << name;
//     }
//     if (LIKELY(interface)) {
//         broker->register_service(interface);
//     }
//     // if (interface)
//     // std::cout << "created service " << std_name << " : " << interface->get_name() << " " << interface->get_id() << std::endl;
//     co_return interface;
// }

// Task<bool>  RuntimeManager::releaseService(ServiceRef service, bool await) {
//     Q_D(RuntimeManager);
//     ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
//     uint16_t service_id = service->get_id();
//     service->markStale();
//     qDebug() << "relase service " << service->get_name();
//     detail::BlockingContext context(d->corbaService, await);
//     bool result;
//     if ((result = co_await d->corbaService->release_service(service_id))) {
//         broker->release_service(service);
//     }
//     // Normally, at this point there should be no pending requests left,
//     // but just in case, we release the pending requests futures only after
//     // we received acknowledgement from the runtime, since some futures 
//     // may still be awaiting for a response before we sent the release command.
//     // If we release a pending request before the release command is sent, then
//     // the future's slot will be freed. This will be problematic if a new 
//     // request acquire this slot just before the response arrives as the
//     // reponse is not intended for this request's future.
//     while (!service->m_pendingRequests.empty()) {
//         auto& future = broker->get_future(service->m_pendingRequests.front());
//         future.cancel(); // discard any pending requests after release
//     }
//     co_return result;
// }

// Task<bool> RuntimeManager::beginTransaction(ServiceInterface*service) {
//     Q_ASSERT(service != nullptr);
//     Q_D(RuntimeManager);

//     ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
//     co_await TransactionAwaiter(service);
//     co_return broker->is_connected() && (d->currentTransaction = service);
// }

// void RuntimeManager::endTransaction() {
//     Q_D(RuntimeManager);
//     d->currentTransaction = nullptr;

//     while (!d->pendingRequests.empty()) {
//         d->pendingRequests.front().resume(); d->pendingRequests.removeFirst();
//     }
// }

// ServiceInterface* RuntimeManager::currentTransaction() const {
//     Q_D(const RuntimeManager);
//     return d->currentTransaction;
// }

// void RuntimeManager::registerTransactionAwaiter(std::coroutine_handle<>&& handle) {
//     Q_D(RuntimeManager);
//     d->pendingRequests.append(std::move(handle));
// }

// Task<bool> RuntimeManager::sendMessage( const rpc::MessageHeader& header,
//                                         const ArgsCallback& callback) {
//     using header_t = rpc::ServiceCallHeader;
//     // ServiceInterface* service = static_cast<ServiceInterface*>(
//     //     broker->get_service(std::get<header_t>(header).service_id)
//     // );
//     // co_await TransactionAwaiter(service);
//     ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
//     co_return broker->send_message(header, callback);
// }