// Copyright 2023 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "objectRequestBroker.h"
#include "serviceInterface.h"
#include "messageAwaitable.h"
#include "editor.h"
#include "runtimeManager.h"
#include <memory>


// bool TransactionAwaiter::await_ready() {
//     RuntimeManager* runtime = Editor::globalInstance()->runtime();
//     const auto current = runtime->currentTransaction();
    
//     return !runtime->isConnected() || !current || current == m_service;
// }


// ServiceInterface* TransactionAwaiter::getService() const {
//     return m_service;
// }


// bool TransactionAwaiter::do_suspend(std::coroutine_handle<> handle) {
//     RuntimeManager* runtime = Editor::globalInstance()->runtime();
//     runtime->registerTransactionAwaiter(std::move(handle));
//     return true;
// }


Future<DatagramIterator>& MessageAwaiter::await_resume() {
    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
    return broker->get_future(m_request->id);
}


ServiceInterface* MessageAwaiter::getService() const {
    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
    return static_cast<ServiceInterface*>(
        broker->get_service(m_request->service_id)
    );
}

bool MessageAwaiter::do_suspend(std::coroutine_handle<> handle) {

    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
    Future<DatagramIterator>& future = broker->get_future(m_request->id);

    future.then([handle](Future<DatagramIterator>& future) mutable {
        if (!handle.done()) handle.resume();
    });
    return true; // suspend so that the future can be resumed
}


bool MessageAwaiter::await_ready() {
    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
    Future<DatagramIterator>& future = broker->get_future(m_request->id);

    return future.has_value();
}



bool RPCAwaiterInterface::await_suspend(std::coroutine_handle<> handle) {
    
    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
    ServiceInterface* service = getService();

    if (!service->blockingContext()) {
        return do_suspend(std::move(handle));
    }
    // while (!this->await_ready()) {
    //     broker->handle_one_message();
    // }
    return false;
}
