// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.



#ifndef __SERVICEHANDLE_H__
#define __SERVICEHANDLE_H__

#include <memory>
#include <functional>

#include "serviceInterface.h"
#include "editor.h"
#include "runtimeManager.h"


template<std::derived_from<ServiceInterface> ServiceType>
class ServiceHandle : public std::shared_ptr<ServiceType> {

    typedef std::function<ServiceInterface*(uint16_t)> Factory;
    // Factory _factory;
public:
    template<class... A> requires(std::is_constructible_v<ServiceType, A...>)
    ServiceHandle(A&&... args) : std::shared_ptr<ServiceType>(
        std::make_shared<ServiceType>(std::forward<A>(args)...)) {

    }
    ~ServiceHandle() { release(); }


    Task<bool> release(bool await = false) {
        ServiceType* service = std::shared_ptr<ServiceType>::get();
        co_return co_await service->release(await);
    }

    [[nodiscard]] Task<ServiceType*> acquire() noexcept {
        
        ServiceType* service = std::shared_ptr<ServiceType>::get();
        if (service->get_id() == 0 && !co_await service->bind()) {
            co_return static_cast<ServiceType*>(nullptr);
        }
        co_return service;
    }
    // class TransactionScope final : std::shared_ptr<ServiceType> {
    //     DISABLE_COPY(TransactionScope);

    //     inline TransactionScope(std::shared_ptr<ServiceType>&& ref):
    //         std::shared_ptr<ServiceType>(std::move(ref)) {}

    //     inline ~TransactionScope() {
    //         if (std::shared_ptr<ServiceType>::get() != nullptr) {
    //             RuntimeManager* runtime = Editor::globalInstance()->runtime();
    //             runtime->endTransaction(*this);
    //         }
    //     }
    //     TransactionScope& operator=(TransactionScope&& ref) {
    //         PointerTo<ServiceType>::operator=(std::move(ref));
    //     }
    // };
    // [[nodiscard]] Task<TransactionScope> acquireTransaction() {
    //     std::shared_ptr<ServiceType> service = acquire();
    //     RuntimeManager* runtime = Editor::globalInstance()->runtime();

    //     if (service && !co_await runtime->beginTransaction(service)) {
    //         service.reset();
    //     }
    //     co_return TransactionScope(std::move(service));
    // };
};

#endif // __SERVICEHANDLE_H__
