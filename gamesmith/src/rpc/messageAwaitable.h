// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __MESSAGEAWAITABLE_H__
#define __MESSAGEAWAITABLE_H__

#include "task.h"


class DatagramIterator;
class RequestBase;
class ServiceInterface;

namespace detail { class ResultRequest; }

struct RPCAwaiterInterface {

    virtual ServiceInterface* getService() const = 0;

    bool await_suspend(std::coroutine_handle<> handle);
    virtual bool await_ready() = 0;
    virtual bool do_suspend(std::coroutine_handle<> handle) = 0;
};


class MessageAwaiter: public RPCAwaiterInterface {
public:
    MessageAwaiter(detail::ResultRequest* request): m_request(request) {}

    ServiceInterface* getService() const override;

    Future<DatagramIterator>& await_resume();
    bool do_suspend(std::coroutine_handle<> handle) override;
    bool await_ready() override;

protected:
    const detail::ResultRequest* m_request;
};

// class TransactionAwaiter: public RPCAwaiterInterface {
// public:
//     TransactionAwaiter(ServiceInterface* service): m_service(service) {}

//     ServiceInterface* getService() const override;

//     void await_resume() {}
//     bool do_suspend(std::coroutine_handle<> handle) override;
//     bool await_ready() override;

// private:
//     ServiceInterface* m_service;
// };


#endif // __MESSAGEAWAITABLE_H__