// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SERVICEINTERFACE_H__
#define __SERVICEINTERFACE_H__

#include <serviceBase.h>
#include <meta.h>
#include <objectRequestBroker.h>
#include <deque>

#include "messageAwaitable.h"
#include "runtimeManager.h"
#include "editor.h"
#include "task.h"


class ServiceInterface;


namespace detail {

using FillerType = std::function<void (Datagram&)>;

// RAII class to manage the blocking context of a service
class BlockingContext {
public:
    using ServiceRef = std::shared_ptr<ServiceInterface>;
    BlockingContext(const ServiceRef& svc, bool block);
    ~BlockingContext();
private:
    ServiceRef m_service;
};

class ResultRequest: public rpc::ServiceCallHeader {
public:
    ResultRequest(ServiceInterface* service, uint16_t cid, uint16_t hid);
    ~ResultRequest();
    inline ServiceInterface* service() const { return m_service; }
private:
    ServiceInterface* m_service;
};


template<class Type>
class Request: public std::conditional_t<!std::is_void_v<Type>,
                                        ResultRequest,
                                        rpc::ServiceCallHeader> {

public:
    Request(ServiceInterface* service, uint16_t cid, uint16_t hid)
        requires(!std::is_void_v<Type>);
    
    Request(ServiceInterface* service, uint16_t cid, uint16_t hid)
        requires(std::is_void_v<Type>);

    Task<Type> send(const FillerType& callback);
};

}


class ServiceInterface : public ServiceBase {

public:
    using base_service = ServiceBase;

    ServiceInterface(uint16_t id = 0);
    virtual ~ServiceInterface();

    const char* get_name() const override { return "ServiceInterface"; }

    constexpr bool blockingContext() const { return m_blocking; }
    constexpr bool isStale() const { return m_isStale; }
    constexpr bool isBound() const { return m_isBound; }

    static Task<bool> bind(ServiceInterface* service);
    static Task<bool> unbind(ServiceInterface* service);

    Task<bool> acquire();

protected:
    friend class detail::BlockingContext;
    friend class detail::ResultRequest;
    friend class RuntimeManager;
    DISABLE_COPY_MOVE(ServiceInterface);

    template<class ReturnType, size_t CLS, size_t ID, class... Args>
    Task<ReturnType> _call_endpoint(const Args&... args) {
        if (Q_LIKELY(!m_isStale && m_isBound)) {
            detail::Request<ReturnType> request(this, CLS, ID);
            try {
                co_return co_await request.send([&args...](auto& msg) {
                    (generic_write_datagram(msg, args), ...);
                });
            } catch (const std::exception& error) {
                std::cerr << "call_endpoint: " << error.what() << std::endl;
            }
        }
        co_return ReturnType{};
    }
    bool handle_call(const rpc::ServiceCallHeader& header,
            Datagram& datagram, DatagramIterator& scan) override;

private:
    bool m_blocking : 1, m_isStale : 1, m_isBound : 1;
    std::deque<uint64_t> m_pendingRequests;
};








namespace detail {

template<class Type>
Request<Type>::Request(ServiceInterface* service, uint16_t cid, uint16_t hid)
    requires(std::is_void_v<Type>):
    rpc::ServiceCallHeader{0UL, service->get_id(), cid, hid, false} {

}

template<class Type>
Request<Type>::Request(ServiceInterface* service, uint16_t cid, uint16_t hid)
    requires(!std::is_void_v<Type>):
    ResultRequest(service, cid, hid) {

}


template<class Type> 
inline Task<Type> Request<Type>::send(const FillerType& callback) {
    
    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();

    if (UNLIKELY(!broker->send_message(*this, callback))) {
        co_return Type();
    }
    else if constexpr (!std::is_void_v<Type>) {
        Type returnValue = Type();
        Future<DatagramIterator>& future = co_await MessageAwaiter(this);
        if (future.has_value()) {
            generic_read_datagram(returnValue, future.get_result());
        }
        co_return returnValue;
    }
}

}

namespace stub { using ServiceInterface = ::ServiceInterface; }

#endif // __SERVICEINTERFACE_H__