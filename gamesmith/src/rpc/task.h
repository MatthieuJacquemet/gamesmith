// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __TASK_H__
#define __TASK_H__

#include <functional>
#include <variant>

#include "coroutine.h"
#include "future.h"
#include <future>

namespace detail {
    template<typename T>
    concept destructible = std::is_nothrow_destructible_v<T>;

    template<typename T, typename ... Args>
    concept constructible_from = destructible<T> && std::is_constructible_v<T, Args...>;
}

// A program-defined type on which the coroutine_traits specializations below depend
struct as_coroutine {};

// Enable the use of std::future<T> as a coroutine type
// by using a std::promise<T> as the promise type.
template <typename T, typename... Args>
requires(!std::is_void_v<T> && !std::is_reference_v<T>)
struct std::coroutine_traits<std::future<T>, as_coroutine, Args...> {

    struct promise_type : std::promise<T> {
        std::future<T> get_return_object() noexcept {
            return this->get_future();
        }

        std::suspend_never initial_suspend() const noexcept { return {}; }
        std::suspend_never final_suspend() const noexcept { return {}; }

        void return_value(const T &value) noexcept(std::is_nothrow_copy_constructible_v<T>) {
            this->set_value(value);
        }
        void return_value(T &&value) noexcept(std::is_nothrow_move_constructible_v<T>) {
            this->set_value(std::move(value));
        }
        void unhandled_exception() noexcept {
            this->set_exception(std::current_exception());
        }
    };
};


template <typename... Args>
struct std::coroutine_traits<std::future<void>, as_coroutine, Args...> {

    struct promise_type : std::promise<void> {
        std::future<void> get_return_object() noexcept {
            return this->get_future();
        }
        
        std::suspend_never initial_suspend() const noexcept { return {}; }
        std::suspend_never final_suspend() const noexcept { return {}; }
        
        void return_void() noexcept {
            this->set_value();
        }
        void unhandled_exception() noexcept {
            this->set_exception(std::current_exception());
        }
    };
};

// Allow co_await'ing std::future<T> and std::future<void>
// by naively spawning a new thread for each co_await.
template <typename T>
auto operator co_await(std::future<T> future) noexcept requires(!std::is_reference_v<T>) {
    struct awaiter : std::future<T> {
        bool await_ready() const noexcept {
            using namespace std::chrono_literals;
            return this->wait_for(0s) != std::future_status::timeout;
        }
        void await_suspend(std::coroutine_handle<> cont) const {
            std::thread([this, cont] {
                this->wait();
                cont.resume();
            }).detach();
        }
        T await_resume() { return this->get(); }
    };
    return awaiter{std::move(future)};
}



template<typename T>
concept has_await_methods = requires(T t) {
    { t.await_ready() } -> std::same_as<bool>;
    { t.await_suspend(std::declval<std::coroutine_handle<>>()) };
    { t.await_resume() };
};

template<typename T>
concept has_operator_coawait = requires(T t) {
    { t.operator co_await() };
};


template<typename T>
concept Awaitable = has_operator_coawait<T> || has_await_methods<T>;


template<class DataType> class Task;


class FinalSuspend {
public:
    explicit FinalSuspend(const std::vector<std::coroutine_handle<>>& coroutines)
        : _awaiting_coroutines(coroutines) {}


    bool await_ready() const noexcept { return false; }

    template<typename Promise>
    void await_suspend(std::coroutine_handle<Promise> finished) noexcept {
        Promise& promise = finished.promise();

        for (auto& awaiter : _awaiting_coroutines) {
            awaiter.resume();
        }
        _awaiting_coroutines.clear();

        if (promise.set_destroy_handle()) {
            finished.destroy();
        }
    }
    constexpr void await_resume() const noexcept {}

private:
    std::vector<std::coroutine_handle<>> _awaiting_coroutines;
};


class TaskPromiseBase {
public:
    TaskPromiseBase() = default;
    ~TaskPromiseBase() = default;

    inline std::suspend_never initial_suspend() { return {}; }

    FinalSuspend final_suspend() const noexcept {
        return FinalSuspend(_awaiting_coroutines);
    }

    template<typename T>
    auto&& await_transform(Task<T>&& task) { return std::move(task); }

    template<typename T>
    auto& await_transform(Task<T>& task) { return task; }

    template<Awaitable T>
    auto&& await_transform(T&& awaitable) { return std::move(awaitable); }

    template<Awaitable T>
    auto& await_transform(T& awaitable) { return awaitable; }

    void add_awaiting_coroutine(std::coroutine_handle<>&& coroutine) {
        _awaiting_coroutines.emplace_back(std::move(coroutine));
    }

    bool has_awaiting_coroutines() const {
        return !_awaiting_coroutines.empty();
    }

    bool set_destroy_handle() noexcept {
        return _destroy.exchange(true, std::memory_order_acq_rel);
    }
private:
    friend class FinalSuspend;

    std::vector<std::coroutine_handle<>> _awaiting_coroutines;
    std::atomic<bool> _destroy = false;
};


template<typename Promise> class TaskAwaiterBase {
public:
    bool await_ready() const noexcept {
        return !_coroutine || _coroutine.done();
    }
    void await_suspend(std::coroutine_handle<> coroutine) noexcept {
        _coroutine.promise().add_awaiting_coroutine(std::move(coroutine));
    }
protected:
    TaskAwaiterBase(std::coroutine_handle<Promise> coroutine):
        _coroutine(coroutine) {}

    std::coroutine_handle<Promise> _coroutine = {};
};


template<class DataType> class TaskPromise : public TaskPromiseBase {
public:
    TaskPromise() = default;
    ~TaskPromise() = default;
    
    inline Task<DataType> get_return_object() noexcept;

    inline void unhandled_exception() {
        _value = std::current_exception();
    }

    inline void return_value(DataType&& value) noexcept {
        _value.template emplace<DataType>(std::move(value));
    }

    inline void return_value(const DataType& value) noexcept {
        _value = value;
    }

    template<detail::constructible_from<DataType> U>
    inline void return_value(U&& value) noexcept {
        _value = std::forward<U>(value);
    }

    template<detail::constructible_from<DataType> U>
    inline void return_value(const U& value) noexcept {
        _value = value;
    }

    inline DataType& result() & {
        if (std::holds_alternative<std::exception_ptr>(_value)) {
            assert(std::get<std::exception_ptr>(_value) != nullptr);
            std::rethrow_exception(std::get<std::exception_ptr>(_value));
        }
        return std::get<DataType>(_value);
    }

    inline DataType&& result() && {
        if (std::holds_alternative<std::exception_ptr>(_value)) {
            assert(std::get<std::exception_ptr>(_value) != nullptr);
            std::rethrow_exception(std::get<std::exception_ptr>(_value));
        }
        return std::move(std::get<DataType>(_value));
    }
private:
    std::variant<std::monostate, DataType, std::exception_ptr> _value;
};


template<> class TaskPromise<void> : public TaskPromiseBase {
public:
    TaskPromise() = default;
    ~TaskPromise() = default;

    inline Task<void> get_return_object() noexcept;

    inline void unhandled_exception() {
        _exception = std::current_exception();
    }
    inline void return_void() noexcept {}

    inline void result() {
        if (UNLIKELY(_exception)) {
            std::rethrow_exception(_exception);
        }
    }
private:
    std::exception_ptr _exception;
};


template<class DataType = void> class Task {
public:
    using promise_type = TaskPromise<DataType>;
    using value_type = DataType;

    explicit Task() noexcept = default;
    Task(const Task&) = delete;
    Task& operator=(const Task&) = delete;

    explicit Task(std::coroutine_handle<promise_type> coroutine): 
        _coroutine(std::exchange(coroutine, {})) {}

    inline Task(Task&& other) noexcept :
        _coroutine(std::exchange(other._coroutine, {})) {}

    inline Task& operator=(Task&& other) noexcept {
        if (std::addressof(other) != this) {
            if (_coroutine) {
                if (_coroutine.promise().set_destroy_handle()) {
                    _coroutine.destroy();
                }
            }
            _coroutine = std::exchange(other._coroutine, {});
        }
        return *this;
    }
    ~Task() {
        if (_coroutine && _coroutine.promise().set_destroy_handle()) {
            _coroutine.destroy();
        }
    };

    DataType execute() {
        if (LIKELY(!_coroutine.done())) {
            _coroutine.resume();
            return _coroutine.promise().result();
        }
        return DataType();
    }

    auto operator co_await() const noexcept {
        struct TaskAwaiter : public TaskAwaiterBase<promise_type> {

            TaskAwaiter(std::coroutine_handle<promise_type> coroutine):
                TaskAwaiterBase<promise_type>(coroutine) {}

            auto await_resume() {
                assert(TaskAwaiterBase<promise_type>::_coroutine != nullptr);
                auto& promise = TaskAwaiterBase<promise_type>::_coroutine.promise();
                if constexpr (std::is_void_v<DataType>) {
                    promise.result();
                } else {
                    return std::move(promise.result());
                }
            }
        };
        return TaskAwaiter(_coroutine);
    }
protected:
    std::coroutine_handle<promise_type> _coroutine = {};
};


inline Task<void> TaskPromise<void>::get_return_object() noexcept {
    return Task<void>{std::coroutine_handle<TaskPromise>::from_promise(*this)};
}

template<class DataType>
inline Task<DataType> TaskPromise<DataType>::get_return_object() noexcept {
    return Task<DataType>(std::coroutine_handle<TaskPromise>::from_promise(*this));
}










// template<>
// struct std::coroutine_traits<QFuture<Datagram>> {
//     struct promise_type : QPromise<Datagram> {
//         QFuture<Datagram> get_return_object() { return future(); }
//         std::suspend_never initial_suspend() noexcept { return {}; }
//         std::suspend_never final_suspend() noexcept { return {}; }
//         void return_value(Datagram&& value) { addResult(std::move(value)); }
//         void unhandled_exception() {
//             setException(std::current_exception());
//         }
//     };
//     struct awaiter: public QFuture<Datagram> {
//         bool await_ready() { return false; } // suspend always
//         void await_suspend(std::coroutine_handle<> handle) {
//             then([handle](QFuture<Datagram>) {
//                 handle.resume();
//             });
//         }
//         Datagram await_resume() { return takeResult(); }
//     };
// };


// auto operator co_await(QFuture<Datagram> future) {
//     using TypeTrait = std::coroutine_traits<QFuture<Datagram>>;
//     return TypeTrait::awaiter{std::move(future)};
// }


// namespace QCoro::detail {
// template<>
// struct awaiter_type<QFuture<Datagram>> {
//     using type = typename std::coroutine_traits<QFuture<Datagram>>::awaiter;
// };

// }






// class IAsyncHandlerManager {

// public:
//     virtual ~IAsyncHandlerManager() = default;

//     struct IPromise {
//         struct abort_exception : std::exception {};

//         virtual bool await_response(Datagram& resp) = 0;
//         virtual void abort() = 0;
//         virtual void ready(Datagram&& datagram) = 0;
//     };

//     virtual IPromise* create_promise() const = 0;

// protected:
//     IAsyncHandlerManager() = default;
// };

#endif // __TASK_H__