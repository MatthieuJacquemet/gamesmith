// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include "objectRequestBroker.h"
#include "runtimeManager.h"
#include "serviceInterface.h"
#include <CORBAService.h>
#include <objectRequestBroker.h>


static std::set<ServiceInterface*> active_services;


ServiceInterface::ServiceInterface(uint16_t id) : ServiceBase(id),
    m_blocking(false), m_isStale(false), m_isBound(false) {
    
    Q_ASSERT(active_services.insert(this).second);
}


Task<bool> ServiceInterface::bind(ServiceInterface* service) {
    if (Q_UNLIKELY(service->m_isBound || service->m_isStale)) {
        co_return false;
    }
    detail::Request<bool> request(
        service, CORBAService::_derived_id, CORBAService::create_id
    );
    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
    service->_id = broker->register_service(service);

    service->m_isBound = co_await request.send([service](Datagram& datagram) {
        generic_write_datagram(datagram, service->_id);
        generic_write_datagram(datagram, std::string(service->get_name()));
    });
    if (Q_UNLIKELY(!service->m_isBound)) {
        broker->release_service(std::exchange(service->_id, 0));
    }
    co_return service->m_isBound;
}

Task<bool> ServiceInterface::unbind(ServiceInterface* service) {
    // we unbind even if the service is not bound, as it may have initied
    // a bind request but have not received the response yet.
    if (Q_UNLIKELY(service->m_isStale || service->_id == 0)) {
        co_return false;
    }
    uint16_t serviceId  = service->get_id();
    service->m_isStale = true;

    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
    std::deque<uint64_t> pendings = service->m_pendingRequests;

    while (!service->m_pendingRequests.empty()) {
        auto& r = broker->get_future(service->m_pendingRequests.front());
        r.cancel();
    }
    ServiceInterface stub;
    bool isBound;
    if (Q_LIKELY((isBound = service->m_isBound))) {
        service->m_isBound = false;
        broker->register_service(&stub, serviceId);
    }
    detail::Request<bool> request(
        &stub, CORBAService::_derived_id, CORBAService::release_id
    );
    bool res = co_await request.send([serviceId](Datagram& datagram) {
        generic_write_datagram(datagram, serviceId);
    });
    for (uint64_t pending_request_id : pendings) {
        broker->release_pending_request(pending_request_id);
    }
    if (Q_LIKELY(isBound)) broker->release_service(serviceId);
    co_return res;
}

ServiceInterface::~ServiceInterface() {
    ServiceInterface::unbind(this);
    Q_ASSERT(active_services.erase(this) == 1);
}

bool ServiceInterface::handle_call( const rpc::ServiceCallHeader& header,
                            Datagram& datagram, DatagramIterator& scan) {
    if (Q_UNLIKELY(!m_isBound)) {
        return false;
    }
    return ServiceBase::handle_call(header, datagram, scan);    
}

Task<bool> ServiceInterface::acquire() {
    if (LIKELY(m_isBound)) {
        co_return true;
    } else {
        co_return co_await ServiceInterface::bind(this);
    }
}





namespace detail {

BlockingContext::BlockingContext(const ServiceRef& service, bool block):
    m_service(service) {
    service->m_blocking = block;
}

BlockingContext::~BlockingContext() {
    m_service->m_blocking = false;
}

ResultRequest::ResultRequest(ServiceInterface* svc, uint16_t cid, uint16_t hid):
    ServiceCallHeader{0, svc->get_id(), cid, hid, true},
    m_service(svc) {
    
    auto* broker = ObjectRequestBroker::get_global_ptr();
    svc->m_pendingRequests.push_back(id = broker->register_pending_request());
    // std::cout << "acuire request : " << id << " for service : " << svc->get_name() << std::endl;
}

ResultRequest::~ResultRequest() {
    ObjectRequestBroker* broker = ObjectRequestBroker::get_global_ptr();
    // is the service is stale, then this request has been cancelled.
    // We must keep the future in, as we may still receive the response later
    // on. If we don't, a new request might acquire our slot and handle
    // this request's response, which is not desirable. The future's slot
    // will be released only when we receive the unbinding acknowledgment.
    Q_ASSERT(active_services.count(m_service) == 1);

    if (Q_LIKELY(!m_service->isStale())) {
        // std::cout << "release request : " << id << " for service : " << m_service->get_name() << std::endl;
        broker->release_pending_request(id);
    }
    Q_ASSERT(m_service->m_pendingRequests.front() == id);
    m_service->m_pendingRequests.pop_back();
}

}