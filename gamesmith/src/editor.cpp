// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QFileDialog>
#include <QDebug>
#include <QStandardPaths>
#include <QCommandLineParser>
#include <QStyleFactory>
#include <QShortcut>
#include <QXmlStreamReader>
#include <QSvgRenderer>

#include <qnamespace.h>
#include <utils.h>
#include <config.h>
#include <pluginManager.h>
#include <environmentVariableProvider.h>
#include <svgIconengine.h>

#include "editorWidget.h"
#include "editor_utils.h"
#include "pluginManager.h"
#include "gamesmith_config.h"
#include "mainWindow.h"
#include "greetingDialog.h"
#include "runtimeManager.h"
#include "project.h"
#include "engineMetadata.h"
#include "editor.h"
#include "phantomstyle.h"


static inline QVector<ConstructCallback::Callback>& createCallbacks() {
    static QVector<ConstructCallback::Callback> callbacks;
    return callbacks;
}



Editor::Editor(int& argc, char** argv): QApplication(argc, argv),
    m_pluginManager(new PluginManager("org.gamesmith.Plugin", this)),
    m_project(new Project(this)),
    m_runtime(new RuntimeManager(this)),
    m_metadata(new EngineMetadata(this)) {

    createCallbacks().clear();
    reloadStyle();

    new QShortcut(QKeySequence(Qt::Key_F5), m_window,
                    this, &Editor::reloadStyle);

    connect(this, &QApplication::aboutToQuit, this, [this] {
        delete m_window;
        delete m_metadata;
        delete m_runtime;
    });

    m_window = MainWindow::createWindow();
}

Editor::~Editor() {

}


void Editor::setupCommand(const QCommandLineParser& parser) {
    Q_ASSERT(m_config == nullptr);

    m_config = new Config(parser.value("config"), Config::NativeFormat, this);

    m_config->registerProvider(
        new EnvironmentVariableProvider("env", this)
    );
#ifdef Q_OS_LINUX
	#define DEFAULT_PLUGIN_DIRS {"/usr/lib/" APP_NAME "/plugins"}
#else
    #define DEFAULT_PLUGIN_DIRS {relativePath("plugins")}
#endif
    auto pluginDirs = m_config->get<QStringList>("editor/plugins",
                                                DEFAULT_PLUGIN_DIRS);
    
    auto iconFile = m_config->get<QString>( "editor/icon-file",
                                            DEFAULT_ICON_FILE);
    
    m_svgRenderer = new QSvgRenderer(qMove(iconFile), this);

    initializeProject();
    
    m_metadata->initialize();
    m_window->initialize();
    m_runtime->initialize();

    for (const QDir& pluginsDir : pluginDirs) {
        m_pluginManager->readPlugins(pluginsDir);
    }
    m_pluginManager->initializePlugins();
}

QIcon Editor::icon(const QString& uri) const noexcept {
    if (uri.contains("/")) {
        return QIcon(uri);
    } else {
        QIcon& icon = m_icons[uri];
        if (Q_UNLIKELY(icon.isNull())) {
            icon = QIcon(new SvgIconEngine(m_svgRenderer, uri));
        }
        return icon;
    }
}

void Editor::reloadStyle() {

    QFont font;
    font.setFamilies({"Arial", "Liberation Sans"});
    setFont(font);

    const QWidgetList topLevels = topLevelWidgets();
    for (QWidget *widget : topLevels) {
        // this is needed with Qt >= 5.13.1 but is harmless otherwise
        widget->setAttribute(Qt::WA_NoSystemBackground, false);
        widget->setAttribute(Qt::WA_TranslucentBackground, false);
    }
    // setStyleSheet(STYLESHEET_PATH);
    setStyle(new PhantomStyle);
    setPalette(style()->standardPalette());
    // Qt5 has QEvent::ThemeChange
    const QWidgetList widgets = QApplication::allWidgets();
    for (QWidget *widget : widgets) {
        QEvent event(QEvent::ThemeChange);
        QApplication::sendEvent(widget, &event);
    }
}


// void Editor::initializeMonitor() {

    // quint64 interval = config<int>("editor/status_bar_update", 1000);
    // m_monitor = QSharedPointer<SystemMonitorImpl>::create(interval);

    // connect(&m_timer, &QTimer::timeout, [this] {
    //     m_monitor->update();
    //     Q_EMIT monitorUpdated(m_monitor);
    // });

    // m_timer.setInterval(interval);
	// m_timer.start();
// }


void Editor::initializeProject() {
    
    // GreetingDialog dialog;

    // if (dialog.exec()) {
    //     m_project->open(dialog.openedProject());
    // } else {
    //     exit();
    // }
}



QString Editor::relativePath(const QString& name) {

    QDir dir(applicationDirPath());
    return dir.absoluteFilePath(name);
}

void ConstructCallback::onCreated(const Callback& callbackFn) {

    auto& callbacks = createCallbacks();
    Editor::startingUp() ? callbacks.append(callbackFn) :
                            callbackFn(Editor::globalInstance());
}



ConstructCallback::ConstructCallback() {

    for (const Callback& callbackFn: createCallbacks()) {
        callbackFn(Editor::globalInstance());
    }
}
