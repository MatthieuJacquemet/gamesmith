/**
 * Copyright (C) 2022 Matthieu Jacquemet
 * 
 * This file is part of GameSmith.
 * 
 * GameSmith is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * GameSmith is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PROJECT_H__
#define __PROJECT_H__

#include <QMap>
#include <QDir>
#include <QSettings>

#include "projectData.h"


class Project: public QObject {

    Q_OBJECT

public:
    Project(QObject* parent=nullptr);
    virtual ~Project() = default;

    void saveCopy();
    void saveAs();
    void save();
    void open(const QDir& dir);

    void setModified();
    bool modified() const;
    QString name() const;
    QDir currentDir() const;

Q_SIGNALS:
    void projectOpened();
    void projectSaved();

protected:
    ProjectData m_data;
    QSettings m_settings;
    QDir m_currentDir;
    bool m_modified;
};

#endif // __PROJECT_H__

