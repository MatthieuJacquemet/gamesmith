// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QWidget>
#include <QDir>
#include <QStyle>
#include <QLineEdit>
#include <QCheckBox>

#include <property.h>
#include <color.h>

#include "colorPicker.h"
#include "spinBox.h"
#include "styleoptions.h"
#include "utils.h"
#include "editor_utils.h"





EnumInfo getEnums(const QMetaObject& meta_class, const QString& id) {
    
    int index = meta_class.indexOfEnumerator(id.toUtf8().data());
    EnumInfo result;

    if (LIKELY(index != -1)) {

        QMetaEnum meta_enum = meta_class.enumerator(index);
        int count = meta_enum.keyCount();
    
        for (int i=0; i<count; ++i)
            result.append({ meta_enum.value(i),
                            meta_enum.key(i)});
    }
    return result;
}


void copyRecursive(const QDir& src, const QDir& dst) {

    QFileInfo info(src.absolutePath());

    if (info.isFile()) {
        QFile::copy(src.absolutePath(), dst.absolutePath());
    } 
    else if (info.isDir()) {
        dst.mkpath(info.baseName());
        QStringList items = src.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
        for (const QString& d: items) {
            copyRecursive(src.absoluteFilePath(d), dst.absoluteFilePath(d));
        }
    }
}


QString toolTipText(const QString& text) {
    
    if (Q_UNLIKELY(text.isEmpty())) {
        return QString();
    }
    int w = QApplication::style()->pixelMetric(Gs::pm(Gs::PM_ToolTipMinWidth));
    QFontMetrics fontMetric(QApplication::font());

    int tipWidth = fontMetric.horizontalAdvance(text);
    QString newText = text.toHtmlEscaped();

    int index = tipWidth <= w ? text.size() : w / tipWidth * text.size();
    newText.insert(index, "</p>");

    return "<style>p { margin: 0 0 0 0 }</style><p style='white-space:pre'>" + newText;
}

QRect clampedTo(QRect rect, const QRect& bounds) {

    if (rect.left() < bounds.left()) rect.moveLeft(bounds.left());
    if (rect.right() > bounds.right()) rect.moveRight(bounds.right());
    if (rect.top() < bounds.top()) rect.moveTop(bounds.top());
    if (rect.bottom() > bounds.bottom()) rect.moveBottom(bounds.bottom());

    return rect;
}


QColor lColorToQColor(const LColor& color) {
    QColor qcolor(QColor::ExtendedRgb);
    qcolor.setRedF  (color[0]);
    qcolor.setGreenF(color[1]);
    qcolor.setBlueF (color[2]);
    qcolor.setAlphaF(color[3]);
    return qcolor;
}

LColor qColorToLColor(const QColor& color) {
    return {float(color.redF()),  float(color.greenF()),
            float(color.blueF()), float(color.alphaF())};
}




QHash<TypeHandle, PropFactory>& propFactories() {
    static QHash<TypeHandle, PropFactory> factories;
    return factories;
}


void registerPropertyWidgetFactory(TypeHandle type, PropFactory&& factory) {
    propFactories()[type] = std::move(factory);
}

PropertyWidget createWidgetForProperty( const PropertyInfo& property,
                                        const PropListener& listener) {
    auto& factories = propFactories();
    if (auto factory = factories.value(property.value.get_type())) {
        return factory(property, listener);
    }
    return PropertyWidget{nullptr, nullptr};
}


template<class Type>
Type getAttribute(  const PropertyBase::Attributes& attributes,
                    int id, const Type& default_value = Type()) {
    
    auto it = attributes.find(PropertyBase::Attribute(id));
    if (it == attributes.end()) {
        return default_value;
    } else {
        return it->second.get_value(default_value);
    }
}

template<class T, class D, size_t... Is> requires(is_vector_any<T>::value)
inline auto toVectorHelper(const std::array<D, sizeof...(Is)>& data,
                           std::index_sequence<Is...>) {
    return T(data[Is]...);
}

template<class Type, class D, size_t N> requires(is_vector_any<Type>::value)
inline auto spinBoxGroupToVector(const std::array<D, N>& data) {
    static_assert(Type::num_components == N);
    return toVectorHelper<Type>(data, std::make_index_sequence<N>{});
}



template<class Type> requires(std::is_arithmetic_v<Type>)
void registerNumericWidget() {
    auto registerFactory = registerPropertyWidgetFactory;
    
    using SpinBox = typename SpinBoxType<Type>::type;
    using comp_t = typename SpinBox::DataType;
    using Getr = Variant (*)(QWidget*);

    TypeHandle variantType = get_type_handle(Type);

    registerFactory(variantType, PropFactory([](const PropertyInfo& info,
                                                const PropListener& list) {

        SpinBox* spinbox = new SpinBox;

        constexpr auto min = std::numeric_limits<Type>::lowest();
        constexpr auto max = std::numeric_limits<Type>::max();

        spinbox->setValue(info.value.get_value<Type>());
        spinbox->setRange(
            getAttribute(info.attributes, Range<Type>::A_min_value, min),
            getAttribute(info.attributes, Range<Type>::A_max_value, max)
        );
        auto signal = QOverload<comp_t>::of(&SpinBox::valueChanged);
        QObject::connect(spinbox, signal, [list](Type value) {
            list(value);
        });
        return PropertyWidget(spinbox, Getr([](QWidget* widget) -> Variant {
            if (auto* spinbox = qobject_cast<SpinBox*>(widget)) {
                return Type(spinbox->value());
            }
            return Variant();
        }));
    }));
}

template<class Type> requires(is_vector_any<Type>::value)
void registerVectorWidget() {
    auto registerFactory = registerPropertyWidgetFactory;

    using comp_t = std::conditional_t<Type::is_int, int, double>;
    using SpinBox = SpinBoxVector<Type::num_components, comp_t>;
    using Getr = Variant (*)(QWidget*);

    TypeHandle variantType = get_type_handle(Type);

    registerFactory(variantType, PropFactory([](const PropertyInfo& info,
                                                const PropListener& list) {

        SpinBox* spinbox = new SpinBox(QBoxLayout::LeftToRight);

        QObject::connect(spinbox, &SpinBox::valueChanged, [=] {
            list(spinBoxGroupToVector<Type>(spinbox->values()));
        });
        return PropertyWidget(spinbox, Getr([](QWidget* widget) -> Variant {
            SpinBoxVectorBase* base;
            if ((base = qobject_cast<SpinBoxVectorBase*>(widget)) &&
                 base->getSpinBoxCount() == Type::num_components) {
                
                auto* spinbox = static_cast<SpinBox*>(base);
                return spinBoxGroupToVector<Type>(spinbox->values());
            }
            return Variant();
        }));
    }));
}

void registerLegacyPropertieWidgetsFactories() {

    auto registerFactory = registerPropertyWidgetFactory;
    using Getr = Variant (*)(QWidget*);

    TypeHandle variantType;

    variantType = get_type_handle(std::string);
    registerFactory(variantType, PropFactory([](const PropertyInfo& info,
                                                const PropListener& list) {
        
        QLineEdit* lineEdit = new QLineEdit(QString::fromStdString(
            info.value.get_value<std::string>()
        ));
        QObject::connect(lineEdit, &QLineEdit::textEdited, [list](auto& text) {
            list(text.toStdString());
        });
        return PropertyWidget(lineEdit, Getr([](QWidget* widget) -> Variant {
            if (auto* lineEdit = qobject_cast<QLineEdit*>(widget)) {
                return lineEdit->text().toStdString();
            }
            return Variant();
        }));
    }));

    variantType = get_type_handle(bool);
    registerFactory(variantType, PropFactory([](const PropertyInfo& info,
                                                const PropListener& list) {
        QCheckBox* checkbox = new QCheckBox;
        checkbox->setChecked(info.value.get_value<bool>(false));
        
        QObject::connect(checkbox, &QCheckBox::stateChanged, [list](auto state) {
            list(state == Qt::Checked);
        });
        return PropertyWidget(checkbox, Getr([](QWidget* widget) -> Variant {
            if (auto* checkbox = qobject_cast<QCheckBox*>(widget)) {
                return checkbox->checkState() != Qt::Unchecked;
            }
            return Variant();
        }));
    }));

    variantType = get_type_handle(Color);
    registerFactory(variantType, PropFactory([](const PropertyInfo& info,
                                                const PropListener& list) {
        auto* picker = new ColorPicker;
        picker->setColor(lColorToQColor(info.value.get_value<Color>()));
        picker->setDisplayAlpha(
            getAttribute(info.attributes, PropertyBase::A_no_alpha, false)
        );
        QObject::connect(picker, &ColorPicker::colorChanged, [list](auto color) {
            list(Color(qColorToLColor(color)));
        });
        return PropertyWidget(picker, Getr([](QWidget* widget) -> Variant {
            if (auto* picker = qobject_cast<ColorPicker*>(widget)) {
                return Color(qColorToLColor(picker->color()));
            }
            return Variant();
        }));
    }));

    registerNumericWidget<int8_t>();
    registerNumericWidget<int16_t>();
    registerNumericWidget<int32_t>();
    registerNumericWidget<uint8_t>();
    registerNumericWidget<uint16_t>();
    registerNumericWidget<uint32_t>();
    registerNumericWidget<float>();
    registerNumericWidget<double>();

    registerVectorWidget<LVecBase2f>();
    registerVectorWidget<LVecBase3f>();
    registerVectorWidget<LVecBase4f>();

    registerVectorWidget<LVecBase2i>();
    registerVectorWidget<LVecBase3i>();
    registerVectorWidget<LVecBase4i>();
}


void clearPropertyWidgetsFactories() {
    propFactories().clear();
}

QSize smallIconSize(const QIcon& currentIcon, QWidget* widget) {

    const QList<QSize> iconSizes = currentIcon.availableSizes();
    QSize iconSize;
    if (Q_LIKELY(!iconSizes.isEmpty())) {
        iconSize = iconSizes.constFirst();
    } else {
        int size = widget->style()->pixelMetric(QStyle::PM_SmallIconSize);
        iconSize = QSize(size, size);
    }
    const qreal logicalFactor = widget->logicalDpiX() / 96.0;

#if defined(Q_OS_LINUX)
    const qreal dpr = widget->devicePixelRatioF();
    const qreal combinedFactor = logicalFactor * dpr;
    
    if (scalingFactorIsSupported(combinedFactor)) {
        iconSize *= combinedFactor;
    }
#elif defined(Q_OS_WIN) && QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    // Probably Windows could use the same code path as Linux, but I'm seeing too thick icons on Windows...
    if (!QGuiApplication::testAttribute(Qt::AA_EnableHighDpiScaling)
        && scalingFactorIsSupported(logicalFactor)) {
        iconSize = opt.iconSize * logicalFactor;
    }
#else
    Q_UNUSED(logicalFactor);
#endif
    return iconSize;
}