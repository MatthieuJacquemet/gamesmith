// Copyright (C) 2022 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QFile>
#include <QStandardPaths>
#include <QFileDialog>
#include <QDebug>

#include "projectData.h"
#include "editor_defines.h"
#include "project.h"




Project::Project(QObject* parent): QObject(parent),
    m_modified(false) {
    
}


bool Project::modified() const {
    return m_modified;
}

QString Project::name() const {
    return "";
}

QDir Project::currentDir() const {
    return m_currentDir;
}

void Project::setModified() {
    m_modified = true;
}


void Project::open(const QDir& dir) {

    // QString path = QStandardPaths::writableLocation(
    //     QStandardPaths::DocumentsLocation
    // );

    // QString filename = QFileDialog::getOpenFileName(Q_NULLPTR,
    //     tr("Open Project"), path,
    //     tr("Alluvion Project Files (*.alv)")
    // );

    // m_data.loadData(filename)
    // m_modified = false;
    // Q_EMIT projectOpened();

}


void Project::save() {
    // QFileInfo::exists(m_currentDir) ? saveData(m_filename) : saveAs();
}


void Project::saveAs() {
    
    // QDir path = QStandardPaths::writableLocation(
    //     QStandardPaths::DocumentsLocation
    // );
    // QString filename = QFileDialog::getSaveFileName(Q_NULLPTR,
    //     tr("Save project as"),
    //     path.absoluteFilePath(m_name),
    //     tr("GameSmith Project Files (*" PROJECT_EXT ")")
    // );
    // if (filename.isEmpty()) {
    //     return;
    // } else if (!filename.endsWith(PROJECT_EXT)) {
    //     filename += PROJECT_EXT;
    // }
    // saveData(filename);
    // m_filename = filename;
}


void Project::saveCopy(){

    // if (QFileInfo::exists(m_filename)) {

    //     QFileInfo filename(m_filename);
    //     QDir dir = filename.dir();

    //     QString abspath = dir.absoluteFilePath(filename.baseName());
    //     QString fmt = abspath + ".%1" PROJECT_EXT;
    //     QString new_path;

    //     for (int i=0; QFileInfo::exists(new_path); ++i) {
    //         new_path = fmt.arg(i++, 3, 10, QChar('0'));
    //     }
    //     saveData(new_path);
    // }  else {
    //     saveAs();
    // }
}