// Copyright 2023 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "panda3d/interrogateDatabase.h"
#include "panda3d/interrogate_request.h"

#include <private/qobject_p.h>
#include <QXmlNamePool>
#include <QXmlQuery>
#include <QXmlResultItems>
#include <QComboBox>
#include <QDebug>
#include <QFile>


#include "editor_utils.h"
#include "domNodeModel.h"
#include "engineMetadata.h"


class EngineMetadata::EngineMetadataPrivate : public QObjectPrivate {

public:
    EngineMetadataPrivate() = default;
    ~EngineMetadataPrivate() = default;


    QHash<QString, QStringList> enums;
    QDomDocument document;
};




EngineMetadata::EngineMetadata(QObject* parent) :
    QObject(*new EngineMetadataPrivate, parent) {
// QStringList& enums = m_enums[element.attribute(QStringLiteral("name"))];

//         QDomNodeList nodes = element.childNodes();

//         for (int i=0; i<nodes.size(); i++) {
//             QDomNode node = nodes.at(i);
//             if (node.isElement()) {
//                 enums.append(node.toElement().attribute(QStringLiteral("label")));
//             }
//         }
}

inline QString toolTip(const std::string& description) {
    return toolTipText(QString::fromStdString(description).remove("//"));
}

inline QString normalizeEnum(const QString& label, const QString& type) {
    QString prefix, result = label;
    for (const QChar& c : label) {
        if (c.isUpper()) prefix += c;
    }
    if (label.startsWith(prefix + "_")) {
        result.remove(0, prefix.length() + 1);
    }
    return result.replace("_", " ");
}

void EngineMetadata::initialize() {
    Q_D(EngineMetadata);

    QString modulesPath = config<QString>("core/engine-metadata-path",
                                        DEFAULT_ENGINE_METADATA_PATH);

    const char* defaultModules[] = { "p3pgraph.in", "p3mathutil.in" };
    for (const char* module : defaultModules) {
        QString modulePath = modulesPath + "/" + module;
        interrogate_request_database(modulePath.toStdString().c_str());
    }

    QFile file(QStringLiteral(":/data/engineMetadata.xml"));

    if (Q_UNLIKELY(!file.open(QIODevice::ReadOnly))) {
        return;
    }
    d->document.setContent(&file);
    for (auto node : findNodes("/metadata/enum[@name]/value[@label]")) {
        if (Q_UNLIKELY(!node.isElement())) {
            continue;
        }
        QDomElement element = node.toElement();
        QDomElement parent = node.parentNode().toElement();
        
        QString name = parent.attribute(QStringLiteral("name"));
        QString label = element.attribute(QStringLiteral("label"));

        d->enums[std::move(name)].append(std::move(label));
    }
}


void EngineMetadata::getClassSpec(const QString& name) const {

    InterrogateDatabase* database = InterrogateDatabase::get_ptr();
    auto index = database->lookup_type_by_name(name.toStdString());
    if (index != 0) {
        InterrogateType type = database->get_type(index);
        // spec.label = name;
        // spec.description = QString::fromStdString(type.get_comment());
    }
}

void EngineMetadata::
initComboBoxFromEnum(QComboBox* combo, const QString& name, bool norm) const {
 
    InterrogateDatabase* database = InterrogateDatabase::get_ptr();
    auto index = database->lookup_type_by_name(name.toStdString());
    if (Q_UNLIKELY(index == 0)) {
        return;
    }
    InterrogateType type = database->get_type(index);
    if (type.is_enum()) {
        combo->setToolTip(toolTip(type.get_comment()));

        int numValues = type.number_of_enum_values();
        for (int i = 0; i < numValues; ++i) {
            auto label = QString::fromStdString(type.get_enum_value_name(i));
            if (norm) {
                label = normalizeEnum(label, name);
            }
            combo->addItem(label, qlonglong(type.get_enum_value(i)));
            combo->setItemData(i, toolTip(type.get_enum_value_comment(i)),
                                Qt::ToolTipRole);
        }
    }
}

QStringList EngineMetadata::getEnum(const QString& name) const {
    Q_D(const EngineMetadata);
    return d->enums.value(name);
}


QList<QDomNode> EngineMetadata::findNodes(const QString& xpath) const {
    Q_D(const EngineMetadata);

    QList<QDomNode> foundNodes;
    QXmlNamePool pool;

    DomNodeModel model(pool, d->document);
    QXmlQuery query(pool);

    auto fromIndex = model.fromDomNode(d->document.documentElement());
    query.setFocus(QXmlItem(fromIndex));

    query.setQuery(xpath);
    if (Q_UNLIKELY(!query.isValid())) {
        qWarning() << "Xml query" << xpath << "is not valid";
        return foundNodes;
    }
    QXmlResultItems result;
    query.evaluateTo(&result);

    if (Q_UNLIKELY(result.hasError())) {
        qDebug() << "Query evaluation failed for" << xpath;
        return foundNodes;
    }
    while (!result.next().isNull()) {
        QXmlItem current = result.current();
        foundNodes.append(model.toDomNode(current.toNodeModelIndex()));
    }
    return foundNodes;
}

