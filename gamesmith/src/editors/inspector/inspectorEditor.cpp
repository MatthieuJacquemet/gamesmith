// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QSpacerItem>
#include <QBoxLayout>
#include <qboxlayout.h>
#include <qscrollarea.h>
#include "inspectorPanel.h"
#include "inspectorEditor.h"
#include "spoiler.h"
#include "runtimeManager.h"


QMap<QString, InspectorEditor::Factory>& factories() {
    static QMap<QString, InspectorEditor::Factory> factories;
    return factories;
}


void InspectorEditor::InspectorAdaptor::node_changed(int32_t type_id) {
    std::function<void(int32_t)> visitNodeClass;
    std::unordered_set<int32_t> visited;
    RuntimeManager* runtime = Editor::globalInstance()->runtime();

    QScrollArea* scrollArea = m_editor->m_scrollArea;

    qDeleteAll(scrollArea->widget()->children());
    QVBoxLayout* layout = new QVBoxLayout(scrollArea->widget());

    visitNodeClass = [&, runtime, this](int32_t id) -> void {
        QString typeName = runtime->typeName(id);
        if (LIKELY(!visited.insert(id).second)) {
            return; // diamond inheritance
        }
        else if (LIKELY(typeName != "PandaNode")) {
            for (int32_t parent : runtime->parents(type_id)) {
                visitNodeClass(parent); // first add topmost parents
            }
        }
        auto factory = factories().value(typeName, [](auto* editor) {
            return new InspectorPanelBase(editor);
        });
        Spoiler* spoiler = new Spoiler(typeName);
        layout->addWidget(spoiler);
        spoiler->setContentWidget(factory(m_editor));
    };

    if (Q_LIKELY(type_id != 0)) visitNodeClass(type_id), layout->addStretch();
}



InspectorEditor::InspectorEditor(const QString& label): EditorWidget(label),
    m_service(this),
    m_scrollArea(new QScrollArea) {
    
    QBoxLayout* layout = new QBoxLayout(QBoxLayout::TopToBottom, this);
    layout->setMargin(0);
    layout->addWidget(m_scrollArea);

    m_scrollArea->setWidgetResizable(true);
    m_scrollArea->setWidget(new QWidget);
    m_scrollArea->setBackgroundRole(QPalette::ColorRole::Base);
}

InspectorEditor::~InspectorEditor() {
    
}


void InspectorEditor::registerPanel(const char* nodeTypeName,
                                    Factory&& factory) {
    
    factories().insert(nodeTypeName, std::move(factory));
}