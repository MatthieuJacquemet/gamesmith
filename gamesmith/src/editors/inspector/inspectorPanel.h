// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __INSPECTORPANEL_H__
#define __INSPECTORPANEL_H__

#include <QWidget>
#include <concepts>
#include <memory>
#include <qobjectdefs.h>
#include <qpushbutton.h>
#include <qscrollarea.h>
#include "inspectorEditor.h"
#include "inspectorService.h"
#include "pandaNodeService.gs_rpc_stub.h"


class InspectorEditor;


class InspectorPanelBase : public QWidget {
    Q_OBJECT
public:
    InspectorPanelBase(InspectorEditor* editorWidget);

    virtual ~InspectorPanelBase() = default;
    virtual Task<> initialize() { return Task<>{}; }

protected:
    InspectorEditor* const m_editor;
};


#endif // __INSPECTORPANEL_H__