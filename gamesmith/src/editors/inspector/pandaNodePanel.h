// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __PANDANODEPANEL_H__
#define __PANDANODEPANEL_H__

#include "inspectorPanel.h"
#include "spinBox.h"
#include "pandaNodeService.gs_rpc_stub.h"

class QLineEditr;
class QComboBox;
class TagList;


class PandaNodePanel : public InspectorPanelBase {

    Q_OBJECT
public:
    PandaNodePanel(InspectorEditor* editorWidget);
    virtual ~PandaNodePanel();

    Task<> initialize() override;

protected:
    void setTransform(const LVector3& pos, const LVector3& rot,
                    const LVector3& scale, const LVector3& shear);
    
    void addRenderEffect(const RenderEffectInfo& info);

    class DrawMaskButton;
    class RenderEffectWidget;
    class AddRenderEffectButton;

    class PandaNodeServiceAdaptor: public stub::PandaNodeService {
        PandaNodePanel* m_panel;
    public:
        PandaNodeServiceAdaptor(PandaNodePanel* panel):
            m_panel(panel) {}
        void transforms_changed(const LVector3& position,
                                const LVector3& rotation,
                                const LVector3& scale,
                                const LVector3& shear) override {
            
            m_panel->setTransform(position, rotation, scale, shear);
        }
    };
    PandaNodeServiceAdaptor m_service;
    std::tuple<const char*, SpinBoxVector3d*> m_components[4];
    QLineEdit* m_nameLineEdit;
    DrawMaskButton* m_drawMaskButton;
    QComboBox* m_boundsCombo;
    TagList* m_tagList;
    QVBoxLayout* m_renderEffects;
    bool m_inhibitTransform = false;
};

#endif // __PANDANODEPANEL_H__