// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QHeaderView>
#include <QTreeWidget>
#include <QPainter>
#include <QPainterPath>
#include <QPaintEvent>

#include "editor_utils.h"
#include "editor.h"
#include "tagList.h"

class TagList::ListView : public QTreeWidget {

public:
    ListView(QWidget* parent = nullptr): QTreeWidget(parent) {
        setAlternatingRowColors(false);
        setColumnCount(2);
        setUniformRowHeights(true);
        setFixedHeight(100);
        headerItem()->setText(0, tr("Key"));
        headerItem()->setText(1, tr("Value"));
        viewport()->setAutoFillBackground(false);
        header()->viewport()->setAutoFillBackground(false);
        header()->setProperty("rounded", true);
        header()->setSectionResizeMode(QHeaderView::Stretch);
    }
    void paintEvent(QPaintEvent* event) override {
        QPainter painter(viewport());
        QPainterPath path;
        QRect rect = viewport()->rect();
        path.addRoundedRect(rect.adjusted(0, -4, 0, 0), 4, 4);
        
        painter.setRenderHint(QPainter::Antialiasing, true);
        painter.setClipRect(rect);
        painter.fillPath(path, palette().color(QPalette::Dark));

        QRegion region(path.toFillPolygon().toPolygon());

        viewport()->setMask(region);
        QTreeWidget::paintEvent(event);
    }
};

TagList::TagList(QWidget *parent): QWidget(parent),
    m_listView(new TagList::ListView) {
    
    // setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    Editor* editor = Editor::globalInstance();

    QHBoxLayout* layout = new QHBoxLayout(this);
    QVBoxLayout* buttonsLayout = new QVBoxLayout;
    buttonsLayout->setSpacing(1);

    QPushButton* addButton = new QPushButton(editor->icon("add"), "");
    QPushButton* delButton = new QPushButton(editor->icon("del"), "");
    
    buttonsLayout->addWidget(addButton);
    buttonsLayout->addWidget(delButton);
    buttonsLayout->addStretch();

    layout->setMargin(0);
    layout->addWidget(m_listView);
    layout->addLayout(buttonsLayout);
    
    addButton->setIconSize(smallIconSize(addButton->icon(), addButton));
    delButton->setIconSize(smallIconSize(addButton->icon(), addButton));

    const auto findUniqueKey = [](QTreeWidget* listView) -> QString {
        QString key = "key";
        QSet<QString> keys;
        for (int i = 0; i < listView->topLevelItemCount(); i++) {
            keys.insert(listView->topLevelItem(i)->text(0));
        }
        int index = 0;
        while (keys.contains(key)) {
            key = QString("key.%1").arg(index++, 3, 10, QChar('0'));
        }
        return key;
    };

    const auto emitModified = [this] {
        if (m_inhibitModified) return;

        QList<QPair<QString, QString>> items;
        for (int i = 0; i < m_listView->topLevelItemCount(); i++) {
            items.append({  m_listView->topLevelItem(i)->text(0),
                            m_listView->topLevelItem(i)->text(1)});
        }
        emit modified(items);
    };

    connect(addButton, &QPushButton::clicked, this, [=, this] {
        addTag(findUniqueKey(m_listView), "");
        emitModified();
    });
    connect(delButton, &QPushButton::clicked, this, [=, this] {
        qDeleteAll(m_listView->selectedItems());
        emitModified();
    });
    connect(m_listView, &QTreeWidget::itemChanged, this, [=](auto* item, int) {
        emitModified(); 
    });
}

void TagList::addTag(const QString& key, const QString& value) {
    m_inhibitModified = true;
    QTreeWidgetItem* item = new QTreeWidgetItem(m_listView);
    item->setText(0, key);
    item->setText(1, value);
    item->setFlags(item->flags() | Qt::ItemIsEditable);
    m_inhibitModified = false;
}