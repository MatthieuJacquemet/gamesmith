// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QPushButton>
#include <QLayout>
#include <QWidgetAction>
#include <QButtonGroup>
#include <QStackedWidget>
#include <QMenu>
#include <QCheckBox>
#include <QRadioButton>
#include <QFontDatabase>
#include <QLabel>
#include <QLineEdit>
#include <QTimer>
#include <QComboBox>
#include <QPainter>
#include <QPainterPath>
#include <QResizeEvent>


#include "spoiler.h"
#include "editor_utils.h"
#include "tagList.h"
#include "engineMetadata.h"
#include "pandaNodeService.gs_rpc_stub.h"
#include "spinBox.h"
#include "inspectorPanel.h"
#include "sceneGraphModel.h"
#include "pandaNodePanel.h"



class PandaNodePanel::RenderEffectWidget : public QWidget {
    
    struct InitializerData {
        std::string name;
        std::vector<PropertyWidget> properties;
    };
    PandaNodePanel* m_panel;
    std::vector<InitializerData> m_initializers;
    int32_t m_typeId;
public:
    RenderEffectWidget(PandaNodePanel* panel, const RenderEffectInfo& info);

private:
    Task<> updateRenderEffect(int initializerIndex) {
        Q_ASSERT(initializerIndex < m_initializers.size());

        auto& initializeData = m_initializers[initializerIndex];
        std::vector<Variant> arguments;
        arguments.reserve(initializeData.properties.size());

        for (const auto [widget, getter] : initializeData.properties) {
            bool ok = widget && getter;
            arguments.emplace_back(ok ? getter(widget) : Variant());
        }
        co_await m_panel->m_service.update_effect(
            m_typeId, initializeData.name, arguments,
            RenderEffectInfo::A_init
        );
    }
};


PandaNodePanel::
RenderEffectWidget::RenderEffectWidget( PandaNodePanel* panel,
                                        const RenderEffectInfo& info):
    m_panel(panel), m_typeId(info.typeId) {

    QGridLayout* layout = new QGridLayout(this);
    layout->setColumnStretch(0, 2);
    layout->setColumnStretch(1, 3);
    
    // setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);

    std::map<std::string, int> properties_map;
    for (int i = 0; i < info.properties.size(); ++i) {
        properties_map[info.properties[i].name] = i;
    }
    
    QStackedWidget* stack           = nullptr;
    QButtonGroup* group             = nullptr;
    QHBoxLayout* selectorLayout     = nullptr;
    int row = -1;

    if (info.initializers.size() > 1) {
        group = new QButtonGroup, stack = new QStackedWidget;
        QHBoxLayout* selectorLayout = new QHBoxLayout;
        selectorLayout->setMargin(1);

        stack->setCurrentIndex(0);
        layout->addLayout(selectorLayout, ++row, 0, 1, 2, Qt::AlignCenter);

        connect(group, &QButtonGroup::idClicked,
                stack, &QStackedWidget::setCurrentIndex);
    }
    
    for (const auto& initializer : info.initializers) {

        QWidget* container = new QWidget;
        QGridLayout* initializerLayout = new QGridLayout(container);
        initializerLayout->setColumnStretch(0, 2);
        initializerLayout->setColumnStretch(1, 3);
        
        if (stack) stack->addWidget(container);
        else layout->addWidget(container, ++row, 0, 1, 2);

        QRadioButton* button = new QRadioButton(
            QString::fromStdString(initializer.label)
        );
        if (selectorLayout) selectorLayout->addWidget(button);
        if (group) group->addButton(button);
        
        int index = m_initializers.size();
        auto& [name, props] = m_initializers.emplace_back();
        name = initializer.label;
        props.reserve(initializer.parameters.size());

        int propIndex = -1;
        for (const auto& propName : initializer.parameters) {

            auto it = properties_map.find(propName);
            if (Q_UNLIKELY(it == properties_map.end())) {
                continue;
            }
            auto& prop = info.properties[it->second];
            initializerLayout->addWidget(
                new QLabel(QString::fromStdString(prop.name)),
                ++propIndex, 0, 1, 1, Qt::AlignLeft
            );
            const auto& [widget, _] = props.emplace_back(createWidgetForProperty(
                prop, [index, this](auto) { updateRenderEffect(index); }
            ));
            if (Q_LIKELY(widget != nullptr)) {
                initializerLayout->addWidget(widget, propIndex, 1, 1, 1);
            }
        }
    }
}


class PandaNodePanel::AddRenderEffectButton : public QPushButton {

public:
    AddRenderEffectButton(PandaNodePanel* panel):
        QPushButton(tr("add render effect")) {
        
        QMenu* menu = new QMenu(tr("render effects"));
        menu->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Minimum);
        menu->setMaximumWidth(0);
        setMenu(menu);
        
        RuntimeManager* runtime = Editor::globalInstance()->runtime();
        auto subTypes = runtime->subTypes(QStringLiteral("RenderEffect"));
        
        const auto signal = &QAction::triggered;
        for (auto& [id, name] : subTypes) {
            QAction* action = new QAction(name);
            action->setIcon(Editor::globalInstance()->icon(name));
            connect(action, signal, this, [panel, typeId = id]() -> Task<> {
                if (Q_LIKELY(co_await panel->m_service.acquire())) {
                    panel->addRenderEffect(
                        co_await panel->m_service.add_effect(typeId)
                    );
                }
            });
            menu->addAction(action);
        }
        setIcon(Editor::globalInstance()->icon(QStringLiteral("add")));
        setIconSize(smallIconSize(icon(), this));
    }
protected:
    void resizeEvent(QResizeEvent* event) override {
        QPushButton::resizeEvent(event);

        QMenu* menu = this->menu();
        QSize oldSize = menu->size();
        menu->setMinimumWidth(width());

        QResizeEvent resizeEvent(menu->size(), oldSize);
        QApplication::sendEvent(menu, &resizeEvent);
    }
};





class PandaNodePanel::DrawMaskButton : public QPushButton {

    enum { num_layers = 32 };

    class LayerAction : public QWidgetAction {
    public:
        LayerAction(const QString& name);
        inline QCheckBox* checkbox() const {
            QLayout* layout = defaultWidget()->layout();
            return qobject_cast<QCheckBox*>(layout->itemAt(2)->widget());
        }
    };
    PandaNodePanel* m_panel;
    bool m_blockEvents = false;
public:
    DrawMaskButton(PandaNodePanel* panel): QPushButton(tr("draw mask"), panel),
        m_panel(panel) {
        setMenu(new QMenu(tr("layers")));
        menu()->setMinimumWidth(150);

        const auto handler = [this](int state) -> Task<> {
            if (!m_blockEvents) {
                if (Q_LIKELY(co_await m_panel->m_service.acquire())) {
                    auto [show, control] = computeDrawMasks();
                    co_await m_panel->m_service.set_draw_masks(show, control);
                }
            }
        };
        for (int i=0; i<num_layers; ++i) {
            auto* action = new LayerAction("Layer " + QString::number(i));
            menu()->addAction(action);
            connect(action->checkbox(), &QCheckBox::stateChanged, this, handler);
        }
    }
    QPair<DrawMask, DrawMask> computeDrawMasks() const {
        QList<QAction*> actions = menu()->actions();
        QPair<DrawMask, DrawMask> masks;
        for (int i=0; i<num_layers; ++i) {
            auto* action = static_cast<LayerAction*>(actions[i]);
            uint8_t bits = uint8_t(action->checkbox()->checkState() + 2);
            masks.first.set_bit_to(i, bits & 0x1);
            masks.second.set_bit_to(i, bits & 0x2);
        }
        return masks;
    }
    void setDrawMasks(DrawMask draw, DrawMask control) {
        m_blockEvents = true;
        QList<QAction*> actions = menu()->actions();
        for (int i=0; i<num_layers; ++i) {
            auto* action = static_cast<LayerAction*>(actions[i]);
            uint8_t bits = draw.get_bit(i) | (control.get_bit(i) << 1);
            action->checkbox()->setCheckState(Qt::CheckState((bits - 2) & 0x3));
        }
        m_blockEvents = false;
    }
};

PandaNodePanel::DrawMaskButton::
LayerAction::LayerAction(const QString& name): QWidgetAction(0) {

    QLabel* label       = new QLabel(name);
    QHBoxLayout* layout = new QHBoxLayout;
    QWidget* widget     = new QWidget;
    QCheckBox* checkbox = new QCheckBox;
    checkbox->setTristate(true);
    checkbox->setCheckState(Qt::CheckState::Checked);
    label->setAlignment(Qt::AlignLeft);
    layout->setMargin(2);
    layout->addWidget(label);
    layout->addStretch();
    layout->addWidget(checkbox);
    widget->setLayout(layout);

    setDefaultWidget(widget);
    connect(checkbox, &QCheckBox::stateChanged, this, &QWidgetAction::changed);
}



PandaNodePanel::PandaNodePanel(InspectorEditor* editorWidget):
    InspectorPanelBase(editorWidget),
    m_service(this),
    m_nameLineEdit(new QLineEdit(tr("name"), this)),
    m_drawMaskButton(new DrawMaskButton(this)),
    m_boundsCombo(new QComboBox(this)),
    m_tagList(new TagList(this)),
    m_renderEffects(new QVBoxLayout),
    m_components{
        {"translation", new SpinBoxVector3d(QBoxLayout::LeftToRight)},
        {"rotation",    new SpinBoxVector3d(QBoxLayout::LeftToRight)},
        {"scale",       new SpinBoxVector3d(QBoxLayout::LeftToRight)},
        {"shear",       new SpinBoxVector3d(QBoxLayout::LeftToRight)},
    } {

    // setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum);
    QGridLayout* layout = qobject_cast<QGridLayout*>(this->layout());

    connect(m_nameLineEdit, &QLineEdit::editingFinished, [this]() -> Task<> {
        if (Q_LIKELY(co_await m_service.acquire())) {
            co_await m_service.set_name(m_nameLineEdit->text().toStdString());
        }
    });

    EngineMetadata* metadata = Editor::globalInstance()->metadata();
    metadata->initComboBoxFromEnum(m_boundsCombo, "BoundsType");

    int row = -1;

    layout->setColumnStretch(0, 2);
    layout->setColumnStretch(1, 3);
    layout->addWidget(m_nameLineEdit, ++row, 0, 1, 2, Qt::AlignTop);
    
    layout->addWidget(new QLabel(tr("bounds")), ++row, 0, 1, 1, Qt::AlignLeft);
    layout->addWidget(m_boundsCombo, row, 1, 1, 1);
    layout->addWidget(m_drawMaskButton, ++row, 1, 1, 1);

    const auto createSectionLabel = [](const QString& text) {
        QLabel* label = new QLabel(text);
        label->setContentsMargins(0, 10, 0, 0);
        QFont font = label->font();
        font.setBold(true);
        label->setFont(font);
        return label;
    };

    layout->addWidget(  createSectionLabel(tr("Transform:")), ++row, 0, 1, 2,
                        Qt::AlignLeft | Qt::AlignBottom);


    const auto transformChanged = [this]() mutable -> Task<> {
        if (m_inhibitTransform) co_return;
        LVector3f vectors[4];
        for (int i = 0; i < 4; ++i) {
            auto& [ label, group ] = m_components[i];
            for (int j = 0; j < 3; ++j) {
                auto* spinbox = std::get<DoubleSpinBox*>(group->getSpinBox(j));
                vectors[i][j] = spinbox->value();
            }
        }
        if (Q_LIKELY(co_await m_service.acquire())) {
            co_await m_service.set_transform(vectors[0], vectors[1],
                                             vectors[2], vectors[3]);
        }
    };

    QGridLayout* transformLabelLayout = new QGridLayout;
    layout->addLayout(transformLabelLayout, ++row, 1, 1, 1);

    for (int i = 0; i < 3; ++i) {
        QLabel* label = new QLabel(QChar("xyz"[i]));
        label->setAlignment(Qt::AlignCenter);
        transformLabelLayout->addWidget(label, 0, i, Qt::AlignCenter);
    }

    for (auto [label, group] : m_components) {
        QHBoxLayout* transformLayout = new QHBoxLayout;
        layout->addWidget(new QLabel(tr(label)), ++row, 0, 1, 1, Qt::AlignLeft);
        layout->addWidget(group, row, 1, 1, 1);
    
        for (int i = 0; i < 3; ++i) {
            auto* spinbox = std::get<DoubleSpinBox*>(group->getSpinBox(i));
            spinbox->setMinimum(std::numeric_limits<double>::lowest());
            spinbox->setMaximum(std::numeric_limits<double>::max());
            spinbox->setSingleStep(0.1);

            auto sig = QOverload<double>::of(&QDoubleSpinBox::valueChanged);
            connect(spinbox, sig, this, transformChanged);
        }
    }
    auto signal = QOverload<int>::of(&QComboBox::activated);
    connect(m_boundsCombo, signal, [this](int index) mutable -> Task<> {
        if (Q_LIKELY(co_await m_service.acquire())) {
            using BoundsType = BoundingVolume::BoundsType;
            co_await m_service.set_bound_type(
                BoundsType(m_boundsCombo->currentData().toLongLong())
            );
        }
    });

    connect(m_tagList, &TagList::modified, this, [this](auto& data) -> Task<> {
        if (Q_LIKELY(co_await m_service.acquire())) {
            std::vector<TagInfo> newTags;
            newTags.reserve(data.length());
            for (auto& [ key, value ] : data) {
                newTags.emplace_back(key.toStdString(), value.toStdString());
            }
            co_await m_service.set_tags(newTags);
        }        
    });

    layout->addWidget(createSectionLabel(tr("Tags:")), ++row, 0, 1, 1);
    layout->addWidget(m_tagList, ++row, 0, 1, 2, Qt::AlignTop);
    

    AddRenderEffectButton* addEffect = new AddRenderEffectButton(this);

    layout->addLayout(m_renderEffects, ++row, 0, 1, 2, Qt::AlignTop);
    layout->addWidget(addEffect, ++row, 0, 1, 2, Qt::AlignTop);


    const auto fetchInitialData = [](   AddRenderEffectButton* addEffect,
                                        PandaNodePanel* panel) -> Task<> {
        
        if (Q_LIKELY(co_await panel->m_service.acquire())) {
            auto data = co_await panel->m_service.get_node_info();

            panel->m_nameLineEdit->setText(QString::fromStdString(data.name));
            panel->m_drawMaskButton->setDrawMasks(  data.draw_mask,
                                                    data.control_mask);
 
            panel->setTransform(data.position, data.rotation,
                                data.scale, data.shear);

            panel->m_boundsCombo->setCurrentIndex(
                panel->m_boundsCombo->findData(qlonglong(data.bounds_type))
            );
            for (const auto& [key, value] : data.tags) {
                panel->m_tagList->addTag(QString::fromStdString(key),
                                         QString::fromStdString(value));
            }
            for (const auto& effect : data.effects) {
                panel->addRenderEffect(effect);
            }
        }
    };
    fetchInitialData(addEffect, this);
}



PandaNodePanel::~PandaNodePanel() {

}



Task<> PandaNodePanel::initialize() {


}

void PandaNodePanel::setTransform(  const LVector3& position,
                                    const LVector3& rotation,
                                    const LVector3& scale,
                                    const LVector3& shear) {
    m_inhibitTransform = true;
    const std::reference_wrapper<const LVector3> components[4] {
        position, rotation, scale, shear
    };
    for (int i = 0; i < 4; ++i) {
        auto& [ label, group ] = m_components[i];
        for (int j = 0; j < 3; ++j) {
            auto* spinbox = std::get<DoubleSpinBox*>(group->getSpinBox(j));
            spinbox->setValue(components[i].get()[j]);
        }
    }
    m_inhibitTransform = false;
}

void PandaNodePanel::addRenderEffect(const RenderEffectInfo& info) {
    auto* runtime = Editor::globalInstance()->runtime();

    auto* container = new Spoiler(runtime->typeName(info.typeId));
    container->setContentWidget(new RenderEffectWidget(this, info));
    container->enableFeatures(Spoiler::CloseButton);

    auto signal = &Spoiler::closeButtonPressed;
    connect(container, signal, this, [=, this]() -> Task<> {
        delete container;
        if (Q_LIKELY(co_await m_service.acquire())) {
            co_await m_service.remove_effect(info.typeId);
        }
    });
    m_renderEffects->addWidget(container, 0, Qt::AlignTop);
}




INITIALIZER(PandaNodePanel) {
    InspectorEditor::registerPanel<PandaNodePanel>("PandaNode");
}
