// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __TAGLIST_H__
#define __TAGLIST_H__

#include <QWidget>

class QTreeWidget;


class TagList : public QWidget {

    Q_OBJECT

public:
    TagList(QWidget *parent = nullptr);
    virtual ~TagList() = default;

    void addTag(const QString& key, const QString& value);

signals:
    void modified(const QList<QPair<QString, QString>>& tags);

protected:
    class ListView;
    bool m_inhibitModified = false;
    ListView* m_listView;
};

#endif // __TAGLIST_H__