// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __INSPECTOREDITOR_H__
#define __INSPECTOREDITOR_H__

#include "objectRequestBroker.h"
#include "editorWidget.h"
#include "pandaNodeService.gs_rpc_stub.h"
#include "inspectorService.gs_rpc_stub.h"
#include <concepts>
#include <qscrollarea.h>


class InspectorPanelBase;


class InspectorEditor : public EditorWidget {

    Q_OBJECT

public:
    InspectorEditor(const QString& name=tr("inspector"));
    virtual ~InspectorEditor();

    typedef std::function<InspectorPanelBase*(InspectorEditor*)> Factory;

    static void registerPanel(  const char* nodeTypeName,
                                Factory&& factory);

    template<std::derived_from<InspectorPanelBase> Panel>
    static void registerPanel(const char* nodeType) {
        registerPanel(nodeType, [](InspectorEditor* editor) {
            return new Panel(editor);
        });
    }
    void runtimeConnected() override { Q_UNUSED(m_service.acquire()); }

protected:
    QScrollArea* m_scrollArea;
    
    class InspectorAdaptor : public stub::InspectorService {
        InspectorEditor* m_editor;
    public:
        inline InspectorAdaptor(InspectorEditor* ed): m_editor(ed) {}

        void node_changed(int32_t type_id) override;
    };
    InspectorAdaptor m_service;
};

#endif // __INSPECTOREDITOR_H__