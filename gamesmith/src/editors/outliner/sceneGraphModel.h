// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __SCENEGRAPHMODEL_H__
#define __SCENEGRAPHMODEL_H__

#include <QAbstractItemModel>
#include <qglobal.h>
#include <qobjectdefs.h>
#include "sceneGraphService.h"
#include "sceneGraphService.gs_rpc_stub.h"


class SceneGraphModel : public QAbstractItemModel {

    Q_OBJECT

public:
    SceneGraphModel(QObject* parent = nullptr);
    virtual ~SceneGraphModel();

    QVariant data(const QModelIndex& index, int role) const override;
    bool setData(const QModelIndex& index, const QVariant& value,
                int role) override;

    Qt::ItemFlags flags(const QModelIndex &index) const override;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column,
                      const QModelIndex &parent = QModelIndex()) const override;
    QModelIndex parent(const QModelIndex &index) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    enum DataType { Name, Class, Visible, NumChildren };

signals:
    void selectionUpdated(const QModelIndexList& selected);

public slots:
    Task<> renameNode(int32_t id, const QString& name);
    Task<> selectionChanged(const QModelIndexList& selected);

protected:
    struct IndexData; struct Node;

    void updateGraph(const SceneGraphService::Graph& graph);
    void unlink_node(const QSharedPointer<Node>& node);
    IndexData* indexData(const QModelIndex& index) const;

    std::unique_ptr<IndexData> m_rootIndex;
    QHash<quint64, QSharedPointer<Node>> m_nodeLookup;

    class SceneGraphServiceAdapter : public stub::SceneGraphService {
        SceneGraphModel* m_model;
    public:
        inline SceneGraphServiceAdapter(SceneGraphModel* model):
                m_model(model) {}
        void selection_changed(const std::vector<uint64_t>& ids) override;
        void graph_changed(const ::SceneGraphService::Graph& graph) override;
        void stale_nodes(const std::vector<uint64_t>& nodes) override;
        void node_renamed(uint64_t id, const std::string& name) override;
    };
    SceneGraphServiceAdapter m_service;
};

#endif // __SCENEGRAPHMODEL_H__