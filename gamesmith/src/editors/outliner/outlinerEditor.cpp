// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QVBoxLayout>
#include <QTreeView>
#include <QLineEdit>
#include <QStyledItemDelegate>
#include <QSortFilterProxyModel>
#include <QTimer>
#include <QHeaderView>
#include <QItemSelectionModel>
#include <qobject.h>

#include "sceneGraphModel.h"
#include "outlinerEditor.h"


class OutlinerEditor::Delegate final : public QStyledItemDelegate {

public:
    Delegate(QObject* parent): QStyledItemDelegate(parent) {}

    void paint( QPainter* painter, const QStyleOptionViewItem& option,
                const QModelIndex& index) const {
        
        OutlinerEditor* editor = static_cast<OutlinerEditor*>(parent());
        
        QStyleOptionViewItem optCopy = option;
        if (!editor->m_model->filterRegExp().isEmpty() &&
            !editor->m_matchedRows.contains(index)) {

            QColor textColor = optCopy.palette.color(QPalette::Text);
            textColor.setAlpha(textColor.alpha() / 2);
            optCopy.palette.setColor(QPalette::Text, textColor);
        }
        if (index.column() == SceneGraphModel::Class) {
            QColor textColor = optCopy.palette.color(QPalette::Text);
            textColor.setAlpha(textColor.alpha() / 2);
            optCopy.palette.setColor(QPalette::Text, textColor);
        }
        QStyledItemDelegate::paint(painter, optCopy, index);
    }
};



OutlinerEditor::OutlinerEditor(const QString& label): EditorWidget(label),
    m_treeView(new QTreeView(this)),
    m_model(new QSortFilterProxyModel(this))  {

    SceneGraphModel* sourceModel = new SceneGraphModel(this);
    QWidget* headerBar = new QWidget(this);
    QLineEdit* searchBar = new QLineEdit(this);
    QFrame* separator = new QFrame(this);


    searchBar->setPlaceholderText(tr("Search"));

    separator->setFrameShape(QFrame::HLine);
    headerBar->setLayout(new QHBoxLayout);
    headerBar->layout()->setMargin(4);
    headerBar->layout()->addWidget(searchBar);

    setLayout(new QVBoxLayout(this));
    layout()->setMargin(0);
    layout()->setSpacing(0);
    layout()->addWidget(headerBar);
    layout()->addWidget(separator);
    layout()->addWidget(m_treeView);

    m_model->setRecursiveFilteringEnabled(true);
    m_model->setFilterKeyColumn(SceneGraphModel::Name);
    m_model->setSourceModel(sourceModel);

    m_treeView->setHeaderHidden(true);
    m_treeView->setItemDelegate(new Delegate(this));
    m_treeView->setModel(m_model);
    m_treeView->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
    m_treeView->setSelectionMode(QAbstractItemView::ExtendedSelection);

    QItemSelectionModel* selectionModel = m_treeView->selectionModel();

    const auto handleSelection = [this, sourceModel]() -> void {
        QItemSelectionModel* model = m_treeView->selectionModel();
        QModelIndexList selectedindexes;
        for (QModelIndex& index : model->selectedIndexes()) {
            selectedindexes.append(m_model->mapToSource(index));
        }
        emit sourceModel->selectionChanged(selectedindexes);
    };

    auto conn = connect(selectionModel, &QItemSelectionModel::selectionChanged,
                        this, handleSelection);

    connect(sourceModel, &SceneGraphModel::selectionUpdated, this,
        [=, this](const QModelIndexList& selection) mutable {
            QItemSelectionModel* selectionModel = m_treeView->selectionModel();
            disconnect(conn);
            selectionModel->clearSelection();
            for (const QModelIndex& index : selection) {
                auto mapped = m_model->mapFromSource(index);
                selectionModel->select(mapped, QItemSelectionModel::Select);
            }
            conn = connect(selectionModel, &QItemSelectionModel::selectionChanged,
                            this, handleSelection);
        }
    );
    connect(sourceModel, &QAbstractItemModel::rowsInserted, this,
        [this](const QModelIndex& parent, int first, int last) {
            if (!parent.isValid() && first == 0 && last == 0) {
                auto index = m_model->index(0, 0, parent);
                m_treeView->expand(index);
            }
        }
    );
    connect(searchBar, &QLineEdit::textChanged,
            this, &OutlinerEditor::searchTextChanged);
}

OutlinerEditor::~OutlinerEditor() {
    
}

void OutlinerEditor::searchTextChanged(const QString& searchText) {
    m_matchedRows.clear();

    if (Q_UNLIKELY(searchText.isEmpty())) {
        m_model->setFilterRegExp(QRegExp());
        m_treeView->collapseAll();
        for (const QModelIndex& index : m_expandedIndices) {
            m_treeView->setExpanded(m_model->mapFromSource(index), true);
        }
        return m_expandedIndices.clear();
    }
    else if (m_model->filterRegExp().isEmpty()) {
        QAbstractItemModel* sourceModel = m_model->sourceModel();

        std::function<void(const QModelIndex&)> visitNode;
        visitNode = [&](const QModelIndex& index) -> void {
            if  (m_treeView->isExpanded(m_model->mapFromSource(index))) {
                m_expandedIndices.append(index);
            }
            int num_children = sourceModel->rowCount(index);
            for (int i = 0; i < num_children; ++i) {
                visitNode(sourceModel->index(i, 0, index));
            }
        };
        visitNode(sourceModel->index(0, 0, QModelIndex()));
    }
    QRegExp regExp(searchText, Qt::CaseInsensitive, QRegExp::FixedString);
    m_model->setFilterRegExp(regExp);

    Qt::MatchFlags flags;
    flags.setFlag(Qt::MatchFlag::MatchFixedString,  true);
    flags.setFlag(Qt::MatchFlag::MatchStartsWith,   true);
    flags.setFlag(Qt::MatchFlag::MatchRecursive,    true);
    flags.setFlag(Qt::MatchFlag::MatchWrap,         true);

    const Qt::ItemDataRole role = Qt::ItemDataRole::DisplayRole;
    QModelIndex index = m_model->index(0, 0);

    for (auto index : m_model->match(index, role, searchText, -1, flags)) {
        m_matchedRows.insert(index);
        auto expanderIndex = index.parent();
        while (expanderIndex.isValid()) {
            m_treeView->expand(expanderIndex);
            expanderIndex = expanderIndex.parent();
        }
    }
    m_treeView->repaint();
}