// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#include <QTreeView>
#include <QDebug>
#include <QTimer>
#include <cstdint>
#include <memory>
#include <qabstractitemmodel.h>
#include <qglobal.h>
#include <qnamespace.h>
#include <string>

#include "editor.h"
#include "runtimeManager.h"
#include "sceneGraphModel.h"
#include "sceneGraphService.h"
#include "serviceInterface.h"


struct SceneGraphModel::IndexData {
    IndexData* parent           = nullptr;
    QSharedPointer<Node> node   = nullptr;
    int row                     = 0;
    std::vector<std::unique_ptr<IndexData>> children;
};


struct SceneGraphModel::Node {
    uint64_t id                         = 0;
    int32_t classId                     = 0;
    QString name                        = "";
    uint8_t visible                     : 1;
    uint8_t selectable                  : 1;
    QVector<QSharedPointer<Node>> children;
    QVector<QWeakPointer<Node>> parents;
    QSet<SceneGraphModel::IndexData*> up;
};




void SceneGraphModel::
SceneGraphServiceAdapter::selection_changed(const std::vector<uint64_t>& ids) {
    QModelIndexList indices;
    for (uint64_t id : ids) {
        if (auto node = m_model->m_nodeLookup.value(id)) {
            for (SceneGraphModel::IndexData* child : node->up) {
                indices.append({
                    m_model->createIndex(child->row, 0, child),
                    m_model->createIndex(child->row, 1, child)
                });
            }
        }
    }
    emit m_model->selectionUpdated(indices);
}
void SceneGraphModel::
SceneGraphServiceAdapter::graph_changed(const ::SceneGraphService::Graph& graph) {
    m_model->updateGraph(graph);
}

void SceneGraphModel::
SceneGraphServiceAdapter::stale_nodes(const std::vector<uint64_t>& nodes) {
    for (uint64_t id : nodes) {
        if (auto node = m_model->m_nodeLookup.value(id)) {
            m_model->unlink_node(node);
        }
    }
}
void SceneGraphModel::
SceneGraphServiceAdapter::node_renamed(uint64_t id, const std::string& name) {
    if (auto node = m_model->m_nodeLookup.value(id)) {
        node->name = QString::fromStdString(name);
        for (auto* indexData : node->up) {
            emit m_model->dataChanged(
                m_model->createIndex(indexData->row, 0, indexData),
                m_model->createIndex(indexData->row, 0, indexData),
                {Qt::DisplayRole}
            );
        }
    }
}



SceneGraphModel::SceneGraphModel(QObject* parent) : QAbstractItemModel(parent),
    m_service(this) {

    m_rootIndex.reset(new IndexData {
        .parent = nullptr, 
        .node = QSharedPointer<Node>::create()
    });
    RuntimeManager* runtime = Editor::globalInstance()->runtime();
    if (runtime->isConnected()) {
        ServiceInterface::bind(&m_service);
    } else {
        connect(runtime, &RuntimeManager::clientConnected, this, [this] {
            ServiceInterface::bind(&m_service);
        });
    }
}

SceneGraphModel::~SceneGraphModel() {

}

void SceneGraphModel::unlink_node(const QSharedPointer<Node>& node) {
    for (QWeakPointer<Node>& weakParent : node->parents) {
        if (QSharedPointer<Node> parent = weakParent.lock()) {
            parent->children.removeOne(node);
        }
    }
    for (SceneGraphModel::IndexData* data : node->up) {
        if (data) data->parent->children[data->row].reset();
    }
    m_nodeLookup.remove(node->id);
}
        
SceneGraphModel::IndexData*
SceneGraphModel::indexData(const QModelIndex& index) const {
    if (index.isValid()) {
        return static_cast<IndexData*>(index.internalPointer());
    }
    return nullptr;
}

QModelIndex
SceneGraphModel::index(int row, int column, const QModelIndex& parent) const {
    if (!hasIndex(row, column, parent)) {
        return QModelIndex();
    }
    IndexData* data = indexData(parent);
    if (data == nullptr && row == 0) {
        return createIndex(row, column, m_rootIndex.get());
    } 
    else if (row >= 0 && row < data->children.size()) {
        return createIndex(row, column, data->children[row].get());
    }
    return QModelIndex();
}


QModelIndex SceneGraphModel::parent(const QModelIndex& index) const {
    IndexData* data = indexData(index);
    if (data == nullptr || data->parent == nullptr) {
        return QModelIndex();
    }
    return createIndex(data->parent->row, 0, data->parent);
}

int SceneGraphModel::rowCount(const QModelIndex& index) const {
    IndexData* data = indexData(index);
    return data ? data->node->children.count() : 1;
}

int SceneGraphModel::columnCount(const QModelIndex& index) const {
    return 2;
}

Task<> SceneGraphModel::renameNode(int32_t id, const QString& name) {

    if (Q_LIKELY(co_await m_service.acquire())) {
        co_await m_service.rename_node(id, name.toStdString());
    }
}

Task<> SceneGraphModel::selectionChanged(const QModelIndexList& selected) {

    if (Q_LIKELY(co_await m_service.acquire())) {
        std::vector<uint64_t> selectedIds;
        selectedIds.reserve(selected.size());
        for (const QModelIndex& index : selected) {
            if (IndexData* data = indexData(index)) {
                selectedIds.push_back(data->node->id);
            }
        }
        co_await m_service.update_selection(selectedIds);
    }
}

void SceneGraphModel::updateGraph(const SceneGraphService::Graph& graph) {
    
    using NodeRef = QSharedPointer<Node>;
    std::function<void(const NodeRef&, const QModelIndex&)> visitNode;

    const auto addNode = [&](const SceneGraphService::NodeEntry& entry) {
        QSharedPointer<Node>& node = m_nodeLookup[entry.id];
        if (Q_LIKELY(node.isNull())) {
            node.reset(new Node{ entry.id, entry.classId,
                QString::fromStdString(entry.name), true, true
            });
            node->children.reserve(entry.children.size());
            for (int32_t childId : entry.children) {
                node->children.append(m_nodeLookup[childId]);
                node->children.back()->parents.append(node);
            }
        }
    };
    visitNode = [&](const NodeRef& node, const QModelIndex& parentIndex) {
        IndexData* indexData = new IndexData{
            .parent = this->indexData(parentIndex), .node = node
        };
        node->up.insert(indexData);
        if (Q_LIKELY(indexData->parent)) {
            indexData->row = indexData->parent->children.size();
            indexData->parent->children.emplace_back(indexData);
        } else {
            m_rootIndex.reset(indexData);
        }
        QModelIndex index = createIndex(indexData->row, 0, indexData);
        for (auto& child : node->children) visitNode(child, index);
    };
    if (Q_LIKELY(!graph.empty())) {
        m_nodeLookup.clear();
        // so we construct the graph from buttom up
        std::for_each(graph.begin(), graph.end(), addNode);
        // then construct the hierarchy from top down
        beginResetModel();
        // beginInsertRows(QModelIndex(), 0, 0);
        visitNode(m_nodeLookup[graph.back().id], QModelIndex());
        // endInsertRows();
        endResetModel();
    }
}

QVariant SceneGraphModel::headerData(int section, Qt::Orientation orientation,
                                    int role) const {
    if (role != Qt::DisplayRole || orientation != Qt::Horizontal) {
        return QVariant();
    }
    switch (section) {
        case Name:  return tr("name");
        case Class: return tr("class");
        default: break;
    }
    return QVariant();
}

QVariant SceneGraphModel::data(const QModelIndex& index, int role) const {
    if (!index.isValid() || (role != Qt::DisplayRole && role != Qt::EditRole)) {
        return QVariant();
    }
    RuntimeManager* runtime = Editor::globalInstance()->runtime();
    IndexData* data = indexData(index);
    switch (index.column()) {
    case Name:          return data->node->name;
    case Class:         return runtime->typeName(data->node->classId);
    case Visible:       return bool(data->node->visible);
    case NumChildren:   return data->node->children.count();
    default:
        qWarning() << "Invalid column" << role;
    };
    return QVariant();
}

Qt::ItemFlags SceneGraphModel::flags(const QModelIndex& index) const {
    if (!index.isValid()) {
        return Qt::NoItemFlags;
    }
    Qt::ItemFlags flags =   Qt::ItemIsEnabled | Qt::ItemIsDragEnabled |
                            Qt::ItemIsDropEnabled | Qt::ItemIsSelectable;
    if (index.isValid()) {
        IndexData* data = indexData(index);
        if (data->node && data->node->selectable) {
            flags |= Qt::ItemIsSelectable;
        }
        if (index.column() == Name) {
            flags |= Qt::ItemIsEditable;
        }
    }
    return flags;
}

bool SceneGraphModel::setData(const QModelIndex& index, const QVariant& value,
                            int role) {
    if (role == Qt::EditRole && value.type() == QVariant::String) {
        IndexData* data = indexData(index);
        if (Q_LIKELY(data && !data->node.isNull())) {
            renameNode(data->node->id, data->node->name = value.toString());
            dataChanged(index, index, { Qt::DisplayRole });
            return true;
        }
    }
    return false;
}