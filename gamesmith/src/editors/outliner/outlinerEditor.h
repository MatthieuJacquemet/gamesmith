// Copyright (C) 2023 Matthieu Jacquemet
// 
// This file is part of GameSmith.
// 
// GameSmith is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// GameSmith is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __OUTLINEREDITOR_H__
#define __OUTLINEREDITOR_H__

#include "editorWidget.h"
#include <QSet>

class QSortFilterProxyModel;
class QTreeView;

class OutlinerEditor : public EditorWidget {

    Q_OBJECT

public:
    OutlinerEditor(const QString& label = tr("outliner"));
    virtual ~OutlinerEditor();

private slots:
    void searchTextChanged(const QString& text);

protected:
    class Delegate; class SearchFilter;
    QTreeView* m_treeView;
    QSortFilterProxyModel* m_model;
    QList<QModelIndex> m_expandedIndices;
    QSet<QModelIndex> m_matchedRows;
};

#endif // __OUTLINEREDITOR_H__