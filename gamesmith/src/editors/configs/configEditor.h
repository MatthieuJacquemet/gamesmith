// Copyright 2023 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __CONFIGEDITOR_H__
#define __CONFIGEDITOR_H__

#include <configService.gs_rpc_stub.h>
#include "editorWidget.h"

class QTreeWidgetItem;
class QTreeWidget;


class ConfigEditor : public EditorWidget {
    
    Q_OBJECT

public:
    ConfigEditor(const QString& label = tr("configuration"));
    virtual ~ConfigEditor() = default;

    Task<> modifyVariable(const QString& name,
                    const ConfigService::ValueSpec& value);

protected:
    Task<> loadConfigSpecs();

    virtual void runtimeConnected() override;
    virtual void runtimeDisconnected()  override;
    
    stub::ConfigService m_service;
    struct ConfigSpec {
        QString sectionName;
        QMap<QString, QVariant> attributes;
    };
    void registerSpec(const ConfigService::ConfigSpec& spec);

    QTreeWidget* m_treeView = nullptr;
    QHash<QString, ConfigSpec> m_configSpecs;
    QHash<QRegExp, ConfigSpec> m_patternSpecs;
    QHash<QString, QTreeWidgetItem*> m_sectionItems;
};

#endif // __CONFIGEDITOR_H__