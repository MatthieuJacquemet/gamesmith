// Copyright 2023 Matthieu Jacquemet
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QVBoxLayout>
#include <QTreeWidget>
#include <QtXml>
#include <QRegExp>
#include <QPainter>
#include <QProxyStyle>
#include <QComboBox>
#include <QCheckBox>
#include <QItemDelegate>

#include "engineMetadata.h"
#include "editor_utils.h"
#include "widgets.h"
#include "spinBox.h"
#include "configEditor.h"
#include "colorPicker.h"

// class TreeViewSyleProxy : public QProxyStyle {

// public:
//     TreeViewSyleProxy(QStyle* style) : QProxyStyle(style) {}

//     void drawPrimitive(PrimitiveElement element, const QStyleOption* option,
//                         QPainter* painter, const QWidget* widget = 0) const {
        
//         if (element == PE_IndicatorBranch) {
//             if (auto* opt = qstyleoption_cast<const QStyleOptionViewItem*>(option)) {

//                 if (opt->features & QStyleOptionViewItem::Alternate) {
//                     painter->fillRect(opt->rect, opt->palette.alternateBase());
//                 }
//             }
//         }
//         QProxyStyle::drawPrimitive(element, option, painter, widget);
//     }
// };



// class ConfigEditorWidget : public QTreeWidget {

// public:
//     void drawAlternateRowColors(QPainter *painter, QStyleOptionViewItem *option, int y, int bottom) {
//         if (!style()->styleHint(QStyle::SH_ItemView_PaintAlternatingRowColorsForEmptyArea, option, this)) {
//             return;
//         }
//         int rowHeight = 50;
//         if (rowHeight <= 0) {
//             rowHeight = itemDelegate()->sizeHint(*option, QModelIndex()).height();
//             /* Checking if the rowHeight is less than or equal to 0. */
//             if (rowHeight <= 0) {
//                 return;
//             }
//         }
//         int current = 0;
//         while (y <= bottom) {
//             option->rect.setRect(0, y, viewport()->width(), rowHeight);
//             option->features.setFlag(QStyleOptionViewItem::Alternate, current & 1);
//             ++current;
//             style()->drawPrimitive(QStyle::PE_PanelItemViewRow, option, painter, this);
//             y += rowHeight;
//         }
//     }

//     void drawRow(QPainter* painter, const QStyleOptionViewItem& option,
//                 const QModelIndex &index) const override {
        
//         QStyleOptionViewItem opt = option;
//         bool hasValue = true;
//         QStyle* style = QApplication::style();
//         if (hasValue) {
//             // const QColor c = style->standardPalette().color(index.row() & 0x1 ? QPalette::Base : QPalette::AlternateBase);
//             // painter->fillRect(option.rect, c);
//             // opt.palette.setColor(QPalette::AlternateBase, c);
//         } else {
//             // const QColor c = m_editorPrivate->calculatedBackgroundColor(m_editorPrivate->indexToBrowserItem(index));
//             // if (c.isValid()) {
//             //     painter->fillRect(option.rect, c);
//             //     opt.palette.setColor(QPalette::AlternateBase, c.lighter(112));
//             // }
//         }
//         QTreeWidget::drawRow(painter, opt, index);
//         // QColor color = static_cast<QRgb>(style->styleHint(QStyle::SH_Table_GridLineColor, &opt));
//         // painter->save();
//         // painter->setPen(QPen(color));
//         // painter->drawLine(opt.rect.x(), opt.rect.bottom(), opt.rect.right(), opt.rect.bottom());
//         // painter->restore();
//     }
// };


class ItemDelegate : public QItemDelegate {
public:
    ItemDelegate(QObject* parent = nullptr): QItemDelegate(parent) {}

    // Use this for setting tree item height.
    QSize sizeHint( const QStyleOptionViewItem& option,
                    const QModelIndex &index) const override {

        QSize size = QItemDelegate::sizeHint(option, index);
        size.setHeight(22);
        return size;
    }
};

template<class T, class... Args>
T* addWidget(QTreeWidgetItem* item, Args&&... args) {
    T* widget = new T(std::forward<Args>(args)...);
    addWidget(item, widget);
    return widget;
}

void addWidget(QTreeWidgetItem* item, QWidget* widget) {
    QWidget* container = new QWidget;
    container->setLayout(new QVBoxLayout);
    container->layout()->setAlignment(Qt::AlignVCenter);
    container->layout()->setMargin(0);
    container->layout()->addWidget(widget);

    item->treeWidget()->setItemWidget(item, 1, container);
}

template<ConfigFlags::ValueType Type>
struct ConfigValueType { using type = void; };

#define BIND_CONFIG_VALUE_TYPE(TYPE, ID) \
    template<> struct ConfigValueType<TYPE> { using type = ID; };


BIND_CONFIG_VALUE_TYPE(ConfigFlags::VT_double,      double)
BIND_CONFIG_VALUE_TYPE(ConfigFlags::VT_int,         int32_t)
BIND_CONFIG_VALUE_TYPE(ConfigFlags::VT_int64,       int64_t)
BIND_CONFIG_VALUE_TYPE(ConfigFlags::VT_bool,        bool)
BIND_CONFIG_VALUE_TYPE(ConfigFlags::VT_string,      QString)
BIND_CONFIG_VALUE_TYPE(ConfigFlags::VT_enum,        QString)
BIND_CONFIG_VALUE_TYPE(ConfigFlags::VT_color,       QColor)
BIND_CONFIG_VALUE_TYPE(ConfigFlags::VT_list,        QStringList)
BIND_CONFIG_VALUE_TYPE(ConfigFlags::VT_search_path, QString)
BIND_CONFIG_VALUE_TYPE(ConfigFlags::VT_filename,    QString)



template<ConfigFlags::ValueType>
void doAddVariable( QTreeWidgetItem*, const ConfigService::ConfigSpec&,
                    const QMap<QString, QVariant>&) {
    nout << "do_add_variable: not implemented" << std::endl;
}



template<size_t... Is>
void addVariable(   std::index_sequence<Is...>, QTreeWidgetItem* item,
                    const ConfigService::ConfigSpec& spec,
                    const QMap<QString, QVariant>& attributes) {

    constexpr void (*handlers[])(QTreeWidgetItem*,
                                const ConfigService::ConfigSpec&,
                                const QMap<QString, QVariant>&) = { 
        &doAddVariable<ConfigFlags::ValueType(Is)>...
    };
    ConfigFlags::ValueType type = spec.default_value.value_type;

    if (LIKELY(type < sizeof...(Is))) {
        handlers[type](item, spec, attributes);
    } else {
        nout << "draw_component: index out of range" << std::endl;
    }
}


template<ConfigFlags::ValueType Type>
void doAddNumericVariable(  const ConfigService::ConfigSpec& spec,
                            const QMap<QString, QVariant>& attributes,
                            QTreeWidgetItem* item) {
    
    using DataType = typename ConfigValueType<Type>::type;

    QVariant attrib = attributes.value(QStringLiteral("numWords"));
    int numWords = attrib.isValid() ? attrib.toInt() : 1;
    auto* editor = qobject_cast<EditorWidget*>(item->treeWidget()->parentWidget());

    SpinBoxVectorBase* spv = SpinBoxVectorBase::create<DataType>(
        numWords, QBoxLayout::LeftToRight
    );
    addWidget(item, spv);

    for (int index=0; index<spv->getSpinBoxCount(); ++index) {
        using type = typename SpinBoxType<DataType>::type;
        using limits = std::numeric_limits<DataType>;

        auto* spinbox = std::get<type*>(spv->getSpinBox(index));

        attrib = attributes.value(QStringLiteral("minValue"), limits::lowest());
        spinbox->setMinimum(attrib.value<DataType>());

        attrib = attributes.value(QStringLiteral("maxValue"), limits::max());
        spinbox->setMaximum(attrib.value<DataType>());
 
        attrib = attributes.value(QStringLiteral("unit"));
        if (attrib.isValid()) {
            spinbox->setSuffix(attrib.toString());
        }
        spinbox->setValue(std::get<DataType>(spec.default_value.value));

        auto handlerFunc = QOverload<DataType>::of(&type::valueChanged);

        QObject::connect(spinbox, handlerFunc, [item](DataType value) {
            ConfigEditor* editor = qobject_cast<ConfigEditor*>(
                item->treeWidget()->parentWidget()
            );
            editor->modifyVariable(item->text(0), ConfigService::ValueSpec {
                .value_type = Type, .value = value
            });
        });
    }
}

template<>
void doAddVariable<ConfigFlags::VT_int>(QTreeWidgetItem* item,
                            const ConfigService::ConfigSpec& spec,
                            const QMap<QString, QVariant>& attributes) {
    
    doAddNumericVariable<ConfigFlags::VT_int>(spec, attributes, item);
}

template<>
void doAddVariable<ConfigFlags::VT_double>(QTreeWidgetItem* item,
                            const ConfigService::ConfigSpec& spec,
                            const QMap<QString, QVariant>& attributes) {
    
    doAddNumericVariable<ConfigFlags::VT_double>(spec, attributes, item);
}


template<>
void doAddVariable<ConfigFlags::VT_string>(QTreeWidgetItem* item,
                            const ConfigService::ConfigSpec& spec,
                            const QMap<QString, QVariant>& attributes) {
    
    QLineEdit* lineEdit = addWidget<QLineEdit>(item);

    std::string defaultText = std::get<std::string>(spec.default_value.value);
    lineEdit->setText(QString::fromStdString(std::move(defaultText)));

    QObject::connect(lineEdit, &QLineEdit::textChanged, [item](const QString& value) {
        ConfigEditor* editor = qobject_cast<ConfigEditor*>(
            item->treeWidget()->parentWidget()
        );
        editor->modifyVariable(item->text(0), ConfigService::ValueSpec {
            .value_type = ConfigFlags::VT_string, .value = value.toStdString()
        });
    });
}

template<>
void doAddVariable<ConfigFlags::VT_enum>(QTreeWidgetItem* item,
                            const ConfigService::ConfigSpec& spec,
                            const QMap<QString, QVariant>& attributes) {
    
    QVariant attrib = attributes.value(QStringLiteral("enum"));
    if (Q_UNLIKELY(!attrib.isValid())) {
        qWarning() << "enum:" << QString::fromStdString(spec.name)
                    << "has not registered enum values";
    }
    QComboBox* comboBox = addWidget<QComboBox>(item);

    std::string defaultText = std::get<std::string>(spec.default_value.value);
    for (const QString& value : attrib.toStringList()) {
        comboBox->addItem(value);
    }
    comboBox->setCurrentText(QString::fromStdString(std::move(defaultText)));

    QObject::connect(comboBox, &QComboBox::currentTextChanged, [item](auto& value) {
        ConfigEditor* editor = qobject_cast<ConfigEditor*>(
            item->treeWidget()->parentWidget()
        );
        editor->modifyVariable(item->text(0), ConfigService::ValueSpec {
            .value_type = ConfigFlags::VT_enum, .value = value.toStdString()
        });
    });
}

template<>
void doAddVariable<ConfigFlags::VT_bool>(QTreeWidgetItem* item,
                            const ConfigService::ConfigSpec& spec,
                            const QMap<QString, QVariant>& attributes) {
    
    QCheckBox* checkbox = addWidget<QCheckBox>(item);
    checkbox->setChecked(std::get<bool>(spec.default_value.value));

    QObject::connect(checkbox, &QCheckBox::stateChanged, [item](bool value) {
        ConfigEditor* editor = qobject_cast<ConfigEditor*>(
            item->treeWidget()->parentWidget()
        );
        editor->modifyVariable(item->text(0), ConfigService::ValueSpec {
            .value_type = ConfigFlags::VT_bool, .value = value
        });
    });
}

template<>
void doAddVariable<ConfigFlags::VT_color>(QTreeWidgetItem* item,
                            const ConfigService::ConfigSpec& spec,
                            const QMap<QString, QVariant>& attributes) {
    
    ColorPicker* colorPicker = addWidget<ColorPicker>(item);
    LColor color = std::get<LColor>(spec.default_value.value);
    colorPicker->setColor(lColorToQColor(color));

    QObject::connect(colorPicker, &ColorPicker::colorChanged, [item](auto& color) {
        ConfigEditor* editor = qobject_cast<ConfigEditor*>(
            item->treeWidget()->parentWidget()
        );
        editor->modifyVariable(item->text(0), ConfigService::ValueSpec {
            .value_type = ConfigFlags::VT_color, .value = qColorToLColor(color)
        });
    });
}

static const auto& attributeParsers() {
    typedef std::function<QVariant (const QString&)> Parser;

    auto rangeParser = [](const QString& value) -> QVariant { 
        return value.contains(".") ? value.toDouble() : value.toInt();
    };
    static QList<QPair<QString, Parser>> parsers = {
        {QStringLiteral("enum"), [](const QString& value) { 
            return Editor::globalInstance()->metadata()->getEnum(value);
        }},
        {QStringLiteral("maxValue"), rangeParser},
        {QStringLiteral("minValue"), rangeParser},
        {QStringLiteral("numWords"), [](const QString& value) {
            return value == "*" ? INT_MAX : value.toInt();
        }},
        {QStringLiteral("ignore"), [](const QString& value) {
            return value == "true";
        }},
        {QStringLiteral("unit"), [](const QString& value) {
            return value;
        }}
    };
    return parsers;
}


ConfigEditor::ConfigEditor(const QString& label): EditorWidget(label) {
    
    setLayout(new QVBoxLayout(this));
    layout()->addWidget(m_treeView = new QTreeWidget);
    layout()->setMargin(0);

    EngineMetadata* metadata = Editor::globalInstance()->metadata();
    m_sectionItems.insert(QStringLiteral("misc"), nullptr);

    m_treeView->setAlternatingRowColors(true);
    m_treeView->setHeaderHidden(true);
    m_treeView->setColumnCount(3);
    m_treeView->setUniformRowHeights(true);
    m_treeView->setColumnWidth(0, 300);
    m_treeView->setColumnWidth(1, 200);
    m_treeView->setItemDelegate(new ItemDelegate(this));

    // QPalette currentPalette = m_treeView->palette();
    // currentPalette.setColor(QPalette::Base, QColor(35,35,35));
    // currentPalette.setColor(QPalette::AlternateBase, QColor(40,40,40));

    // m_treeView->setPalette(currentPalette);


    for (auto& node: metadata->findNodes("/metadata/module/variable[@name]")) {

        QDomElement element = node.toElement();
        QDomElement parent = element.parentNode().toElement();
    
        QString sectionName =  parent.attribute(QStringLiteral("name"));
        QString variableName = element.attribute(QStringLiteral("name"));

        if (Q_UNLIKELY(sectionName.isEmpty())) {
            sectionName = QStringLiteral("misc");
        }
        ConfigSpec spec { .sectionName = sectionName };

        for (const auto& [key, value] : attributeParsers()) {
            if (element.hasAttribute(key)) {
                spec.attributes[key] = value(element.attribute(key));
            }
        }
        if (variableName.contains(QStringLiteral("*"))) {
            m_patternSpecs.insert(QRegExp{std::move(variableName),
                                    Qt::CaseSensitive,
                                    QRegExp::Wildcard}, std::move(spec));
        } else {
            m_configSpecs.insert(std::move(variableName), std::move(spec));
        }
        m_sectionItems.insert(std::move(sectionName), nullptr);
    }

    for (auto it = m_sectionItems.begin(); it != m_sectionItems.end(); ++it) {
        it.value() = new QTreeWidgetItem(m_treeView);
        it.value()->setText(0, it.key());
        it.value()->setHidden(true);
    }
}


Task<> ConfigEditor::loadConfigSpecs() {
    
    if (Q_LIKELY(co_await m_service.acquire())) {

        for (const auto& spec : co_await m_service.get_config_specs()) {
            registerSpec(spec);
        }
        for (QTreeWidgetItem* section : m_sectionItems.values()) {
            if (section->childCount() > 0) {
                section->setHidden(false);
            }
        }
    }
}

void ConfigEditor::runtimeConnected() {
    loadConfigSpecs();
}

void ConfigEditor::runtimeDisconnected() {
    
}

Task<> ConfigEditor::modifyVariable(const QString& name,
                                const ConfigService::ValueSpec& value) {
    
    if (Q_LIKELY(co_await m_service.acquire())) {
        co_await m_service.set_config_variable(name.toStdString(), value);
    }
}

void ConfigEditor::registerSpec(const ConfigService::ConfigSpec& spec) {
    
    ConfigSpec* configSpec = nullptr;

    for (auto it = m_patternSpecs.begin(); it != m_patternSpecs.end(); ++it) {
        if (it.key().exactMatch(QString::fromStdString(spec.name))) {
            configSpec = &it.value();
            break;
        }
    }
    if (configSpec == nullptr) {
        auto it = m_configSpecs.find(QString::fromStdString(spec.name));
        if (it != m_configSpecs.end()) {
            configSpec = &it.value();
        }
    }
    if (configSpec == nullptr) {
        static ConfigSpec spec{ .sectionName = "misc" };
        configSpec = &spec;
    }
    auto ignore = configSpec->attributes.value(QStringLiteral("ignore"));
    if (Q_UNLIKELY(ignore.isValid() && ignore.toBool())) {
        return;
    }

    QTreeWidgetItem* parent = m_sectionItems[configSpec->sectionName];
    QTreeWidgetItem* item = new QTreeWidgetItem(parent);

    item->setText(0, QString::fromStdString(spec.name));
    item->setToolTip(1, toolTipText(QString::fromStdString(spec.description)));

    auto is = std::make_index_sequence<ConfigFlags::VT_color + 1>{};
    addVariable(is, item, spec, configSpec->attributes);
}

