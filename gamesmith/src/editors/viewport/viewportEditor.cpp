// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <QWindow>
#include <QStylePainter>
#include <QStyleOption>
#include <QDebug>
#include <QResizeEvent>
#include <QBoxLayout>
#include <qabstractbutton.h>
#include <qevent.h>
#include <qnamespace.h>
#include <qpushbutton.h>
#include <QTimer>
#include <QLabel>
#include <QCheckBox>

#include "editor_utils.h"
#include "popup.h"
#include "mainWindow.h"
#include "editor.h"
#include "runtimeManager.h"
#include "viewportEditor.h"



inline bool supportWindowReparenting() {
	return 	Editor::platformName() == QLatin1String("xcb") ||
			Editor::platformName() == QLatin1String("windows");
}



class ViewportEditor::LayoutDropDown : public QPushButton {
	Popup* m_popup;
public:
	LayoutDropDown(QWidget* parent) : QPushButton(parent) {
		m_popup = new Popup;
		m_popup->setMinimumSize(210, 320);
		m_popup->setParentWidget(this);
		m_popup->installEventFilter(this);

		QBoxLayout* layout = new QBoxLayout(QBoxLayout::TopToBottom, m_popup);
		m_popup->frame()->setLayout(layout);

		using Comp = RenderOverlayLayer::OverlayComponent;
	
		const auto addOption = [=, this](const QString& name, Comp component) {
			QHBoxLayout* itemLayout = new QHBoxLayout;
			QCheckBox* checkBox = new QCheckBox;
			connect(checkBox, &QCheckBox::toggled, checkBox,
				[this, component](bool checked) -> Task<> {
					if (Q_LIKELY(co_await editor()->m_service.acquire())) {
						co_await editor()->m_service.enable_overlay_component(
							component, checked
						);
					}
				}
			);
			itemLayout->addWidget(new QLabel(name));
			itemLayout->addWidget(checkBox);
			layout->addLayout(itemLayout);
		};
		layout->addWidget(new QLabel(tr("layers")));

		addOption(tr("floor grid"), RenderOverlayLayer::OC_floor_grid);
		addOption(tr("axes"), RenderOverlayLayer::OC_axes);
		addOption(tr("active tool"), RenderOverlayLayer::OC_active_tool);
		addOption(tr("tight bounds"), RenderOverlayLayer::OC_floor_grid);
		addOption(tr("bounds"), RenderOverlayLayer::OC_floor_grid);
		addOption(tr("statistics"), RenderOverlayLayer::OC_profiling_stats);
		addOption(tr("parent relationship"), RenderOverlayLayer::OC_parent_relationship);

		connect(this, &QPushButton::pressed, this, [this, parent] {
			setDown(true);
			m_popup->open();
		});
	}
	virtual ~LayoutDropDown() { delete m_popup; }

	inline ViewportEditor* editor() const {
		return qobject_cast<ViewportEditor*>(parentWidget()->parentWidget());
	}
	virtual bool eventFilter(QObject* watched, QEvent* event) override {
		if (event->type() == QEvent::Hide) {
			setDown(false);
		}
		return false;
	}

	virtual void paintEvent(QPaintEvent* event) override {
		QStylePainter painter(this);
		QStyleOptionButton options;
		initStyleOption(&options);

		painter.drawControl(QStyle::CE_PushButtonBevel, options);

		int mbi = style()->pixelMetric(QStyle::PM_MenuButtonIndicator,
										&options, this);

		style()->drawPrimitive(QStyle::PE_IndicatorArrowDown, &options,
								&painter, this);
	}
	QSize sizeHint() const override {
		QSize size = QPushButton::sizeHint();
		QStyleOptionButton options;
		initStyleOption(&options);

		size.setWidth(style()->pixelMetric(QStyle::PM_MenuButtonIndicator,
											&options, this));
		return size;
	}
};


class ViewportEditor::CameraMaskButton : public QPushButton {

    enum { num_layers = 32 };

    ViewportEditor* m_viewport;
public:
    CameraMaskButton(ViewportEditor* viewport): QPushButton(tr("camera mask"),
															viewport),
        m_viewport(viewport) {
        setMenu(new QMenu(tr("camera mask")));
        menu()->setMinimumWidth(150);

        const auto handler = [this](bool triggered) -> Task<> {
			if (Q_LIKELY(co_await m_viewport->m_service.acquire())) {
				co_await m_viewport->m_service.set_camera_mask(computeCameraMask());
			}
        };
        for (int i=0; i<num_layers; ++i) {
            auto* action = menu()->addAction("Layer " + QString::number(i));
			action->setCheckable(true);
			if (i < num_layers - 1) action->setChecked(true);
			else action->setEnabled(false);

            connect(action, &QAction::triggered, this, handler);
        }
    }
    DrawMask computeCameraMask() const {
        QList<QAction*> actions = menu()->actions();
        DrawMask mask;
        for (int index=0; index<num_layers; ++index) {
            mask.set_bit_to(index, actions[index]->isChecked());
        }
        return mask;
    }
    void setCameraMask(DrawMask mask) {
        QList<QAction*> actions = menu()->actions();
        for (int i=0; i<num_layers; ++i) {
            actions[i]->setChecked(mask.get_bit(i));
        }
    }
};




ViewportEditor::ViewportEditor(const QString& label): EditorWidget(label),
	m_windowPlaceholder(new QWidget(this)) {

	m_windowPlaceholder->setSizePolicy(	QSizePolicy::Expanding,
										QSizePolicy::Expanding);

	m_windowPlaceholder->setLayout(new QBoxLayout(QBoxLayout::TopToBottom));
	m_windowPlaceholder->layout()->setMargin(0);

	setLayout(new QBoxLayout(QBoxLayout::Direction::TopToBottom));
    QWidget* headerBar = new QWidget(this);
	QPushButton* toggleOverlay = new QPushButton(headerBar);
	LayoutDropDown* overlayDropdown = new LayoutDropDown(headerBar);

	toggleOverlay->setCheckable(true);
	toggleOverlay->setChecked(true);
	toggleOverlay->setIcon(Editor::globalInstance()->icon("overlay"));
	toggleOverlay->setIconSize(QSize(14, 14));

	connect(toggleOverlay, &QPushButton::pressed, this, [this]() -> Task<> {
		QPushButton* button = qobject_cast<QPushButton*>(sender());
		if (Q_LIKELY(co_await m_service.acquire())) {
			co_await m_service.enable_view_layer(ViewportService::VL_overlay,
												!button->isChecked());
		}
	});

	auto* headerLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);
	auto* overlayLayout = new QBoxLayout(QBoxLayout::Direction::LeftToRight);
	
	headerLayout->setMargin(2);
	headerLayout->addStretch();
	headerLayout->addWidget(new CameraMaskButton(this));
	headerLayout->addLayout(overlayLayout);

	headerBar->setLayout(headerLayout);
	
	overlayLayout->setMargin(0);
	overlayLayout->setSpacing(1);
	overlayLayout->addWidget(toggleOverlay);
	overlayLayout->addWidget(overlayDropdown);

	layout()->addWidget(headerBar);
	layout()->addWidget(m_windowPlaceholder);
	layout()->setSpacing(0);
	layout()->setMargin(0);

	connect(Editor::globalInstance()->window(), &MainWindow::closing,
			this, &ViewportEditor::close);
}

ViewportEditor::~ViewportEditor() {

}


void ViewportEditor::runtimeConnected() {
	
	if (UNLIKELY(m_windowContainer != nullptr)) {
		return;
	}
	auto* runtime = Editor::globalInstance()->runtime();

	if (runtime->isLocal() && supportWindowReparenting()) {
		bindFramebufferImpl();
	} else {
		qWarning() << "ViewportEditor only support local runtime"
					<< "and reparenting windowing systems";
		// TODO: implement remote viewport (see ffmpeg / rtp)
	}
}

void ViewportEditor::runtimeDisconnected() {
	layout()->removeWidget(m_windowContainer);
	m_windowContainer->deleteLater();
	m_windowContainer = nullptr;
}

void ViewportEditor::closeEvent(QCloseEvent* event) {
	// m_serviceHandle.release(true);
}


Task<> ViewportEditor::bindFramebufferImpl() {

	if (Q_LIKELY(co_await m_service.acquire())) {
		QSize size = m_windowPlaceholder->size();
		
		uint64_t framebufferWid = co_await m_service.create_window(
			m_windowPlaceholder->winId(), size.width(), size.height()
		);
        if (framebufferWid == -1UL) {
			qWarning() << "Failed to open a window as viewport";
		}
		m_windowContainer = QWidget::createWindowContainer(
			QWindow::fromWinId(framebufferWid)
		);
		m_windowContainer->setSizePolicy(QSizePolicy::Expanding,
										QSizePolicy::Expanding);
		
		m_windowPlaceholder->layout()->addWidget(m_windowContainer);
    }
}

// Task<> ViewportEditor::resizeEventImpl(QResizeEvent* event) {
	
// 	QSize newSize = event->size();

// 	if (auto service = co_await m_service.acquire()) {
// 		co_await m_service->set_size(newSize.width(), newSize.height());
// 	}
// }


// void ViewportEditor::paintEvent(QPaintEvent* event) {
// 	// QPainter p(this);
//     // p.setRenderHint(QPainter::Antialiasing);

//     // p.fillRect(rect(), Qt::red);
// }
