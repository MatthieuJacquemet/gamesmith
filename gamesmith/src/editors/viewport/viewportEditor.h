// Copyright 2021 matthieu
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef __VIEWPORTEDITOR_H__
#define __VIEWPORTEDITOR_H__

#include "editorWidget.h"
#include "editor_defines.h"

#include "viewportService.gs_rpc_stub.h"


class ViewportEditor: public EditorWidget {

	Q_OBJECT

public:
	ViewportEditor(const QString& label = tr("untitled"));
	virtual ~ViewportEditor();

	virtual void runtimeConnected() override;
	virtual void runtimeDisconnected() override;

protected:
	class LayoutDropDown;
	class CameraMaskButton;
	void closeEvent(QCloseEvent* event) override;

	Task<> bindFramebufferImpl();
	Task<> resizeEventImpl(QResizeEvent* event);

	QWidget* m_windowContainer = nullptr;
	QWidget* m_windowPlaceholder = nullptr;
	stub::ViewportService m_service;
};

#endif // __VIEWPORTEDITOR_H__