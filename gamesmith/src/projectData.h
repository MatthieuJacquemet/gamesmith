/**
 * Copyright (C) 2022 Matthieu Jacquemet
 * 
 * This file is part of GameSmith.
 * 
 * GameSmith is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * GameSmith is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __PROJECTDATA_H__
#define __PROJECTDATA_H__

#include <qglobal.h>

#include "serializable.h"

class QString;
class QDataStream;


class ProjectData: public Serializable {

    Q_INTERFACES(Serializable)

public:
    ProjectData();
    virtual ~ProjectData() = default;

    void serialize(QDataStream& stream) override;
    void deserialize(QDataStream& stream) override;

    void loadData(const QString& path);
    void saveData(const QString& path);

    static void add(const QString& id, Serializable* obj);

private:
    typedef QMap<QString, Serializable*> Objects;
    static Objects m_objects;

    void writeSection(const QString& name, QDataStream& s);
    void readSection(QDataStream& s);

    QString m_name, m_organization;
    QString m_version, m_logo;
};

#endif // __PROJECTDATA_H__