/**
 * Copyright (C) 2022 Matthieu Jacquemet
 * 
 * This file is part of GameSmith.
 * 
 * GameSmith is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * GameSmith is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with GameSmith.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __SERIALIZABLE_H__
#define __SERIALIZABLE_H__

#include <QObject>

#include "editor_defines.h"


class Serializable {

public:
    Serializable() = default;
    virtual ~Serializable() = default;

    virtual void serialize(QDataStream& stream) = 0;
    virtual void deserialize(QDataStream& stream) = 0;
};


inline QDataStream& operator <<(QDataStream& stream, Serializable& obj) {
    obj.serialize(stream);
    return stream;
}

inline QDataStream& operator >>(QDataStream& stream, Serializable& obj) {
    obj.deserialize(stream);
    return stream;
}


Q_DECLARE_INTERFACE(Serializable, SERIALIZABLE_IID)


#endif // __SERIALIZABLE_H__